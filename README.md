# Tax reports #
The app is used for automatic generation of tax document packages according to federal laws.

This is an ASP.NET MVC App.

Used "Onion architecture" with core entities separated from infrastructure and presentation logic. Web application is built with MVVM pattern using KnockoutJS library. Repository pattern is utilized for separating concerns along with Unit of Work pattern. Dependencies are handled with Ninject. 

Developer environment: **C# (.NET Framework 4.5), Visual Studio**.

Used:

 * **ASP.NET MVC**;
 * **KnockoutJS** - MVVM for presentation layer;
 * **Ninject** - for handling dependency injection; 
 * **Bootstrap** - to facilitate web interface development.