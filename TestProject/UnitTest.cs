﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Infrastructure;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Infrastructure.ReposImpl;
using System.Linq;
using SGC.TaxClaims.Core.Saperion;
using SGC.TaxClaims.Core.ExportIFNS;
using System.Xml.Linq;
using SGC.TaxClaims.Infrastructure.Services;

namespace TestProject
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestInsertUpdateDeleteDocument()
        {
            IDBRepository documentRepository = new DBRepository();
            Guid documentUid = Guid.NewGuid();
            Claim claim = new Claim();
            claim.UId = documentUid;
            claim.DocTypeId = 1;
            claim.DocStateId = 1;
            claim.DocumentHolderUserUId = null;
            claim.Name = "Тестовое требование 4";
            claim.Number = "3";
            claim.DocDate = DateTime.Now;
            claim.DateReady = DateTime.Now;
            claim.Comment = "Коментарии";
            claim.DateChanged = DateTime.Now;
            claim.OrgId = 2;
            //Вставляем документ
            claim.ChangeStatus = System.Data.Entity.EntityState.Added;
            documentRepository.SaveDocument(claim,null);
            claim.Name = "Тестовое требование 5";
            //Обновляем документ
            claim.ChangeStatus = System.Data.Entity.EntityState.Modified;
            documentRepository.SaveDocument(claim,null);
            var result = documentRepository.GetClaims("sibgenco\\kalininmv");
            Claim claimFromBase = result.Data.FirstOrDefault(s => s.UId == documentUid);
            Assert.AreEqual(claim.Name, claimFromBase.Name);
            //Удаляем документ
            claim.ChangeStatus = System.Data.Entity.EntityState.Deleted;
            documentRepository.SaveDocument(claim,null);
            result = documentRepository.GetClaims("sibgenco\\kalininmv");
            claimFromBase = result.Data.FirstOrDefault(s => s.UId == documentUid);
            Assert.AreEqual(claimFromBase,null);
        }

        [TestMethod]
        public void TestSaperion()
        {
            //Проверяем подгрузку индексев//
            using (ISaperion saperion = new SaperionRepository())
            {
                var result = saperion.GetIndexes(new string[] { "88850574/4300004087" });
                Assert.AreEqual(result.IsSucceed, true);
                var indexes = result.Data;
                Assert.AreNotEqual(indexes.Count(), 0);
                //Получаем файлы
                string index = indexes[0].DocIndexes[0];
                var files = saperion.GetFiles(index).Data;
                Assert.AreNotEqual(files.Length, 0);
                //Тестируем получение XML
                OperationResult<SPRFile[]> eDocFilesResult = saperion.GetEDocFiles("150000026", SGC.TaxClaims.Core.Saperion.SPRDocType.Torg12);
                SPRFile[] eDocFiles = eDocFilesResult.Data;
                Assert.AreEqual(eDocFiles.Length, 2);
            }
        }

        [TestMethod]
        public void TestExpDocuments()
        {
            ExpClaim expClaim = new ExpClaim(Guid.Empty);
            string expClaimBuild = expClaim.ToString();
            //Дополнительный лист книги покупок//
            ExpPurchasesLedgerSupplement expPurchasesLedgerSupplement = new ExpPurchasesLedgerSupplement(Guid.Empty,"1.01", "Файл доп. листа книги покупок.xml");
            var expPurchasesLedgerSupplementXmlTest = "<Document claimItemNo=\"1.01\">\r\n  <PurchasesLedgerSupplement>\r\n    <XmlContent>xml/Файл доп. листа книги покупок.xml</XmlContent>\r\n  </PurchasesLedgerSupplement>\r\n</Document>";
            string expPurchasesLedgerSupplementXmlBuild = expPurchasesLedgerSupplement.BuildDocument().ToString();
            Assert.AreEqual(expPurchasesLedgerSupplementXmlTest, expPurchasesLedgerSupplementXmlBuild);
            //Дополнительный лист книги продаж//
            ExtSalesLedgerSupplement extSalesLedgerSupplement = new ExtSalesLedgerSupplement(Guid.Empty,"1.01", "xml/Файл доп. листа книги продаж.xml");
            string extSalesLedgerSupplementBuilder = extSalesLedgerSupplement.BuildDocument().ToString();
            string extSalesLedgerSupplementTest = "<Document claimItemNo=\"1.01\">\r\n  <SalesLedgerSupplement>\r\n    <XmlContent>xml/xml/Файл доп. листа книги продаж.xml</XmlContent>\r\n  </SalesLedgerSupplement>\r\n</Document>";
            Assert.AreEqual(extSalesLedgerSupplementBuilder, extSalesLedgerSupplementTest);
            //Журнал полученных и выставленных счетов-фактру//
            ExpInvoiceRegistry expInvoiceRegistry = new ExpInvoiceRegistry(Guid.Empty, "1.01", "Файл журнала счетов-фактур.xml");
            string expInvoiceRegistryBuilder = expInvoiceRegistry.BuildDocument().ToString();
            string expInvoiceRegistryTest = "<Document claimItemNo=\"1.01\">\r\n  <InvoiceRegistry>\r\n    <XmlContent>xml/Файл журнала счетов-фактур.xml</XmlContent>\r\n  </InvoiceRegistry>\r\n</Document>";
            Assert.AreEqual(expInvoiceRegistryBuilder, expInvoiceRegistryTest);
            //Книга покупок//
            ExpPurchasesLedger expPurchasesLedger = new ExpPurchasesLedger(Guid.Empty, "1.01", "Файл книги покупок.xml");
            string expPurchasesLedgerBuilder = expPurchasesLedger.BuildDocument().ToString();
            string expPurchasesLedgerTest = "<Document claimItemNo=\"1.01\">\r\n  <PurchasesLedger>\r\n    <XmlContent>xml/Файл книги покупок.xml</XmlContent>\r\n  </PurchasesLedger>\r\n</Document>";
            Assert.AreEqual(expPurchasesLedgerBuilder, expPurchasesLedgerTest);
            //Книга продаж//
            ExpSalesLedger expSalesLedger = new ExpSalesLedger(Guid.Empty, "1.01", "Файл книги продаж.xml");
            string expSalesLedgerBuild = expSalesLedger.BuildDocument().ToString();
            string expSalesLedgerTest = "<Document>\r\n  <SalesLedger>\r\n    <XmlContent>xml/Файл книги продаж.xml</XmlContent>\r\n  </SalesLedger>\r\n</Document>";
            Assert.AreEqual(expSalesLedgerBuild, expSalesLedgerTest);
            //Акт приемки сдачи работ// 
            ExpFormalizedAccCert expFormalizedAccCert = new ExpFormalizedAccCert(
                Guid.Empty,
                "1.01",
                "Акт-13145.21",
                "DP_IAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839.xml",
                "DP_IAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839SGN.sgn",
                "DP_ZAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839.xml",
                "DP_ZAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839SGN.sgn",
                "Д-1234",
                DateTime.ParseExact("2014-02-03","yyyy-MM-dd",null));
            string expFormalizedAccCertBuild = expFormalizedAccCert.BuildDocument().ToString();
            string expFormalizedAccCertTest = "<Document claimItemNo=\"1.01\">\r\n  <FormalizedAccCert>\r\n    <ExtRef>Акт-13145.21</ExtRef>\r\n    <XmlContent>xml/DP_IAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839.xml</XmlContent>\r\n    <Signature>xml/DP_IAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839SGN.sgn</Signature>\r\n    <BuyerTitleXmlContent>xml/DP_ZAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839.xml</BuyerTitleXmlContent>\r\n    <BuyerTitleSignature>xml/DP_ZAKTPRM_2BM-7777777775-2012121906344538513230000000000_2BM-7700000087-20120914053320839SGN.sgn</BuyerTitleSignature>\r\n    <ParentDocId>\r\n      <DocNumber>Д-1234</DocNumber>\r\n      <DocDate>2014-02-03</DocDate>\r\n    </ParentDocId>\r\n  </FormalizedAccCert>\r\n</Document>";
            Assert.AreEqual(expFormalizedAccCertBuild, expFormalizedAccCertTest);
            //Счет-фактура//
            ExpFormalizedInvoice expFormalizedInvoice = new ExpFormalizedInvoice(
                Guid.Empty,
                "1.01",
                "СФ-12345",
                "СФ-пример1.xml",
                "СФ-пример1SGN.sgn");
            string expFormalizedInvoiceBuild = expFormalizedInvoice.BuildDocument().ToString();
            string expFormalizedInvoiceTest = "<Document claimItemNo=\"1.01\">\r\n  <FormalizedInvoice>\r\n    <ExtRef>СФ-12345</ExtRef>\r\n    <XmlContent>xml/СФ-пример1.xml</XmlContent>\r\n    <Signature>xml/СФ-пример1SGN.sgn</Signature>\r\n  </FormalizedInvoice>\r\n</Document>";
            Assert.AreEqual(expFormalizedInvoiceBuild, expFormalizedInvoiceTest);
            //Товарная накладная//
            ExpFormalizedTorg12 expFormalizedTorg12 = new ExpFormalizedTorg12(
                Guid.Empty,
                "1.01",
                "D60AEC4E-96D5-42EB-B5F4-ACC5C7E34084",
                "DP_OTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332.xml",
                "DP_OTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332SGN.sgn",
                "DP_PTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332.xml",
                "DP_PTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332SGN.sgn"
                );
            string expFormalizedTorg12Build = expFormalizedTorg12.BuildDocument().ToString();
            string expFormalizedTorg12Test = "<Document claimItemNo=\"1.01\">\r\n  <FormalizedTorg12>\r\n    <ExtRef>1.01</ExtRef>\r\n    <XmlContent>xml/DP_OTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332.xml</XmlContent>\r\n    <Signature>xml/DP_OTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332SGN.sgn</Signature>\r\n    <BuyerTitleXmlContent>xml/DP_PTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332.xml</BuyerTitleXmlContent>\r\n    <BuyerTitleSignature>xml/DP_PTORG12_2BM-7700000087-2012091405332083946830000000000_2BM-7788000003-20120914060424332SGN.sgn</BuyerTitleSignature>\r\n  </FormalizedTorg12>\r\n</Document>";
            Assert.AreEqual(expFormalizedTorg12Build, expFormalizedTorg12Test);
            //Акт приемки-сдачи работ сканированный//
            ExpScannedAccCert expScannedAccCert = new ExpScannedAccCert(
                Guid.Empty,
                "2.01",
                "9762/A17",
                new ImageFileInfo[] { new ImageFileInfo("150dpi.jpg") },
                DateTime.ParseExact("2014-01-31", "yyyy-MM-dd", null),
                "Договор-13/125",
                DateTime.ParseExact("2014-02-12", "yyyy-MM-dd", null),
                12345,
                new OrganizationInfo(Guid.Empty,"123456789", "1231231230", "ООО \"Придуманный\""),
                new PersonalInfo(Guid.Empty,"772802057746","Иван","Иванович","Иванов"));
            string expScannedAccCertBuild = expScannedAccCert.BuildDocument().ToString();
            string expScannedAccCertTest = "<Document claimItemNo=\"2.01\">\r\n  <ScannedAccCert>\r\n    <ExtRef>9762/A17</ExtRef>\r\n    <ImageFile>xml/150dpi.jpg</ImageFile>\r\n    <DocId>\r\n      <DocDate>2014-01-31</DocDate>\r\n    </DocId>\r\n    <ParentDocId>\r\n      <DocNumber>Договор-13/125</DocNumber>\r\n      <DocDate>2014-02-12</DocDate>\r\n    </ParentDocId>\r\n    <DocAmounts total=\"12345.00\" />\r\n    <Participant role=\"Seller\">\r\n      <OrgInfo kpp=\"123456789\" inn=\"1231231230\">\r\n        <Name>ООО \"Придуманный\"</Name>\r\n      </OrgInfo>\r\n    </Participant>\r\n    <Participant role=\"Buyer\">\r\n      <PersonInfo inn=\"772802057746\" />\r\n      <Name>Иван</Name>\r\n      <MiddleName>Иванович</MiddleName>\r\n      <LastName>Иванов</LastName>\r\n    </Participant>\r\n  </ScannedAccCert>\r\n</Document>";
            Assert.AreEqual(expScannedAccCertBuild, expScannedAccCertTest);
            //Корректировочная счет-фактура сканированная//
            ExpScannedInvoiceCorrection expScannedInvoiceCorrection = 
                new ExpScannedInvoiceCorrection(
                    Guid.Empty,
                    "2.01", 
                    "СФ-123-NN14-1",
                    new ImageFileInfo[] { new ImageFileInfo("Sample (4 pages).pdf",3,3) },
                    "123-NN14-1",
                    DateTime.ParseExact("2014-02-14", "yyyy-MM-dd", null),
                    "123-NN14",
                    DateTime.ParseExact("2014-02-12", "yyyy-MM-dd", null),
                    1234,
                    12345, 
                    new OrganizationInfo(Guid.Empty,"123456789", "1231231230", "ООО \"Придуманный\""),
                    new PersonalInfo(Guid.Empty,"772802057746", "Иван", "Иванович", "Иванов")
                    );
            string expScannedInvoiceCorrectionBuild = expScannedInvoiceCorrection.BuildDocument().ToString();
            string expScannedInvoiceCorrectionTest = "<Document claimItemNo=\"2.01\">\r\n  <ScannedInvoiceCorrection>\r\n    <ExtRef>СФ-123-NN14-1</ExtRef>\r\n    <ImageFile pageTo=\"3\" pageFrom=\"3\">xml/Sample (4 pages).pdf</ImageFile>\r\n    <DocId>\r\n      <DocNumber>123-NN14-1</DocNumber>\r\n      <DocDate>2014-02-14</DocDate>\r\n    </DocId>\r\n    <ParentDocId>\r\n      <DocNumber>123-NN14</DocNumber>\r\n      <DocDate>2014-02-12</DocDate>\r\n    </ParentDocId>\r\n    <DocAmounts tax=\"1234.00\" total=\"12345.00\" />\r\n    <Participant role=\"Seller\">\r\n      <OrgInfo kpp=\"123456789\" inn=\"1231231230\">\r\n        <Name>ООО \"Придуманный\"</Name>\r\n      </OrgInfo>\r\n    </Participant>\r\n    <Participant role=\"Buyer\">\r\n      <PersonInfo inn=\"772802057746\">\r\n        <Name>Иван</Name>\r\n        <MiddleName>Иванович</MiddleName>\r\n        <LastName>Иванов</LastName>\r\n      </PersonInfo>\r\n    </Participant>\r\n  </ScannedInvoiceCorrection>\r\n</Document>";
            Assert.AreEqual(expScannedInvoiceCorrectionBuild, expScannedInvoiceCorrectionTest);
            //Счет-фактура сканированный//
            ExpScannedInvoice expScannedInvoice = new ExpScannedInvoice(
                Guid.Empty,
                "2.01", 
                "СФ-123-NN14",
                new ImageFileInfo[] { new ImageFileInfo("Sample (4 pages).pdf",1,2) },
                "123-NN14",
                DateTime.ParseExact("2014-02-12", "yyyy-MM-dd", null),
                1234, 
                12345,
                new OrganizationInfo(Guid.Empty,"123456789", "1231231230", "ООО \"Придуманный\""),
                new PersonalInfo(Guid.Empty,"772802057746", "Иван", "Иванович", "Иванов"));
            string expScannedInvoiceBuild = expScannedInvoice.BuildDocument().ToString();
            string expScannedInvoiceTest = "<Document claimItemNo=\"2.01\">\r\n  <ScannedInvoiceCorrection>\r\n    <ExtRef>СФ-123-NN14</ExtRef>\r\n    <ImageFile pageTo=\"2\" pageFrom=\"1\">xml/Sample (4 pages).pdf</ImageFile>\r\n    <DocId>\r\n      <DocNumber>123-NN14</DocNumber>\r\n      <DocDate>2014-02-12</DocDate>\r\n    </DocId>\r\n    <DocAmounts tax=\"1234.00\" total=\"12345.00\" />\r\n    <Participant role=\"Seller\">\r\n      <OrgInfo kpp=\"123456789\" inn=\"1231231230\">\r\n        <Name>ООО \"Придуманный\"</Name>\r\n      </OrgInfo>\r\n    </Participant>\r\n    <Participant role=\"Buyer\">\r\n      <PersonInfo inn=\"772802057746\">\r\n        <Name>Иван</Name>\r\n        <MiddleName>Иванович</MiddleName>\r\n        <LastName>Иванов</LastName>\r\n      </PersonInfo>\r\n    </Participant>\r\n  </ScannedInvoiceCorrection>\r\n</Document>";
            Assert.AreEqual(expScannedInvoiceBuild, expScannedInvoiceTest);
            //Товарная накладная ТОРГ-12//
            ExpScannedTorg12 expScannedTorg12 = new ExpScannedTorg12(
                Guid.Empty,
                "2.01",
                "ТН-321",
                new ImageFileInfo[] 
                { 
                    new ImageFileInfo("multiframe.tiff",1,1),
                    new ImageFileInfo("multiframe2.tiff",1,1)
                },
                DateTime.ParseExact("2014-02-12", "yyyy-MM-dd", null),
                12345,
                new OrganizationInfo(Guid.Empty,"123456789", "1231231230", "ООО \"Придуманный\""),
                new PersonalInfo(Guid.Empty,"772802057746", "Иван", "Иванович", "Иванов"));
            string expScannedTorg12Build = expScannedTorg12.BuildDocument().ToString();
            string expScannedTorg12Text = "<Document claimItemNo=\"2.01\">\r\n  <ScannedTorg12>\r\n    <ExtRef>ТН-321</ExtRef>\r\n    <ImageFile pageTo=\"1\" pageFrom=\"1\">xml/multiframe.tiff</ImageFile>\r\n    <ImageFile pageTo=\"1\" pageFrom=\"1\">xml/multiframe2.tiff</ImageFile>\r\n    <DocId>\r\n      <DocDate>2014-02-12</DocDate>\r\n    </DocId>\r\n    <DocAmounts total=\"12345.00\" />\r\n    <Participant role=\"Seller\">\r\n      <OrgInfo kpp=\"123456789\" inn=\"1231231230\">\r\n        <Name>ООО \"Придуманный\"</Name>\r\n      </OrgInfo>\r\n    </Participant>\r\n    <Participant role=\"Buyer\">\r\n      <PersonInfo inn=\"772802057746\">\r\n        <Name>Иван</Name>\r\n        <MiddleName>Иванович</MiddleName>\r\n        <LastName>Иванов</LastName>\r\n      </PersonInfo>\r\n    </Participant>\r\n  </ScannedTorg12>\r\n</Document>";
            Assert.AreEqual(expScannedTorg12Build, expScannedTorg12Text);
            IIFNSExporter iFNSExporter = new IFNSExporter();
            var operationResult = iFNSExporter.CreateArchive("D:\\zipTest.zip",expClaim,new ExpFileInfo[]{ new ExpFileInfo("test.xml",new byte[]{1})},new ExpFileInfo[]{ new ExpFileInfo("test.jpg",new byte[]{1})});
            Assert.AreEqual(operationResult.IsSystemError, false);
        }

        /*[TestMethod]
        public void CanMoveState()
        {
            DBRepository dbRepository = new DBRepository();
            var result = dbRepository.CanMoveNextState(1, 2, 1, Guid.Parse("ED101C95-C6F8-4596-860B-0312610A2BC8"), Guid.Parse("ED101C95-C6F8-4596-860B-0312610A2BC8"));
            int count = result.Data.Length;
        }*/
        [TestMethod]
        public void TestExcel()
        {
            string fullFileName = @"D:\Documents\Налоговая отчетность\Примеры реестров\Универсальный формат.xlsx";
            //string fullFileName = @"\\tools\SAPShared\GILOYANPS_S_20150423_043552 - copy.xml";
            using (ExcelImport excelRepository = new ExcelImport())
            {
                var result = excelRepository.GetRegistries(fullFileName);
                //Assert.AreEqual(result.Data.Count(), 2);
            }
        }

        //29382CA5-6E0F-4BD3-B1E7-277C0D42689F//
        [TestMethod]
        public void TestLoadDocFile()
        {
            using(ISaperion saperion = new SaperionRepository())
            {
                IDBRepository documentRepository = new DBRepository();
                IIFNSExporter ifmsExporter = new IFNSExporter();
                DocumentService documentService = new DocumentService(documentRepository, saperion, ifmsExporter);
                var result = documentService.AttachRegistryDoc(Guid.Parse("8988608A-6848-439B-B86A-926D827285BB"),new int[] {3, 4, 5, 11});
                Assert.IsTrue(result.IsSucceed);
            }
        }

        [TestMethod]
        public void TestExportIFNS()
        {
            using (ISaperion saperion = new SaperionRepository())
            {
                IDBRepository documentRepository = new DBRepository();
                IIFNSExporter ifmsExporter = new IFNSExporter();
                DocumentService documentService = new DocumentService(documentRepository, saperion, ifmsExporter);
                var result = documentService.ExportClaim(Guid.Parse("29382CA5-6E0F-4BD3-B1E7-277C0D42689F"), "D:\\exportTest.zip");
                Assert.IsTrue(result.Data.Length == 0);
                Assert.IsTrue(result.IsSucceed);
            }
        }

        [TestMethod]
        public void TestImportFromSap()
        {
            using (ISaperion saperion = new SaperionRepository())
            {
                IDBRepository documentRepository = new DBRepository();
                /*DocumentService documentService = new DocumentService(documentRepository, null, null);
                documentService.LoadXml(Guid.NewGuid(), "BRIUKHANOVAP");*/
                Sap sap = new Sap(documentRepository);
                var operationResult = sap.LoadXml(Guid.Parse("3EA8FB38-CFD1-448A-BC68-23970CAFEC88"), "sibgenco\\giloyanps");
                Guid registryGuid = operationResult.Data;
                Assert.IsTrue(operationResult.Result.IsSucceed);
            }
        }
        [TestMethod]
        public void TestRegistryDocs()
        {
            DBRepository repository = new DBRepository();
            var result = repository.GetRegistryDocsSP(Guid.Parse("a46312bb-b2d7-4022-a7c1-2d0544121512"));
        }
    }
}
