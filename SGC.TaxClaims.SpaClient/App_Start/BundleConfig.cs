using System;
using System.Web.Optimization;

namespace SGC.TaxClaims.SpaClient
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);

            bundles.Add(
              new ScriptBundle("~/scripts/vendor")
                .Include("~/scripts/jquery-{version}.js")
                .Include("~/scripts/free-jqGrid/jquery.jqGrid.src.js")
                .Include("~/scripts/jquery.blockUI.js")

                .Include("~/scripts/knockout-{version}.debug.js")
                .Include("~/scripts/knockout.mapping-latest.debug.js")
                .Include("~/scripts/plugins/knockout.customBindings.js")
                .Include("~/scripts/plugins/knockout.utils.js")
                .Include("~/scripts/koGrid-{version}.debug.js")
                .Include("~/scripts/plugins/knockout.block.js")

                .Include("~/scripts/plugins/knockout.activity.js")
                .Include("~/scripts/plugins/knockout.command.js")
                .Include("~/scripts/plugins/knockout.dirtyFlag.js")
                .Include("~/scripts/plugins/knockout.autocomplete.js")

                .Include("~/scripts/knockout.validation.js")
                .Include("~/scripts/knockout.validation.ru-RU.js")

                .Include("~/scripts/toastr.js")
                .Include("~/scripts/Q.js")
                //.Include("~/scripts/breeze.debug.js")
                .Include("~/scripts/bootstrap.js")
                .Include("~/scripts/moment.js")
                .Include("~/Scripts/jquery-ui-{version}.js")
                .Include("~/Scripts/plugins/jquery.ui.datepicker-ru.js")
                .Include("~/Scripts/globalize/globalize.js")
                .Include("~/Scripts/globalize/cultures/globalize.culture.ru-RU.js")
                .Include("~/Scripts/knockout-jqueryui.js")
                .Include("~/Scripts/jQuery.FileUpload/jquery.fileupload.js")
                .Include("~/Scripts/jQuery.FileUpload/jquery.fileupload-jquery-ui.js")
                .Include("~/Scripts/spin.js")
                .Include("~/Scripts/prettify/prettify.js")
              );
            
            bundles.Add(
              new StyleBundle("~/Content/css")
                .Include("~/Content/jquery-ui.css")
                .Include("~/Content/jquery-ui.structure.css")
                .Include("~/Content/jquery-ui.theme.css")
                .Include("~/Content/jQuery.FileUpload/css/jquery.fileupload.css")
                .Include("~/Content/jQuery.FileUpload/css/jquery.fileupload-ui.css")
                .Include("~/Content/ie10mobile.css")
                .Include("~/Content/bootstrap.css")
                .Include("~/Content/durandal.css")
                .Include("~/Content/toastr.css")
                .Include("~/Content/ui.jqgrid.css")
                .Include("~/Content/KoGrid.css")
                .Include("~/Content/font-awesome.css")
                .Include("~/Content/prettify/prettify.css")

                .Include("~/Content/app.css")
              );
        }
        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
            {
                throw new ArgumentNullException("ignoreList");
            }

            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");

            //ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            //ignoreList.Ignore("*.min.js", OptimizationMode.WhenDisabled);
            //ignoreList.Ignore("*.min.css", OptimizationMode.WhenDisabled);
        }
    }
}