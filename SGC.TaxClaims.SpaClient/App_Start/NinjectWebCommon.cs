[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(SGC.TaxClaims.SpaClient.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(SGC.TaxClaims.SpaClient.App_Start.NinjectWebCommon), "Stop")]

namespace SGC.TaxClaims.SpaClient.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using SGC.TaxClaims.Core.Contracts;
    using SGC.TaxClaims.Core.Model;
    using SGC.TaxClaims.Infrastructure.EFRepos;
    using SGC.TaxClaims.Infrastructure.ReposImpl;
    using SGC.TaxClaims.Core;
    using SGC.TaxClaims.Infrastructure;
    using System.Collections.Generic;
    using System.Data.Entity;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<LockManager>().To<LockManager>().InSingletonScope();
            kernel.Bind<IDBRepository>().To<DBRepository>().InSingletonScope().WithPropertyValue("Logger", Logger.CreateSystemLogger<IDBRepository>()); // ������������ � �������������� ������
            kernel.Bind<ISaperion>().To<Mocks.SaperionMock>().WithPropertyValue("Logger", Logger.CreateSystemLogger<IDBRepository>()); ;//.To<SaperionRepository>(); .To<Mocks.SaperionMock>(); 
            kernel.Bind<ISap>().To<Sap>().InSingletonScope().WithConstructorArgument(typeof(IDBRepository), kernel.Get<IDBRepository>()).WithPropertyValue("Logger", Logger.CreateSystemLogger<IDBRepository>()); ;
            kernel.Bind<IExcelImport>().To<ExcelImport>();//.WithPropertyValue("Logger", Logger.CreateSystemLogger<IExcelImport>());
            kernel.Bind<IIFNSExporter>().To<IFNSExporter>().WithPropertyValue("Logger", Logger.CreateSystemLogger<IDBRepository>()); ;
            kernel.Bind<IUow>().To<Uow>()
                .OnActivation(delegate(Uow uow)
                {
                    //Logger.Debug("Unity Of Work activated."); 
                });
            //kernel.Bind<RepositoryFactories>().To<RepositoryFactories>().InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>()
                .WithConstructorArgument(typeof(IDictionary<Type, Func<DbContext, IUnityLocker, object>>),
                    new Dictionary<Type, Func<DbContext, IUnityLocker, object>>
                                    {
                                       {typeof(IUserRepository), (dbContext, unityLocker) => new UserRepository(dbContext, unityLocker)}
                                    }
                                );
        }        
    }
}
