﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using SGC.TaxClaims.Common;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.SpaClient.ViewModels;

namespace SGC.TaxClaims.SpaClient
{
    /// <summary>
    /// Сериализатор в Json и из Json'а для иерархии View-моделей. Десериализует в зависимости от значения свойства объекта DocTypeId.
    /// </summary>
    public class RegistryDocJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(RegistryDoc);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var o = JObject.FromObject(value);     //Here infinite recurrence occur
            o.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JObject.Load(reader);

            JsonReader reader2 = token.CreateReader(); // Создаем второй Reader для десериализации самого объекта

            // Считываем тип объекта
            int? docTypeId = (int?)token.SelectToken("docTypeId");

            //int? docTypeId = Serializer.GetPropertyValueAsInt32(reader, "docTypeId"); //ReflectionHelper.GetPropertyName<Int32>(() => (new RegistryDoc()).DocTypeId));
           
            DocumentType docType = docTypeId.HasValue ? (DocumentType)docTypeId.Value : DocumentType.Unspecified;

            // Согласно типу объекта десериализуем объект
            switch (docType)
            {
                case DocumentType.Unspecified:
                    return (NotSpecified)serializer.Deserialize(reader2, typeof(NotSpecified));
                case DocumentType.Invoice:
                    return (Invoice)serializer.Deserialize(reader2, typeof(Invoice));
                case DocumentType.InvoiceCorrection:
                    return (InvoiceCorr)serializer.Deserialize(reader2, typeof(InvoiceCorr));
                case DocumentType.Torg12:
                    return (Torg12)serializer.Deserialize(reader2, typeof(Torg12));
                case DocumentType.AcceptanceCertificate:
                    return (ACert)serializer.Deserialize(reader2, typeof(ACert));
                default:
                    return null;
            }
        }
    }
}