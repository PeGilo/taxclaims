﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace SGC.TaxClaims.SpaClient
{
    public class CustomContractResolver : DefaultContractResolver
    {
        private static readonly JsonConverter _converter = new RegistryDocJsonConverter();
        private static Type _type = typeof(SGC.TaxClaims.SpaClient.ViewModels.RegistryDoc);

        protected override JsonConverter ResolveContractConverter(Type objectType)
        {
            if (objectType == null || _type != objectType) // !_type.IsAssignableFrom(objectType)) 
            {
                return base.ResolveContractConverter(objectType);
            }

            return _converter;
        }

    }
}