﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient
{
    public static class Serializer
    {
        public static string SerializeToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, GetSettings());//, new SGC.TaxClaims.SpaClient.ViewModels.CustomDocumentJsonConverter());// GetSettings());
        }

        public static T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, GetSettings());
        }

        private static JsonSerializerSettings GetSettings()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings() { /* ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() */};
            //settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.DateFormatString = "dd.MM.yyyy";
            settings.ContractResolver = new CustomContractResolver();
            //settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            return settings;
        }

        /// <summary>
        /// Читает из Json свойство с указанным именем и возвращает его значение.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static int? GetPropertyValueAsInt32(JsonReader reader, string propertyName)
        {
            while (reader.Read())
            {
                if (reader.TokenType == JsonToken.PropertyName
                    && (string)reader.Value == propertyName)
                {
                    int? value = reader.ReadAsInt32();
                    return value;
                }
            }
            return default(int?);
        }
    }
}