﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient
{
    public static class FileHelper
    {
        /// <summary>
        /// Возвращает MIME-type по расширению файла
        /// </summary>
        /// <param name="extension">Расширение файла</param>
        /// <returns></returns>
        public static string GetContentTypeByExtension(string extension)
        {
            if (!String.IsNullOrEmpty(extension))
            {
                // Привести расширение к единому виду
                extension = (extension[0] != '.') ? "." + extension : extension;
                extension = extension.ToLower();

                return _contentTypes.ContainsKey(extension) ? _contentTypes[extension] : String.Empty;
            }
            return String.Empty;
        }

        private static readonly Dictionary<string, string> _contentTypes = new Dictionary<string, string>()
        {
            { ".jpg", "image/jpeg" },
            { ".jpeg", "image/jpeg" },
            { ".png", "image/png" },
            { ".tiff", "image/tiff" },
            { ".tif", "image/tiff" },
            { ".xml", "text/plain" },
            { ".pdf", "application/pdf" },
            { ".txt", "text/plain" }
        };

        /// <summary>
        /// Определяет, является ли файл SGN-фалом, по имени файла.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool IsSign(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(ext))
            {
                return ext.ToLower() == ".sign";
            }
            return false;
        }

        /// <summary>
        /// Определяет, является ли файл XML-фалом, по имени файла.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool IsXml(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(ext))
            {
                return ext.ToLower() == ".xml";
            }
            return false;
        }
    }
}