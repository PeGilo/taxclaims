﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.SpaClient
{
    public class FileManager
    {
        private static readonly string _PATH = Path.GetTempPath();
        private string _imagesFolder;

        public FileManager(string imagesFolder)
        {
            _imagesFolder = imagesFolder;
        }

        /// <summary>
        /// Сохраняет содержимое файл во временное хранилище
        /// </summary>
        /// <param name="content">Содержимое файла</param>
        /// <param name="extension">Расширение файла с точнок. Наприме ".jpg"</param>
        /// <param name="guid">Если указан, то файл сохраняется с таким Uid</param>
        public string StoreTemporaryFile(byte[] content, System.Guid guid, string extension, out string previewType)
        {
            string storageId = guid.ToString();

            string fullPath = Path.Combine(_PATH, guid.ToString() + extension);

            if (extension.ToLower() == ".tiff" || extension.ToLower() == ".tif")
            {
                previewType = ".jpg";
            }
            else {
                previewType = (extension != null) ? extension.ToLower() : "";
            }

            // 1. Проверить, что файл еще не существует такой
            if (File.Exists(fullPath))
            {
                return storageId;
            }

            // 2. Сохранить сам файл с новым именем
            
            File.WriteAllBytes(fullPath, content);

            // 3. Если TIFF, создать preview
            if(extension.ToLower() == ".tiff" || extension.ToLower() == ".tif")
            {
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(fullPath);

                bmp.Save(
                    Path.Combine(_PATH, Path.GetFileNameWithoutExtension(fullPath) + ".jpg"),
                    System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            // 4. Создать thumb

            //File.WriteAllBytes(fullPath, content);

            File.Copy(Path.Combine(_imagesFolder + "/thumbs", "default.jpg"),
                Path.Combine(_PATH, Path.GetFileNameWithoutExtension(fullPath) + "_thumb.jpg"));

            return storageId;
        }

        public byte[] GetFile(string storageId)
        {
            // 1. Найти файл
            string filePath = Directory.GetFiles(_PATH, storageId + ".*").FirstOrDefault();

            // 2. Прочитать содержимое
            return File.ReadAllBytes(filePath);
        }

        public System.Web.Mvc.FileResult GetThumb(string storageId)
        {
            // Попробовать найти файл GUID_thumb.jpg, если не нашли, то возрващаем по расширению файла, соответствующую картинку
            if(File.Exists(Path.Combine(_PATH, storageId + "_thumb.jpg")))
            {
                return new System.Web.Mvc.FilePathResult(
                    Path.Combine(_PATH, storageId + "_thumb.jpg"), 
                    FileHelper.GetContentTypeByExtension(".jpg"));
            }

            string filePath = Directory.GetFiles(_PATH, storageId + ".*").FirstOrDefault();
            //if (!String.IsNullOrEmpty(file))
            //{
            //    return 
            //}
            return new System.Web.Mvc.FilePathResult(Path.Combine(_imagesFolder + "/thumbs", "default.jpg"),
                FileHelper.GetContentTypeByExtension(".jpg"));
        }

        public System.Web.Mvc.ActionResult GetPreview(string storageId)
        {
            // Попробовать найти файл GUID_preview.jpg, либо вернуть сам файл
            if (File.Exists(Path.Combine(_PATH, storageId + "_preview.jpg")))
            {
                return new System.Web.Mvc.FilePathResult(
                    Path.Combine(_PATH, storageId + "_preview.jpg"),
                    FileHelper.GetContentTypeByExtension(".jpg"));
            }

            string file = Directory.GetFiles(_PATH, storageId + ".*").FirstOrDefault();
            if (!String.IsNullOrEmpty(file))
            {
                // Предобработка XML-файлов
                if (FileHelper.IsXml(file))
                {
                    Encoding sourceEncoding = Encoding.GetEncoding("windows-1251");

                    string xmlContent = File.ReadAllText(file, sourceEncoding).Replace("<", "&lt;").Replace(">", "&gt;");

                    byte[] bytes = sourceEncoding.GetBytes(xmlContent);

                    return new EncodedFileContentResult(bytes, FileHelper.GetContentTypeByExtension(Path.GetExtension(file)), sourceEncoding);
                    //return new System.Web.Mvc.FileContentResult(bytes, FileHelper.GetContentTypeByExtension(Path.GetExtension(file)));
                }

                return new System.Web.Mvc.FilePathResult(file, FileHelper.GetContentTypeByExtension(Path.GetExtension(file)));
            }

            // default
            return new System.Web.Mvc.FilePathResult(Path.Combine(_imagesFolder + "/thumbs", "default.jpg"),
                FileHelper.GetContentTypeByExtension(".jpg"));
        }
    }


}
