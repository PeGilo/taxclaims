﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient
{
    /// <summary>
    /// Конвертирование Model во ViewModel и обратно
    /// </summary>
    public static class ModelConverter
    {
        public static string DateToString(DateTime? dateTime)
        {
            return dateTime == null ? string.Empty : DateToString(dateTime.Value);
        }

        public static string DateToString(DateTime dateTime)
        {
            return dateTime.ToString("dd.MM.yyyy");
        }

        public static DateTime StringToDate(string dateTimeString)
        {
            return DateTime.ParseExact(dateTimeString, "dd.MM.yyyy", null);
        }

        /// <summary>
        /// Удаляет из имени пользователя домен.
        /// </summary>
        /// <param name="userName">Например, sibgenco\ivanovvv</param>
        /// <returns>Логин без домена</returns>
        public static String TrimDomain(string userName)
        {
            if (!String.IsNullOrEmpty(userName))
            {
                int separatorIndex;
                if ((separatorIndex = userName.IndexOf('\\')) >= 0)
                {
                    return userName.Remove(0, separatorIndex + 1);
                }
            }
            return userName;
        }
    }
}