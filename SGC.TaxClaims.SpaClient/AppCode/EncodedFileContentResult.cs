﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.SpaClient
{
    public class EncodedFileContentResult : ActionResult
    {
        

        public EncodedFileContentResult(byte[] fileContents, string contentType, Encoding encoding)
            //: base(contentType)
        {
            if (fileContents == null || string.IsNullOrEmpty(contentType))
            {
                throw new ArgumentNullException();
            }

            FileContents = fileContents;
            ContentType = contentType;
            Encoding = encoding;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            //var encoding = UnicodeEncoding.UTF8;
            var request = context.HttpContext.Request;
            var response = context.HttpContext.Response;

            response.Clear();
            response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", (request.Browser.Browser == "IE") ? HttpUtility.UrlEncode(FileDownloadName, Encoding) : FileDownloadName));
            response.ContentType = ContentType;
            response.Charset = Encoding.WebName;
            response.HeaderEncoding = Encoding;
            response.ContentEncoding = Encoding;
            response.BinaryWrite(FileContents);
            response.End();
        }

        public byte[] FileContents { get; private set; }

        public string ContentType { get; private set; }

        public string FileDownloadName { get; set; }

        public Encoding Encoding { get; private set; }
    }
}