﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class Claim
    {
        public Guid Uid { get; set; }

        public string InspectionName { get; set; }

        public string ClaimNumber { get; set; }

        public string TargetOrganization { get; set; }

        //TODO: поменять на id
        public int? OrganizationId { get; set; }

        public string Comment { get; set; }

        public DateTime? DateReady { get; set; }

        public DateTime DateCreated { get; set; }

        public DocFile[] Files { get;set; }

        public string DateReadyString 
        { 
            get 
            {
                return ModelConverter.DateToString(DateReady);
            } 
        }

        public string DateCreatedString
        {
            get
            {
                return ModelConverter.DateToString(DateCreated);
            }
        }

        public string Status { get; set; }
    }
}