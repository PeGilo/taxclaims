﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public struct Org
    {
        public int Id;
        public string Name;
    }
}