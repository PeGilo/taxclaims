﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class DocFile
    {
        private string _previewType = "";
        public System.Guid? UId { get; set; }
        public System.Guid? DocumentUid { get; set; }

        /// <summary>
        /// Тип файла
        /// </summary>
        public int FileTypeId { get; set; }

        public int SprPage { get; set; }

        public string FileTypeName { get; set; }

        /// <summary>
        /// Имя файла, отображаемое в списке
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Выбран ли файл пользователем
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// Можно ли выбрать файл, либо можно только просмотреть
        /// </summary>
        public bool IsSelectable { get; set; }

        /// <summary>
        /// Cохранен ли файл в базе
        /// </summary>
        public bool IsAttached { get; set; }

        /// <summary>
        /// Показывать ли файл пользователю
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Имя файла во временном хранилище, по которому его можно будет найти
        /// </summary>
        public string StorageId { get; set; }

        public string ThumbUrl { get; set; }
        public string PreviewUrl { get; set; }
        public string DownloadUrl { get; set; }

        /// <summary>
        /// Тип preview (XML, PDF, JPG, ...)
        /// </summary>
        public string PreviewType
        {
            get { return _previewType != null ? _previewType.ToLower() : ""; }
            set { _previewType = value; }
        }

        public bool IsDeleted { get; set; }

        public bool IsAdded { get; set; }

        public bool IsChanged { get; set; }
        //public string Url
        //{
        //    get
        //    {
        //        return (UId != System.Guid.Empty)? String.Format("/app/fileg?uid={0}", UId.ToString()) : String.Format("/app/filen?name={0}", StorageName);

        //    }
        //    set { }
        //}

    }
}