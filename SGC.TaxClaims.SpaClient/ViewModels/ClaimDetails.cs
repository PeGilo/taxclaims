﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class ClaimDetails
    {
        public Guid Uid { get; set; }

        public string InspectionName { get; set; }

        public string ClaimNumber { get; set; }

        public string OrganizationName { get; set; }

        public string Comment { get; set; }

        public DateTime? DateReady { get; set; }

        public string DateReadyString 
        { 
            get 
            {
                return ModelConverter.DateToString(DateReady);
            } 
        }

        public DateTime DateCreated { get; set; }

        public string Status { get; set; }

        public string State { get; set; }
        ///////////////////////////////////////////////////////


        public List<RegistryDetails> Registries { get; set; }
        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        public List<DocFile> Files { get; set; }

        public ClaimDetails()
        {
            Files = new List<DocFile>();
            Registries = new List<RegistryDetails>();
        }
    }
}