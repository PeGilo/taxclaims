﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class Partner
    {
        public System.Guid UId { get; set; }
        public int PartnerTypeId { get; set; }
        public string Inn { get; set; }
        public string Kpp { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public Partner()
        {

        }

        public Partner(Core.Model.Partner entity)
        {
            UId = entity.UId;
            PartnerTypeId = entity.PartnerTypeId;
            Inn = entity.Inn;
            Kpp = entity.Kpp;
            Name = entity.Name;
            MiddleName = entity.MiddleName;
            LastName = entity.LastName;
        }
    }
}