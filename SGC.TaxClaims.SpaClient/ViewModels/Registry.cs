﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    /// <summary>
    /// View model для реестра
    /// </summary>
    public class Registry
    {
        public Guid Uid { get; set; }

        /// <summary>
        /// Идентификатор требования, к которому прикрепляется реестр
        /// </summary>
        public Guid ClaimId { get; set; }

        [DisplayName("Наименование реестра")]
        [Description("Наименование реестра")]
        public string RegistryName { get; set; }

        [DisplayName("Пункт требования")]
        [Description("Пункт требования, к которому относится реестр")]
        [MaxLength(20, ErrorMessage="Текст не должен содержать более 20 симоволов.")]
        public string ClaimItemNo { get; set; }

        public string Number { get; set; }

        [DisplayName("Срок подготовки данных")]
        [Description("Срок подготовки данных")]
        [UIHint("DateTimePicker")]
        public DateTime? DateReady { get; set; }

        public DateTime DateCreated { get; set; }

        public Guid CreatedUserUid { get; set; }

        public string ReadyDateString
        {
            get
            {
                return ModelConverter.DateToString(DateReady);
            }
        }

        public string Status { get; set; }

        /// <summary>
        /// Список документов, входящих в реестр
        /// </summary>
        public List<RegistryDoc> Docs { get; set; }

        public Registry()
        {
            Docs = new List<RegistryDoc>();
        }

        public Registry(Core.Model.Registry model, Core.Model.RegistryDoc[] childDocuments) //, Dictionary<int, ViewModels.DocType> docTypes)
            : this()
        {
            Uid = model.UId;
            RegistryName = model.Name;
            ClaimItemNo = model.ClaimItemNo;
            DateCreated = model.DateCreated;
            DateReady = model.DateReady;
            Number = model.Number;
            

            // Получить идентификатор родительского требования
            var parentRelation = model.ParentDocUId; // Предполагаем, что родительский документ м.б. только один
            if (parentRelation != null)
            {
                ClaimId = parentRelation.Value;
            }

            // Получить список дочерних документов
            foreach (Core.Model.RegistryDoc childDoc in childDocuments)
            {
                RegistryDoc vmDoc = RegistryDocFactory.CreateDoc(childDoc.DocTypeId, childDoc);

                // Инициализируем структуру типа документа
                //vmDoc.DocType = docTypes[childDoc.DocTypeId];
                Docs.Add(vmDoc);
            }
        }

        public void UpdateModel(Core.Model.Registry model)
        {
            Guid registryGuid = Guid.NewGuid();
            model.UId = registryGuid;
            model.Name = this.RegistryName;
            model.DateReady = this.DateReady;
            model.ClaimItemNo = this.ClaimItemNo;
            model.DateCreated = this.DateCreated;
            model.Number = this.Number;

            foreach (var doc in this.Docs)
            {
                // TODO: [Петр] перенести в UpdateModel
                Core.Model.RegistryDoc childDoc = new Core.Model.RegistryDoc()
                {
                    UId = Guid.NewGuid(),
                };

                doc.UpdateModel(childDoc);

                childDoc.ParentDocUId = registryGuid;

                model.Document1.Add(childDoc);

                /*Core.Model.DocRelation relation = new Core.Model.DocRelation()
                {
                    ParentDocUId = registryGuid,
                    DocumentParent = model,
                    ChildDocUId = childDoc.UId,
                    DocumentChild = childDoc,
                    DocRelTypeId = 1, // "Включает в себя"
                    ChangeStatus = System.Data.Entity.EntityState.Added
                };

                model.DocRelationParent.Add(relation);*/
            }

            model.ChangeStatus = System.Data.Entity.EntityState.Added;
        }
    }
}