﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    /// <summary>
    /// View model для реестра
    /// </summary>
    public class RegistryDetails
    {
        public Guid Uid { get; set; }

        /// <summary>
        /// Идентификатор требования, к которому прикрепляется реестр
        /// </summary>
        public Guid ClaimId { get; set; }

        public string RegistryName { get; set; }

        public string ClaimItemNo { get; set; }

        public DateTime? DateReady { get; set; }

        public string Number { get; set; }

        public string ReadyDateString
        {
            get
            {
                return ModelConverter.DateToString(DateReady);
            }
        }

        public string Status { get; set; }

        public DateTime DateCreated { get; set; }

        public string UserCreator { get; set; }

        public Guid CreatorUserUid { get; set; }

        /// <summary>
        /// Список документов, входящих в реестр
        /// </summary>
        //public List<RegistryDoc> Docs { get; set; }

        public RegistryDetails()
        {
            //Docs = new List<RegistryDoc>();
        }

        public RegistryDetails(Core.Model.Registry model)//, Dictionary<int, ViewModels.DocType> docTypes)
            : this()
        {
            Uid = model.UId;
            RegistryName = model.Name;
            ClaimItemNo = model.ClaimItemNo;
            DateCreated = model.DateCreated;
            DateReady = model.DateReady;
            CreatorUserUid = model.CreatorUserUid;
            Number = model.Number;
            if (model.ParentDocUId != null)
                ClaimId = model.ParentDocUId.Value;
            // Получить идентификатор родительского требования
            var parentRelation = model.ParentDocUId; // Предполагаем, что родительский документ м.б. только один
            if (parentRelation != null)
            {
                ClaimId = parentRelation.Value;
            }
        }
    }
}