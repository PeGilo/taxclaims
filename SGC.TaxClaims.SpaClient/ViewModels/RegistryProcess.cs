﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Web;
//using SGC.TaxClaims.Core.Model;

//namespace SGC.TaxClaims.SpaClient.ViewModels
//{
//    // TODO: [Петр] Много общего с ViewModel.Registry
//    /// <summary>
//    /// View model для процессинга реестра
//    /// </summary>
//    public class RegistryProcess
//    {
//        public Guid Uid { get; set; }

//        /// <summary>
//        /// Идентификатор требования, к которому прикрепляется реестр
//        /// </summary>
//        public Guid ClaimId { get; set; }

//        [DisplayName("Наименование реестра")]
//        [Description("Наименование реестра")]
//        public string RegistryName { get; set; }

//        [DisplayName("Пункт требования")]
//        [Description("Пункт требования, к которому относится реестр")]
//        [MaxLength(20, ErrorMessage="Текст не должен содержать более 20 симоволов.")]
//        public string ClaimItemNo { get; set; }

//        [DisplayName("Срок подготовки данных")]
//        [Description("Срок подготовки данных")]
//        [UIHint("DateTimePicker")]
//        public DateTime? DateReady { get; set; }

//        public string ReadyDateString
//        {
//            get
//            {
//                return ModelConverter.DateToString(DateReady);
//            }
//        }

//        /// <summary>
//        /// Список документов, входящих в реестр
//        /// </summary>
//        public IList<DocBaseEditModel> Docs { get; set; }

//        public RegistryProcess()
//        {
//            Docs = new List<DocBaseEditModel>();
//            SourceDocCreateModel = new SourceDocCreateModel();
//            ReportDocCreateModel = new ReportDocCreateModel();
//        }

//        public RegistryProcess(Registry model, RegistryDoc[] childDocuments)
//            : this()
//        {
//            RegistryName = model.Name;
//            ClaimItemNo = model.ClaimItemNo;
//            DateReady = model.DateReady;

//            foreach (var childDoc in childDocuments)
//            {
//                DocBaseEditModel doc;

//                // TODO: [Петр] переделать в фабрику (?)
//                switch (childDoc.DocTypeId)
//                {
//                    case 3: // Invoice
//                        doc = new InvoiceEditModel();
//                        break;
//                    case 5: // Torg12
//                        doc = new Torg12EditModel();
//                        break;
//                    default:
//                        throw new NotImplementedException();
//                }

//                doc.DocNumber = childDoc.Number;
//                doc.DocDate = childDoc.DocDate;
//                doc.PackageIndex = childDoc.PackageIndex;
//                Docs.Add(doc);
//            }
//        }

//        //public void UpdateModel(Registry model)
//        //{
//        //    model.Name = this.RegistryName;
//        //    model.DateReady = this.DateReady;
//        //    model.ClaimItemNo = this.ClaimItemNo;
//        //    model.DateChanged = DateTime.UtcNow;

//        //    foreach (var doc in this.Docs)
//        //    {
//        //        // TODO: [Петр] сделать редактирование входящих документов
//        //        // Какие-то документы могут быть, добавлены, какие-то удалены и какие-то изменены
//        //        //

//        //    //    Document childDoc = new Document()
//        //    //    {
//        //    //    };

//        //    //    doc.UpdateModel(childDoc);

//        //    //    DocRelation relation = new DocRelation()
//        //    //    {
//        //    //        ParentDocUId = registryGuid,
//        //    //        DocumentParent = model,
//        //    //        ChildDocUId = childDoc.UId,
//        //    //        DocumentChild = childDoc,
//        //    //        DocRelTypeId = 1, // "Включает в себя"
//        //    //        ChangeStatus = System.Data.Entity.EntityState.Added
//        //    //    };

//        //    //    model.DocRelationParent.Add(relation);
//        //    }

//        //    model.ChangeStatus = System.Data.Entity.EntityState.Modified;
//        //}
//    }
//}