﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class InvoiceCorr : RegistryDoc
    {
        public InvoiceCorr()
        {
        }

        public InvoiceCorr(Core.Model.RegistryDoc entity)
            : base(entity)
        {
            this.DocTypeId = entity.DocTypeId;
            this.DocData.DocNumber = entity.Number;
            this.DocData.DocDate = entity.DocDate;
            this.PackageIndex = entity.PackageIndex;
        }

        public InvoiceCorr(ViewModels.InvoiceCorr vm)
            : base(vm)
        {
        }

        public override void UpdateModel(Core.Model.RegistryDoc doc)
        {
            base.UpdateModel(doc);

            doc.DocTypeId = 11; // Корректировочная счет-фактура

            doc.Number = this.DocData.DocNumber;
            doc.DocDate = this.DocData.DocDate;
            doc.PackageIndex = this.PackageIndex;
        }

        public override IEnumerable<RuleViolation> Validate()
        {
            List<RuleViolation> rv = new List<RuleViolation>(base.Validate());

            if (IsScan && (DocData == null || !DocData.DocDate.HasValue))
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Дата документа\"", "DocDate"));
            }

            if (IsScan && (DocData == null || String.IsNullOrEmpty(DocData.DocNumber)))
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Номер документа\"", "DocNumber"));
            }

            if (IsScan && !SumTotal.HasValue)
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Сумма всего\"", "SumTotal"));
            }

            if (IsScan && String.IsNullOrEmpty(ParentDocNumber))
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Номер документа основания\"", "ParentDocNumber"));
            }

            if (IsScan && !ParentDocDate.HasValue)
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Дата документа основания\"", "ParentDocDate"));
            }

            return rv;
        }

        #region "ICloneable implementation"

        public override object Clone()
        {
            return new InvoiceCorr(this);
        }

        #endregion
    }
}