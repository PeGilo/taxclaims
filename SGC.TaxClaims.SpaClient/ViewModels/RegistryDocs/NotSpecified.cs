﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class NotSpecified : RegistryDoc
    {
        public NotSpecified()
        {
        }

        public NotSpecified(Core.Model.RegistryDoc entity)
            : base(entity)
        {
            this.DocTypeId = entity.DocTypeId;
            this.DocData.DocNumber = entity.Number;
            this.DocData.DocDate = entity.DocDate;
            this.PackageIndex = entity.PackageIndex;
        }

        public NotSpecified(ViewModels.NotSpecified vm)
            : base(vm)
        {
        }

        public override void UpdateModel(Core.Model.RegistryDoc doc)
        {
            base.UpdateModel(doc);

            doc.DocTypeId = 0; // Документ не установленного типа

            doc.Number = this.DocData.DocNumber;
            doc.DocDate = this.DocData.DocDate;
            doc.PackageIndex = this.PackageIndex;
        }

        #region "ICloneable implementation"

        public override object Clone()
        {
            return new NotSpecified(this);
        }

        #endregion
    }
}