﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class DocData
    {
        public virtual string DocNumber { get; set; }

        public virtual DateTime? DocDate { get; set; }
    }

    
    public class RegistryDoc : ICloneable
    {
        public virtual System.Guid Uid { get; set; }
        public virtual Nullable<System.Guid> ParentDocUId { get; set; }

        public virtual int DocTypeId { get; set; }

        public DocData DocData { get; set; }

        public virtual string PackageIndex { get; set; }

        public virtual bool IsScan { get; set; }

        public virtual string ParentDocNumber { get; set; }

        public virtual DateTime? ParentDocDate { get; set; }

        // ---
        public string PageNumbers { get; set; }
        public Nullable<decimal> SumTotal { get; set; }
        public Nullable<decimal> SumTax { get; set; }
        public string FactoryCode { get; set; }
        public string FactoryName { get; set; }
        public string PartnerName { get; set; }
        public string PartnerINN { get; set; }
        public string PartnerKPP { get; set; }

        public Nullable<int> CorrectionTypeId { get; set; }
        public string CorrectionTypeName { get; set; }

        public List<RegistryDoc> Children { get; set; }

        public int SourceId { get; set; }
        public virtual Nullable<System.Guid> BuyerUId { get; set; }
        public string BuyerName { get; set; }
        public string BuyerInn { get; set; }
        public string BuyerKpp { get; set; }

        public virtual Nullable<System.Guid> SellerUId { get; set; }
        public string SellerName { get; set; }
        public string SellerInn { get; set; }
        public string SellerKpp { get; set; }

        public Guid CreatorUserUId { get; set; }
        public string CreatorUserName { get; set; }

        // Код завода	КодЗав
        // Наименование завода	НаимЗав
        // Наименование покупателя	НаимПок
        // ИНН покупателя 	ИННПок
        // КПП покупателя	КПППок
           
        // Номер исправления счф	НомИспр
        // Дата исправления счф	ДатаИспр
        // Номер корректировочного счф	НомКСчф
        // Дата корректировочного счф	ДатаКСчф
        // Номер исправления корректировочного счф	НомерИспрКСчф
        // Дата исправления корректировочного счф	ДатаИспрКСчф
           
        // Номер документа основания	НомерДокОсн
        // Дата документа основания	ДатаДокОсн
           
        // Сумма всего	СтоимСчФВс
        // Сумма НДС	СумНДССчф
           
        // Признак формата обмена	ПризФО
        // Вид корректировки	ВидКорр

        public RegistryDoc()
        {
            DocData = new DocData();
            Children = new List<RegistryDoc>();
        }

        public RegistryDoc(Core.Model.RegistryDoc doc)
            : this()
        {
            Uid = doc.UId;
            IsScan = doc.IsScan;
            ParentDocNumber = doc.ParentDocNumber;
            ParentDocDate = doc.ParentDocDate;

            PageNumbers = doc.PageNumbers;
            SumTotal = doc.SumTotal;
            SumTax = doc.SumTax;
            FactoryCode = doc.FactoryCode;
            FactoryName = doc.FactoryName;
            PartnerName = doc.PartnerName;
            PartnerINN = doc.PartnerINN;
            PartnerKPP = doc.PartnerKPP;

            SourceId = doc.DocSourceId;
            ParentDocUId = doc.ParentDocUId;
            BuyerUId = doc.BuyerUId;
            SellerUId = doc.SellerUId;

            BuyerName = (doc.Buyer != null) ? doc.Buyer.Name : "";
            BuyerInn = (doc.Buyer != null) ? doc.Buyer.Inn : "";
            BuyerKpp = (doc.Buyer != null) ? doc.Buyer.Kpp : "";

            SellerName = (doc.Seller != null) ? doc.Seller.Name : "";
            SellerInn = (doc.Seller != null) ? doc.Seller.Inn : "";
            SellerKpp = (doc.Seller != null) ? doc.Seller.Kpp : "";

            CorrectionTypeId = doc.CorrectionTypeId;
            CorrectionTypeName = doc.CorrectionType != null ? doc.CorrectionType.Name : "";

            CreatorUserUId = doc.CreatorUserUid;
            CreatorUserName = doc.CreatorUser != null ? ModelConverter.TrimDomain(doc.CreatorUser.UserName) : null;

            // Получить список дочерних документов
            foreach (Core.Model.RegistryDoc childDoc in doc.Document1)
            {
                RegistryDoc child = RegistryDocFactory.CreateDoc(childDoc.DocTypeId, childDoc);

                // Инициализируем структуру типа документа
                //vmDoc.DocType = docTypes[childDoc.DocTypeId];
                Children.Add(child);
            }
        }

        public RegistryDoc(ViewModels.RegistryDoc vm)
        {
            Uid = vm.Uid;
            DocTypeId = vm.DocTypeId;
            DocData = new DocData()
            {
                DocDate = vm.DocData.DocDate,
                DocNumber = vm.DocData.DocNumber
            };
            SourceId = vm.SourceId;
            IsScan = vm.IsScan;
            PackageIndex = vm.PackageIndex;
            PageNumbers = vm.PageNumbers;
            ParentDocNumber = vm.ParentDocNumber;
            ParentDocDate = vm.ParentDocDate;
            CorrectionTypeId = vm.CorrectionTypeId;
            CorrectionTypeName = vm.CorrectionTypeName;
            FactoryCode = vm.FactoryCode;
            FactoryName = vm.FactoryName;
            PartnerINN = vm.PartnerINN;
            PartnerKPP = vm.PartnerKPP;
            PartnerName = vm.PartnerName;
            SumTax = vm.SumTax;
            SumTotal = vm.SumTotal;
            BuyerUId = vm.BuyerUId;
            SellerUId = vm.SellerUId;

            CreatorUserUId = vm.CreatorUserUId;
            CreatorUserName = vm.CreatorUserName;
        }

        public virtual void UpdateModel(Core.Model.RegistryDoc doc)
        {
            //doc.Name = "";
            //doc.DateCreated = DateTime.UtcNow;
            //doc.ChangeStatus = System.Data.Entity.EntityState.Added;
            doc.IsScan = this.IsScan;
            doc.ParentDocNumber = this.ParentDocNumber;
            doc.ParentDocDate = this.ParentDocDate;
            doc.SumTax = this.SumTax;
            doc.SumTotal = this.SumTotal;
            doc.BuyerUId = this.BuyerUId;
            doc.SellerUId = this.SellerUId;
        }

        public virtual IEnumerable<RuleViolation> Validate()
        {
            List<RuleViolation> rv = new List<RuleViolation>();

            if (IsScan && (!BuyerUId.HasValue || BuyerUId == Guid.Empty))
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Покупатель\"", "BuyerUId"));
            }

            if (IsScan && (!SellerUId.HasValue || SellerUId == Guid.Empty))
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Продавец\"", "SellerUId"));
            }

            return rv;
        }

        #region "ICloneable implementation"

        public virtual object Clone()
        {
            return new RegistryDoc(this);
        }

        #endregion
    }
}