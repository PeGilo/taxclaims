﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class Torg12 : RegistryDoc
    {
        public Torg12()
        {
        }

        public Torg12(Core.Model.RegistryDoc entity)
            :base(entity)
        {
            this.DocTypeId = entity.DocTypeId;
            this.DocData.DocNumber = entity.Number;
            this.DocData.DocDate = entity.DocDate;
            this.PackageIndex = entity.PackageIndex;
        }

        public Torg12(ViewModels.Torg12 vm)
            : base(vm)
        {
        }

        public override void UpdateModel(Core.Model.RegistryDoc doc)
        {
            base.UpdateModel(doc);

            doc.DocTypeId = 5; // Товарная накладная (Торг-12)

            doc.Number = this.DocData.DocNumber;
            doc.DocDate = this.DocData.DocDate;
            doc.PackageIndex = this.PackageIndex;
        }

        public override IEnumerable<RuleViolation> Validate()
        {
            List<RuleViolation> rv = new List<RuleViolation>(base.Validate());

            if (IsScan && (DocData == null || !DocData.DocDate.HasValue))
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Дата документа\"", "DocDate"));
            }

            if (IsScan && !SumTotal.HasValue)
            {
                rv.Add(new RuleViolation("Не заполнено поле \"Сумма всего\"", "SumTotal"));
            }

            return rv;
        }

        #region "ICloneable implementation"

        public override object Clone()
        {
            return new Torg12(this);
        }

        #endregion
    }
}