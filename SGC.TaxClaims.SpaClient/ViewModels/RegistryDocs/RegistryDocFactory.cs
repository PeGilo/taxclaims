﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGC.TaxClaims.Core;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public static class RegistryDocFactory
    {
        public static RegistryDoc CreateDoc(int docTypeId, Core.Model.RegistryDoc entity)
        {
            DocumentType docType = (DocumentType)docTypeId;

            switch (docType)
            {
                case DocumentType.Unspecified:
                    return new NotSpecified(entity);
                case DocumentType.Invoice:
                    return new Invoice(entity);
                case DocumentType.AcceptanceCertificate:
                    return new ACert(entity);
                case DocumentType.Torg12:
                    return new Torg12(entity);
                case DocumentType.InvoiceCorrection:
                    return new InvoiceCorr(entity);
                default:
                    throw new NotImplementedException("Обработка данного типа документа не реализована.");
            }
        }
    }
}