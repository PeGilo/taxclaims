﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.SpaClient.ViewModels
{
    public class Invoice : RegistryDoc
    {
        public Invoice()
        {
        }

        public Invoice(Core.Model.RegistryDoc entity)
            : base(entity)
        {
            this.DocTypeId = entity.DocTypeId;
            this.DocData.DocNumber = entity.Number;
            this.DocData.DocDate = entity.DocDate;
            this.PackageIndex = entity.PackageIndex;
        }

        public Invoice(ViewModels.Invoice vm)
            : base(vm)
        {
        }

        public override void UpdateModel(Core.Model.RegistryDoc doc)
        {
            base.UpdateModel(doc);

            doc.DocTypeId = 3; // Счет-фактура

            doc.Number = this.DocData.DocNumber;
            doc.DocDate = this.DocData.DocDate;
            doc.PackageIndex = this.PackageIndex;
        }

        public override IEnumerable<RuleViolation> Validate()
        {
            List<RuleViolation> rv = new List<RuleViolation>(base.Validate());

            if (IsScan && (DocData == null || !DocData.DocDate.HasValue)) {
                rv.Add(new RuleViolation("Не заполнено поле \"Дата документа\"", "DocDate"));
            }

            if (IsScan && (DocData == null || String.IsNullOrEmpty(DocData.DocNumber))) {
                rv.Add(new RuleViolation("Не заполнено поле \"Номер документа\"", "DocNumber"));
            }

            if (IsScan && !SumTotal.HasValue) {
                rv.Add(new RuleViolation("Не заполнено поле \"Сумма всего\"", "SumTotal"));
            }

            if (IsScan && !SumTax.HasValue) {
                rv.Add(new RuleViolation("Не заполнено поле \"Сумма налога\"", "SumTax"));
            }

            return rv;
        }

        #region "ICloneable implementation"

        public override object Clone()
        {
            return new Invoice(this);
        }

        #endregion
    }
}