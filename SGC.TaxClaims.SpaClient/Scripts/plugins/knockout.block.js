﻿; (function (ko) {

    ko.bindingHandlers.block = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var boolOrObj = ko.utils.unwrapObservable(valueAccessor());
            var bool;
            if (typeof boolOrObj === "boolean") {
                bool = boolOrObj === true;
            }
            else {
                bool = ko.utils.unwrapObservable(boolOrObj.on);
            }
            if (bool) {
                if (typeof boolOrObj === "boolean") {
                    $(element).block({ message: null });
                } else {
                    $(element).block(boolOrObj);
                }
            } else {
                $(element).unblock();
            }
        }
    };
})(ko);