﻿; (function (ko) {

    ko.bindingHandlers.autoComplete = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var bindings = allBindingsAccessor();
            var postUrl = bindings.source; // url to post to is read here
            var maxCount = bindings.maxCount || 10;
            var selectedObservableInViewModel = valueAccessor();
            var itemPresenter = bindings.itemPresenter;
            var selectedPresenter = bindings.selectedPresenter;
            var leaveEnabled = bindings.leaveEnabled;

            $(element).val(selectedObservableInViewModel());

            $(element).autocomplete({
                //minLength: 2,
                //autoFocus: true,
                //focus: function (event, ui) {
                //    $(element).val(selectedPresenter(ui));

                //    return false;
                //},
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        url: postUrl,
                        data: { term: request.term, maxCount: maxCount },
                        dataType: "json",
                        //contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response(data);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert('Возникла ошибка во время обращения к серверу. ', textStatus);
                        }
                    });
                },
                select: function (event, ui) {
                    //var selectedItem = ui.item;
                    //var leaveEnabledFn = leaveEnabled(); //allBindingsAccessor.get('leaveEnabled');

                    if (leaveEnabled() === false) {
                        event.preventDefault();
                        var currentItem = selectedObservableInViewModel();
                        $(element).val(selectedPresenter(currentItem));
                        alert('Текущая запись не сохранена');
                    }
                    else {
                        $(element).val(selectedPresenter(ui.item));

                        selectedObservableInViewModel(ui.item);
                    }

                    //if (!_.any(selectedObservableArrayInViewModel(), function (item) { return item.id == selectedItem.id; })) { //ensure items with the same id cannot be added twice.
                    //    selectedObservableArrayInViewModel.push(selectedItem);
                    //}
                    return false;
                },
                change: function (event, ui) {
                    var input = $(element).val();
                    if (!input) {
                        selectedObservableInViewModel(null);
                    }
                }
            }).data("ui-autocomplete")._renderItem = itemPresenter;
        }
    };
})(ko);