﻿; (function (ko) {

    //
    // Встраивает в элемент просмотрщик PDF-файла
    //
    ko.bindingHandlers.pdfembed = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            //var value = valueAccessor();
            //var commands = value.execute ? { click: value } : value;
            //var events = {};
            //for (var command in commands) {
            //    events[command] = commands[command].execute;
            //}

            //ko.bindingHandlers.event.init(element, ko.utils.wrapAccessor(events), allBindingsAccessor, viewModel);
        },

        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

            //viewModel.previewType()

            var value = valueAccessor();

            // Next, whether or not the supplied model property is observable, get its current value
            var valueUnwrapped = ko.unwrap(value);

            var doc = $('<embed src="' + valueUnwrapped + '" type="application/pdf" />');
            $(element).empty().append(doc);//.contentDocument.location.reload()

            //var commands = valueAccessor();
            //var canExecute = commands.canExecute || (commands.click ? commands.click.canExecute : null);
            //if (!canExecute) {
            //    return;
            //}

            //ko.bindingHandlers.enable.update(element, canExecute, allBindingsAccessor, viewModel);
        }
    };
})(ko);

; (function (ko) {

    //
    // Встраивает в элемент просмотрщик PDF-файла
    //
    ko.bindingHandlers.iframe = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            //var value = valueAccessor();
            //var commands = value.execute ? { click: value } : value;
            //var events = {};
            //for (var command in commands) {
            //    events[command] = commands[command].execute;
            //}

            //ko.bindingHandlers.event.init(element, ko.utils.wrapAccessor(events), allBindingsAccessor, viewModel);
        },

        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

            //viewModel.previewType()

            var value = valueAccessor();

            // Next, whether or not the supplied model property is observable, get its current value
            var valueUnwrapped = ko.unwrap(value);

            //var doc = $('<iframe src="' + valueUnwrapped + '"></iframe>');
            //$(element).empty().append(doc);//.contentDocument.location.reload()

            $.ajax({
                url: valueUnwrapped,
                method: 'GET',
                dataType: 'text'
            })
                .done(function (response) {
                    var doc = $('<pre class="prettyprint lang-xml">' + response + '</pre>');
                    $(element).empty().append(doc);//.contentDocument.location.reload()
                    prettyPrint();
                })
                .fail(function (jqXHR, textStatus) {
                    alert(textStatus);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });
        }
    };
})(ko);


; (function (ko) {

    ko.bindingHandlers.toggle = {
        init: function (element, valueAccessor) {
            var value = valueAccessor();
            ko.applyBindingsToNode(element, {
                click: function () {
                    value(!value());
                }
            });
        }
    };
})(ko);

////
//// Resize JqGrid
////
//; (function (ko) {

//    ko.bindingHandlers.jqgridresize = {
//        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
//            var value = valueAccessor();
//            var $containerEl = $(element);
//            var gridId = value.gridId;
            
//            //$(window).resize(function () {
//            //    $('#' + gridId).jqGrid("setGridHeight", $containerEl.height() - 100)
//            //    $('#' + gridId).jqGrid("setGridWidth", $containerEl.width() - 50);
//            //});

//            //$('#' + gridId).jqGrid("setGridHeight", $containerEl.height() - 100)
//            //$('#' + gridId).jqGrid("setGridWidth", $containerEl.width() - 50);

//            //ko.bindingHandlers.event.init(element, ko.utils.wrapAccessor(events), allBindingsAccessor, viewModel);
//        },

//        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {


//            //ko.bindingHandlers.enable.update(element, canExecute, allBindingsAccessor, viewModel);
//        }
//    };
//})(ko);

; (function (ko) {

//
// Collapsible
//
ko.bindingHandlers.collapsible = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        //$(element).addClass('collapsible');
        //// hide all div containers  
        //$('div', element).hide();

        //$('div', element).addClass('ui-accordion-content').addClass('ui-helper-reset').addClass(' ui-widget-content');

        //$('h3', element).prepend('<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>');
        //$('h3', element).addClass('ui-accordion-header')
        //    .addClass('ui-state-default').addClass('ui-accordion-icons').addClass('ui-corner-all');
        //// append click event to the a element  
        //$('h3', element).click(function (e) {
        //    // slide down the corresponding div if hidden, or slide up if shown  
        //    $(this).next('div').slideToggle('fast');
        //    // set the current item as active  
        //    $(this).parent().toggleClass('active');
        //    e.preventDefault();
        //});

        var value = valueAccessor();
        var activateHandler = value.activate;

        $(element).accordion({
            //header: '.accordion-header',
            collapsible: true,
            header: "h6",
            heightStyle: 'content',
            activate: function (event, ui) { if (activateHandler) { activateHandler(event, ui); } }
        });
    },

    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        var value = valueAccessor();

        //var commands = valueAccessor();
        //var canExecute = commands.canExecute || (commands.click ? commands.click.canExecute : null);
        //if (!canExecute) {
        //    return;
        //}

        //ko.bindingHandlers.enable.update(element, canExecute, allBindingsAccessor, viewModel);
    }
};
})(ko);


; (function (ko) {

    ko.bindingHandlers.notchecked = {
        init: function (element, valueAccessor, allBindingsAccessor) {

            var $element = $(element);

            var valueUnwrapped = ko.unwrap(valueAccessor());

            $element.prop('checked', valueUnwrapped);

            $element.change(function () {
                var value = valueAccessor();

                var $this = $(this);
                // $this will contain a reference to the checkbox   
                if ($this.is(':checked')) {
                    value(false);
                } else {
                    // the checkbox was unchecked
                    value(true);
                }
            });

            //$(element).focus(function () {
            //    var value = valueAccessor();
            //    value(true);
            //});
            //$(element).blur(function () {
            //    var value = valueAccessor();
            //    value(false);
            //});

            //var value = valueAccessor();
            //var interceptor = ko.computed({
            //    read: function () {
            //        return !value();
            //    },
            //    write: function (newValue) {
            //        value(!newValue);
            //    },
            //    disposeWhenNodeIsRemoved: element
            //});

            //var newValueAccessor = function () { return interceptor; };


            ////keep a reference, so we can use in update function
            //ko.utils.domData.set(element, "newValueAccessor", newValueAccessor);
            ////call the real checked binding's init with the interceptor instead of our real observable
            //ko.bindingHandlers.checked.init(element, newValueAccessor, allBindingsAccessor);
        },
        update: function (element, valueAccessor) {
            //call the real checked binding's update with our interceptor instead of our real observable
            //ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));

          
            var valueUnwrapped = ko.unwrap(valueAccessor());

            $(element).prop('checked', !valueUnwrapped);
        }
    };
})(ko);

; (function (ko) {

    ko.bindingHandlers.kospin = {
        init: function (element, valueAccessor, allBindings) {
            // initialize the object which will return the eventual spinner
            var deferred = $.Deferred();
            element.spinner = deferred.promise();

            // force this to the bottom of the event queue in the rendering thread,
            // so we can get the computed color of the containing element after other bindings
            // (e.g. class, style) have evalutated.
            // add some more delay as the class bindings of the parent fire asynchronously.
            setTimeout(function () {
                var options = {};
                options.color = $(element).css("color");
                $.extend(options, ko.bindingHandlers.kospin.defaultOptions, ko.unwrap(allBindings().spinnerOptions));

                deferred.resolve(new Spinner(options));
            }, 30);
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
            // when the spinner exists, pick up the existing one and call appropriate methods on it

            var result = ko.unwrap(valueAccessor());

            element.spinner.done(function (spinner) {
                var isSpinning = result;
                if (isSpinning) {
                    $(element).show();
                    spinner.spin(element);
                } else {
                    if (spinner.el) { //don't stop first time
                        spinner.stop();
                    }

                    $(element).hide();
                }
            });
        },
        defaultOptions: {
            lines: 11,
            length: 13,
            width: 4,
            corners: 1.0,
            radius: 5,
            speed: 1.0,
            trail: 60
        }
    };
})(ko);


; (function (ko) {

    ko.bindingHandlers.rotation = {
        init: function (element, valueAccessor, allBindingsAccessor) {

            var $element = $(element);

            var valueUnwrapped = ko.unwrap(valueAccessor());

            var rotate = 'rotate(' + valueUnwrapped + 'deg)';
            $element.css({
                '-webkit-transform': rotate,
                '-moz-transform': rotate,
                '-o-transform': rotate,
                '-ms-transform': rotate,
                'transform': rotate
            });

        },
        update: function (element, valueAccessor) {
            //call the real checked binding's update with our interceptor instead of our real observable
            //ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));

            var valueUnwrapped = ko.unwrap(valueAccessor());


            var rotate = 'rotate(' + valueUnwrapped + 'deg)';
            $(element).css({
                '-webkit-transform': rotate,
                '-moz-transform': rotate,
                '-o-transform': rotate,
                '-ms-transform': rotate,
                'transform': rotate
            });
        }
    };
})(ko);