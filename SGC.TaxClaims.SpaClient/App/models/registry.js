﻿define(['knockout'],
    function (ko) {
        var Registry = function () {
            var self = this;
            self.uid = ko.observable();
            self.claimId = ko.observable();
            self.registryName = ko.observable();

            self.claimItemNo = ko.observable().extend({
                                                    required: true
                                                })
                                              .extend({
                                                  pattern: {
                                                      message: 'Номер требования должен состоять из цифр, разделенных точкой, например, 1.11',
                                                      params: '^\d\.\d{2}$'
                                                  }
                                              });

            self.dateReady = ko.observable().extend({
                                                required: true
                                            })
                                              .extend({
                                                  pattern: {
                                                      message: 'Значение должно быть датой, например, 01.01.2015',
                                                      params: '^\d{2}\.\d{2}\.\d{4}$'
                                                  }
                                              });
            self.dateCreated = ko.observable();
            self.status = ko.observable();
            self.number = ko.observable();
            self.docs = ko.observableArray([]);
            self.isNullo = false;

            self.validation = ko.validatedObservable([self.claimItemNo, self.dateReady]);


            return self;
        };

        Registry.Nullo = new Registry().uid(''); //00000000-0000-0000-0000-000000000000
        Registry.Nullo.isNullo = true;

        return Registry;
    });
