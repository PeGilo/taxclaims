﻿define(['knockout'],
    function (ko) {
        var DocFile = function () {
            var self = this;
            //public System.Guid UId { get; set; }
            //public System.Guid DocumentUid { get; set; }
            //public int FileTypeId { get; set; }
            //public string Name { get; set; }
            //public bool IsSelected { get; set; }
            //public bool IsEnabled { get; set; }
            self.uid = ko.observable();
            self.documentUid = ko.observable();
            self.fileTypeId = ko.observable();
            self.sprPage = ko.observable();
            self.fileType = ko.observable();
            self.fileTypeName = ko.observable();
            self.name = ko.observable();
            self.isSelected = ko.observable();
            self.isSelectable = ko.observable();
            self.isVisible = ko.observable();
            self.isAttached = ko.observable();
            self.storageId = ko.observable();
            self.thumbUrl = ko.observable();
            self.previewUrl = ko.observable();
            self.previewType = ko.observable();
            self.downloadUrl = ko.observable();
            self.isDeleted = ko.observable();
            self.isChanged = ko.observable();
            self.isAdded = false;
            self.isNullo = false;
            self.isInited = false;
            self.fileType.subscribe(function (newValue) {
                if (self.isInited) {
                    self.isChanged(true);
                    self.fileTypeId(newValue.id);
                }
            });

            self.dirtyFlag = new ko.DirtyFlag([
                                    self.isSelected
            ]);

            return self;
        };
        /*DocFile.isDeleted(false);
        DocFile.isChanged(false);*/
        DocFile.Nullo = new DocFile().uid(''); //00000000-0000-0000-0000-000000000000
        DocFile.Nullo.isNullo = true;
        
        return DocFile;
    });
