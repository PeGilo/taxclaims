﻿define(['knockout'],
    function (ko) {
        var Claim = function () {
            var self = this;
            self.uid = ko.observable();
            self.inspectionName = ko.observable();
            self.claimNumber = ko.observable();
            self.organizationId = ko.observable();
            self.organizationName = ko.observable();
            self.comment = ko.observable();
            self.dateReady = ko.observable();
            self.dateCreated = ko.observable();
            self.files = ko.observableArray();
            self.registries = ko.observableArray();
           // self.state = ko.observableArray();
            self.isNullo = false;
            return self;
        };
        Claim.Nullo = new Claim().uid(''); //00000000-0000-0000-0000-000000000000
        Claim.Nullo.isNullo = true;

        return Claim;
    });
