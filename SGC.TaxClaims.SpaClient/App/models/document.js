﻿define(['knockout'],
    function (ko) {
        var Document = function (data) {
            var self = this;
            //self.$type = ko.observable(); // ! Должен идти первым, для сериализации // Уже не используется для десериализации
            //self.templateName = ko.observable(); // template используем одну
            self.uid = ko.observable();
            self.docTypeId = ko.observable();
            self.isScan = ko.observable();
            
            //self.typeName = ko.observable();
            //self.docType = ko.observable(); // есть docTypeId
            self.docData = ko.observable();

            self.parentDocNumber = ko.observable();
            self.parentDocDate = ko.observable();

            //self.docNumber = ko.observable();
            //self.docDate = ko.observable();
            self.packageIndex = ko.observable();

            self.pageNumbers = ko.observable();
            self.sumTotal = ko.observable();
            self.sumTax = ko.observable();
            self.factoryCode = ko.observable();
            self.factoryName = ko.observable();
            self.partnerName = ko.observable();
            self.partnerINN = ko.observable();
            self.partnerKPP = ko.observable();

            self.children = ko.observableArray([]); // подчиненные документы
            self.sourceId = ko.observable();

            self.cloneEnabled = ko.observable(false); // Для формы registry.process

            self.isNullo = false;

            // подписка на событие смены полей, после которых надо перечитывать файлы из базы
            self.fileSourceComputed = ko.computed(function () {

                return self.docTypeId() + self.isScan() + self.packageIndex();
            }),

            self.dirtyFlag = new ko.DirtyFlag([
                                                self.docTypeId,
                                                self.isScan,
                                                self.packageIndex
                                                ]);

            // Инициализация
            if (data) {
                self.uid(data.Uid)
                self.docTypeId(data.DocTypeId)
                self.isScan(data.IsScan)
                self.packageIndex(data.PackageIndex)
                self.parentDocNumber(data.ParentDocNumber)
                self.parentDocDate(data.ParentDocDate)

                self.pageNumbers(data.PageNumbers)
                self.sumTotal(data.SumTotal)
                self.sumTax(data.SumTax)
                self.factoryCode(data.FactoryCode)
                self.factoryName(data.FactoryName)
                self.partnerName(data.PartnerName)
                self.partnerINN(data.PartnerINN)
                self.partnerKPP(data.PartnerKPP);

                self.docData({
                    docNumber: data.DocData.DocNumber,
                    docDate: data.DocData.DocDate
                });

                self.children(ko.utils.arrayMap(data.Children, function (item) {
                    var doc = new Document(item);
                    return doc;
                }));

                self.dirtyFlag().reset();
            }

            return self;
        };

        Document.Nullo = new Document().uid(''); //00000000-0000-0000-0000-000000000000
        Document.Nullo.isNullo = true;

        return Document;
    });
