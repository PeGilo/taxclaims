﻿define(['knockout'],
    function (ko) {
        var DocFile = function () {
            var self = this;
            self.id = ko.observable();
            self.name = ko.observable();
            return self;
        };
        return DocFile;
    });