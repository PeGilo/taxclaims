﻿define(['services/logger', 'plugins/router', 'services/dataservice', 'services/presenter'],
function (logger, router, dataservice, presenter) {
    var title = 'Требование',
        currentClaimId = ko.observable(),
        claim = ko.observable(),
        selectedOrganization = ko.observable(),
        organizations = ko.observableArray(),
        files = ko.observableArray(),
        fileTypes = ko.observableArray(),
        isInited = false;
        //Инициализация типов файлов
        initFileType = function () {
            if (fileTypes != null && fileTypes.length > 0)
                fileTypes.removeAll();
            var fileType = dataservice.createFileType();
            fileType.id(2);
            fileType.name('Требование');
            fileTypes.push(fileType);
            fileType = dataservice.createFileType();
            fileType.id(3);
            fileType.name('Обоснование о правомерности');
            fileTypes.push(fileType);
            isInited = true;
        },
        activate = function (claimId) {
            //logger.log(title + ' View Activated', null, title, true);
            if (!isInited) initFileType();
            if (files != null)
                files.removeAll();
            if (claimId) {
                //Редактирование требования
                $.when(dataservice.getClaimById(claimId))
                .done(function (opresult) {
                    if (opresult.code === 0) {
                        var editClaim = opresult.data;
                        var newClaim = dataservice.createClaim();
                        newClaim.uid(editClaim.Uid);
                        newClaim.inspectionName(editClaim.InspectionName);
                        newClaim.claimNumber(editClaim.ClaimNumber);
                        newClaim.organizationId(editClaim.OrganizationId);
                        newClaim.comment(editClaim.Comment);
                        newClaim.dateReady(editClaim.DateReadyString);
                        newClaim.dateCreated(editClaim.DateCreatedString);
                        if (files != null && files().length > 0)
                            files.removeAll();
                        for (var i = 0; i < editClaim.Files.length; i++) {
                            var currentFile = dataservice.createDocFile();
                            currentFile.uid(editClaim.Files[i].UId);
                            currentFile.documentUid(editClaim.Files[i].DocumentUid);
                            currentFile.name(editClaim.Files[i].Name);
                            currentFile.fileTypeId(editClaim.Files[i].FileTypeId);
                            currentFile.isDeleted(false);
                            currentFile.isChanged(false);
                            currentFile.fileType(ko.utils.arrayFirst(fileTypes(), function (item) {
                                return item.id() == editClaim.Files[i].FileTypeId;
                            }));
                            /*
                            currentFile.fileType.subscribe(function (newValue) {
                                currentFile.isChanged(true);
                                currentFile.fileTypeId(newValue.id);
                            });*/
                            currentFile.isInited = true;
                            files.push(currentFile);
                        }
                        
                        //files(editClaim.Files);
                        currentClaimId(newClaim.uid());
                        claim(newClaim);

                        getOrganizations(function () {
                            var selectedOrg = ko.utils.arrayFirst(organizations(), function (item) {
                                return item.Id === newClaim.organizationId();
                            });
                            selectedOrganization(selectedOrg);
                        });
                    }
                    else {
                        logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                    }
                })
                .fail(function (opresult) {
                    logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                });
            }
            else {
                // Создание нового требования
                var newClaim = dataservice.createClaim();
                
                //newClaim.inspectionName = 'Test';
                currentClaimId(newClaim.uid());
               
                claim(newClaim);
                getOrganizations();
            }

            return true;
        },
    bindingComplete = function() {
            //$("input.datefield").datepicker({ dateFormat: "dd.mm.yy" });
        },
    //var file = new DocFile();
    //file.name('Test');
    //files.push(file);
    addFile = ko.asyncCommand({
        execute: function (complete) {
            //var file = new DocFile();
            //file.name('Test');
            //files.push(file);
            files.push({name:'Test'});
        },
        canExecute: function (isExecuting) {
            return true;
        }
    }),

    removeFile = function (file)
    {
        file.isDeleted(true);
    },

    cmdSave = ko.asyncCommand({
        execute: function (complete) {
            presenter.blockUI("Сохранение данных");
            //var id = selectedOrganization().Id;
              claim().organizationId(selectedOrganization().Id);
              claim().files(files);
                dataservice.updateClaim(claim())
                    .done(function () {
                        presenter.unblockUI();
                    })
                    .fail()
                    .always(complete);
                router.navigate('claims');
            },
        canExecute: function (isExecuting) {
            return !isExecuting;
        }
    }),

    cmdCancel = ko.asyncCommand({
            execute: function (complete) {
                complete();
                router.navigate('claims');
            },
            canExecute: function (isExecuting) {
                return true;
            }
    }),

    cmdAttachFile = ko.asyncCommand({
        execute: function (complete) {
            var fileUpload = $("#claim-fileupload");
            fileUpload.click();
            complete();
        },
        canExecute: function (isExecuting) {
            return !isExecuting;
        }
    }),



    compositionComplete = function compositionComplete() {
        $('#claim-fileupload').fileupload({
            dataType: 'json',
            //----------------//
            //Добавление файла//
            //----------------//
            done: function (e, data) {
                var guid = data._response.result.guid;
                var name = data._response.result.name;
                var file = dataservice.createDocFile();
                file.name(name);
                file.uid(guid);
                file.fileTypeId(2);
                file.isAdded = true;
                file.fileType.subscribe(function (newValue) {
                    file.fileTypeId(newValue.id);
                });
                files.push(file);
            },
            send: function (e, data) {
                var test = "";
            },
            submit: function (e, data) {
                var test = "";
            },
            fail: function (e, data) {
                // data.errorThrown
                // data.textStatus;
                // data.jqXHR;
                alert(data.textStatus + " : " + data.errorThrown);
            }
        });
    },

    getOrganizations = function (succeed) {
            if (!organizations().length) {
                dataservice.getOrganizations(
                    {
                    results: organizations
                },
                {succeed: succeed});
            }
        };

    return {
        // props
        claim: claim,
        currentClaimId: currentClaimId,
        organizations: organizations,
        selectedOrganization: selectedOrganization,
        title: title,
        files: files,
        // methods
        activate: activate,
        bindingComplete: bindingComplete,
        cmdCancel: cmdCancel,
        cmdSave: cmdSave,
        addFile: addFile,
        removeFile: removeFile,
        compositionComplete: compositionComplete,
        fileTypes: fileTypes
        
    };
    //#region Internal Methods
    //#endregion
});