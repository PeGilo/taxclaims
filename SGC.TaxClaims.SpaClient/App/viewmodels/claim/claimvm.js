﻿define(['knockout'], function (ko) {

    var getView = ko.computed(function () {
        return 'views/claim/claim.details.html';
    });

    return {
        getView: getView
    };

});