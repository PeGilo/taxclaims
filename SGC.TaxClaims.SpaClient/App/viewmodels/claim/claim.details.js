﻿define(['services/logger', 'plugins/router', 'services/dataservice', 'services/presenter', 'services/converter'],
function (logger, router, dataservice, presenter, converter) {

    var title = 'Требование';
    var claim = ko.observable();
    //Метод, который вызывается, при активации страницы//
    var activate = function (claimId) {
            presenter.blockUI();
            return loadData(claimId);
    };

    var loadData = function (claimId) {
        //Получаем требование
        var result = dataservice.getClaimDetailsAdv(claimId,
        function (obj, res) {
            presenter.unblockUI();
            if (res.code == 0) 
                claim(converter.toClaim(obj));
            else 
                presenter.showErrorMessage(res.message);
        });
        return result;
    };

    var cmdSapExport = ko.asyncCommand(
    {
        execute: function (complete) {

            presenter.blockUI();
            var claimUid = claim().uid();
            dataservice.importFromSapAdv(claimUid,
                function (obj, res) {
                    presenter.unblockUI();
                    if (res.code == 0) {
                        presenter.showConfirmMessage('Реестр создан');
                        claim().registries.push(converter.toRegistry(obj));
                    }
                    else {
                        if (res.isSys)
                            presenter.showErrorMessage("Произошла системная ошибка");
                        else
                            presenter.showErrorMessage(res.message);
                    }
                });
        },
        canExecute: function (isExecuting) {
            return true;
        }
    });

    var cmdExport = ko.asyncCommand({
        execute: function (complete)
        {
            presenter.blockUI("Сохранение данных");
            var claimUid = claim().uid();
            dataservice.exportClaimAdv(claimUid,
                function (obj, res) {
                    presenter.unblockUI();
                    if (res.code == 0)
                        presenter.showConfirmMessage('Архив сформирован');
                    else
                        presenter.showErrorMessage(res);
                    complete();
                });
        },
        canExecute: function (isExecuting) {
            return !isExecuting;
        }
    });

    //Нажать на конопке загрузить из excel
    var cmdLoadFromExcel = ko.asyncCommand({
        execute: function (complete) {

            var fileUpload = $("#excel-fileupload");
            fileUpload.click();
            complete();
        },
        canExecute: function (isExecuting) {
            return !isExecuting;
        }
    });

    //Загрузка из Excel
    var compositionComplete = function compositionComplete() {
        var claimUid = claim().uid();
        $('#excel-fileupload').fileupload({
            dataType: 'json',
            //data: {claimUid: claimUid},
            formData: { claimUid: claimUid },
            //----------------//
            //Добавление файла//
            //----------------//
            done: function (e, data) {
                var result = data._response.result;
                if (result.Code != 0) {
                    if (result.IsSystemError)
                        presenter.showErrorMessage('Произошла системная загрузки из Excel');
                    else
                        presenter.showErrorMessage(result.Message);
                }
                else {
                    claim().registries.push(converter.toRegistry(result.Data));
                    presenter.showConfirmMessage('Реестр успешно создан');
                }
            },
            send: function (e, data) {
                var test = "";
            },
            submit: function (e, data) {
                var test = "";
            },
            fail: function (e, data) {
                // data.errorThrown
                // data.textStatus;
                // data.jqXHR;
                alert(data.textStatus + " : " + data.errorThrown);
            }
        });
    };

    //Двойное нажатие на строке реестра
    var rowDbClick = function (registry) {
        /*var claimItemNo = registry.claimItemNo();
        $('#registryTab').show();//Делаем видной вкладку реестры
        $('li[role=\'presentation\']').attr('class', '');//Снимаем активность со всех вкладок
        $('#registryTab').attr('class', 'active');//Делаем активной вкладку
        $('.tabItem').hide();//Скрываем 
        $('#registryTabItem').show();//Показываем вкладку реестра
        $('#registryTabName').text('Реестр ' + claimItemNo);*///Устанавливаем имя вкладке
          ////////////////////////////////
         //Петр сюда нужно вставить код//
        ////////////////////////////////
        
    };

    return {
        // props
        claim: claim,
        title: title,
        // methods
        activate: activate,
        cmdExport: cmdExport,
        cmdSapExport: cmdSapExport,
        rowDbClick: rowDbClick,
        compositionComplete: compositionComplete,
        cmdLoadFromExcel: cmdLoadFromExcel
    };

    //#region Internal Methods
    //#endregion
});


/*function onTabClick(tabId, tabItemId) {
    $('li[role=\'presentation\']').attr('class', '');
    //$('#claimTab').attr('class', 'active');
    $('#' + tabId).attr('class', 'active');
    $('.tabItem').hide();
    //$('#claimTabItem').show();
    $('#' + tabItemId).show();
};*/
/*$(function () {
    var $table = $('#table-transform');
    $table.bootstrapTable();
});*/