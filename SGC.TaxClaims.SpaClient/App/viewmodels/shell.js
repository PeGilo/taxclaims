﻿define(['durandal/system', 'plugins/router', 'services/logger'],
    function (system, router, logger) {
        var shell = {
            activate: activate,
            router: router
        };
        
        return shell;

        //#region Internal Methods
        function activate() {
            return boot();
        }

        function boot() {
            //log('Hot Towel SPA Loaded!', null, true);

            router.on('router:route:not-found', function (fragment) {
                logError('Маршрут не найден', fragment, true);
            });

            var routes = [
                { route: '', moduleId: 'claim/index', title: 'Требования', nav: 1 },
                { route: 'claims', moduleId: 'claim/index', title: 'Требования', nav: false },
                { route: 'claim(/:claimId)', moduleId: 'claim/claim.form', title: 'Требование', nav: false }, // claimId - необязательный параметр
                { route: 'claim/details/:claimId', moduleId: 'claim/claim.details', title: 'Требование', nav: false }, // claimId - необязательный параметр
                //{ route: 'claim/details/:claimId', moduleId: 'claim/claimvm', title: 'Требование', nav: true }, // claimId - необязательный параметр
                { route: 'registry(/:registryId)/claim(/:claimId)', moduleId: 'registry/registry.form', title: 'Реестр', nav: false }, // создание/редактирование реестров
                { route: 'registry/process/:registryId', moduleId: 'registry/registry.process', title: 'Реестр', nav: false }
            ];

           // router.mapNav({ url: 'claim/details', moduleId: 'viewmodels/claim/claimvm', name: 'claimvm' });

            return router.makeRelative({ moduleId: 'viewmodels' }) // router will look here for viewmodels by convention
                .map(routes) // Map the routesview
                .buildNavigationModel() // Finds all nav routes and readies them
                .activate();            // Activate the router
        }

        function log(msg, data, showToast) {
            logger.log(msg, data, system.getModuleId(shell), showToast);
        }

        function logError(msg, data, showToast) {
            logger.logError(msg, data, system.getModuleId(shell), showToast);
        }
        //#endregion
    });