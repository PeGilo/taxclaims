﻿define(['durandal/app', 'services/logger', 'services/presenter', 'plugins/router', 'services/dataservice', './adddoc.modal'],
function (app, logger, presenter, router, dataservice, AddDocModal) {

    var title = 'Реестр',
        currentRegistryId = ko.observable(),
        registry = ko.observable(),
        //docs = ko.observableArray(),
        docTypes = ko.observableArray(),
        selectedDoc = ko.observable(),

        activate = function (registryId, claimId) {

            ko.validation.locale('ru-RU');

            presenter.blockUI();

            if (registryId) {
                //logger.log('Редактирование реестра', null, title, true);

                //Редактирование требования
                $.when(dataservice.getRegistryById(registryId))
                .done(function (opresult) {
                    if (opresult.code === 0) {
                        var editClaim = opresult.data;

                        var newRegistry = dataservice.createRegistry();
                        newRegistry.uid(opresult.data.Uid);
                        newRegistry.claimId(opresult.data.ClaimId);
                        newRegistry.registryName(opresult.data.RegistryName);
                        newRegistry.claimItemNo(opresult.data.ClaimItemNo);
                        newRegistry.dateCreated(opresult.data.DateCreated);
                        newRegistry.dateReady(opresult.data.DateReady);
                        newRegistry.number(opresult.data.Number);

                        currentRegistryId(newRegistry.uid());
                        registry(newRegistry);
                       
                        }
                    else {
                        logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                    }
                })
                .fail(function (opresult) {
                    logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                });

            }
            else if (claimId) {
                //logger.log('Создание реестра', null, title, true);

                // Создание нового требования
                var newRegistry = dataservice.createRegistry();
                newRegistry.claimId(claimId);
                currentRegistryId(newRegistry.uid);
                registry(newRegistry);

            }

            //registry.claimItemNo.clearError();

            //getDocTypes();
            return loadData();

            //return true;
        },

                loadData = function (complete) {

                    var def = $.Deferred();

                    $.when(dataservice.getDocTypes2())
                        .done(function (opresultDocTypes) {

                            if (opresultDocTypes.code === 0) {

                                docTypes.pushAll(opresultDocTypes.data);
                            }
                            else {
                                logger.log('Произошла ошибка при выполнении запроса: ' + opresultDocTypes.message, null, 'dataservice', true);
                            }

                            def.resolve(true);
                        })
                        .fail(function (opresultDocTypes) {
                            logger.log('Произошла ошибка при выполнении запроса: ' + opresultDocTypes.message, null, 'dataservice', true);
                            def.reject();
                        })
                        .always(function () {

                            //var address = ko.validatedObservable(self);

                            //address.isValid();
                            //errors = ko.validation.group(self);


                            presenter.unblockUI();
                        });

                    return def.promise();
                },

        //getDocTypes = function () {

        //    if (!docTypes().length) {
        //        //// добавляем специальный тип документов
        //        //docTypes.push({ Id: "0", Name: "", TextId: "notspecified" });
        //        dataservice.getDocTypes({
        //            results: docTypes
        //        });
        //    }
        //},

        cmdImportSAP = ko.asyncCommand({

            execute: function (complete) {
                complete();
            },
            canExecute: function (isExecuting) {
                return true;
            }
        }),

        cmdAddDoc = ko.asyncCommand({
            execute: function (complete) {

                AddDocModal.show(docTypes()).then(function (result) {
                    if (result) {
                        //app.showMessage('You answered "' + result.docTypeId + '".');
                        //Добавляем введенный документ в список
                        var doc = dataservice.createDocument()
                            //typeName: (result.docTypeId || {}).Name,
                            //typeId: (result.docTypeId || {}).Id,
                            //docNumber: result.docNumber,
                            //docDate: result.docDate,
                            //packageIndex: result.packageIndex,
                            //.templateName((result.docTypeId || {}).TemplateName)
                            //.$type((result.docTypeId || {}).Type)
                            .docTypeId((result.docTypeId || {}).Id)
                            .isScan(true)
                            .packageIndex(result.PackageIndex)
                            .docData({
                                //typeName: (result.docTypeId || {}).Name,
                                
                                docNumber: result.docNumber,
                                docDate: result.docDate
                                //packageIndex: result.packageIndex,
                            });

                        registry().docs.push(doc);
                    }
                    return true;
                });

                complete();
            },
            canExecute: function (isExecuting) {
                return true;
            }
        }),

        cmdSave = ko.asyncCommand({
            execute: function (complete) {

                // Валидация
                //var errors = ko.validation.group(self);
                //if (errors().length !== 0) {
                //    errors.showAllMessages();
                //    return;
                //}

                if (!registry.validation.isValid()) {
                    alert('Не валид');
                    return;
                }

                // Сохранение

                presenter.blockUI("Сохранение данных");
                //var id = selectedOrganization().Id;

                dataservice.updateRegistry(registry)
                    .done(function () {
                        
                    })
                    .fail()
                    .always(function () {
                        presenter.unblockUI();
                        complete();
                        router.navigate('claims');
                    });
            },
            canExecute: function (isExecuting) {
                return !isExecuting;
            }
        }),

        cmdExcelImport = ko.asyncCommand({
            execute: function (complete) {
                var fileUpload = $("#fileupload");
                fileUpload.click();
                complete();
            },
            canExecute: function (isExecuting) {
                return !isExecuting;
            }
        }),

        compositionComplete = function compositionComplete() {
            $('#fileupload').fileupload({
                dataType: 'json',
                //autoUpload: false,
                //acceptFileTypes:'(xsl|xslx)',
                //----------------//
                //Добавление файла//
                //----------------//
                done: function (e, data) {
                    var documents = data._response.result.Data;
                    for (var i = 0; i < documents.length; i++) {

                        var item = documents[i];
                        var doc = dataservice.createDocument()
                            .uid(item.Uid)
                            //.templateName(item.DocType.TemplateName)
                            //.$type(item.DocType.Type)
                            //.docType(item.DocType)
                            .docTypeId(item.DocTypeId)
                            .isScan(item.IsScan)
                            .packageIndex(item.PackageIndex)
                            .docData({
                                //typeName: item.DocType.Name,
                                //docTypeId: item.DocType.Id,
                                docNumber: item.DocData.DocNumber,
                                docDate: item.DocData.DocDate
                                //packageIndex: item.DocData.PackageIndex,
                            });

                        registry().docs.push(doc);
                    }

                },
                send: function (e, data) {
                    var test = "";
                },
                submit: function (e, data) {
                    var test = "";
                },
                fail: function (e, data) {
                    // data.errorThrown
                    // data.textStatus;
                    // data.jqXHR;
                    alert(data.textStatus + " : " + data.errorThrown);
                }
            });

        },
        cmdCancel = ko.asyncCommand({
            execute: function (complete) {
                router.navigate('claims');
                complete();
            },
            canExecute: function (isExecuting) {
                return true;
            }
        }),

    cmdSelectDocument = function (docItem) {
        // docItem: { root, document }
        //if (docItem.document && selectedDoc() != docItem.document) {
        //    selectedDoc(docItem.document);
        //}
        return true;
    };

    return {
        // props
        currentRegistryId: currentRegistryId,
        docTypes: docTypes,
        registry: registry,
        title: title,
        selectedDoc: selectedDoc,

        // methods
        activate: activate,
        cmdAddDoc: cmdAddDoc,
        cmdCancel: cmdCancel,
        cmdImportSAP: cmdImportSAP,
        cmdExcelImport: cmdExcelImport,
        cmdSelectDocument: cmdSelectDocument,
        compositionComplete: compositionComplete,
        cmdSave: cmdSave
    };

    //#region Internal Methods

    //#endregion
});