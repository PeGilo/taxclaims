﻿define(['durandal/app', 'services/logger', 'services/presenter', 'plugins/router', 'services/dataservice', './adddoc.modal',
     'viewmodels/registry.processing/RegistryDoc', 'models/docfile'],
function (app, logger, presenter, router, dataservice, AddDocModal, RegistryDoc, DocFile) {

    var self = this,

        //#region "Свойства"

        registryUId = ko.observable(),
        claimId = ko.observable(),
        registryName = ko.observable(),
        claimItemNo = ko.observable(),
        dateReady = ko.observable(),
        dateCreated = ko.observable(),

        docs = ko.observableArray([]),

        docTypes = ko.observableArray(),            // типы документов для выпадающих списков
        selectedDoc = ko.observable(),              // текущий выбранный документ
        selectedDoc_subscription = null,
        selectedDocSource_subscription = null,      
        userUpdatedEvent_subscription = null,

        filteredDocs = ko.observableArray([]),      // отфильтрованные документы

        title = ko.computed(function () {           // заголовок страницы

            var name = registryName();
            var number = claimItemNo();
            var date = dateReady();

            return 'Реестр ' + (name ? '"' + name + '"' : ' -') + ' / ' + (number ? number : '-') + ' / ' + (date ? date : '- ');
        }),

        attributes = ko.observable(new RegistryDoc()),               // Атрибуты документа

        // можно ли выбирать приложенные файлы
        canSelectFiles = ko.computed(function () {
            // выбирать файлы можно только для неэлектронных документов
            return (selectedDoc() && selectedDoc().isScan());
        }),

        // наименование шаблона для вывода атрибутов документа
        templateDocAttr = ko.computed(function () {

            //var doc = selectedDoc();
            var doc = attributes();

            if (doc && !doc.isScan()) {
                switch (doc.docTypeId())
                {
                    case 4:
                        return 'views/docattr/el_acert.html';
                    default:
                        return 'views/docattr/default.html';
                }
                
            }
            else if (doc && doc.isScan()) {
                switch (doc.docTypeId()) {
                    case 3:
                        return 'views/docattr/sc_invoice.html';
                    case 4:
                        return 'views/docattr/sc_acert.html';
                    case 5:
                        return 'views/docattr/sc_torg12.html';
                    case 11:
                        return 'views/docattr/sc_invoicecorr.html';
                    default:
                        return 'views/docattr/default.html';
                }
            }
            else {
                return 'views/docattr/default.html';
            }
        }),

        autocompletePartnerPresenter = function (ul, item) {

            var text = ( item.Name ? item.Name : "");

            return $("<li>")
              .append("<a>" + text+ "</a>")
              .appendTo(ul); 
        },

        //#endregion

        //#region "Свойства относящиеся к списку файлов"

        files = ko.observableArray([]),             // список файлов, прикрепленных к выделенному документу
        selectedFile = ko.observable(),             // выбранный файл в списке файлов
        selectedFile_subscription = null,
        isFileRequestActive = ko.observable(false), // происходит ли в данный момент запрос к серверу на получение списка файлов
        isFileRequestFailed = ko.observable(false), // завершился ли текущий запрос на файлы ошибкой


        // выбирает только измененные файлы из списка
        dirtyFiles = ko.computed(function() {
            return ko.utils.arrayFilter(files(), function (item) {
                return item.dirtyFlag().isDirty();
            });
        }),
    
        // сбрасывает бит IsDirty для файлов
        resetFiles = ko.computed(function () {
            ko.utils.arrayForEach(files(), function (item) {
                item.dirtyFlag().reset();
            });
        }),

        // изменены ли файлы
        hasDirtyFiles = ko.computed(function() {
            return dirtyFiles().length > 0;
        }), 

        //#endregion

        //#region "Свойства относящиеся к предпросмотру файлов"

        imgRotate = ko.observable(0),

        //#endregion

        //#region "Обработчики событий Life Cycle формы"

        //
        // обработчик события активации формы
        //
        activate = function (registryId) {

            presenter.blockUI();

            // подписка на события
            if (!selectedDoc_subscription) {
                selectedDoc_subscription = selectedDoc.subscribe(selectedDoc_Changed);
            }

            if (!selectedFile_subscription) {
                selectedFile_subscription = selectedFile.subscribe(selectedFile_Changed);
            }

            // загрузка данных, возвращается Deferred
            return loadData(registryId);
        },

        compositionComplete = function () {

        },

        //#endregion

        //docTypeChanged_subscription = null,
        //indexChanged_subscription = null,

        //#region "Обработчики событий"

        // когда изменяется выбранный документ
        selectedDoc_Changed = function (newValue) {

            if (selectedDocSource_subscription) {
                selectedDocSource_subscription.dispose();
            }
            if (userUpdatedEvent_subscription) {
                userUpdatedEvent_subscription.dispose();
            }

            // Очистка атрибутов
            // attributes.clear();

            if (newValue == null) {
                return;
            }

            if (newValue.sourceId() !== 0) {    // Если выбрали пакет, то создаем его копию, чтобы не править свойства самого пакета
                attributes(newValue.clone());
            }
            else {
                attributes(newValue);
            }

            // подписаться на события смены значений документа
            selectedDocSource_subscription = selectedDoc().fileSourceComputed.subscribe(fileSourceChanged);

            // подписаться на события редактирования данных документа
            userUpdatedEvent_subscription = selectedDoc().packageIndex.subscribe(selectedDoc_Updated);

            // перезагрузить список файлов
            reloadFileList(newValue);
        },

        // когда изменяется выбранный файл
        selectedFile_Changed = function (newValue) {
            // Убрать поворот картинки
            imgRotate(0);
        },

        // когда пользователь редактирует выбранный документ
        selectedDoc_Updated = function (newValue) {

            cmdSaveDoc.execute(true /* удалить вложенные пакеты */);
        },

        // когда у выбранного документа изменяется [тип документа]
        fileSourceChanged = function (newValue) {

            var selectedDocValue = selectedDoc();

            // Перезагрузка списка файлов
            if (selectedDocValue.sourceId() == 0 && selectedDocValue.isScan() == false) {
                reloadFileList(selectedDocValue);
            }
        },

        doc_onMouseOver = function (data) {
            // data: {document, root}
            data.cloneEnabled(true);
        },

        doc_onMouseOut = function (data) {
            // data: {document, root}
            data.cloneEnabled(false);
        },

        showDocumentElement = function (elem) {
            //$(elem).slideDown();
        },

        docTemplate_onAttached = function () {

        },
        //#endregion

        
        //
        // Получение имени типа документа из списка типов по ИД типа
        //
        getDocTypeName = function (registryDoc) {

            // Если документ содержит подчиенные, то складываем все типы в одну строку. 
            // Иначе передаем просто тип документа
            if ( registryDoc.sourceId() != 0) { // registryDoc.children().length > 0) {
                
                var types = ko.utils.arrayMap(registryDoc.children(), function (item) {
                    return getDocTypeName(item);
                });
                return types.join('/');
            }
            else {
                var docTypeId = registryDoc.docTypeId();
                var docType = ko.utils.arrayFirst(docTypes(), function (item) {
                    return item.Id == docTypeId;
                });
                return docType.ShortName;
            }
        },

        //#region "Команды"

        cmdCloneDocument = ko.asyncCommand({
            execute: function (data, complete) {

                var selectedDocValue = selectedDoc();
                if (selectedDocValue && selectedDocValue.dirtyFlag().isDirty()) {
                    presenter.showMessage("Выбранный документ не сохранен. Сохраните либо отмените изменения.");
                    return;
                }

                // получение выбранного документа

                var def = $.Deferred();
                $.when(dataservice.cloneRegistryDoc(data.document.uid()))
                    .done(function (opresult) {
                        if (opresult.code === 0) {
                            // Вставка нового документа в массив
                            
                            insertDocAfter(data.document, opresult.data);
                        }
                        else {
                            logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                        }

                        def.resolve(true);
                    })
                    .fail(function (opresult) {
                        logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                        def.reject();
                    })
                    .always(function () {
                        complete();
                    });

            },
            canExecute: function (isExecuting) {
                return !isExecuting;
            }
        }),

        cmdRotateLeft = ko.command({
            execute: function (data) {
                imgRotate((imgRotate() + 270) % 360);
            },
            canExecute: function () {
                return true;
            }
        }),

        cmdRotateRight = ko.command({
            execute: function (data) {
                imgRotate((imgRotate() + 90) % 360);
            },
            canExecute: function () {
                return true;
            }
        }),

        // Команда выбора документа
        // docItem: { root, document }
        cmdSelectDocument = function (docItem) {

            if (docItem && selectedDoc() != docItem) {

                // Не разрешать переходить на другой элемент, пока есть несохраненные изменения
                if (selectedDoc() && (selectedDoc().dirtyFlag().isDirty() || hasDirtyFiles())) {
                    presenter.showMessage("Выбранный документ не сохранен. Сохраните либо отмените изменения.");
                    return false;
                }

                // смена документа, надо подписаться на изменения, и отписаться от старого изменения

                //var subscription = myViewModel.personName.subscribe(function (newValue) { /* do stuff */ });
                //subscription.dispose(); // I no longer want notifications

                selectedDoc(docItem);
                selectedFile(null); // сбросить выбор файла
            }
            return true;
        },

        // выбран файл
        cmdSelectFile = function (fileItem) {

            selectedFile(fileItem);
            return true;
        },
        
        //
        // Сохранение атрибутов документа
        //
        cmdSaveDoc = ko.asyncCommand({
            execute: function (removeChildren, complete) {

                var updatedDocValue = selectedDoc();

                if (updatedDocValue) {
                    $.when(dataservice.saveDoc(registryUId(), updatedDocValue, removeChildren))
                    .done(function (opresult) {
                        if (opresult.code === 0) {
                            presenter.showConfirmMessage('Документ сохранен');

                            if (removeChildren) { updatedDocValue.children([]); }

                            reloadDocument(updatedDocValue.uid());

                            updatedDocValue.dirtyFlag().reset();

                            //// Инициировать обновление списка файлов
                            //reloadFileList(updatedDocValue);
                        }
                        else if (opresult.code === 1002) { // ошибка валидации
                            presenter.showValidationMessage(opresult.message);
                        }
                        else {
                            // TODO: В случае ошибки сохранения загрузить старые данные документа
                            presenter.showErrorMessage(opresult.message);
                        }
                    })
                    .fail(function (opresult) {
                        logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                    })
                    .always(function () {
                        complete();
                    });
                }
                else {
                    complete();
                }
            },
            canExecute: function (isExecuting) {
                return true;
            }
        }),

        // сохранение документа и выбранных файлов
        cmdProcessDoc = ko.asyncCommand({
            execute: function (complete) {

                var selectedDocValue = selectedDoc();

                //selectedDocValue.docType(ko.utils.arrayFirst(docTypes(), function (item) {
                //                            return item.Id == selectedDocValue.docTypeId();
                //}));

                //selectedDocValue.$type(selectedDocValue.docType().Type);

                if (selectedDocValue) {
                    $.when(dataservice.processDoc(registryUId(), attributes(), files()))  // (dataservice.processDoc(registryUId(), selectedDocValue, files()))
                    .done(function (opresult) {
                        if(opresult.code === 0) {
                            presenter.showConfirmMessage('Документ сохранен');

                            selectedDoc().dirtyFlag().reset();

                            // если был добавлен новый документ, то добавить его в дерево и перейти на него
                            var addedDoc = attachDocument(opresult.data);

                            // сбросить биты изменения для файлов
                            resetFiles();
                            // перезагрузить список файлов
                            //reloadFileList(selectedDoc());
                        }
                        else if (opresult.code === 1002) { // ошибка валидации
                            presenter.showValidationMessage(opresult.message);
                        }
                    })
                    .fail(function (opresult) {
                        logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                    })
                    .always(function () {
                        complete();
                    });
                }
                else {
                    complete();
                }
            },
            canExecute: function (isExecuting) {
                return !isExecuting;
            }
        }),

        // Удаление выбранного документа
        cmdDeleteDoc = ko.asyncCommand({
            execute: function (regDoc, arg2, complete) {

                if (!confirm('Удалить документ?')) {
                    complete();
                    return;
                }

                var docUid = regDoc.uid();

                if (docUid) {
                    $.when(dataservice.deleteDoc(docUid))
                    .done(function (opresult) {
                        if (opresult.code === 0) {
                            presenter.showConfirmMessage('Документ удален');

                            //docs.remove(regDoc);
                            
                            removeDocFromList(docUid)

                            selectedDoc(null);
                        }
                    })
                    .fail(function (opresult) {
                        logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                    })
                    .always(function () {
                        complete();
                    });
                }
                else {
                    complete();
                }
            },
            canExecute: function (isExecuting) {
                return !isExecuting;
            }
        }),

        // отмена изменения документа
        cmdCancelDoc = ko.asyncCommand({
            execute: function (complete) {
                // загрузить изначальные данные документа
                var selectedDocValue = selectedDoc();
                if (selectedDocValue) {
                    var docUid = selectedDocValue.uid();

                    reloadDocument(docUid);
                    //reloadFileList(selectedDocValue);

                    selectedDocValue.dirtyFlag().reset();
                }

                complete();
            },
            canExecute: function (isExecuting) {
                // Для электронного документ из SAP кнопка Cancel не имеет смысла
                var selectedDocObj = selectedDoc();

                return (selectedDocObj && (selectedDocObj.sourceId() == 0 || selectedDocObj.isScan()) && (selectedDocObj.dirtyFlag().isDirty() || hasDirtyFiles()))
                && !isExecuting;
            }
        });

    //#endregion

    var vm = {
        // свойства
        //
        attributes: attributes,
        registryUId: registryUId,
        claimId: claimId,
        registryName: registryName,
        claimItemNo: claimItemNo,
        dateReady: dateReady,
        dateCreated: dateCreated,

        docs: docs,

        canSelectFiles: canSelectFiles,
        //currentRegistryId: currentRegistryId,
        docTypes: docTypes,
        imgRotate: imgRotate,
        isFileRequestActive: isFileRequestActive,
        isFileRequestFailed: isFileRequestFailed,
        files: files,
        
        title: title,
        selectedDoc: selectedDoc, 
        selectedFile: selectedFile,
        templateDocAttr: templateDocAttr,

        // обработчики событий
        doc_onMouseOver: doc_onMouseOver,
        doc_onMouseOut: doc_onMouseOut,
        showDocumentElement: showDocumentElement,
        docTemplate_onAttached: docTemplate_onAttached,

        // методы
        //
        activate: activate,
        compositionComplete: compositionComplete,
        cmdCloneDocument: cmdCloneDocument,
        cmdCancelDoc: cmdCancelDoc,
        cmdDeleteDoc: cmdDeleteDoc,
        cmdRotateLeft: cmdRotateLeft,
        cmdRotateRight: cmdRotateRight,
        cmdSaveDoc: cmdSaveDoc,
        cmdProcessDoc: cmdProcessDoc,
        cmdSelectDocument: cmdSelectDocument,
        cmdSelectFile: cmdSelectFile,
        getDocTypeName: getDocTypeName,
        autocompletePartnerPresenter: autocompletePartnerPresenter
    };

    return vm;

    //#region Internal Methods

    function reloadDocument(docUid) {
        var def = $.Deferred();

        $.when(dataservice.getRegistryDocById(docUid))
            .done(function (opresult) {

                if (opresult.code === 0) {

                    ko.utils.arrayForEach(docs(), function (item) {
                        if (item && item.uid() === docUid) {
                            item.docTypeId(opresult.data.DocTypeId);
                            item.packageIndex(opresult.data.PackageIndex)
                        }
                    });
                }
                else {
                    logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                }

                def.resolve(true);
            })
            .fail(function (opresult, opresultDocTypes) {
                logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                def.reject();
            })
            .always(function () {
            });

        return def.promise();
    }

    //
    // Вставляет документ после документа с указанным UId
    //
    function insertDocAfter(documentAnchor, insertedDoc) {

        var insertAt = docs.indexOf(documentAnchor) + 1;
        docs.splice(insertAt, 0, insertedDoc);

        //ko.utils.arrayForEach(registry().docs(), function (item) {
        //    if (item && item.uid() === documentUid) {

        //    }
        //});
    }

    //
    // Удаляет документ из массива
    //
    function removeDocFromList(docUid) {

        var array = docs.remove(function (item) { return item.uid() === docUid; });
        if (array.length == 0) {
            ko.utils.arrayForEach(docs(), function (item) {
                var a2 = item.remove(function (doc) { return doc.uid() === docUid; });
                if (array.length > 0) {
                    return;
                }
            });
        }
    }

    function attachDocument(insertedDoc) {

        var doc = new RegistryDoc(insertedDoc);

        ko.utils.arrayForEach(docs(), function (item) {
            if (item && item.uid() === insertedDoc.ParentDocUId) {
                item.children.push(doc);
            }
        });
        return doc;
    }

    ////
    //// Сохраняет выбранный документ.
    //// @removeChildren - нужно ли удалять прикрепленные документы
    ////
    //function saveDoc(registryDoc, removeChildren) {

    //}

    //
    // Загружает данные на форму при первичной инициализации
    //
    function loadData (registryId) {

        var def = $.Deferred();

        $.when(dataservice.getRegistryById(registryId), dataservice.getDocTypes2())
            .done(function (opresult, opresultDocTypes) {

                if (opresult.code === 0 && opresultDocTypes.code === 0) {

                    // Копирование данных реестра
                    
                    registryUId(opresult.data.Uid);
                    claimId(opresult.data.ClaimId);
                    registryName(opresult.data.RegistryName);
                    claimItemNo(opresult.data.ClaimItemNo);
                    dateReady(opresult.data.DateReady);

                    // Копирование списка документов
                    docs(ko.utils.arrayMap(opresult.data.Docs, function (item) {
                        return new RegistryDoc(item);
                    }));

                    // копирование списка типов документов
                    if(docTypes().length == 0) {
                        docTypes.pushAll(opresultDocTypes.data);
                    }
                }
                else {
                    logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                }

                def.resolve(true);
            })
            .fail(function (opresult, opresultDocTypes) {
                logger.log('Произошла ошибка при выполнении запроса: ' + opresult.message, null, 'dataservice', true);
                def.reject();
            })
            .always(function () {
                presenter.unblockUI();
            });

        return def.promise();
    }

    var lastFileRequest = null; // хранит последний асинхронный запрос к серверу на получение файлов. Чтобы можно было отменить старые.

    //
    // Заполняет список файлов по выбранному документу
    //
    function reloadFileList(selectedDocValue) {
        // Очистить заполненные контролы
        files([]);

        if (selectedDocValue == null) {
            return;
        }

        // Задизаблить список файлов на период обновления
        isFileRequestActive(true);

        var selectedDocUid = selectedDocValue.uid();
        var selectedDocIndex = selectedDocValue.packageIndex();
        var docType = selectedDocValue.docTypeId();
        var sourceId = selectedDocValue.sourceId();
        var isScan = selectedDocValue.isScan(); //(sourceId == 1) ? true : selectedDocValue.isScan(); // Если документ из SAP, то загружаем всегда печатные формы из SUEK
        var sourceId = selectedDocValue.sourceId();
        var isDirty = selectedDocValue.dirtyFlag().isDirty();


        isFileRequestFailed(false);

        var $promise = dataservice.getAttachmentFiles(selectedDocUid, docType, sourceId, isScan, selectedDocIndex, isDirty);
        lastFileRequest = $promise; // сохраняем текущий запрос
        // Сначала обработать результаты getAttachedFiles
        $promise
            .done(function (opResult) {
                if (lastFileRequest == $promise) { // выполняем только последний запрос
                    if (opResult.code === 0) {
                        appendFilesToList(opResult.data);
                    }
                    else {
                        presenter.showErrorMessage(opResult.message);
                    }
                }
            })
            .fail(function (opResult) {
                if (lastFileRequest == $promise) { // выполняем только последний запрос
                    logger.log('Произошла ошибка при выполнении запроса: ' + opResult.message, null, 'dataservice', true);
                    isFileRequestFailed(true);
                }
            })
            .always(function () {
                if (lastFileRequest == $promise) { // выполняем только последний запрос
                    isFileRequestActive(false);
                }
            });
    }

    //
    // Копирует списка файлов в observableArray
    //
    function appendFilesToList(fileArray) {
        for (var i = 0; i < fileArray.length; i++) {

            var docFile = new DocFile()
                .uid(fileArray[i].UId)
                .documentUid(fileArray[i].DocumentUid)
                .sprPage(fileArray[i].SprPage)
                .fileTypeId(fileArray[i].FileTypeId)
                .name(fileArray[i].Name)
                .storageId(fileArray[i].StorageId)
                .isSelected(fileArray[i].IsSelected)
                .isSelectable(fileArray[i].IsSelectable)
                .isAttached(fileArray[i].IsAttached)
                .isVisible(fileArray[i].IsVisible)
                .thumbUrl(fileArray[i].ThumbUrl)
                .previewUrl(fileArray[i].PreviewUrl)
                .previewType(fileArray[i].PreviewType);
            docFile.dirtyFlag().reset();

            files.push(docFile);
        }
    }



    //#endregion
});