﻿define(['plugins/dialog', 'services/logger', 'plugins/router', 'services/dataservice'],
function (dialog, logger, router, dataservice) {

    //var AddDocModal = function () {
    //    this.title = ko.observable('Привет');
    //    this.message = ko.observable('Message');
    //};

    //AddDocModal.show = function () {
    //    return dialog.show(new AddDocModal());
    //};

    //AddDocModal.prototype.cmdCancel = function () {
    //    dialog.close(this);
    //};

    //AddDocModal.prototype.cmdSave = function () {

    //};

    //return AddDocModal;


    var
        title = ko.observable('ДОКУМЕНТ'),
        docNumber = ko.observable(),
        docDate = ko.observable(),
        packageIndex = ko.observable(),
        docTypeId = ko.observable(),
        docTypes = ko.observableArray(),

        show = function (docTypesArray) {
            docTypes(docTypesArray);

            return dialog.show(this);//new AddDocModal());
        },

        cmdCancel = function () {
            dialog.close(this);
        },

        cmdSave = function () {
            dialog.close(this, {
                docTypeId: docTypeId(),
                docNumber: docNumber(),
                docDate: docDate(),
                packageIndex: packageIndex()
            });
        };

    return {
        // props
        title: title,
        docNumber: docNumber,
        docDate: docDate,
        packageIndex : packageIndex,
        docTypeId: docTypeId,
        docTypes: docTypes,

        // methods
        show: show,
        cmdCancel: cmdCancel,
        cmdSave: cmdSave
    }

});