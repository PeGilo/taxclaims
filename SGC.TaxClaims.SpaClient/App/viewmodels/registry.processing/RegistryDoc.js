﻿define(['knockout'],
    function (ko) {

        // Объект для встроенного редактирования полей
        function EditableText(ko_observable, confirmDelegate) {

            var self = this;
            self.origin = ko_observable;
            self.text = ko.observable(ko_observable());

            // Workaround: Чтобы пустые DIV'ы отображались, заменяем пустой текст на непустой
            self.text_ne = ko.pureComputed(function () { // Not empty text
                var value = this.text();
                if (typeof value === "undefined" || value === null || value.length == 0)
                {
                    return "         ";
                }
                else {
                    return value;
                }
            }, this);

            self.editing = ko.observable(false);

            self.edit = function () { self.editing(true); }

            function onEditingChange(oldValue) {
                if (oldValue === true) {
                    if (confirmDelegate) {
                        if (confirmDelegate()) {
                            self.origin(self.text());
                        }
                        else {
                            self.text(self.origin());
                        }
                    }
                    else {
                        self.origin(self.text());
                    }
                }
            }

            function onOriginalChange(newValue) {
                self.text(newValue);
            }

            self.editing.subscribe(onEditingChange, null, "beforeChange");
            ko_observable.subscribe(onOriginalChange);
        }

        var RegistryDoc = function (data) {
            var self = this;
            //self.$type = ko.observable(); // ! Должен идти первым, для сериализации // Уже не используется для десериализации
            //self.templateName = ko.observable(); // template используем одну
            self.uid = ko.observable();
            self.parentDocUId = ko.observable();
            self.docTypeId = ko.observable();

            self.displayDocTypeId = ko.pureComputed({
                read: function () {
                    return (self.sourceId() == 0) ? self.docTypeId() : 0;// Для документа из SAP мы не задаем тип документа
                },
                write: function (value) {
                    self.docTypeId(value);
                },
                owner: self
            });

            self.isScan = ko.observable();
            
            //self.typeName = ko.observable();
            //self.docType = ko.observable(); // есть docTypeId
            self.docData = ko.observable();

            self.parentDocNumber = ko.observable();
            self.parentDocDate = ko.observable();

            //self.docNumber = ko.observable();
            //self.docDate = ko.observable();
            //self.packageIndex = ko.observable();
            self.packageIndex = ko.observable();
            self.packageIndexEditor = ko.observable(new EditableText(self.packageIndex,
                                                                     function () {
                                                                         if (self.children().length > 0) {
                                                                             return confirm("При изменении индекса будут удалены приложенные документы");
                                                                         }
                                                                         return true;
                                                                     }));

            self.pageNumbers = ko.observable();
            self.sumTotal = ko.observable();
            self.sumTax = ko.observable();
            self.factoryCode = ko.observable();
            self.factoryName = ko.observable();
            self.partnerName = ko.observable();
            self.partnerINN = ko.observable();
            self.partnerKPP = ko.observable();

            self.children = ko.observableArray([]); // подчиненные документы
            self.sourceId = ko.observable();

            self.creatorUserUId = ko.observable();
            self.creatorUserName = ko.observable();

            self.buyerUid = ko.observable();
            self.buyerName = ko.observable();
            self.buyerInn = ko.observable();
            self.buyerKpp = ko.observable();

            self.buyer = ko.pureComputed({
                read: function () {
                    return this.buyerName();
                },
                write: function (buyer) {
                    if (buyer) {
                        this.buyerUid(buyer.UId);
                        this.buyerName(buyer.Name);
                        this.buyerInn(buyer.Inn);
                        this.buyerKpp(buyer.Kpp);
                    }
                    else {
                        this.buyerUid(null);
                        this.buyerName(null);
                        this.buyerInn(null);
                        this.buyerKpp(null);
                    }
                },
                owner: self
            });

            self.sellerUid = ko.observable();
            self.sellerName = ko.observable();
            self.sellerInn = ko.observable();
            self.sellerKpp = ko.observable();

            self.seller = ko.pureComputed({
                read: function () {
                    return this.sellerName();
                },
                write: function (seller) {
                    if (seller) {
                        this.sellerUid(seller.UId);
                        this.sellerName(seller.Name);
                        this.sellerInn(seller.Inn);
                        this.sellerKpp(seller.Kpp);
                    }
                    else {
                        this.sellerUid(null);
                        this.sellerName(null);
                        this.sellerInn(null);
                        this.sellerKpp(null);
                    }
                },
                owner: self
            });

            self.cloneEnabled = ko.observable(false); 
            self.expanded = ko.observable(false);

            self.displayText = ko.computed(function () {
                var d = self.docData();
                return (d ? d.docNumber : '') + ' / ' + (d ? d.docDate : '');
            });

            self.isNullo = false;

            // подписка на событие смены полей, после которых надо перечитывать файлы из базы
            self.fileSourceComputed = ko.computed(function () {

                return self.docTypeId();
            }),

            //// событие, измненения данных документа пользователем
            //self.packageIndexUpdatedEvent = ko.computed(function () {

            //    return self.packageIndex();
            //}),

            // Контекстно-зависимое свойство docTypeId для вычисления свойства DirtyFlag
            self.docTypeIdDirty = ko.computed(function () {
                return self.sourceId() ? 0 : self.docTypeId();
            }),

            self.dirtyFlag = new ko.DirtyFlag([
                                                self.docTypeIdDirty, // Для источника SAP/Excel IsDirty не должен меняться при изменении docTypeId
                                                self.isScan,
                                                self.packageIndex
                                                ]);

            self.clone = function () {
                var cloneDoc = new RegistryDoc();
                self.copyTo(cloneDoc);
                return cloneDoc;
            };

            self.copyTo = function (regDoc) {
                
                regDoc.uid(self.uid());
                regDoc.parentDocUId(self.parentDocUId());
                regDoc.docTypeId(self.docTypeId());
                regDoc.isScan(self.isScan());
                regDoc.packageIndex(self.packageIndex());
                regDoc.parentDocNumber(self.parentDocNumber());
                regDoc.parentDocDate(self.parentDocDate());
                
                regDoc.pageNumbers(self.pageNumbers());
                regDoc.sumTotal(self.sumTotal());
                regDoc.sumTax(self.sumTax());
                regDoc.factoryCode(self.factoryCode());
                regDoc.factoryName(self.factoryName());
                regDoc.partnerName(self.partnerName());
                regDoc.partnerINN(self.partnerINN());
                regDoc.partnerKPP(self.partnerKPP());
                
                regDoc.sourceId(self.sourceId());
                regDoc.buyerUid(self.buyerUid());
                regDoc.buyerName(self.buyerName());
                regDoc.buyerInn(self.buyerInn());
                regDoc.buyerKpp(self.buyerKpp());

                regDoc.sellerUid(self.sellerUid());
                regDoc.sellerName(self.sellerName());
                regDoc.sellerInn(self.sellerInn());
                regDoc.sellerKpp(self.sellerKpp());

                regDoc.creatorUserUId(self.creatorUserUId());
                regDoc.creatorUserName(self.creatorUserName());

                regDoc.docData({
                    docNumber: self.docData().docNumber,
                    docDate: self.docData().docDate
                });

                regDoc.dirtyFlag().reset();
            };

            // Инициализация
            if (data) {
                self.uid(data.Uid);
                self.parentDocUId(data.ParentDocUId);
                self.docTypeId(data.DocTypeId);
                self.isScan(data.IsScan);
                self.packageIndex(data.PackageIndex);
                self.parentDocNumber(data.ParentDocNumber);
                self.parentDocDate(data.ParentDocDate);

                self.pageNumbers(data.PageNumbers);
                self.sumTotal(data.SumTotal);
                self.sumTax(data.SumTax);
                self.factoryCode(data.FactoryCode);
                self.factoryName(data.FactoryName);
                self.partnerName(data.PartnerName);
                self.partnerINN(data.PartnerINN);
                self.partnerKPP(data.PartnerKPP);

                self.sourceId(data.SourceId);

                self.buyerUid(data.BuyerUId);
                self.buyerName(data.BuyerName);
                self.buyerInn(data.BuyerInn);
                self.buyerKpp(data.BuyerKpp);

                self.sellerUid(data.SellerUId);
                self.sellerName(data.SellerName);
                self.sellerInn(data.SellerInn);
                self.sellerKpp(data.SellerKpp);

                self.creatorUserUId(data.CreatorUserUId);
                self.creatorUserName(data.CreatorUserName);

                self.docData({
                    docNumber: data.DocData.DocNumber,
                    docDate: data.DocData.DocDate
                });

                self.children(ko.utils.arrayMap(data.Children, function (item) {
                    var doc = new RegistryDoc(item);
                    return doc;
                }));

                self.dirtyFlag().reset();
            }

            return self;
        };

        RegistryDoc.Nullo = new RegistryDoc().uid(''); //00000000-0000-0000-0000-000000000000
        RegistryDoc.Nullo.isNullo = true;

        return RegistryDoc;
    });
