﻿define(['knockout'],
    function (ko) {
        var OperationResultDto = function (code, message, source, isSystemError, data) {

            var self = this;

            self.code = code;
            self.message = message;
            self.source = source;
            self.isSys = isSystemError;
            self.data = data;

            return self;
        };

        // TODO: какая-то своя нумерация, надо убрать и использовать только ту, что на сервере
        OperationResultDto.SystemError = new OperationResultDto();
        OperationResultDto.SystemError.code = 20001;
        OperationResultDto.SystemError.source = 'browser';
        OperationResultDto.SystemError.isSystemError = true;

        return OperationResultDto;
    });
