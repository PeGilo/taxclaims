﻿define(['durandal/system', 'services/logger', 'models/claim', 'models/registry', 'models/document', 'models/docfile', 'dto/operationResult', 'models/filetype'],
    function (system, logger, Claim, Registry, Document, DocFile, OperationResultDto, FileType) {
        
        //#region Internal Methods
        function createClaim() {
            return new Claim();
        }

        function createRegistry() {
            return new Registry();
        }

        
        function createDocument(initialValues) {
            var doc = new Document();
            return doc;
               //.templateName
               //.typeName(initialValues.typeName)
               //.typeId(initialValues.typeId)
               //.docNumber(initialValues.docNumber)
               //.docDate(initialValues.docDate)
               //.packageIndex(initialValues.packageIndex);
        }

        function createDocFile() {
            var docFile = new DocFile();
            return docFile;
        }

        function createFileType() {
            var fileType = new FileType();
            return fileType;
        }

        function updateClaim(claim) {

            var entityJson = ko.toJSON(claim);
            var def = $.Deferred();

            $.ajax({
                url: '/app/claim',
                method: 'POST',
                dataType: 'json',
                data: { value: entityJson }
            })
                .done(function (response) {
                    def.resolve(response);
                })
                .fail(function (jqXHR, textStatus) {
                    def.reject(jqXHR, jqXHR.statusText);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown){});

            return def.promise();
        }

        function importFromSap(claimUid) {

                var def = $.Deferred();
                $.ajax(
                {
                    url: '/app/ImportFromSap',
                    method: 'POST',
                    dataType: 'json',
                    data: { claimUid: claimUid }
                })
                .done(function (response) {
                    def.resolve(response);
                })
                .fail(function (jqXHR, textStatus) {
                    def.reject(jqXHR, jqXHR.statusText);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }


        function getClaimDetails(claimUid) {
            var def = $.Deferred();

            $.ajax({
                url: '/app/GetClaimDetails',
                method: 'GET',
                dataType: 'json',
                data: { claimUid: claimUid }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function getClaimById(claimUid) {
            var def = $.Deferred();

            $.ajax({
                url: '/app/GetClaimById',
                method: 'GET',
                dataType: 'json',
                data: { claimUid: claimUid, dt: (new Date()).getTime() }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        //
        // Вставляет в реестр копию документа и возвращает новый документ
        //
        function cloneRegistryDoc(documentUid) {

            var def = $.Deferred();

            $.ajax({
                url: '/app/CloneRegistryDoc',
                method: 'POST',
                dataType: 'json',
                data: { documentUid: documentUid }
            })
                .done(function (response) { // Возвращается RegistryDoc

                    var doc = createDocument();

                    // В dataservice не должно быть преобразования во ViewModel, т.к. View-моделей много, а dataservice один.
                    if(response.Code === 0) {
                        doc
                        .uid(response.Data.Uid)
                        .docTypeId(response.Data.DocTypeId)
                        .isScan(response.Data.IsScan)
                        .packageIndex(response.Data.PackageIndex)
                        .docData({
                            docNumber: response.Data.DocData.DocNumber,
                            docDate: response.Data.DocData.DocDate
                        });
                        doc.dirtyFlag().reset();
                    }

                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, doc);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function getRegistryDocById(documentUid) {

            var def = $.Deferred();

            $.ajax({
                url: '/app/GetRegistryDocById',
                method: 'GET',
                dataType: 'json',
                data: { documentUid: documentUid }
            })
                .done(function (response) { // Возвращается RegistryDoc
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function getRegistryById(registryUid) {

            var def = $.Deferred();

            $.ajax({
                url: '/app/getregistrybyid',
                method: 'GET',
                dataType: 'json',
                data: { registryUid: registryUid }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function updateRegistry(registry) {

            var json = ko.toJSON(registry);

            var def = $.Deferred();

            $.ajax({
                url: '/app/updateregistry',
                method: 'POST',
                dataType: 'json',
                data: { jobject: json }
            })
                .done(function (response) {
                    def.resolve(response);
                })
                .fail(function (jqXHR, textStatus) {
                    def.reject(jqXHR, jqXHR.statusText);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        // TODO: Переделать: результат должен наверное возвращаться через callback как в остальных функциях (?)
        function getOrganizations(options, callbacks) {

            var results = options && options.results;
            var succeed = (callbacks || {}).succeed;

            $.ajax({
                url: '/app/organizations',
                method: 'GET',
                dataType: 'json',
                data: { }
            })
                .done(function (response) {
                    if (response.Code === 0) {
                        results(response.Data);
                        if (succeed) { succeed(); }
                    }
                    else {
                        logger.log(response.Message, null, response.Source, true);
                    }
                })
                .fail(function (jqXHR, textStatus) {
                    logger.log('Ошибка при выполнении запроса: ' + jqXHR.statusText, null, 'dataservice', true);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

        }

        // Новая версия функции getDocTypes
        function getDocTypes2() {

            var def = $.Deferred();

            $.ajax({
                url: '/app/doctypes',
                method: 'GET',
                dataType: 'json',
                data: { }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        // Вместо этой функции надо использовать getDocTypes2
        function getDocTypes(options) {

            var results = options && options.results;

            $.ajax({
                url: '/app/doctypes',
                method: 'GET',
                dataType: 'json',
                data: {}
            })
                .done(function (response) {
                    if (response.Code === 0) {
                        results.pushAll(response.Data);
                    }
                    else {
                        logger.log(response.Message, null, response.Source, true);
                    }
                })
                .fail(function (jqXHR, textStatus) {
                    logger.log('Ошибка при выполнении запроса: ' + jqXHR.statusText, null, 'dataservice', true);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

        }

        function getAttachmentFiles(docUid, docType, sourceId, isScan, packageIndex, isDirty) {

            var def = $.Deferred();

            logger.log('getAttachmentFiles: ' + 'docUid: ' + docUid + ' docType: ' + docType + ' sourceId: ' + sourceId + ' isScan: ' + isScan + ' packageIndex: ' + packageIndex + ' isDirty: ' + isDirty, null, 'dataservice', false);

            $.ajax({
                url: '/app/GetAttachmentFiles',
                method: 'GET',
                dataType: 'json',
                data: { docUid: docUid, docType: docType, sourceId: sourceId, isScan: isScan, packageIndex: packageIndex, isDirty: isDirty }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function processDoc(registryUid, registryDoc, files) {

            var jsonFiles = ko.toJSON(files);
            var jsonRegistryDoc = ko.toJSON(registryDoc);
            //var jsonAttr = ko.toJSON(attributes);

            var def = $.Deferred();

            $.ajax({
                url: '/App/ProcessDoc',
                method: 'POST',
                dataType: 'json',
                data: { registryUid: registryUid, jsonRegistryDoc: jsonRegistryDoc, jsonFiles: jsonFiles }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function saveDoc(registryUid, registryDoc, removeChildren) {

            var jsonRegistryDoc = ko.toJSON(registryDoc);

            var def = $.Deferred();

            $.ajax({
                url: '/App/SaveDoc',
                method: 'POST',
                dataType: 'json',
                data: { registryUid: registryUid, jsonRegistryDoc: jsonRegistryDoc, removeChildren: removeChildren }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }


        function deleteDoc(docUId) {

            var def = $.Deferred();

            $.ajax({
                url: '/App/DeleteDoc',
                method: 'POST',
                dataType: 'json',
                data: { docUId: docUId }
            })
                .done(function (response) {
                    var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                    def.resolve(opResult);
                })
                .fail(function (jqXHR, textStatus) {
                    var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                    def.reject(opResult);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function exportClaim(claimUid) {

            var def = $.Deferred();

            $.ajax({
                url: '/App/ExportClaim',
                method: 'POST',
                dataType: 'json',
                data: { claimUid: claimUid }
            })
            .done(function (response) {
                var opResult = new OperationResultDto(response.Code, response.Message, response.Source, response.IsSystemError, response.Data);
                def.resolve(opResult);
            })
            .fail(function (jqXHR, textStatus) {
                var opResult = new OperationResultDto(20001, jqXHR.statusText, 'browser', true, null);
                def.reject(opResult);
            })
            .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }


        function ajaxMethod(controller,methodName,methodType,sendData)
        {
            var def = $.Deferred();
            $.ajax({
                url: '/' + controller + '/'+ methodName,
                method: methodType,
                dataType: 'json',
                data: sendData
            })
                .done(function (response) {
                    def.resolve(response);
                })
                .fail(function (jqXHR, textStatus) {
                    def.reject(jqXHR, jqXHR.statusText);
                })
                .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) { });

            return def.promise();
        }

        function execAjax(controller,methodName,methodType, sendData, callback) {
            var def = $.Deferred();
            $.when(ajaxMethod(controller,methodName,methodType,sendData))
                .done(function (opresult) {
                    if (opresult.Code != 0) {
                        logger.log('Произошла ошибка : ' + opresult.Message, null, 'dataservice', false);
                    }
                    def.resolve(true);
                    //callback(opresult.Data || null, { code: opresult.Code, message: opresult.Message, source: opresult.Source, isSys: opresult.IsSystem });
                    callback(opresult.Data || null, new OperationResultDto(opresult.Code, opresult.Message, opresult.Source, opresult.IsSystemError, opresult.Data));
                })
                .fail(function (opresult, opresultDocTypes) {
                    logger.log('Произошла ошибка : ' + opresult.Message, null, 'dataservice', false);
                    def.reject();
                    //callback(opresult.Data || null, { code: opresult.Code, message: opresult.Message, source: opresult.Source, isSys: opresult.IsSystem});
                    callback(opresult.Data || null, new OperationResultDto(opresult.Code, opresult.Message, opresult.Source, opresult.IsSystemError, opresult.Data));
                });
            return def.promise();
        }

        //Получаем требование по идентификатору
        function getClaimByIdAdv(claimUid, callback) {
            var values = { claimUid: claimUid, dt: (new Date()).getTime() };
            return execAjax('App', 'GetClaimById', 'GET', values, callback);
        }

        //Получаем детали требования
        function getClaimDetailsAdv(claimUid,callback)
        {
            var values = { claimUid: claimUid, dt: (new Date()).getTime() }
            return execAjax('App', 'GetClaimDetails', 'GET', values, callback);
        }

        //Импортируем из Сап
        function importFromSapAdv(claimUid, callback) {
            var values = { claimUid: claimUid, dt: (new Date()).getTime() }
            return execAjax('App', 'ImportFromSap', 'POST', values, callback);
        }
        //Экспортируем Выгрузку
        function exportClaimAdv(claimUid, callback) {
            var values = { claimUid: claimUid, dt: (new Date()).getTime() }
            return execAjax('App', 'ExportClaim', 'POST', values, callback);
        }

        return {
            createClaim: createClaim,
            updateClaim: updateClaim,
            exportClaim: exportClaim,
            getRegistryById: getRegistryById,
            getRegistryDocById: getRegistryDocById,
            deleteDoc: deleteDoc,
            createRegistry: createRegistry,
            updateRegistry: updateRegistry,
            cloneRegistryDoc: cloneRegistryDoc,
            createDocument: createDocument,
            getAttachmentFiles: getAttachmentFiles,
            //getSaperionFiles: getSaperionFiles,
            getDocTypes: getDocTypes,
            getDocTypes2: getDocTypes2,
            getOrganizations: getOrganizations,
            processDoc: processDoc,
            saveDoc: saveDoc,
            getClaimById: getClaimById,
            getClaimByIdAdv: getClaimByIdAdv,
            createDocFile: createDocFile,
            createFileType: createFileType,
            getClaimDetails: getClaimDetails,
            getClaimDetailsAdv: getClaimDetailsAdv,
            importFromSap: importFromSap,
            importFromSapAdv: importFromSapAdv,
            exportClaimAdv: exportClaimAdv
        };
    });