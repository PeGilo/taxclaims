define(['durandal/system'],
    function (system) {
        var presenter = {
            blockUI: blockUI,
            unblockUI: unblockUI,
            showConfirmMessage: showConfirmMessage,
            showErrorMessage: showErrorMessage,
            showMessage: showMessage,
            showValidationMessage: showValidationMessage
        };

        return presenter;

        function blockUI(message) {

            $busyScreen = $('#busy-screen');
            $("#busy-screen-message", $busyScreen).html((message) ? message : "Загрузка...");
            $.blockUI({ message: $busyScreen, overlayCSS: { backgroundColor: '#fff', opacity: 1.0 } });
        }

        function unblockUI() {
            $.unblockUI();
        }

        function showConfirmMessage(message) {
            toastr.success(message);
        }

        function showMessage(message) {
            toastr.info(message);
        }

        function showErrorMessage(message) {
            
            var outMessage;
            if (message && message.message !== undefined) { //Пришел OperationResult
                if (message.isSys)
                    outMessage = 'Произошла системная ошибка';
                else
                    outMessage = message.message;
            }
            else {
                outMessage = message;
            }
            toastr.error(outMessage);
        }

        function showValidationMessage(message) {

            toastr.error(message.replace('\r\n', '<br/>'));
        }
    });