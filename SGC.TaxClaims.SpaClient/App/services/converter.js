﻿define(['knockout', 'models/claim', 'models/registry','models/docfile'],
    function (ko, Claim,Registry,DocFile) {
        //Преобразует требование из json в Objservable
        function toClaim(claimJs)
        {
            //Добавляем требование//
            var claim = new Claim()
                            .uid(claimJs.Uid)
                            .inspectionName(claimJs.InspectionName)
                            .claimNumber(claimJs.ClaimNumber)
                            .organizationName(claimJs.OrganizationName)
                            .comment(claimJs.Comment)
                            .dateReady(claimJs.DateReady)
                            .dateCreated(claimJs.DateCreated);
            //--------------------------//
            //Добавляем файлы требования//
            //-------------------------//
            if (claimJs.Files.length > 0) {
                claim.files(ko.utils.arrayMap(claimJs.Files, function (item) {
                    return new DocFile()
                        .uid(item.Uid)
                        .name(item.Name)
                        .fileTypeName(item.FileTypeName)
                        .downloadUrl(item.DownloadUrl);
                }));
            }
            //----------------//
            //Заполняем реестр//
            //----------------//
            if (claimJs.Registries.length > 0) {
                claim.registries(ko.utils.arrayMap(claimJs.Registries, function (item) {
                    return new Registry()
                        .uid(item.Uid)
                        .claimId(item.ClaimId)
                        .claimItemNo(item.ClaimItemNo)
                        .status(item.Status)
                        .number(item.Number)
                        .registryName(item.RegistryName)
                        .dateReady(item.DateReady)
                        .dateCreated(item.DateCreated);
                }));
            }
            return claim;
        }

        function toRegistry(registryJs) {
            var registry = new Registry()
            .uid(registryJs.Uid)
            .claimId(registryJs.ClaimId)
            .registryName(registryJs.RegistryName)
            .claimItemNo(registryJs.ClaimItemNo)
            .dateReady(registryJs.DateReady)
            .dateCreated(registryJs.DateCreated)
            .status(registryJs.Status)
            .number(registryJs.Number);
            return registry;
        }

        var converter = {
            toClaim: toClaim,
            toRegistry: toRegistry
        };
        return converter;
});