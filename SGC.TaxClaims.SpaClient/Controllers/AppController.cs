﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGC.TaxClaims.Common.Filtering;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Core.Saperion;
using System.IO;
using System.Web;
using SGC.TaxClaims.Infrastructure.Services;
using SGC.TaxClaims.Core.ImportXls;
using System.Xml.Linq;

namespace SGC.TaxClaims.SpaClient.Controllers
{
    [OutputCache(Duration = 0)]
    [Authorize(Roles = "Сотрудник БиНУ, Сотрудник ОOУ, Сотрудник ОБиНО")]
    public class AppController : BaseController
    {
        private IDBRepository _documentRepository;
        private ISaperion _sprRepository;
        private IExcelImport _excelImport;
        private IIFNSExporter _ifnsExporter;
        private ISap _sap;

        public AppController(
            IDBRepository documentRepository,
            ISaperion sprRepository,
            IExcelImport excelImport,
            IIFNSExporter ifnsExporter,
            ISap sap)
        {
            _documentRepository = documentRepository;
            _sprRepository = sprRepository;
            _excelImport = excelImport;
            _ifnsExporter = ifnsExporter;
            _sap = sap;
        }

        [HttpPost]
        public ActionResult Claim(string value)
        {
            ViewModels.Claim claimVM = Serializer.DeserializeJson<ViewModels.Claim>(value);
            Claim claim = new Claim();
            claim.UId = claimVM.Uid == Guid.Empty ? Guid.NewGuid() : claimVM.Uid;
            claim.InspectionName = claimVM.InspectionName;
            claim.Number = claimVM.ClaimNumber;
            claim.OrgId = claimVM.OrganizationId;
            claim.Comment = claimVM.Comment;
            claim.DateReady = claimVM.DateReady;
            claim.DocStateId = 1;
            claim.DocTypeId = 1;
            //claim.DocDate = DateTime.Now;
            if (claimVM.Uid == Guid.Empty)
            {
                claim.DateCreated = DateTime.Now;
            }
            else
            {
                claim.DateChanged = DateTime.Now;
                claim.DateCreated = claimVM.DateCreated;
            }
            OperationResult<aspnet_Users> currentUser = _documentRepository.GetUserByName(User.Identity.Name);
            claim.CreatorUserUid = currentUser.Data.UserId;
            // TODO: [Петр] Check OperationResult
            // ... Получение состояния документооборота, в который надо выставить документ при создании
            //OperationResult<DocState> initialState = _documentRepository.GetInitialState(claim.DocTypeId, (int)DocumentRoute.ToIFNS);
            //claim.DocStateId = initialState.Data.Id;
            claim.DocStateId = 1;
            // ... Получение состояния документоборота, в которое надо перевести документ после создания
            // TODO: [Петр] Check OperationResult
            //OperationResult<DocState[]> nextStates = _documentRepository.GetNextPossibleStates(initialState.Data);
            // Сохранение документа
            claim.ChangeStatus = claimVM.Uid == Guid.Empty ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
            //Добавляем файлы 
            if (claimVM.Files != null)
            {
                List<DocFile> docFiles = new List<DocFile>();
                foreach (var file in claimVM.Files.Where(s => s.IsAdded || s.IsDeleted || s.IsChanged))
                {
                    DocFile docFile = new DocFile();
                    docFile.UId = file.UId.Value;
                    docFile.Document = claim;
                    docFile.Name = file.Name;
                    docFile.DocumentUid = claim.UId;
                    docFile.FileTypeId = file.FileTypeId;
                    //docFile.ChangeStatus = file.IsAdded ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Deleted;
                    if (file.IsAdded)
                    {
                        string fullPath = Path.Combine(Path.GetTempPath(), docFile.UId.ToString());
                        docFile.Content = System.IO.File.ReadAllBytes(fullPath);
                        docFile.ChangeStatus = System.Data.Entity.EntityState.Added;
                    }
                    else if (file.IsChanged)
                    {
                        var getFileResult = _documentRepository.GetFile(docFile.UId);
                        if (!getFileResult.IsSucceed) return Json(getFileResult.Result);
                        docFile.ChangeStatus = System.Data.Entity.EntityState.Modified;
                        docFile.Content = getFileResult.Data.Content;
                        docFile.NextFileUid = getFileResult.Data.NextFileUid;
                    }
                    else if (file.IsDeleted)
                        docFile.ChangeStatus = System.Data.Entity.EntityState.Deleted;
                    docFiles.Add(docFile);
                }
                claim.DocFile = docFiles;
            }
            // TODO: [Петр] Check ValidationInfo
            // TODO: [Петр] Добавь передачу идентификатора пользователя
            OperationResult<ValidationInfo[]> result = _documentRepository.SaveAndMove(claim, null, claim.DocStateId, Guid.Empty); // Передвигаем требование в следующее состояние
            return Json(result.Result);
        }

        #region "Grid data getters"

        /// <summary>
        /// Метод, к которому обращается Grid для загрузки данных
        /// </summary>
        /// <param name="gridSettings"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetClaimsPage(MvcJqGrid.GridSettings gridSettings)
        {
            int pageSize = gridSettings.PageSize;
            int pageIndex = gridSettings.PageIndex - 1;
            string sortColumn = gridSettings.SortColumn;
            bool? asc = gridSettings.SortOrder == "asc";
            int totalRecords;

            FilterGroup filter = JqGridHelper.ParseFilter(gridSettings.Where);

            // TODO: [Петр] Check OperationResults
            OperationResult<PageResponse<Claim>> claims = _documentRepository.GetClaimsPage(pageSize, pageIndex, sortColumn, asc, filter, out totalRecords);

            return Json(
                new
                {
                    total = claims.Data.PagesCount,
                    page = claims.Data.PageIndex + 1,
                    records = claims.Data.RecordsCount,
                    rows = claims.Data.Rows
                        .OrderByDescending(claim => claim.DateReady)
                        .Select(claim => new ViewModels.Claim()
                        {
                            Uid = claim.UId,
                            ClaimNumber = claim.Number,
                            Comment = claim.Comment,
                            InspectionName = claim.InspectionName,
                            Status = claim.DocState.DisplayName,
                            TargetOrganization = claim.Organization == null ? string.Empty : claim.Organization.Name,
                            DateReady = claim.DateReady
                        })
                }
                , JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Метод, к которому обращается Grid для загрузки данных
        /// </summary>
        /// <param name="gridSettings"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetRegistryPage(MvcJqGrid.GridSettings gridSettings, Guid claimUid)
        {
            int pageSize = gridSettings.PageSize;
            int pageIndex = gridSettings.PageIndex - 1;
            string sortColumn = gridSettings.SortColumn;
            bool? asc = gridSettings.SortOrder == "asc";
            int totalRecords;

            FilterGroup filter = JqGridHelper.ParseFilter(gridSettings.Where);

            var operationResult = _documentRepository.GetRegistriesByClaimUid(claimUid);
            //TODO: [Петр] тут наверно что-то нужно вывести на экран в случае неудачи

            //OperationResult<PageResponse<Registry>> registries = _documentRepository.GetRegistriesPage(pageSize, pageIndex, sortColumn, asc, filter, out totalRecords);

            return Json(
                new
                {
                    total = 0, // registries.Data.PagesCount,
                    page = 1, // registries.Data.PageIndex + 1,
                    records = 0, // registries.Data.RecordsCount,
                    rows = operationResult.Data.Select(s =>
                            new ViewModels.Registry()
                            {
                                Uid = s.UId,
                                ClaimItemNo = s.ClaimItemNo,
                                DateReady = s.DateReady,
                                RegistryName = s.Name,
                                Status = s.DocState.DisplayName
                            }).ToList()
                }
                , JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpGet]
        public ActionResult GetClaimDetails(System.Guid claimUid)
        {
            OperationResult<Claim> claimResult = _documentRepository.GetClaimById(claimUid);
            if (claimResult.Code != 0)
            {
                return Json(claimResult.Result, JsonRequestBehavior.AllowGet);
            }

            var claim = claimResult.Data;

            // Чтение реестров
            //
            OperationResult<Registry[]> registriesResult = _documentRepository.GetRegistriesByClaimUid(claimUid);
            if (registriesResult.Code != 0)
            {
                return Json(registriesResult.Result, JsonRequestBehavior.AllowGet);
            }

            var registries = registriesResult.Data;

            List<ViewModels.RegistryDetails> registriesDetails = new List<ViewModels.RegistryDetails>();

            foreach (var registry in registries)
            {
                registriesDetails.Add(new ViewModels.RegistryDetails(registry));
            }

            ViewModels.ClaimDetails claimDetails = new ViewModels.ClaimDetails()
            {
                Uid = claim.UId,
                ClaimNumber = claim.Number,
                Comment = claim.Comment,
                DateReady = claim.DateReady,
                DateCreated = claim.DateCreated,
                InspectionName = claim.InspectionName,
                OrganizationName = claim.Organization != null ? claim.Organization.Name : string.Empty,
                Registries = registriesDetails/*,
                State = claim.DocState.DisplayName*/
            };
            var filesResult = _documentRepository.GetDocumentFilesDiscritionOnly(claimUid);
            if (filesResult.Code != 0)
            {
                return Json(filesResult.Result, JsonRequestBehavior.AllowGet);
            }

            claimDetails.Files = filesResult.Data
                .Where(file => file.DocumentUid == claimUid)
                .Select(file => new ViewModels.DocFile()
                {
                    UId = file.UId,
                    Name = file.Name,
                    FileTypeName = file.FileType.Name,
                    DownloadUrl = "/app/downloaduid?uid=" + file.UId
                })
                .ToList();

            return Json(new OperationResult<ViewModels.ClaimDetails>(OperationResult.Succeed, claimDetails), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetClaimById(Guid claimUid)
        {
            OperationResult<Claim> registriesResult = _documentRepository.GetClaimById(claimUid);
            var claim = registriesResult.Data;
            if (registriesResult.Code != 0) return Json(registriesResult.Result, JsonRequestBehavior.AllowGet);
            ViewModels.Claim claimVM = new ViewModels.Claim();
            claimVM.Uid = claim.UId;
            claimVM.ClaimNumber = claim.Number;
            claimVM.DateReady = claim.DateReady;
            claimVM.DateCreated = claim.DateCreated;
            claimVM.Comment = claim.Comment;
            claimVM.InspectionName = claim.InspectionName;
            //claimVM.OrganizationId = new ViewModels.Org() { Id = claim.Organization.Id, Name = claim.Organization.Name };
            claimVM.OrganizationId = claim.OrgId;
            var getDocFileResult = _documentRepository.GetDocumentFilesDiscritionOnly(claim.UId);
            claimVM.Files = getDocFileResult.Data.Select(file => new ViewModels.DocFile() { UId = file.UId, Name = file.Name, DocumentUid = file.DocumentUid, FileTypeId = file.FileTypeId }).ToArray();
            return Json(new OperationResult<ViewModels.Claim>(OperationResult.Succeed, claimVM), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetRegistryDocById(System.Guid documentUid)
        {
            // 1. Получить реестр
            // 
            OperationResult<RegistryDoc> docResult = _documentRepository.GetRegistryDocById(documentUid);

            if (docResult.Code != 0)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении документа (Идентификатор документа = {0}).", documentUid), docResult.Result);
                return Json(docResult.Result);
            }

            ViewModels.RegistryDoc vm = ViewModels.RegistryDocFactory.CreateDoc(docResult.Data.DocTypeId, docResult.Data);

            return Json(new OperationResult<ViewModels.RegistryDoc>(OperationResult.Succeed, vm), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetRegistryById(System.Guid registryUid)
        {
            // 1. Получить реестр
            // 
            OperationResult<Registry> regResult = _documentRepository.GetRegistryByUid(registryUid);

            if (regResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении реестра (Идентификатор реестра = {0}).", registryUid), regResult.Result);
                return Json(regResult.Result, JsonRequestBehavior.AllowGet);
            }
            else if (regResult.Data == null)
            {
                return Json(regResult.Result, JsonRequestBehavior.AllowGet);
            }

            // 2. Получить документы, входящие в реестр
            OperationResult<RegistryDoc[]> docsResult = _documentRepository.GetRegistryDocs(registryUid);

            if (docsResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении дочерних документов реестра (Идентификатор реестра = {0}).", registryUid), docsResult.Result);
                return Json(docsResult.Result, JsonRequestBehavior.AllowGet);
            }

            ViewModels.Registry vm = new ViewModels.Registry(regResult.Data, docsResult.Data); //, GetDocTypes());
            return Json(new OperationResult<ViewModels.Registry>(OperationResult.Succeed, vm), JsonRequestBehavior.AllowGet);
        }

        /*[HttpPost]
        public ActionResult GetRegistriesByClaimUid(System.Guid claimUid)
        {
            var result = _documentRepository.GetRegistriesByClaimUid(claimUid);
            if (!result.IsSucceed) 
                return Json(new OperationResult<ViewModels.Registry[]>(result.Result, new ViewModels.Registry[] { }));
            List<ViewModels.Registry> registries = new List<ViewModels.Registry>();
            foreach (var registry in result.Data)
                registries.Add(new ViewModels.Registry(registry,new RegistryDoc[]{}));
            return Json(new OperationResult<ViewModels.Registry[]>(result.Result, registries.ToArray()));
        }*/

        [HttpPost]
        public ActionResult UpdateRegistry(string jobject)
        {
            ViewModels.Registry vm = Serializer.DeserializeJson<ViewModels.Registry>(jobject);

            // TODO: [Петр] валидация

            //if (!ModelState.IsValid)
            //{
            //    return View(viewModel);
            //}

            //return View(viewModel);


            // Редактирование
            if (vm.Uid != Guid.Empty)
            {
                // 1. Получить реестр
                // 
                OperationResult<Registry> regResult = _documentRepository.GetRegistryByUid(vm.Uid);

                if (regResult.IsSystemError)
                {
                    AppLogger.Error(String.Format("Возникла ошибка при получении реестра (Идентификатор реестра = {0}).", vm.Uid), regResult.Result);
                    return Json(regResult.Result);
                }
                else if (regResult.Data == null)
                {
                    return Json(regResult.Result);
                }

                regResult.Data.Name = vm.RegistryName;
                regResult.Data.ClaimItemNo = vm.ClaimItemNo;
                regResult.Data.DateReady = vm.DateReady;
                regResult.Data.DateChanged = DateTime.UtcNow;
                regResult.Data.ChangeStatus = System.Data.Entity.EntityState.Modified;

                OperationResult or = _documentRepository.SaveDocument(regResult.Data, regResult.Data.ParentDocUId);
                return Json(or);
            }

            // Создание
            Registry registry = Registry.Create();
            vm.UpdateModel(registry);

            // Получение текущего пользователя
            OperationResult<aspnet_Users> currentUser = _documentRepository.GetUserByName(User.Identity.Name);
            if (currentUser.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении пользователя (UserName = {0}).", User.Identity.Name), currentUser.Result);
                return Json(currentUser.Result);
            }
            else if (currentUser.Data == null)
            {
                return Json(currentUser.Result);
            }

            Infrastructure.Services.DocumentService docService = new Infrastructure.Services.DocumentService(_documentRepository, _sprRepository, _ifnsExporter);

            // 1. Получить требование
            // 
            OperationResult<Claim> claimResult = _documentRepository.GetClaimById(vm.ClaimId);

            if (claimResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при создании реестра (Идентификатор требования = {0}).", vm.ClaimId), claimResult.Result);
                return Json(claimResult.Result);

            }
            else if (claimResult.Data == null)
            {
                return Json(claimResult.Result);
            }

            // 2. Проверить, что требование находится в состоянии, в котором еще возможно создание реестра
            // 
            OperationResult grantResult = docService.CanCreateDocumentInCurrentState(claimResult.Data, registry.DocTypeId);

            if (grantResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при создании реестра (Идентификатор требования = {0}).", vm.ClaimId), grantResult);
                return Json(grantResult);
            }
            else if (!grantResult.IsSucceed)
            {
                //ModelState.AddRuleViolation(new RuleViolation(grantResult.Message, ""));
                return Json(grantResult);
            }
            //registry.Document1
            //var registryDocs _documentRepository.GetRegistryDocs(registry.UId);
            // 3. Инициализация вложенных документов
            foreach (var childDoc in registry.Document1)
            {
                //var childDoc = relation.DocumentChild;

                //OperationResult<DocState> initialState = _documentRepository.GetInitialState(childDoc.DocTypeId, (int)DocumentRoute.ToIFNS);
                //childDoc.DocStateId = initialState.Data.Id;
                childDoc.DocStateId = claimResult.Data.DocStateId; // Состояние (DocState) всех дочерних документов должно быть таким же как у корневого документа (требования)
                childDoc.CreatorUserUid = currentUser.Data.UserId;
            }

            // 4. Создание реестра
            //
            OperationResult createResult = docService.CreateRegistry(currentUser.Data.UserId, claimResult.Data, registry, new int[] { 3, 4, 5, 11 });

            if (createResult.IsSucceed)
            {

                //return Json(createResult);
            }
            else if (createResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при создании реестра (Идентификатор требования = {0}).", vm.ClaimId), createResult);
                //return Json(createResult);
            }
            else
            {
                //ModelState.AddRuleViolation(new RuleViolation(createResult.Message, ""));
                //return Json(createResult);
            }

            return Json(createResult);
        }

        /// <summary>
        /// Получение списка файлов для документа при заполнении реестра
        /// </summary>
        /// <param name="docUid">Идентификатор документа</param>
        /// <param name="docType">Тип документа</param>
        /// <param name="sourceId">Из SAP или не из SAP</param>
        /// <param name="isScan">Бумажный или электронный</param>
        /// <param name="packageIndex">Индекс пакета саперион</param>
        /// <param name="isDirty">Изменены ли тип документа, индекс пакета или признак "эл/бум"</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAttachmentFiles(System.Guid? docUid, int? docType, int sourceId, bool? isScan, string packageIndex, bool isDirty)
        {
            // По-умолчанию принимаем, что загружаем бумажные документы
            if (!isScan.HasValue)
            {
                isScan = true;
            }

            // Для электронных документов обязательно должен быть указан тип документа
            if (isScan.Value == false && !docType.HasValue)
            {
                return Json(
                    new OperationResult(OperationResult.InvalidArgument.Code, "Для электронного документа должен быть указан его тип", OperationResult.BusinessLogic, false), JsonRequestBehavior.AllowGet);
            }

            DocFile[] docFiles = new DocFile[0];
            SPRFile[] sprFiles = new SPRFile[0];

            // 1. Загрузить файлы из базы. Имеет смысл, только если данные документа не были изменены.
            if (docUid.HasValue && !isDirty &&
                (sourceId != 1)) // Если isSAP=1 / ЭЛ=1, то никакие файлы не могут быть прикреплены, поэтому не читаем из базы
            {
                OperationResult<DocFile[]> filesResult = _documentRepository.GetDocumentFiles(docUid.Value);

                if (filesResult.Code != 0)
                {
                    return Json(filesResult.Result, JsonRequestBehavior.AllowGet);
                }

                docFiles = filesResult.Data;

                //// Если документ электронный, то возвращаем сразу файлы, не лезем в Саперион.
                //if (isScan.Value == false)
                //{
                //    return Json(new OperationResult<ViewModels.DocFile[]>(OperationResult.Succeed, 
                //        LoadAndMergeFiles(docFiles, sprFiles)
                //        ), JsonRequestBehavior.AllowGet);
                //}
            }

            // 2. Загрузить файлы из сапериона, в зависимости от того, бумажный документ или электронный. Если документ электронный, то тип документа должен быть указан.

            if (!String.IsNullOrEmpty(packageIndex) &&
                  (
                    (sourceId == 1) // Если из SAP, то показываем бумажные документы
                    || (sourceId == 0)
                  )
                ) // Если из SAP, то загружаем бумажную версию
            //&& (isScan.Value == true || (docType.HasValue && docType.Value != 0)))
            {
                OperationResult<SPRFile[]> filesResult;

                if (sourceId == 1)
                {
                    filesResult = _sprRepository.GetFiles(packageIndex);
                }
                else if (isScan.Value || (isScan.Value == false && docType.HasValue && docType.Value == 0)) // Для электронных Unspecified тоже показываем бумажную версию
                {
                    filesResult = _sprRepository.GetFiles(packageIndex);
                }
                else
                {
                    filesResult = _sprRepository.GetEDocFiles(packageIndex, ConvertToSprType((DocumentType)docType.Value));
                }

                if (filesResult.Code != 0)
                {
                    return Json(filesResult.Result, JsonRequestBehavior.AllowGet);
                }

                sprFiles = filesResult.Data;
            }

            // 3. Составить один список

            ViewModels.DocFile[] responseFiles = LoadAndMergeFiles(docFiles, sprFiles).OrderBy(vm => vm.SprPage).ThenBy(vm => vm.Name).ToArray();
            return Json(new OperationResult<ViewModels.DocFile[]>(OperationResult.Succeed, responseFiles), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Сохранение атрибутов документа
        /// </summary>
        /// <param name="registryUid"></param>
        /// <param name="jsonRegistryDoc"></param>
        /// <param name="jsonFiles"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveDoc(System.Guid registryUid, string jsonRegistryDoc, bool removeChildren)
        {
            FileManager fm = new FileManager(Server.MapPath("~/content/images/"));
            DocumentService docService = new DocumentService(_documentRepository, _sprRepository, _ifnsExporter);

            // Получение текущего пользователя
            OperationResult<aspnet_Users> currentUser = _documentRepository.GetUserByName(User.Identity.Name);
            if (currentUser.Code != 0)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении пользователя (UserName = {0}).", User.Identity.Name), currentUser.Result);
                return Json(currentUser.Result);
            }

            ViewModels.RegistryDoc vmDoc = Serializer.DeserializeJson<ViewModels.RegistryDoc>(jsonRegistryDoc);

            // Обновление документа
            OperationResult<RegistryDoc> resultDoc = _documentRepository.GetRegistryDocById(vmDoc.Uid);
            if (resultDoc.Code != 0)
            {
                return Json(resultDoc.Result);
            }
            RegistryDoc doc = resultDoc.Data;
            doc.ChangeStatus = System.Data.Entity.EntityState.Modified;
            doc.DocType = null;
            doc.ParentDocUId = registryUid;

            doc.CreatorUserUid = currentUser.Data.UserId;

            // 2. Загружаем данные в модель с клиента
            //
            vmDoc.UpdateModel(doc);

            // 3. Валидация
            IEnumerable<RuleViolation> validationErrors = vmDoc.Validate();
            if (validationErrors.FirstOrDefault() != null)
            {
                return Json(new OperationResult(OperationResult.ValidationError.Code, String.Join("\r\n", validationErrors.Select(e => e.ErrorMessage)), false));
            }

            // Если необходимо, удаляем вложенные документы
            if (removeChildren)
            {
                // Если документ является пакетом, то удалить вложенные документы. Иначе - удалить приложенные файлы и сбросить тип документа.
                if (vmDoc.SourceId != 0)
                {
                    MarkDeleted(doc.Document1);

                    // TODO: Объединить в одну транзакцию со следующией операцией
                    OperationResult saveResults = _documentRepository.SaveDocuments(doc.Document1, doc.UId);
                    if (saveResults.Code != 0)
                    {
                        return Json(saveResults);
                    }
                }
                else
                {
                    OperationResult<DocFile[]> resultDocs = _documentRepository.GetDocumentFilesDiscritionOnly(vmDoc.Uid);
                    if (resultDocs.Code != 0)
                    {
                        return Json(resultDocs.Result);
                    }

                    List<DocFile> files = resultDocs.Data.ToList();
                    files.ForEach(file => { file.ChangeStatus = System.Data.Entity.EntityState.Deleted; });
                    doc.DocTypeId = 0;
                    doc.DocFile = files;
                }

            }

            // 5. Сохранение документа и файлов в базу

            OperationResult saveResult = _documentRepository.SaveDocument(doc, doc.ParentDocUId);
            //OperationResult saveResult = _documentRepository.SaveDocuments(doc);

            return Json(saveResult);
        }

        private void MarkDeleted(ICollection<Document> docs)
        {
            foreach (var doc in docs)
            {
                doc.ChangeStatus = System.Data.Entity.EntityState.Deleted;
                MarkDeleted(doc.Document1);
            }
        }

        [HttpPost]
        public ActionResult DeleteDoc(System.Guid docUId)
        {
            FileManager fm = new FileManager(Server.MapPath("~/content/images/"));
            DocumentService docService = new DocumentService(_documentRepository, _sprRepository, _ifnsExporter);

            OperationResult<RegistryDoc> resultDoc = _documentRepository.GetRegistryDocById(docUId);
            if (resultDoc.Code != 0)
            {
                return Json(resultDoc.Result);
            }

            // Помечаем сам документ на удаление
            RegistryDoc doc = resultDoc.Data;
            doc.ChangeStatus = System.Data.Entity.EntityState.Deleted;
            doc.DocType = null;

            // Удаление связанных сущностей
            /*if (doc.DocRelationChild != null)
            {
                foreach (var rel in doc.DocRelationChild)
                {
                    rel.ChangeStatus = System.Data.Entity.EntityState.Deleted;
                }
            }

            if (doc.DocRelationParent != null)
            {
                foreach (var rel in doc.DocRelationParent)
                {
                    rel.ChangeStatus = System.Data.Entity.EntityState.Deleted;
                }
            }*/

            MarkDeleted(doc.Document1);
            // TODO: Объединить в одну транзакцию со следующией операцией
            OperationResult saveResults = _documentRepository.SaveDocuments(doc.Document1.ToList(), doc.UId);

            OperationResult saveResult = _documentRepository.SaveDocument(doc, doc.ParentDocUId);

            return Json(saveResult);
        }

        /// <summary>
        /// Типизирование документа из формы заполнения реестра
        /// </summary>
        /// <param name="registryUid"></param>
        /// <param name="jsonRegistryDoc">Выбранный документ</param>
        /// <param name="jsonAttr">Введенные атрибуты</param>
        /// <param name="jsonFiles"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ProcessDoc(System.Guid registryUid, string jsonRegistryDoc, string jsonFiles)
        {
            FileManager fm = new FileManager(Server.MapPath("~/content/images/"));
            DocumentService docService = new DocumentService(_documentRepository, _sprRepository, _ifnsExporter);

            // Получение текущего пользователя
            OperationResult<aspnet_Users> currentUser = _documentRepository.GetUserByName(User.Identity.Name);
            if (currentUser.Code != 0)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении пользователя (UserName = {0}).", User.Identity.Name), currentUser.Result);
                return Json(currentUser.Result);
            }

            // Получение данных реестра
            OperationResult<Registry> registryResult = _documentRepository.GetRegistryByUid(registryUid);
            if (registryResult.Code != 0)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении реестра (UID = {0}).", registryUid), registryResult.Result);
                return Json(registryResult.Result);
            }

            ViewModels.RegistryDoc vmDoc = Serializer.DeserializeJson<ViewModels.RegistryDoc>(jsonRegistryDoc);
            //ViewModels.RegistryDoc vmAttr = Serializer.DeserializeJson<ViewModels.RegistryDoc>(jsonAttr);
            ViewModels.DocFile[] files = Serializer.DeserializeJson<ViewModels.DocFile[]>(jsonFiles);

            // Валидация
            IEnumerable<RuleViolation> validationErrors = vmDoc.Validate();
            if (validationErrors.FirstOrDefault() != null)
            {
                return Json(new OperationResult(OperationResult.ValidationError.Code, String.Join("\r\n", validationErrors.Select(e => e.ErrorMessage)), false));
            }

            bool blockAcquired = false; // Используем ли блокировку документа

            try
            {
                // 1. Если документ из SAP или из Excel, то прикрепить к нему новый документ. Иначе, изменяем тот же самый документ.
                //
                RegistryDoc doc;

                if (vmDoc.SourceId != (int)DocSourceType.Unspecified)
                {
                    // Блокируем родительский документ от изменений
                    Guid blockHolderUid;
                    OperationResult<bool> lockResult = _documentRepository.TryLockDocument(vmDoc.Uid, currentUser.Data.UserId, out blockHolderUid);
                    if (lockResult.Code != 0)
                    {
                        return Json(lockResult.Result);
                    }
                    blockAcquired = true;

                    doc = new RegistryDoc();
                    doc.UId = Guid.NewGuid();
                    doc.ChangeStatus = System.Data.Entity.EntityState.Added;
                    doc.ParentDocUId = vmDoc.Uid;
                    doc.DocStateId = registryResult.Data.DocStateId; // Состояние (DocState) всех дочерних документов должно быть таким же как у родительского документа
                    
                    doc.DateCreated = DateTime.UtcNow;
                    //doc.SumTax = regDocRes.Data.SumTax;
                    //doc.SumTotal = regDocRes.Data.SumTotal;
                    //doc.BuyerUId = regDocRes.Data.BuyerUId;
                    //doc.SellerUId = regDocRes.Data.SellerUId;
                }
                else
                {
                    OperationResult<RegistryDoc> resultDoc = _documentRepository.GetRegistryDocById(vmDoc.Uid);
                    if (resultDoc.Code != 0)
                    {
                        return Json(resultDoc.Result);
                    }
                    doc = resultDoc.Data;
                    doc.ChangeStatus = System.Data.Entity.EntityState.Modified;
                    doc.DocType = null;
                    doc.ParentDocUId = registryUid;
                }

                // При создании и изменении документа сохраняем имя пользователя.
                // TODO: Создать отдельное поле EditorUserUID?
                doc.CreatorUserUid = currentUser.Data.UserId;

                // 2. Загружаем данные в модель с клиента
                //
                vmDoc.UpdateModel(doc);

                // 3. Проверить, что у родительского документа нет вложенного документа с таким же типом. Имеет смысл только для документов-пакетов.

                if (vmDoc.SourceId != 0)
                {
                    OperationResult<Document[]> childrenRes = _documentRepository.GetChildDocuments(doc.ParentDocUId.Value);
                    if (childrenRes.Code != 0)
                    {
                        return Json(childrenRes.Result);
                    }
                    if (doc.DocTypeId != (int)DocumentType.Unspecified && childrenRes.Data.Any(d => d.DocTypeId == doc.DocTypeId))
                    {
                        return Json(new OperationResult(OperationResult.ValidationError.Code, "К пакету уже прикреплен документ такого типа.", false));
                    }
                }

                // 4. Прикрепление файлов

                #region "Прикрепление файлов к документу"

                List<DocFile> entities;

                if (vmDoc.SourceId == (int)DocSourceType.SAP)
                {
                    entities = new List<DocFile>();
                }
                else
                {
                    // считать список прикрепленных файлов из базы
                    OperationResult<DocFile[]> resultDocs = _documentRepository.GetDocumentFilesDiscritionOnly(vmDoc.Uid);
                    if (resultDocs.Code != 0)
                    {
                        return Json(resultDocs.Result);
                    }
                    entities = new List<DocFile>(resultDocs.Data);
                }

                // Если документ электронный и из SAP, то считываем файлы из Сапериона. Иначе сохраняем загруженные ранее файлы.
                //
                if (vmDoc.SourceId == (int)DocSourceType.SAP && !vmDoc.IsScan)
                {
                    OperationResult<DocFile[]> attachResult = docService.AttachFileToDocument(doc, true);
                    if (attachResult.Code != 0)
                    {
                        return Json(attachResult.Result);
                    }
                    entities.AddRange(attachResult.Data);
                }
                else
                {
                    // 1. Удаляем все файлы из базы, Uid которых нет в выбранных файлах, т.е. пользователь снял галочки с этих файлов.
                    for (int i = 0; i < entities.Count; i++)
                    {
                        if (!files.Where(df => df.IsSelected == true && df.UId == entities[i].UId).Any())
                        {
                            entities[i].ChangeStatus = System.Data.Entity.EntityState.Deleted;
                        }
                        else
                        {
                            entities.RemoveAt(i); // Если документ не требуется изменять, то удаляем его из списка.
                            i--;
                        }
                    }

                    // 2. Добавляем в базу все выбранные файлы, если у них уже нет Uid
                    foreach (var file in files.Where(f => f.IsSelected == true))
                    {
                        if (!file.UId.HasValue || file.UId.Value == Guid.Empty)
                        {
                            entities.Add(new DocFile()
                            {
                                ChangeStatus = System.Data.Entity.EntityState.Added,
                                Content = fm.GetFile(file.StorageId),
                                DocumentUid = doc.UId,
                                Name = file.Name,
                                SprPage = file.SprPage,
                                UId = Guid.NewGuid(), // Новый GUID для нового файла
                                FileTypeId = file.FileTypeId
                            });
                        }
                    }
                }
                doc.DocFile = entities;

                #endregion

                // 5. Сохранение документа и файлов в базу
                OperationResult saveResult = _documentRepository.SaveDocument(doc, doc.ParentDocUId);

                // doc.Document1 на этом этапе обнуляется, поэтому заново перечитываем документ из базы
                OperationResult<RegistryDoc> resultDoc1 = _documentRepository.GetRegistryDocById(doc.UId);
                if (resultDoc1.Code != 0)
                {
                    return Json(resultDoc1.Result);
                }
                doc = resultDoc1.Data;

                if (saveResult.Code == 0)
                {
                    return Json(new OperationResult<ViewModels.RegistryDoc>(saveResult, ViewModels.RegistryDocFactory.CreateDoc(doc.DocTypeId, doc)));
                }

                return Json(saveResult);
            }
            finally
            {
                // Разблокировка документа
                if (blockAcquired)
                {
                    _documentRepository.UnlockDocument(vmDoc.Uid, currentUser.Data.UserId);
                }
            }
        }

        [HttpPost]
        public ActionResult CloneRegistryDoc(Guid documentUid)
        {
            OperationResult<RegistryDoc> registryDocResult = _documentRepository.GetRegistryDocById(documentUid);
            if (registryDocResult.Code != 0)
            {
                return Json(registryDocResult.Result);
            }

            // чтение Uid родительского документа
            RegistryDoc originalDoc = registryDocResult.Data;
            Guid registryId = originalDoc.ParentDocUId.Value; // Считаем, что родительский документ может быть только один

            // копирование документа
            RegistryDoc newDoc = originalDoc.CloneDocument();
            newDoc.ChangeStatus = System.Data.Entity.EntityState.Added;

            // сохранение документа
            OperationResult saveResult = _documentRepository.SaveDocument(newDoc, registryId);
            if (saveResult.Code != 0)
            {
                return Json(registryDocResult.Result);
            }

            // возвращаем новый документ
            var vm = ViewModels.RegistryDocFactory.CreateDoc(newDoc.DocTypeId, newDoc);
            return Json(new OperationResult<ViewModels.RegistryDoc>(OperationResult.Succeed, vm));
        }

        private ViewModels.DocFile[] LoadAndMergeFiles(DocFile[] docFiles, SPRFile[] sprFiles)
        {
            FileManager fm = new FileManager(Server.MapPath("~/content/images/"));
            List<ViewModels.DocFile> vmFiles = new List<ViewModels.DocFile>();

            foreach (DocFile file in docFiles)
            {
                // Сохранить файл во временное хранилище
                string previewType;
                string storageId = fm.StoreTemporaryFile(file.Content, file.UId, Path.GetExtension(file.Name), out previewType);

                vmFiles.Add(new ViewModels.DocFile()
                {
                    UId = file.UId,
                    DocumentUid = file.DocumentUid,
                    FileTypeId = file.FileTypeId,
                    SprPage = file.SprPage,
                    Name = file.Name,
                    StorageId = storageId,
                    IsAttached = true,
                    IsSelected = true,
                    IsSelectable = true,
                    IsVisible = !FileHelper.IsSign(file.Name),
                    PreviewType = previewType,
                    PreviewUrl = "/app/preview?storageid=" + storageId,
                    ThumbUrl = "/app/thumb?storageid=" + storageId
                });
            }

            foreach (SPRFile file in sprFiles)
            {
                // Сохранить файл во временное хранилище
                string previewType;
                string storageId = fm.StoreTemporaryFile(file.Content, Guid.NewGuid(), Path.GetExtension(file.Name), out previewType);

                // Добавляем в список, только, если его еще там нет
                if (vmFiles.Where(df => df.Name == file.Name && df.SprPage == file.Page).Count() == 0)
                {
                    vmFiles.Add(new ViewModels.DocFile()
                    {
                        UId = null,
                        DocumentUid = null,
                        FileTypeId = file.FileType,
                        SprPage = file.Page,
                        Name = file.Name,
                        StorageId = storageId,
                        IsAttached = false,
                        IsSelected = false,
                        IsSelectable = true,
                        IsVisible = !FileHelper.IsSign(file.Name),
                        PreviewType = previewType,
                        PreviewUrl = "/app/preview?storageid=" + storageId,
                        ThumbUrl = "/app/thumb?storageid=" + storageId
                    });
                }
            }

            return vmFiles.ToArray();
        }

        [HttpGet]
        public ActionResult Preview(string storageId)
        {
            FileManager fm = new FileManager(Server.MapPath("~/content/images/"));
            return fm.GetPreview(storageId);
        }

        [HttpGet]
        public ActionResult Thumb(string storageId)
        {
            FileManager fm = new FileManager(Server.MapPath("~/content/images/"));
            return fm.GetThumb(storageId);
        }

        [HttpGet]
        public ActionResult DownloadUid(System.Guid uid)
        {
            OperationResult<DocFile> fileResult = _documentRepository.GetFile(uid);
            if (fileResult.Code == 0)
            {
                return new FileContentResult(fileResult.Data.Content, FileHelper.GetContentTypeByExtension(Path.GetExtension(fileResult.Data.Name)));
            }
            else
            {
                return new HttpNotFoundResult();
            }
        }

        //[HttpGet]
        //public ActionResult GetAttachedFiles(System.Guid docUid) {

        //    System.Threading.Thread.Sleep(2000);

        //    return Json(new OperationResult<ViewModels.DocFile[]>(OperationResult.Succeed,
        //        new ViewModels.DocFile[]{
        //            new ViewModels.DocFile() { UId = new Guid("BEAF0624-0BC9-497B-A207-E25582B962A5"),DocumentUid = new Guid("BEAF0624-0BC9-497B-A207-E25582B962B5"), FileTypeId = 0, Name = "SF_DP123123", StorageName = ""},
        //            new ViewModels.DocFile() { UId = new Guid("BEAF0624-0BC9-497B-A207-E25582B962A6"),DocumentUid = new Guid("BEAF0624-0BC9-497B-A207-E25582B962B6"), FileTypeId = 0, Name = "SF_DP123124", StorageName = ""},
        //            new ViewModels.DocFile() { UId = new Guid("BEAF0624-0BC9-497B-A207-E25582B962A7"),DocumentUid = new Guid("BEAF0624-0BC9-497B-A207-E25582B962B7"), FileTypeId = 0, Name = "SF_DP123125", StorageName = ""},
        //            new ViewModels.DocFile() { UId = new Guid("BEAF0624-0BC9-497B-A207-E25582B962A8"),DocumentUid = new Guid("BEAF0624-0BC9-497B-A207-E25582B962B8"), FileTypeId = 0, Name = "SF_DP123126", StorageName = ""}
        //        }
        //    ), JsonRequestBehavior.AllowGet);

        //    OperationResult<DocFile[]> filesResult = _documentRepository.GetDocumentFilesDiscritionOnly(docUid);

        //    if (filesResult.Code == 0)
        //    {
        //        // Преобразовываем файлы
        //        return Json(new OperationResult<ViewModels.DocFile[]>(
        //            OperationResult.Succeed,
        //            filesResult.Data.Select(df => new ViewModels.DocFile()
        //            {
        //                UId = df.UId,
        //                DocumentUid = df.DocumentUid,
        //                FileTypeId = df.FileTypeId,
        //                Name = df.Name,
        //                StorageName = ""
        //            }).ToArray()), JsonRequestBehavior.AllowGet);
        //    }

        //    return Json(filesResult.Result, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public ActionResult GetSaperionFiles(string packageIndex)
        //{
        //    System.Threading.Thread.Sleep(2000);

        //    return Json(new OperationResult<ViewModels.DocFile[]>(OperationResult.Succeed, 
        //        new ViewModels.DocFile[]{
        //            new ViewModels.DocFile() { UId = Guid.Empty,DocumentUid = Guid.Empty, FileTypeId = 0, Name = "SF_DP123123", StorageName = "GUID-GUID-GUID1"},
        //            new ViewModels.DocFile() { UId = Guid.Empty,DocumentUid = Guid.Empty, FileTypeId = 0, Name = "SF_DP123124", StorageName = "GUID-GUID-GUID2"},
        //            new ViewModels.DocFile() { UId = Guid.Empty,DocumentUid = Guid.Empty, FileTypeId = 0, Name = "SF_DP123125", StorageName = "GUID-GUID-GUID3"},
        //            new ViewModels.DocFile() { UId = Guid.Empty,DocumentUid = Guid.Empty, FileTypeId = 0, Name = "SF_DP123126", StorageName = "GUID-GUID-GUID4"}
        //        }
        //        ), JsonRequestBehavior.AllowGet);

        //    if (!String.IsNullOrWhiteSpace(packageIndex))
        //    {
        //        OperationResult<SPRFile[]> fileResult = _sprRepository.GetFiles(packageIndex);

        //        if (fileResult.Code == 0)
        //        {
        //            // Сохраняем каждый файл во временной хранилище, присваивая уникальный GUID, по которому его потом можно будет найти
        //            // TODO: [Петр] не забыть удалить потом эти временные файлы
        //            //

        //            List<ViewModels.DocFile> vmFiles = new List<ViewModels.DocFile>();

        //            FileManager fm = new FileManager();

        //            foreach (var file in fileResult.Data)
        //            {
        //                // Сгенерировать имя для файла - GUID + расширение
        //                string storageName = Guid.NewGuid().ToString() + Path.GetExtension(file.Name);

        //                // Сохранить файл во временное хранилище
        //                fm.StoreTemporaryFile(file.Content, storageName);

        //                vmFiles.Add(new ViewModels.DocFile()
        //                {
        //                    UId = Guid.Empty,
        //                    DocumentUid = Guid.Empty,
        //                    FileTypeId = 0, // ??
        //                    Name = file.Name,
        //                    StorageName = storageName

        //                });
        //            }

        //            return Json(new OperationResult<ViewModels.DocFile[]>(OperationResult.Succeed, vmFiles.ToArray()), JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return Json(fileResult.Result, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    else
        //    {
        //        return Json(OperationResult.DocNotFound, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //[HttpGet]
        //public ActionResult Filen(string name)
        //{
        //    //string fileName = Path.Combine(Path.GetTempPath(), name);

        //    //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(fileName);

        //    //bmp.Save(fileName + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

        //    //return new FilePathResult(fileName + ".jpg", "image/jpeg");

        //    return new FilePathResult("D:\\temp\\xlo9kqz6dvyolm3xb5gd.png", "image/png");
        //}

        //[HttpGet]
        //public ActionResult Fileg(System.Guid uid) 
        //{
        //    return new FilePathResult("D:\\temp\\xlo9kqz6dvyolm3xb5gd.png", "image/png");
        //}


        [HttpGet]
        public ActionResult Organizations()
        {
            OperationResult<Organization[]> result = _documentRepository.GetListOfOrganizations();
            ViewModels.Org[] organizations = result.Data.Select(s =>
                new ViewModels.Org() { Id = s.Id, Name = s.Name }).ToArray();

            return Json(new OperationResult<ViewModels.Org[]>(
                OperationResult.Succeed,
                organizations), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DocTypes()
        {
            return Json(new OperationResult<ViewModels.DocType[]>(OperationResult.Succeed, GetDocTypes().Values.ToArray()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Partners(string term, int maxCount)
        {
            OperationResult<Partner[]> partnersResult = _documentRepository.GetPartners(term, maxCount);
            if (partnersResult.Code == 0)
            {
                List<ViewModels.Partner> viewmodels = new List<ViewModels.Partner>(partnersResult.Data.Length);
                foreach (var entity in partnersResult.Data)
                {
                    viewmodels.Add(new ViewModels.Partner(entity));
                }
                return Json(viewmodels);
            }
            AppLogger.Error(String.Format("Возникла ошибка при поиске контрагента (term={0},maxCount={1}).", term, maxCount), partnersResult.Result);
            return Json(new Partner[0]);
        }

        // TODO: [Петр] вынести куда-нибудь
        private Dictionary<int, ViewModels.DocType> GetDocTypes()
        {
            // Типы документов, с указанием шаблона HTML для этого документа и .NET-типом для десериализации
            return new Dictionary<int, ViewModels.DocType>()
            {
                { 0, new ViewModels.DocType{ Id = 0, Name = "", ShortName = "" } }, //TemplateName = "views/doc/notspecified", Type = String.Format("{0}, {1}",  typeof(ViewModels.NotSpecified).FullName, typeof(ViewModels.NotSpecified).Assembly.GetName().Name) } },
                { 3, new ViewModels.DocType{ Id = 3, Name = "Счет-фактура", ShortName = "СФ" } }, //TemplateName = "views/doc/invoice", Type = String.Format("{0}, {1}",  typeof(ViewModels.Invoice).FullName, typeof(ViewModels.Invoice).Assembly.GetName().Name) } },
                { 4, new ViewModels.DocType{ Id = 11, Name = "Корректировочный счет-фактура", ShortName = "КСФ" } }, //TemplateName = "views/doc/invoicecorr", Type = String.Format("{0}, {1}",  typeof(ViewModels.InvoiceCorr).FullName, typeof(ViewModels.InvoiceCorr).Assembly.GetName().Name) } },
                { 11, new ViewModels.DocType{ Id = 4, Name = "Акт приемки-сдачи работ/услуг", ShortName = "Акт" } }, //TemplateName = "views/doc/acert", Type = String.Format("{0}, {1}",  typeof(ViewModels.ACert).FullName, typeof(ViewModels.ACert).Assembly.GetName().Name) } },
                { 5, new ViewModels.DocType{ Id = 5, Name = "Товарная накладная ТОРГ-12", ShortName = "Торг-12" } } //, TemplateName = "views/doc/torg12", Type = String.Format("{0}, {1}",  typeof(ViewModels.Torg12).FullName, typeof(ViewModels.Torg12).Assembly.GetName().Name) } }
            };
        }

        private SPRDocType ConvertToSprType(DocumentType docType)
        {
            switch (docType)
            {
                case DocumentType.Invoice:
                    return SPRDocType.Invoice;
                case DocumentType.InvoiceCorrection:
                    return SPRDocType.InvoiceCorrection;
                case DocumentType.AcceptanceCertificate:
                    return SPRDocType.AcceptanceCertificate;
                case DocumentType.Torg12:
                    return SPRDocType.Torg12;
                default:
                    throw new InvalidOperationException("Соответствие типов не задано.");
            }
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            // TODO: [Петр] Перенести в FileManager
            if (Request.Files.Count == 0) return Json(new { guid = string.Empty });
            var file = Request.Files[0];
            // Сгенерировать имя для файла - GUID
            var fileUid = Guid.NewGuid();
            string systemFileName = fileUid.ToString();
            string fullPath = Path.Combine(Path.GetTempPath(), systemFileName);
            file.SaveAs(fullPath);
            return Json(new { guid = fileUid, name = file.FileName });
        }

        [HttpPost]
        public ActionResult UploadRegistries()
        {
            // TODO: [Петр] Перенести в FileManager
            if (Request.Files.Count == 0) return Json("false");
            var file = Request.Files[0];
            // Сгенерировать имя для файла - GUID
            var fileUid = Guid.NewGuid();
            string systemFileName = fileUid.ToString() + file.FileName;
            string fullPath = Path.Combine(Path.GetTempPath(), systemFileName);
            file.SaveAs(fullPath);
            var resultRegistry = _excelImport.GetRegistries(fullPath);
            System.IO.File.Delete(fullPath);

            var docType = GetDocTypes()[0];
            var viewModels = resultRegistry.Data
                .SelectMany(data => data.Documents)
                .Select(doc => new ViewModels.NotSpecified()
                {
                    PackageIndex = doc.PackageIndex,
                    DocTypeId = (int)DocumentType.Unspecified,
                    DocData = new ViewModels.DocData()
                    {
                        DocDate = doc.DocDate,
                        DocNumber = doc.DocNumber
                    }
                }).ToArray();
            return Json(new OperationResult<ViewModels.NotSpecified[]>(resultRegistry.Result, viewModels));
        }

        private string GetUserName()
        {
            string userNameFull = HttpContext.User.Identity.Name;
            string[] parts = userNameFull.Split('\\');
            //Получаем имя пользователя без префикса sibgenco
            string userName = parts.Length == 2 ? parts[1] : parts[0];
            return userName;
        }

        [HttpPost]
        public ActionResult ExportClaim(Guid claimUid)
        {
            DocumentService docService = new DocumentService(_documentRepository, _sprRepository, _ifnsExporter);
            //Получаем имя файла
            string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + GetUserName();
            string directory = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
            directory = directory.EndsWith("\\") ? directory : directory + "\\";
            //Получаем временный путь
            string tempPath = Path.GetTempPath();
            tempPath = tempPath.EndsWith("\\") ? tempPath : tempPath + "\\";
            string tempFullFileName = tempPath + fileName + ".zip";
            string realFullFileName = directory + fileName + ".zip";
            //Экспортируем в файл
            var exportResult = docService.ExportClaim(claimUid, tempFullFileName);
            if (exportResult.IsSucceed) //Все хорошо переносим в реальный архив файлов
                System.IO.File.Copy(tempFullFileName, realFullFileName);
            if (System.IO.File.Exists(tempFullFileName))
                System.IO.File.Delete(tempFullFileName);//Удаляем временный файл
            return Json(exportResult);
        }

        [HttpPost]
        public ActionResult ImportFromSap(Guid claimUid)
        {
            //Смотрим есть ли такое требование.
            var getClaimResult = _documentRepository.GetClaimById(claimUid);
            if (!getClaimResult.IsSucceed)
                return Json(getClaimResult.Result);
            if (getClaimResult == null)//Если нет, то возвращаем ошибку.
                return Json(OperationResult.NeedSaveDocument);
            var userName = HttpContext.User.Identity.Name;
            var operationResult = _sap.LoadXml(claimUid, userName);//Создаем реестр

            if (!operationResult.IsSucceed) return Json(operationResult);
            var registryRes = _documentRepository.GetRegistryByUid(operationResult.Data);
            ViewModels.RegistryDetails registryVM = new ViewModels.RegistryDetails(registryRes.Data);
            return Json(new OperationResult<ViewModels.RegistryDetails>(registryRes.Result, registryVM));
        }


        [HttpPost]
        public ActionResult ImportFromExcel(IEnumerable<HttpPostedFileBase> files, FormCollection forms)
        {
            Guid claimUid = Guid.Parse(forms["claimUid"]);
            var userName = HttpContext.User.Identity.Name;
            if (Request.Files.Count == 0) return Json("false");
            var file = Request.Files[0];
            // Получаем файл
            var fileUid = Guid.NewGuid();
            string systemFileName = fileUid.ToString() + file.FileName;
            string fullPath = Path.Combine(Path.GetTempPath(), systemFileName);
            file.SaveAs(fullPath);
            //Получаем реестр
            var resultRegistry = _excelImport.GetRegistries(fullPath);
            System.IO.File.Delete(fullPath);
            if (!resultRegistry.IsSucceed) return Json(resultRegistry);
            //Валидируем реестры
            var reg = resultRegistry.Data[0];
            List<ErrorXls> errors = new List<ErrorXls>();
            errors.AddRange(reg.CanBuild());
            if (errors.Count() != 0) return Json(new OperationResult<ErrorXls[]>(OperationResult.ExcelExportError, errors.ToArray()));
            //Сохраняем реестр в базу//
            XDocument document = new XDocument(reg.BuidXml());
            var insertRegRes = _documentRepository.InsertRegistry(document, claimUid, userName);
            if (!insertRegRes.IsSucceed) Json(insertRegRes);
            Guid registryUid = insertRegRes.Data;
            //Получаем реестр//
            var registryRes = _documentRepository.GetRegistryByUid(registryUid);
            ViewModels.RegistryDetails registryVM = new ViewModels.RegistryDetails(registryRes.Data);
            return Json(new OperationResult<ViewModels.RegistryDetails>(registryRes.Result, registryVM));
        }
    }
}
