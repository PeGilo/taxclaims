﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGC.TaxClaims.Common.Filtering;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Core.Saperion;
using System.IO;
using System.Web;
using SGC.TaxClaims.Infrastructure.Services;

namespace SGC.TaxClaims.SpaClient.Controllers
{
    [OutputCache(Duration = 0)]
    public class TestController : BaseController
    {
        private IDBRepository _documentRepository;
        private ISaperion _sprRepository;
        private IExcelImport _excelImport;
        private IIFNSExporter _ifnsExporter;
        private ISap _sap;

        public TestController(
            IDBRepository documentRepository,
            ISaperion sprRepository,
            IExcelImport excelImport,
            IIFNSExporter ifnsExporter,
            ISap sap
            )
        {
            _documentRepository = documentRepository;
            _sprRepository = sprRepository;
            _excelImport = excelImport;
            _ifnsExporter = ifnsExporter;
            _sap = sap;
        }


        [HttpGet]
        public ActionResult GetRegistryById(System.Guid registryUid)
        {
            ViewModels.Registry vm = new ViewModels.Registry();

            vm.Uid = registryUid;
            vm.ClaimId = Guid.NewGuid();
            vm.ClaimItemNo = "1";
            vm.DateReady = DateTime.Now;
            vm.RegistryName = "MOCK REGISTRY";
            vm.Status = "No status";
            vm.Docs = new List<ViewModels.RegistryDoc>();

            Guid g = Guid.NewGuid();
            vm.Docs.Add(new ViewModels.RegistryDoc()
            {
                Uid = g,
                IsScan = false,
                DocTypeId = 0,
                PackageIndex = "150000026",
                SourceId = 1, // SAP
                DocData = new ViewModels.DocData()
                {
                    DocNumber = "0090113338/3000000019",
                    DocDate = DateTime.Now
                },
                Children = new List<ViewModels.RegistryDoc>()
                {
                    new ViewModels.RegistryDoc() {
                        Uid = Guid.NewGuid(),
                        IsScan = false,
                        DocTypeId = 5,
                        PackageIndex = "150000026",
                        SourceId = 0,
                        DocData = new ViewModels.DocData()
                        {
                            DocNumber = "0090113338/3000000019",
                            DocDate = DateTime.Now
                        }
                    },
                    new ViewModels.RegistryDoc() {
                        Uid = Guid.NewGuid(),
                        IsScan = false,
                        DocTypeId = 3,
                        PackageIndex = "150000026",
                        SourceId = 0,
                        DocData = new ViewModels.DocData()
                        {
                            DocNumber = "0090113338/3000000019",
                            DocDate = DateTime.Now
                        }
                    }
                }
            });

            g = Guid.NewGuid();
            vm.Docs.Add(new ViewModels.RegistryDoc()
            {
                Uid = g,
                IsScan = false,
                DocTypeId = 0,
                PackageIndex = "150000026",
                SourceId = 1, // SAP
                DocData = new ViewModels.DocData()
                {
                    DocNumber = "0090113338/3000000019",
                    DocDate = DateTime.Now
                },
                Children = new List<ViewModels.RegistryDoc>()
                {
                    new ViewModels.RegistryDoc() {
                        Uid = Guid.NewGuid(),
                        IsScan = false,
                        DocTypeId = 5,
                        PackageIndex = "150000026",
                        SourceId = 0,
                        DocData = new ViewModels.DocData()
                        {
                            DocNumber = "0090113338/3000000019",
                            DocDate = DateTime.Now
                        }
                    },
                    new ViewModels.RegistryDoc() {
                        Uid = Guid.NewGuid(),
                        IsScan = false,
                        DocTypeId = 3,
                        PackageIndex = "150000026",
                        SourceId = 0,
                        DocData = new ViewModels.DocData()
                        {
                            DocNumber = "0090113338/3000000019",
                            DocDate = DateTime.Now
                        }
                    }
                }
            });

            g = Guid.NewGuid();
            vm.Docs.Add(new ViewModels.RegistryDoc()
            {
                Uid = g,
                IsScan = false,
                DocTypeId = 0,
                PackageIndex = "150000026",
                SourceId = 1, // SAP
                DocData = new ViewModels.DocData()
                {
                    DocNumber = "0090113338/3000000019",
                    DocDate = DateTime.Now
                }
            });

            g = Guid.NewGuid();
            vm.Docs.Add(new ViewModels.RegistryDoc()
            {
                Uid = g,
                IsScan = false,
                DocTypeId = 0,
                PackageIndex = "150000026",
                SourceId = 0,
                DocData = new ViewModels.DocData()
                {
                    DocNumber = "0090113338/3000000019",
                    DocDate = DateTime.Now
                }
            });

            return Json(new OperationResult<ViewModels.Registry>(OperationResult.Succeed, vm), JsonRequestBehavior.AllowGet);
        }
    }
}