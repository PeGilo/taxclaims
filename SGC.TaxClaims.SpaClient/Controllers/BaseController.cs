﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Infrastructure;

namespace SGC.TaxClaims.SpaClient.Controllers
{
    /// <summary>
    /// Разные вспомогательные методы, общие для всех контроллеров.
    /// </summary>
    public class BaseController : Controller
    {
        //private User _currentUser = null;

        //public User CurrentUser
        //{
        //    get
        //    {
        //        if (_currentUser == null)
        //        {
        //            string userName = HttpContext.User.Identity.Name;

        //            // Так нельзя получить ProviderUserKey
        //            //MembershipUser mu = Membership.GetUser(userName);
                    
        //            _currentUser = new User()
        //            {
        //                 Name = userName,
        //                 //UId = (Guid)mu.ProviderUserKey
        //            };
        //        }
        //        return _currentUser;
        //    }
        //}

        private ILogger _appLogger;

        public ILogger AppLogger
        {
            get {

                if (_appLogger == null)
                {
                    _appLogger = Logger.CreateAppLogger();
                }

                return _appLogger; 
            }
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding)
        {
            return Json(data, contentType, contentEncoding, JsonRequestBehavior.DenyGet);
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new CustomJsonResult() 
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding, 
                JsonRequestBehavior = behavior
            };
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            // Залогировать
            Logger.CreateAppLogger().Error("Исключение.", filterContext.Exception);

            //// Не использовать редирект, а подменять View, чтобы не вызывать еще один exception

            //ViewDataDictionary viewData = new ViewDataDictionary();

            //string viewName = "Error"; // default view

            ////if (filterContext.Exception is InvalidOperationException)
            ////{
            ////    viewData["ErrorMessage"] = filterContext.Exception.Message;
            ////}

            //if (HttpContext.IsDebuggingEnabled)
            //{
            //    viewData["ErrorMessage"] = filterContext.Exception.Message + ((filterContext.Exception.InnerException != null) ? filterContext.Exception.InnerException.Message : "");
            //}

            //filterContext.ExceptionHandled = true;

            //filterContext.Result = new ViewResult
            //{
            //    ViewName = viewName,
            //    ViewData = viewData
            //};
        }

        ///// <summary>
        ///// Отобразить информацию об общей ошибке приложения
        ///// </summary>
        ///// <param name="result"></param>
        //protected virtual ActionResult RedirectToGenericError()
        //{
        //    return RedirectToAction("Generic", "Error");
        //}

        ///// <summary>
        ///// Выполняет необходимые действия при возникновении ситуации отсутствия 
        ///// запрашиваемых данных в базе
        ///// </summary>
        ///// <returns></returns>
        //protected virtual ActionResult NotFound<T>(object id)
        //{
        //    ViewData["ErrorTitle"] = "Объект не найден";

        //    string text = "";

        //    if (typeof(T) == typeof(SGC.TaxClaims.Core.Model.Claim))
        //    {
        //        text = "Требование не найдено в базе данных";
                 
        //    }

        //    if (id != null)
        //    {
        //        text += String.Format(" (идентификатор объект {0})", id);
        //    }

        //    ViewData["ErrorMessage"] = text;

        //    return RedirectToAction("NotFound", "Error"); // Делается редирект, чтобы можно было вернуться на предыдущую страницу
        //}

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            //bool isControlMenuVisible = false;
            //bool isViolationTypeSubmenuVisible = false;
            //bool isBusinessUnitSubmenuVisible = false;
            //bool isEmployeesSubmenuVisible = false;

            //if (HttpContext.User.Identity.IsAuthenticated
            //    && _userService != null)
            //{
            //    User user = _userService.GetByLogin(HttpContext.User.Identity.Name);

            //    isControlMenuVisible = user.PermissionSet.UserManagementPermission >= Permission4.View;
            //    isViolationTypeSubmenuVisible = user.PermissionSet.ViolationTypeManagementPermission >= Permission3.View;
            //    isBusinessUnitSubmenuVisible = user.PermissionSet.BusinessUnitManagementPermission >= Permission3.View;
            //    isEmployeesSubmenuVisible = user.PermissionSet.EmployeeManagementPermission >= Permission4Import.View;
            //}

            //// Добавить во View данные по видимости элементов меню
            //ViewData["IsControlMenuVisible"] = isControlMenuVisible;
            //ViewData["IsDictionaryMenuVisible"] = isViolationTypeSubmenuVisible || isBusinessUnitSubmenuVisible || isEmployeesSubmenuVisible;
            //ViewData["IsViolationTypeSubmenuVisible"] = isViolationTypeSubmenuVisible;
            //ViewData["IsBusinessUnitSubmenuVisible"] = isBusinessUnitSubmenuVisible;
            //ViewData["IsEmployeesSubmenuVisible"] = isEmployeesSubmenuVisible;
        }
    }
}