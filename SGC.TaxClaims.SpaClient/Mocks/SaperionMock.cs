﻿using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Core.Saperion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace SGC.TaxClaims.SpaClient.Mocks
{
    public class SaperionMock:  ISaperion
    {
        private const string FilesFolder = @"D:\Projects\SGC\TaxClaims\Dev\Source\SGC.TaxClaims.SpaClient\Mocks\Files";

        public OperationResult<SPRDocInfo[]> GetIndexes(string[] docNumbers)
        {
            return new OperationResult<SPRDocInfo[]>(OperationResult.Succeed, new SPRDocInfo[]
            {
                new SPRDocInfo() { DocNumber = docNumbers[0], DocIndexes = new string[] {"", ""}}
            });
        }

        public OperationResult<SPRFile[]> GetFiles(string packageIndex)
        {
            return new OperationResult<SPRFile[]>(OperationResult.Succeed, GetFilesContent(true,
                "01_s2d371D4B430382B61609EAF3ED88608CA3_1.tif",
                "02_s2d371D4B430382B61609EAF3ED88608CA3_1.tif",
                //"03_s2d371D4B430382B61609EAF3ED88608CA3_1.tif",
                //"04_s2d45D980FC6D57761715BC4A2DE4EA2E7C_1.tif",
                //"05_s2d45D980FC6D57761715BC4A2DE4EA2E7C_1.tif",
                //"06_s2d1F51C57746427C8969A96ABADB88A548_1.tif",
                //"07_s2d1F51C57746427C8969A96ABADB88A548_1.tif",
                //"08_s2d1F51C57746427C8969A96ABADB88A548.jpg",
                //"09_s2d1F51C57746427C8969A96ABADB88A548.jpg",
                //"10_Уведомление о получении 1.TIF",
                //"11_Уведомление о получении 1.TIF",
                //"12_Подтверждение оператора 11.TIF",
                //"13_Подтверждение оператора 11.TIF",
                "14_InvoicePrintForm.TIF",
                "15_InvoicePrintForm.TIF",
                "16_XmlAcceptanceCertificatPrintForm.TIF",
                "17_XmlAcceptanceCertificatPrintForm.TIF",
                "18_InvoiceDocument.pdf"
                //"19_InvoiceDocument.txt"
                ));
        }

        public OperationResult<SPRFile[]> GetFiles(string packageIndex, int[] fileOrderNumbers)
        {
            return new OperationResult<SPRFile[]>(OperationResult.Succeed, GetFilesContent(true,
                "01_s2d371D4B430382B61609EAF3ED88608CA3_1.tif",
                "02_s2d371D4B430382B61609EAF3ED88608CA3_1.tif",
                //"03_s2d371D4B430382B61609EAF3ED88608CA3_1.tif",
                //"04_s2d45D980FC6D57761715BC4A2DE4EA2E7C_1.tif",
                //"05_s2d45D980FC6D57761715BC4A2DE4EA2E7C_1.tif",
                //"06_s2d1F51C57746427C8969A96ABADB88A548_1.tif",
                //"07_s2d1F51C57746427C8969A96ABADB88A548_1.tif",
                //"08_s2d1F51C57746427C8969A96ABADB88A548.jpg",
                //"09_s2d1F51C57746427C8969A96ABADB88A548.jpg",
                //"10_Уведомление о получении 1.TIF",
                //"11_Уведомление о получении 1.TIF",
                //"12_Подтверждение оператора 11.TIF",
                //"13_Подтверждение оператора 11.TIF",
                "14_InvoicePrintForm.TIF",
                "15_InvoicePrintForm.TIF",
                "16_XmlAcceptanceCertificatPrintForm.TIF",
                "17_XmlAcceptanceCertificatPrintForm.TIF",
                "18_InvoiceDocument.pdf"
                //"19_InvoiceDocument.txt"
                ));
        }

        public OperationResult<SPRFile[]> GetEDocFiles(string packageIndex, SPRDocType docType)
        {
            switch (docType)
            {
                case SPRDocType.Invoice:
                    return new OperationResult<SPRFile[]>(OperationResult.Succeed, GetFilesContent(false,
                        "ON_SFAKT_2BM-2460000003-2013061905115361686060000000000_2BM-9631837767-2013091911250611184800000000000_20110105_002f3418-2ceb-4876-afb2-783563294ea1.xml",
                        "ON_SFAKT_2BM-2460000003-2013061905115361686060000000000_2BM-9631837767-2013091911250611184800000000000_20110105_002f3418-2ceb-4876-afb2-783563294ea1.xml.sign"
                        ));
                case SPRDocType.InvoiceCorrection:
                    return new OperationResult<SPRFile[]>(OperationResult.Succeed, GetFilesContent(false,
                        "ON_KORSFAKT_2BM-4200000333-2012052808325931222630000000000_2BM-4212024138-2013022203544166668700000000000_20141030_cbe8700b-fd06-454c-be83-f61388961b5f.xml",
                        "ON_KORSFAKT_2BM-4200000333-2012052808325931222630000000000_2BM-4212024138-2013022203544166668700000000000_20141030_cbe8700b-fd06-454c-be83-f61388961b5f.xml.sign"
                        ));
                case SPRDocType.AcceptanceCertificate:
                    return new OperationResult<SPRFile[]>(OperationResult.Succeed, GetFilesContent(false,
                        "DP_IAKTPRM_2BM-2460000003-2013061905115361686060000000000_2BM-9631837767-2013091911250611184800000000000_20131018_e6db50a1-3e0e-4b8b-9059-f70e8930b397.xml",
                        "DP_IAKTPRM_2BM-2460000003-2013061905115361686060000000000_2BM-9631837767-2013091911250611184800000000000_20131018_e6db50a1-3e0e-4b8b-9059-f70e8930b397.xml.sign"
                        ));
                case SPRDocType.Torg12:
                    return new OperationResult<SPRFile[]>(OperationResult.Succeed, GetFilesContent(false,
                        "DP_OTORG12_2BM-2460000003-2013061905115361686060000000000_2BM-9631837767-2013091911250611184800000000000_20131018_e6db50a1-3e0e-4b8b-9059-f70e8930b397.xml",
                        "DP_OTORG12_2BM-2460000003-2013061905115361686060000000000_2BM-9631837767-2013091911250611184800000000000_20131018_e6db50a1-3e0e-4b8b-9059-f70e8930b397.xml.sign"
                        ));
                default:
                    throw new NotImplementedException("Не известный тип документа.");
            }
        }

        private SPRFile[] GetFilesContent(bool trim, params string[] filenames)
        {
            List<SPRFile> files = new List<SPRFile>();

            foreach (var filename in filenames)
            {
                string fn = Path.GetFileName(filename);

                int index = 0;
                if (trim)
                {
                    index = Int32.Parse(fn.Substring(0, 2));
                }

                SPRFile sf = new SPRFile()
                {
                    Page = index,
                    Name = trim ? fn.Substring(3, fn.Length - 3) : fn,
                    Content = File.ReadAllBytes(Path.Combine(FilesFolder, filename)),
                    FileType = GetFileTypeByName(filename)
                };
                files.Add(sf);
            }
            return files.ToArray();
        }

        private int GetFileTypeByName(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return 1;
            fileName = fileName.ToUpper().Trim();
            if (fileName.StartsWith("DP_IAKTPRM") && fileName.EndsWith(".XML")) return 4;
            if (fileName.StartsWith("DP_IAKTPRM") && fileName.EndsWith(".XML.SIGN")) return 5;
            if (fileName.StartsWith("DP_ZAKTPRM") && fileName.EndsWith(".XML")) return 6;
            if (fileName.StartsWith("DP_ZAKTPRM") && fileName.EndsWith(".XML.SIGN")) return 7;
            if (fileName.StartsWith("ON_KORSFAKT") && fileName.EndsWith(".XML")) return 8;
            if (fileName.StartsWith("ON_KORSFAKT") && fileName.EndsWith(".XML.SIGN")) return 9;
            if (fileName.StartsWith("ON_SFAKT") && fileName.EndsWith(".XML")) return 10;
            if (fileName.StartsWith("ON_SFAKT") && fileName.EndsWith(".XML.SIGN")) return 11;
            if (fileName.StartsWith("DP_OTORG12") && fileName.EndsWith(".XML")) return 12;
            if (fileName.StartsWith("DP_OTORG12") && fileName.EndsWith(".XML.SIGN")) return 13;
            if (fileName.StartsWith("DP_PTORG12") && fileName.EndsWith(".XML")) return 14;
            if (fileName.StartsWith("DP_PTORG12") && fileName.EndsWith(".XML.SIGN")) return 15;
            return 1;
        }

        public ILogger Logger { get; set; }

        public void Dispose()
        {
        }
    }
}