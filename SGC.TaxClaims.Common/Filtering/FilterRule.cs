﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGC.TaxClaims.Common.Filtering
{
    public class FilterRule
    {
        public FilterRule()
        {
            FieldName = String.Empty;
            FilterRuleOperation = FilterRuleOperation.Equal;
            Data = String.Empty;
        }

        public string FieldName { get; set; }
        public FilterRuleOperation FilterRuleOperation { get; set; }
        public string Data { get; set; }
    }


}
