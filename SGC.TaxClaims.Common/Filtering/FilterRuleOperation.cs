﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGC.TaxClaims.Common.Filtering
{
    public enum FilterRuleOperation
    {
        Equal = 0,
        NotEqual = 1,
        BeginsWith = 2,
        NotBeginsWith = 3,
        EndsWith = 4,
        NotEndsWith = 5,
        Contains = 6,
        NotContains = 7,
        IncludedIn = 8,
        NotIncludedIn = 9,
        LessThen = 10,
        LessOrEqual = 11,
        GreaterThen = 12,
        GreaterOrEqual = 13
    }
}
