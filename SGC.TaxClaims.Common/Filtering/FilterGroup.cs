﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGC.TaxClaims.Common.Filtering
{
    public class FilterGroup
    {
        public FilterGroup()
        {
            GroupOperation = FilterGroupOperation.And;
            Rules = new List<FilterRule>();
        }

        public FilterGroupOperation GroupOperation { get; set; }

        public IList<FilterRule> Rules { get; set; }
    }
}
