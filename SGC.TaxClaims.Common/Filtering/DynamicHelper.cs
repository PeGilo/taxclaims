﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace SGC.TaxClaims.Common.Filtering
{


    public static class DynamicHelper
    {
        /// <summary>
        /// Преобразовать объект с описанием фильтрации и сформировать динамический запрос Where
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="filterGroup"></param>
        /// <returns></returns>
        public static IQueryable<T> Filter<T>(this IQueryable<T> source, FilterGroup filterGroup)
        {
            if (filterGroup.Rules.Count == 0)
            {
                return source;
            }

            List<object> ps = new List<object>(); // dynamic parameters
            StringBuilder sb = new StringBuilder();
            string whereCondition = "";

            bool isAnd = filterGroup.GroupOperation == FilterGroupOperation.And; // Тип объединения
            int fieldIndexer = 0;

            // Подсчитать кол-во правил, чтобы подсчитать кол-во операторов AND / OR
            //for (int i = 0; i < filterGroup.Rules.Count; i++)
            //{

            //}
            //bool isOperatorAdded = false;
            

            for (int i = 0; i < filterGroup.Rules.Count; i++)
			{
                whereCondition = "";

			    FilterRule rule = filterGroup.Rules[i];

                if (String.IsNullOrEmpty(rule.FieldName))
                {
                    continue;
                }

                // Узнаем тип свойства по его имени
                Type propertyType = ReflectionHelper.GetPropertyType(typeof(T), rule.FieldName);

                switch (rule.FilterRuleOperation)
                {
                    case FilterRuleOperation.Equal:
                        if(propertyType != typeof(String)) {
                            whereCondition = String.Format("{0}=@{1}", rule.FieldName, fieldIndexer++);
                        }
                        break;
                    case FilterRuleOperation.NotEqual:
                        if (propertyType != typeof(String)) {
                            whereCondition = String.Format("{0}!=@{1}", rule.FieldName, fieldIndexer++);
                        }
                        break;
                    //case FilterRuleOperation.BeginsWith:
                    //    whereCondition = String.Format("{0}.StartsWith(@{1})", rule.FieldName, fieldIndexer++);
                    //    break;
                    //case FilterRuleOperation.NotBeginsWith:
                    //    whereCondition = String.Format("!{0}.StartsWith(@{1})", rule.FieldName, fieldIndexer++);
                    //    break;
                    //case FilterRuleOperation.EndsWith:
                    //    whereCondition = String.Format("{0}.EndsWith(@{1})", rule.FieldName, fieldIndexer++);
                    //    break;
                    //case FilterRuleOperation.NotEndsWith:
                    //    whereCondition = String.Format("!{0}.EndsWith(@{1})", rule.FieldName, fieldIndexer++);
                    //    break;
//                    case FilterRuleOperation.Contains:
//                        //whereCondition = String.Format("{0} like '{1}'", rule.FieldName, rule.Data);
////                        whereCondition = String.Format("(SqlFunctions.PatIndex(@{1}, {0}) > 0)", rule.FieldName, i);
//                        whereCondition = String.Format("{0}.Contains(@{1})", rule.FieldName, i);
//                        break;
//                    case FilterRuleOperation.NotContains:
////                        whereCondition = String.Format("!(SqlFunctions.PatIndex(@{1}, {0}) > 0)", rule.FieldName, i);
//                        //whereCondition = String.Format("{0} not like @{1}", rule.FieldName, i);
//                        whereCondition = String.Format("!{0}.Contains(@{1})", rule.FieldName, i);
//                        break;
                    case FilterRuleOperation.LessThen:
                        whereCondition = String.Format("{0}<@{1}", rule.FieldName, fieldIndexer++);
                        break;
                    case FilterRuleOperation.LessOrEqual:
                        whereCondition = String.Format("{0}<=@{1}", rule.FieldName, fieldIndexer++);
                        break;
                    case FilterRuleOperation.GreaterThen:
                        whereCondition = String.Format("{0}>@{1}", rule.FieldName, fieldIndexer++);
                        break;
                    case FilterRuleOperation.GreaterOrEqual:
                        whereCondition = String.Format("{0}>=@{1}", rule.FieldName, fieldIndexer++);
                        break;

                    default:
                        //throw new InvalidOperationException("Неизвестный тип запроса");
                        //whereCondition = "";
                        break;
                }


                // Парсим строку-значение в этот конкректный тип
                var converter = TypeDescriptor.GetConverter(propertyType);
                try
                {
                    if (!String.IsNullOrEmpty(whereCondition))
                    {
                        var result = converter.ConvertFrom(rule.Data);
                        ps.Add(result); // Здесь надо знать какого типа параметр

                        if (ps.Count != 1 )
                        {
                            sb.Append(" " + (isAnd ? "AND" : "OR") + " ");
                        }

                        sb.Append(whereCondition);

                        //// Добавить объединение, т.е. AND или OR
                        //if (i < filterGroup.Rules.Count - 1)
                        //{
                        //    sb.Append(" " + (isAnd ? "AND" : "OR") + " ");
                        //}
                    }
                }
                catch (Exception ex)
                {
                    // Обработать ошибки парсинга в тип
                }
			}

            IQueryable<T> q1;
            bool conditionExits = false;

            if (sb.Length > 0)
            {
                q1 = source.Where(sb.ToString(), ps.ToArray());
                conditionExits = true;
            }
            else
            {
                q1 = source;
                //q1 = new List<T>().AsQueryable();
            }

            if (isAnd)
            {
                // Если условие AND, то добавить Where условия к сформированному запросу
                return FilterContainsOperators(q1, q1, filterGroup, isAnd);
            }
            else
            {
                // Если условие OR, то сделать UNION
                
                if(conditionExits) {
                    var q2 = FilterContainsOperators(q1, source, filterGroup, isAnd);
                    // return q1.Concat(q2);
                    return q1.Union(q2);
                }
                else {
                    var q2 = FilterContainsOperators(new List<T>().AsQueryable(), source, filterGroup, isAnd);
                    return q2;
                }
            }
        }

        /// <summary>
        /// Отдельный метод для фильтра "Содержит"
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="filterGroup"></param>
        /// <returns></returns>
        private static IQueryable<T> FilterContainsOperators<T>(IQueryable<T> query, IQueryable<T> source, FilterGroup filterGroup, bool isAnd)
        {
            if (filterGroup.Rules.Count == 0)
            {
                return source;
            }

            IQueryable<T> q = query;

            //if (isAnd)
            //{
            //    q = source;
            //}
            //else
            //{
            //    q = new List<T>().AsQueryable();
            //}

            //bool isAnd = filterGroup.GroupOperation == FilterGroupOperation.And; // Тип объединения

            for (int i = 0; i < filterGroup.Rules.Count; i++)
            {
                FilterRule rule = filterGroup.Rules[i];

                if (String.IsNullOrEmpty(rule.FieldName))
                {
                    continue;
                }

                // Узнаем тип свойства по его имени
                Type propertyType = ReflectionHelper.GetPropertyType(typeof(T), rule.FieldName);

                //string condition = "%" + PreprocessWildcards(rule.Data) + "%";
                bool trim = !(rule.FilterRuleOperation == FilterRuleOperation.Equal || rule.FilterRuleOperation == FilterRuleOperation.NotEqual);
                string processedData = PreprocessWildcards(rule.Data, trim);

                //Expression<Func<T, bool>> whereCondition = null;

                switch (rule.FilterRuleOperation)
                {
                    case FilterRuleOperation.Equal:
                        if (propertyType == typeof(String))
                        {
                            if (ContainsWildcards(rule.Data))
                            {
                                if(isAnd) {
                                    q = q.WhereLike(rule.FieldName, processedData);
                                }
                                else {
                                    var q2 = source.WhereLike(rule.FieldName, processedData);
                                    q = q.Union(q2);
                                }
                            }
                            else
                            {
                                if(isAnd) {
                                    q = q.Where(String.Format("{0}=@{1}", rule.FieldName, 0), new object[]{ rule.Data });
                                }
                                else {
                                    var q2 = source.Where(String.Format("{0}=@{1}", rule.FieldName, 0), new object[]{ rule.Data });
                                    q = q.Union(q2);
                                }
                            }
                        }
                        break;
                    case FilterRuleOperation.NotEqual:
                        if (propertyType == typeof(String))
                        {
                            if (ContainsWildcards(rule.Data))
                            {
                                if(isAnd) {
                                    q = q.WhereNotLike(rule.FieldName, processedData);
                                }
                                else {
                                    var q2 = source.WhereNotLike(rule.FieldName, processedData);
                                    q = q.Union(q2);
                                }
                            }
                            else
                            {
                                if(isAnd) {
                                    q = q.Where(String.Format("{0}!=@{1}", rule.FieldName, 0), new object[] { rule.Data });
                                }
                                else {
                                    var q2 = source.Where(String.Format("{0}!=@{1}", rule.FieldName, 0), new object[] { rule.Data });
                                    q = q.Union(q2);
                                }
                            }
                        }
                        break;
                    case FilterRuleOperation.BeginsWith:
                        //whereCondition = String.Format("{0}.StartsWith(@{1})", rule.FieldName, fieldIndexer++);
                        if(isAnd) {
                            q = q.WhereLike(rule.FieldName, processedData + "%");
                        }
                        else {
                            var q2 = source.WhereLike(rule.FieldName, processedData + "%");
                            q = q.Union(q2);
                        }

                        break;
                    case FilterRuleOperation.NotBeginsWith:

                                if(isAnd) {
                                    q = q.WhereNotLike(rule.FieldName, processedData + "%");
                                }
                                else {
                                    var q2 = source.WhereNotLike(rule.FieldName, processedData + "%");
                                    q = q.Union(q2);
                                }
                        break;
                    case FilterRuleOperation.EndsWith:

                                if(isAnd) {
                                    q = q.WhereLike(rule.FieldName, "%" + processedData);
                                }
                                else {
                                    var q2 = source.WhereLike(rule.FieldName, "%" + processedData);
                                    q = q.Union(q2);
                                }

                        break;
                    case FilterRuleOperation.NotEndsWith:

                                if(isAnd) {
                                    q = q.WhereNotLike(rule.FieldName, "%" + processedData);
                                }
                                else {
                                    var q2 = source.WhereNotLike(rule.FieldName, "%" + processedData);
                                    q = q.Union(q2);
                                }
                        break;
                    case FilterRuleOperation.Contains:

                                if(isAnd) {
                                    q = q.WhereLike(rule.FieldName, "%" + processedData + "%");
                                }
                                else {
                                    var q2 = source.WhereLike(rule.FieldName, "%" + processedData + "%");
                                    q = q.Union(q2);
                                }
                        break;
                    case FilterRuleOperation.NotContains:

                                if(isAnd) {
                                    q = q.WhereNotLike(rule.FieldName, "%" + processedData  + "%");
                                }
                                else {
                                    var q2 = source.WhereNotLike(rule.FieldName, "%" + processedData  + "%");
                                    q = q.Union(q2);
                                }
                        break;
                    default:
                        break;
                }

            }

            return q;
        }

        //private static IQueryable<T> Combine<T>(IQueryable<T> t1, IQueryable<T> t2, FilterGroupOperation op)
        //{
        //    if (op == FilterGroupOperation.And)
        //    {
        //        return t1.
        //    }
        //}

        /// <summary>
        /// Поменять звездочки (*) на знаки процента. Все проценты
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        private static string PreprocessWildcards(string condition, bool trim)
        {
            if (condition != null)
            {
                string s = condition.Replace('*', '%');
                if(trim) {
                    s = s.Trim(new char[] { '%' });
                }
                return s;
            }
            else
            {
                return String.Empty;
            }
        }

        private static bool ContainsWildcards(string condition)
        {
            if (condition != null)
            {
                return condition.Contains('*');
            }
            else
            {
                return false;
            }
        }
    }
}
