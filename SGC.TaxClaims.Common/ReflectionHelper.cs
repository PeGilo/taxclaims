﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace SGC.TaxClaims.Common
{
    /// <summary>
    /// Возвращает тип свойства класса.
    /// </summary>
    /// <param name="classType"></param>
    /// <param name="propertyName"></param>
    /// <returns></returns>
    public static class ReflectionHelper
    {
        public static Type GetPropertyType(Type classType, string propertyName)
        {
            if (String.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentOutOfRangeException("Имя свойства не должно быть пустым.", propertyName);
            }

            string[] splits = propertyName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            Type currentType = classType;
            PropertyInfo prop = null;

            int i = 0;
            do
            {
                prop = currentType.GetProperty(splits[i]);

                if (prop != null)
                {
                    currentType = prop.PropertyType;
                }
                else
                {
                    throw new InvalidOperationException("Тип не имеет свойства " + propertyName);
                }

            } while (++i < splits.Length);

            return currentType;
        }

        public static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;

            if (memberExpression == null)
                return null;

            return memberExpression.Member.Name;
        }
    }
}
