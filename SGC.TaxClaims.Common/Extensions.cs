﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Data.Linq.SqlClient;

namespace SGC.TaxClaims.Common
{
    public static class Extensions
    {
        public static IQueryable<T> WhereLike<T>(this IQueryable<T> source, string propertyName, string pattern)
        {
            //if (null == source) throw new ArgumentNullException("source");
            //if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException("propertyName");


            //var a = Expression.Parameter(typeof(T), "a");
            //var prop = Expression.Property(a, propertyName);
            //var body = Expression.Call(typeof(SqlMethods), "Like", null, prop, Expression.Constant(pattern));
            //var fn = Expression.Lambda<Func<T, bool>>(body, a);


            return source.Where(source.LikeCondition(propertyName, pattern));
        }

        public static IQueryable<T> WhereNotLike<T>(this IQueryable<T> source, string propertyName, string pattern)
        {
            //if (null == source) throw new ArgumentNullException("source");
            //if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException("propertyName");


            //var a = Expression.Parameter(typeof(T), "a");
            //var prop = Expression.Property(a, propertyName);
            //var body = Expression.Call(typeof(SqlMethods), "Like", null, prop, Expression.Constant(pattern));
            //var fn = Expression.Lambda<Func<T, bool>>(Expression.Not(body), a);


            return source.Where(source.NotLikeCondition(propertyName, pattern));
        }

        public static Expression<Func<T, bool>> LikeCondition<T>(this IQueryable<T> source, string propertyName, string pattern)
        {
            if (null == source) throw new ArgumentNullException("source");
            if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException("propertyName");


            var a = Expression.Parameter(typeof(T), "a");
            var prop = Expression.Property(a, propertyName);
            var body = Expression.Call(typeof(SqlMethods), "Like", null, prop, Expression.Constant(pattern));
            var fn = Expression.Lambda<Func<T, bool>>(body, a);


            return fn;
        }

        public static Expression<Func<T, bool>> NotLikeCondition<T>(this IQueryable<T> source, string propertyName, string pattern)
        {
            if (null == source) throw new ArgumentNullException("source");
            if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException("propertyName");


            var a = Expression.Parameter(typeof(T), "a");
            var prop = Expression.Property(a, propertyName);
            var body = Expression.Call(typeof(SqlMethods), "Like", null, prop, Expression.Constant(pattern));
            var fn = Expression.Lambda<Func<T, bool>>(Expression.Not(body), a);


            return fn;
        }
    }
}
