﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Contracts;

namespace SGC.TaxClaims.Infrastructure
{
    public static class Logger
    {
        private static bool m_isInitialized = false;
        private static ILog m_log;

        private static Dictionary<string, ILog> _m_loggers = new Dictionary<string, ILog>();

        private static ILog getLogger(string name)
        {
            lock (typeof(Logger))
            {
                ensureInitialized();

                ILog log;

                if (!_m_loggers.TryGetValue(name, out log))
                {
                    log = LogManager.GetLogger(name);
                    _m_loggers[name] = log;

                }

                return log;
            }
        }

        private static void ensureInitialized()
        {
            if (!m_isInitialized)
            {
                initialize();
                m_isInitialized = true;
            }
        }

        private static void initialize()
        {
            XmlConfigurator.Configure();
        }

        public static void Debug(string loggerName, string message)
        {
            getLogger(loggerName).Debug(message);
        }

        public static void Info(string loggerName, string message)
        {
            getLogger(loggerName).Info(message);
        }

        public static void Error(string loggerName, string message, Exception ex)
        {
            ILog logWriter = getLogger(loggerName);

            StringBuilder fullMessage = new StringBuilder(message);
            if (ex != null)
            {
                fullMessage.Append(". \nMessage: " + ex.Message + ". \nSource: " + ex.Source + ". \nStacktrace: " + ex.StackTrace);
                if (ex.InnerException != null)
                {
                    fullMessage.Append(". \nInnerException: \nMessage: " + ex.InnerException.Message
                        + ". \nSource: " + ex.InnerException.Source
                        + ". \nStackTrace: " + ex.InnerException.StackTrace);
                }
            }
            logWriter.Error(fullMessage.ToString() + Environment.NewLine);
        }

        public static void Error(string loggerName, string message, OperationResult result)
        {
            ILog logWriter = getLogger(loggerName);

            logWriter.Error(String.Format("{0}\r\nSource={1} Code={2} - {3}", message, result.Source, result.Code, result.Message));
        }

        public static void Error(string loggerName, string message)
        {
            getLogger(loggerName).Error(message);
        }

        /// <summary>
        /// Создает логер для логирования сообщений по работе самого приложения
        /// </summary>
        /// <returns></returns>
        public static ILogger CreateAppLogger()
        {
            return new LoggerProxy("TaxClaims.Web");
        }

        /// <summary>
        /// Создает логер для конкретной подсистемы
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ILogger CreateSystemLogger<T>()
        {
            if (typeof(T) == typeof(IDBRepository))
            {
                return new LoggerProxy("TaxClaims.Database");
            }
            else if (typeof(T) == typeof(ISaperion))
            {
                return new LoggerProxy("TaxClaims.Saperion");
            }
            throw new NotImplementedException("Логер для данной системы не создан.");
        }

        ///// <summary>
        ///// Создает логер для логирования сообщений, получаемых сервисом
        ///// </summary>
        ///// <returns></returns>
        //public static ILogger CreateMessageLogger()
        //{
        //    return new LoggerProxy("IM2PBImport.Message");
        //}

        private class LoggerProxy : ILogger
        {
            private string _loggerName;

            public LoggerProxy(string loggerName)
            {
                _loggerName = loggerName;
            }

            public void Debug(string message)
            {
                Logger.Debug(_loggerName, message);
            }

            public void Info(string message)
            {
                Logger.Info(_loggerName, message);
            }

            public void Error(string message, Exception ex)
            {
                Logger.Error(_loggerName, message, ex);
            }

            public void Error(string message, OperationResult result)
            {
                Logger.Error(_loggerName, message, result);
            }

            public void Error(string message)
            {
                Logger.Error(_loggerName, message);
            }
        }

    }
}
