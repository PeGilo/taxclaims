﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Infrastructure.Saperion
{
    public class SPRCursor: IDisposable
    {
        private dynamic _cursorObj;
        private bool _isDisposed = false;

        public SPRCursor(dynamic cursorObj)
        {
            _cursorObj = cursorObj;
        }
        /// <summary>
        /// Перемещает курсор на первую позицию
        /// </summary>
        /// <returns>Возвращает ложь если нет записей в курсоре</returns>
        public bool First()
        {
            return Convert.ToBoolean(_cursorObj.First());
        }
        /// <summary>
        /// Перемещает курсор на следующую позицию
        /// </summary>
        /// <returns>Возвращает истину если следующая строка есть, если нет, то ложь</returns>
        public bool Next()
        {
            return Convert.ToBoolean(_cursorObj.Next());
        }
        /// <summary>
        /// Количество записей в курсоре
        /// </summary>
        public int Count { get { return Convert.ToInt32(_cursorObj.Count); } }
        /// <summary>
        /// Получает текущий документ
        /// </summary>
        public SPRDocument Document { get { return new SPRDocument(_cursorObj.Document); } }
        /// <summary>
        /// Закрывает курсор и уничтажает все ресурсы
        /// </summary>
        public void Close()
        {
            _cursorObj.Close();
        }

        protected virtual void Disposing(bool isDisposing)
        {
            if (_isDisposed) return;
            if (_cursorObj == null) return;
            _cursorObj.Close();
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_cursorObj);
            _cursorObj = null;
            _isDisposed = true;
        }

        public void Dispose()
        {
            Disposing(true);
        }

        ~SPRCursor()
        {
            Disposing(false);
        }
    }
}
