﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Infrastructure.Saperion
{
    public class SPRApplication: IDisposable
    {
        private bool _isDisposed = false;
        private dynamic _appObj;
        public SPRApplication()
        {
            Type typeApp = Type.GetTypeFromProgID("Saperion.Application");
            _appObj = Activator.CreateInstance(typeApp);
        }

        public SPRCursor SelectSQL(string table, string sql)
        {
            dynamic cursor = _appObj.SelectSQL(table,sql);
            return new SPRCursor(cursor);
        }

        public void Login(string login, string password)
        {
            _appObj.Login(login, password, 1);
        }

        protected virtual void Disposing(bool isDisposing)
        {
            if (_isDisposed) return;
            if (_appObj == null) return;
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_appObj);
            _appObj = null;
            _isDisposed = true;
        }

        public void Dispose()
        {
            Disposing(true);
        }

        ~SPRApplication()
        {
            Disposing(false);
        }
    }
}
