﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Infrastructure.Saperion
{
    public class SPRDocument: IDisposable
    {
        private dynamic _docObj;
        private bool _isDisposed = false;

        public SPRDocument(dynamic docObj)
        {
            _docObj = docObj;
        }

        public string HexUID { get { return Convert.ToString(_docObj.HexUID); } }

        public bool Load()
        {
            return Convert.ToBoolean(_docObj.Load());
        }

        public string GetPackageIndex()
        {
            return Convert.ToString(_docObj.GetProperty("18_INDEX_PAKETA"));
        }

        public SPRDocument SubDocument(int index)
        {
            return new SPRDocument(_docObj.SubDocument(index));
        }

        public int GetNumElements()
        {
            return Convert.ToInt32(_docObj.NumElems);
        }

        public void SaveAs(long element,string toFileName)
        {
            _docObj.SaveAs(element, toFileName);
        }

        public string GetFileName()
        {
            return Convert.ToString(_docObj.FileName);
        }

        protected virtual void Disposing(bool isDisposing)
        {
            if (_isDisposed) return;
            if (_docObj == null) return;
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_docObj);
            _docObj = null;
            _isDisposed = true;
        }

        public void Dispose()
        {
            Disposing(true);
        }

        ~SPRDocument()
        {
            Disposing(false);
        }
    }
}