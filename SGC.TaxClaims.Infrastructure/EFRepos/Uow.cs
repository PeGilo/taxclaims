﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Infrastructure.Model;

namespace SGC.TaxClaims.Infrastructure.EFRepos
{
    public interface IUnityLocker
    {
        void AcquireLock(string owner, LockResource resource);
        void ReleaseLock(string owner, LockResource resource);
    }

    /// <summary>
    /// The "Unit of Work"
    ///     1) decouples the repos from the controllers
    ///     2) decouples the DbContext and EF from the controllers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="Transport"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext).
    /// </remarks>
    public class Uow : IUow, IUnityLocker, IDisposable
    {
        private LockManager _lockManager;
        private List<Tuple<string, LockResource>> _acquiredLocks;

        //private Dictionary<string

        public Uow(IRepositoryProvider repositoryProvider, LockManager lockManager)
        {
            _lockManager = lockManager;
            _acquiredLocks = new List<Tuple<string, LockResource>>();

            CreateDbContext();

            repositoryProvider.DbContext = DbContext;
            repositoryProvider.UnityLocker = this;
            RepositoryProvider = repositoryProvider;
        }

        // Repositories
        public IUserRepository Users { get { return GetRepo<IUserRepository>(); } }

        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        public void Commit()
        {
            try
            {
                DbContext.SaveChanges();
            }
            //catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            //{
            //    // Достать SQLException
            //    //
            //    Exception parentEx = ex;
            //    if (ex.InnerException != null && ex.InnerException is System.Data.UpdateException)
            //    {
            //        parentEx = ex.InnerException;
            //    }

            //    var sqlException = parentEx.InnerException as System.Data.SqlClient.SqlException;

            //    if (sqlException != null && sqlException.Errors.OfType<SqlError>()
            //        .Any(se => se.Number == 2601 || se.Number == 2627 /* PK/UKC violation */))
            //    {

            //        throw new EBM.Core.Exceptions.UniqueConstraintException("Нарушение ограничения уникальности", sqlException);
            //    }
            //    else
            //    {
            //        // it's something else...
            //        throw;
            //    }
            //}
            finally
            {
                ReleaseAllLocks();
            }
        }

        /// <summary>
        /// Используется для отмены операция, чтобы повторно использовать DbContext.
        /// </summary>
        public void UndoChanges()
        {
            var context = DbContext;
            var changedEntries = context.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged).ToList();

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Modified))
            {
                entry.CurrentValues.SetValues(entry.OriginalValues);
                entry.State = EntityState.Unchanged;
            }

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Added))
            {
                entry.State = EntityState.Detached;
            }

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Deleted))
            {
                entry.State = EntityState.Unchanged;
            }
        }

        public void Rollback()
        {
            ReleaseAllLocks();
        }

        protected void CreateDbContext()
        {
            DbContext = new DataEntities();

            // Do NOT enable proxied entities, else serialization fails
            DbContext.Configuration.ProxyCreationEnabled = false;

            // Load navigation properties explicitly (avoid serialization trouble)
            DbContext.Configuration.LazyLoadingEnabled = false;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        protected IRepositoryProvider RepositoryProvider { get; set; }

        //private IRepository<T> GetStandardRepo<T>() where T : class
        //{
        //    return RepositoryProvider.GetRepositoryForEntityType<T>();
        //}
        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        private DataEntities DbContext { get; set; }

        #region IUnityLocker

        public void AcquireLock(string owner, LockResource resource)
        {
            _lockManager.AcquireLock(owner, resource);
            _acquiredLocks.Add(new Tuple<string, LockResource>(owner, resource));
        }

        public void ReleaseLock(string owner, LockResource resource)
        {
            int index = _acquiredLocks.FindIndex(
                tu => tu.Item1 == owner && tu.Item2 == resource);

            if (index >= 0)
            {
                _acquiredLocks.RemoveAt(index);
            }

            _lockManager.ReleaseLock(owner, resource);

        }

        private void ReleaseAllLocks()
        {
            foreach (var lck in _acquiredLocks)
            {
                _lockManager.ReleaseLock(lck.Item1, lck.Item2);
            }
            _acquiredLocks.Clear();
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}
