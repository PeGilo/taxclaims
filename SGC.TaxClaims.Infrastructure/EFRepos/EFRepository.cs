﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SGC.TaxClaims.Core.Contracts;

namespace SGC.TaxClaims.Infrastructure.EFRepos
{
    /// <summary>
    /// The EF-dependent, generic repository for data access
    /// </summary>
    /// <typeparam name="T">Type of entity for this Repository.</typeparam>
    public class EFRepository<T> : IRepository<T> where T : class
    {
        public EFRepository(DbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException("dbContext");
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }

        protected DbContext DbContext { get; set; }

        protected DbSet<T> DbSet { get; set; }

        public virtual void SetUnchanged(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry != null)
            {
                dbEntityEntry.State = EntityState.Unchanged;
                //dbEntityEntry.
                //DbContext.Chan.ObjectStateManager.ChangeObjectState(employee.Department,
                //                                 EntityState.Unchanged);
            }
        }

        public virtual IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public virtual T GetById(int id)
        {
            //return DbSet.FirstOrDefault(PredicateBuilder.GetByIdPredicate<T>(id));
            return DbSet.Find(id);
        }

        public virtual void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public virtual void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void UpdateList(IEnumerable<T> entities)
        {
            IObjectContextAdapter adapter = DbContext as IObjectContextAdapter;
            if (adapter != null)
            {
                foreach (T entity in entities)
                {
                    AddOrUpdateEntity(adapter, entity);
                }
            }
            else
            {
                throw new InvalidOperationException("DbContext is not IObjectContextAdapter");
            }
        }

        protected virtual void AddOrUpdateEntity(IObjectContextAdapter adapter, T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            object pkValue = GetPrimaryKeyValue(adapter, dbEntityEntry);
            if (pkValue is Int32)
            {
                int pk = (Int32)pkValue;
                if (pk == 0)
                {
                    Add(entity);
                }
                else
                {
                    Update(entity);
                }
            }
            else
            {
                throw new InvalidOperationException("Primary key is not an integer");
            }
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }

        public virtual void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null) return; // not found; assume already deleted.
            Delete(entity);
        }

        protected virtual object GetPrimaryKeyValue(IObjectContextAdapter adapter, DbEntityEntry entry)
        {
            var objectStateEntry = adapter.ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            return objectStateEntry.EntityKey.EntityKeyValues[0].Value;
        }
    }
}
