﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Infrastructure.EFRepos
{
    public enum LockResource
    {
        None
        //Violation,
        //ViolationType
    }

    public class LockManager : IDisposable
    {
        private static readonly Object _locker = new Object();

        /// <summary>
        /// resource - mutex
        /// </summary>
        private Dictionary<string, Mutex> _dictionary;

        /// <summary>
        /// Обозначает был ли уже disposed данный объект
        /// </summary>
        private bool _isValid = true;

        public LockManager()
        {
            _dictionary = new Dictionary<string, Mutex>();
        }

        public void AcquireLock(string owner, LockResource resource)
        {
            if (!_isValid)
            {
                throw new InvalidOperationException("Object has already been disposed");
            }

            // If not locked - lock
            // If locked - wait

            Mutex mut;

            lock (_locker)
            {
                if (!_dictionary.TryGetValue(resource.ToString(), out mut))
                {
                    mut = new Mutex();
                    _dictionary[resource.ToString()] = mut;

                    Debug.WriteLine("Mutex created for resource: " + resource.ToString());
                }
            }

            mut.WaitOne();
        }

        public void ReleaseLock(string owner, LockResource resource)
        {
            if (!_isValid)
            {
                throw new InvalidOperationException("Object has already been disposed");
            }

            Mutex mut;

            if (_dictionary.TryGetValue(resource.ToString(), out mut))
            {
                Debug.WriteLine("Release mutex for resource: " + resource.ToString());

                mut.ReleaseMutex();
            }
        }

        //public void ReleaseAllLocks(string owner)
        //{
        //    if (!_isValid)
        //    {
        //        throw new InvalidOperationException("Object has already been disposed");
        //    }

        //    lock (_locker)
        //    {
        //        foreach (string key in _dictionary.Keys)
        //        {
        //            if (key.StartsWith(owner))
        //            {
        //                _dictionary[key].ReleaseMutex();
        //            }
        //        }
        //    }
        //}

        //public void Dispose()
        //{
        //    // RELEASE ALL MUTEXES
        //}

        #region Disposing

        ~LockManager()
        {
            // Так как, это деструктор (вызывается сборщиком мусора), то надо 			
            // указать, что не надо удалять
            // managed объекты, потому что, они могут быть уже удалены
            // сборщиком мусора
            Dispose(false);
        }

        // Этот метод вызывает пользователь, когда хочет явно освободить
        // unmanaged ресурсы
        public void Dispose()
        {
            // Так как все ресурсы (managed и unmanaged) будет полностью
            // удалены, то надо указать сборщику мусора не запускаться
            // второй раз
            GC.SuppressFinalize(this);

            // Очистить все ресурсы (managed и unmanaged)
            // так как вызов пользователя, а не сборщика
            Dispose(true);
        }

        // Синоним метода Dispose(), для удобства пользователя
        public void Close()
        {
            Dispose();
        }

        // private – если класс sealed
        // protected – в других случаях, но не public
        private void Dispose(Boolean disposing)
        {
            lock (this)
            {
                if (disposing)
                {
                    // удаление вызвал пользователь, а не сборщик
                    // Сборщик мусора еще не запущен, можно удалять
                    // managed объекты

                    foreach (string key in _dictionary.Keys)
                    {
                        _dictionary[key].Dispose();
                    }
                    _dictionary.Clear();
                }

                // Здесь удаление unmanaged объектов
                // Проверка, что еще не был удален
                if (_isValid)
                {

                }
                _isValid = false;
            }
        }

        #endregion
    }
}
