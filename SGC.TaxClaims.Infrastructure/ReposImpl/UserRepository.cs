﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Infrastructure.EFRepos;

namespace SGC.TaxClaims.Infrastructure.ReposImpl
{
    public class UserRepository : EFRepository<aspnet_Users>, IUserRepository
    {
        private IUnityLocker _unityLocker;

        public UserRepository(DbContext context, IUnityLocker unityLocker)
            : base(context)
        {
            _unityLocker = unityLocker;
        }
    }
}
