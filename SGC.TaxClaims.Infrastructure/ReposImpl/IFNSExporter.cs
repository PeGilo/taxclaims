﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.ExportIFNS;
using System.IO;
using SGC.TaxClaims.Core.Contracts;

namespace SGC.TaxClaims.Infrastructure.ReposImpl
{
    public class IFNSExporter : IIFNSExporter
    {
        private ILogger _logger;
        public OperationResult CreateArchive(
            string fullFileName, 
            ExpClaim claim, 
            ExpFileInfo[] xmlFiles, 
            ExpFileInfo[] imageFiles)
        {
            try
            {
                if (File.Exists(fullFileName)) 
                    File.Delete(fullFileName);//Если такой файл есть, то удаляем.
                using (var fileStream = File.Open(fullFileName, FileMode.Create))
                {
                    using (ZipArchive archive = new ZipArchive(fileStream, ZipArchiveMode.Create))
                    {
                        //Записываем пакет
                        var packageEntry = archive.CreateEntry("package.xml");
                        WriteText(packageEntry.Open(), claim.CreatePackage());
                        //Записываем требование
                        var claimEntry = archive.CreateEntry("claim.xml");
                        WriteText(claimEntry.Open(), claim.CreateClaim());
                        //Записываем все xml файлы
                        if (xmlFiles != null && xmlFiles.Length > 0)
                            foreach (var xmlFile in xmlFiles)
                            {
                                var xmlFileEntry = archive.CreateEntry(ExpDocument.XmlDirectory + "/" + xmlFile.Name);
                                WriteBinary(xmlFileEntry.Open(), xmlFile.Content);
                            }
                        //Заполняем все image файлы
                        if (imageFiles != null && imageFiles.Length > 0)
                            foreach (var imageFile in imageFiles)
                            {
                                var xmlFileEntry = archive.CreateEntry(ExpDocument.ImageDirectory + "/" + imageFile.Name);
                                WriteBinary(xmlFileEntry.Open(), imageFile.Content);
                            }
                    }
                }
                return OperationResult.Succeed;
            }
            catch(Exception ex)
            {
                return GetOperationResult(ex);
            }
        }
        //Записывает текст
        private void WriteText(Stream stream,string text)
        {
            if (text == null) return;
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(text);
            }
        }
        //записывает файл
        private void WriteBinary(Stream stream,byte[] file)
        {
            if (file == null || file.Length == 0) return;
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(file,0,file.Length);
            }
        }

        public OperationResult GetOperationResult(Exception exception)
        {
            int codeEx = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            int code = codeEx == 0 ? 1 : codeEx;
            string message = exception.ToString();
            string source = OperationResult.WebSite;
            WriteToLog(message);
            return new OperationResult(code, message, source, true);
        }

        private void WriteToLog(string message)
        {
            if (_logger == null) return;
            lock (_logger)
            {
                _logger.Error(message);
            }
        }

        public ILogger Logger
        {
            get
            {
                return _logger;
            }
            set
            {
                _logger = value;
            }
        }
    }
}
