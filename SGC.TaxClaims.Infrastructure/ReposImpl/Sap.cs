﻿using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
namespace SGC.TaxClaims.Infrastructure.ReposImpl
{
    public class Sap: ISap
    {
        private IDBRepository _dbRepository;
        
        private ILogger _logger;

        public Sap(
            IDBRepository dbRepository
            )
        {
            _dbRepository = dbRepository;
        }

        public OperationResult<Guid> LoadXml(Guid claimUid, string userNameFull)
        {
            try
            {
                
                var sapFolder = System.Configuration.ConfigurationManager.AppSettings["SapFolder"];
                DirectoryInfo directory = new DirectoryInfo(sapFolder);
                sapFolder = sapFolder.EndsWith("\\") ? sapFolder : sapFolder + "\\";
                string[] parts = userNameFull.Split('\\');
                //Получаем имя пользователя без префикса sibgenco
                string userName = parts.Length == 2 ? parts[1] : parts[0];
                //Получаем все xml файлы, которые содержат имя пользователя.
                var files = directory.GetFiles().Where(
                    file =>
                    file.Name.ToLower().EndsWith(".xml")
                        &&
                    file.Name.ToLower().Contains(userName.ToLower()));
                if (files.Count() == 0) 
                    return new OperationResult<Guid>(OperationResult.SapFileNotFound, Guid.Empty);
                //Получают максимальную дату создания.
                DateTime dateCreationMax = files.Max(file => file.CreationTime);
                //Получаем необходимый файл.
                var fileXml = files.Where(file => file.CreationTime == dateCreationMax).FirstOrDefault();
                //Читаем файл.
                string fullName = sapFolder + fileXml.Name;
                var document = XDocument.Load(fullName);
                string docstring = document.ToString();
                return _dbRepository.InsertRegistry(document, claimUid, userNameFull);
                //return CreateRegistryFromXmlFast(document, claimUid, userName,userNameFull);
            }
            catch (Exception exception)
            {
                var result = GetOperationResult(exception);
                return new OperationResult<Guid>(result, Guid.Empty);
            }
        }

        /*private OperationResult<Guid> CreateRegistryFromXml(XDocument xml, Guid claimUid, string userName,string userNameFull)
        {
            var registryXml = xml.XPathSelectElement("Файл/Книга");
            string bookType = registryXml.Attribute("ТипКн").Value;
            string user = registryXml.Attribute("Пользователь").Value;
            string dateCreate = registryXml.Attribute("ДатаФорм").Value;
            string timeCreate = registryXml.Attribute("ВремяФорм").Value;
            var docs = registryXml.XPathSelectElements("СведДок");
            //Получаем пользователя
            var getUserResult = _dbRepository.GetUserByName(userNameFull);
            if (!getUserResult.IsSucceed)
                return new OperationResult<Guid>(OperationResult.Succeed, Guid.Empty);
            var aspUser = getUserResult.Data;
            if (aspUser == null)//Если не найден пользователь.
                return new OperationResult<Guid>(OperationResult.UserNotFound,Guid.Empty);
            Registry registry = new Registry();
            registry.ChangeStatus = System.Data.Entity.EntityState.Added;
            Guid registryUid = Guid.NewGuid();
            registry.UId = registryUid;
            registry.DocTypeId = 2;
            registry.DocStateId = 2;
            registry.Number = user + " " + dateCreate;
            DateTime dateCreated = DateTime.Now; //DateTime.ParseExact(dateCreate + " " + timeCreate, "dd.MM.yyyy HH:mm:ss", null);
            registry.DateCreated = dateCreated;
            registry.CreatorUserUid = aspUser.UserId;
            List<RegistryDoc> registryDocs = new List<RegistryDoc>();
            foreach (var doc in docs)
            {
                RegistryDoc registryDoc = new RegistryDoc();
                registryDoc.UId = Guid.NewGuid();
                registryDoc.DocTypeId = 0;
                registryDoc.DocStateId = registry.DocStateId;
                registryDoc.CreatorUserUid = aspUser.UserId;
                registryDoc.ChangeStatus = System.Data.Entity.EntityState.Added;
                registryDoc.DateCreated = DateTime.Now;
                registryDoc.DocSourceId = 1;
                if (doc.Attribute("КодЗав") != null)
                    registryDoc.FactoryCode = doc.Attribute("КодЗав").Value;
                if (doc.Attribute("НаимЗав") != null)
                    registryDoc.FactoryName = doc.Attribute("НаимЗав").Value;
                if (doc.Attribute("НаимПок") != null)
                    registryDoc.PartnerName = doc.Attribute("НаимПок").Value;
                if (doc.Attribute("ИННПок") != null)
                    registryDoc.PartnerINN = doc.Attribute("ИННПок").Value;
                if (doc.Attribute("КПППок") != null)
                    registryDoc.PartnerKPP = doc.Attribute("КПППок").Value;
                if (doc.Attribute("НомСчФ") != null)
                    registryDoc.Number = doc.Attribute("НомСчФ").Value;
                if (doc.Attribute("ДатаСчф") != null)
                    registryDoc.DocDate = DateTime.ParseExact(doc.Attribute("ДатаСчф").Value, "dd.MM.yyyy", null);
                if (doc.Attribute("НомерДокОсн") != null)
                    registryDoc.ParentDocNumber = doc.Attribute("НомерДокОсн").Value;
                if (doc.Attribute("ДатаДокОсн") != null)
                    registryDoc.ParentDocDate = DateTime.ParseExact(doc.Attribute("ДатаДокОсн").Value, "dd.MM.yyyy", null);
                if (doc.Attribute("СтоимСчФВс") != null)
                    registryDoc.SumTotal = string.IsNullOrWhiteSpace(doc.Attribute("СтоимСчФВс").Value) ? (decimal)0 : Convert.ToDecimal(doc.Attribute("СтоимСчФВс").Value, new CultureInfo("en-US"));
                if (doc.Attribute("СумНДССчф") != null)
                    registryDoc.SumTax = string.IsNullOrWhiteSpace(doc.Attribute("СумНДССчф").Value) ? (decimal)0 : Convert.ToDecimal(doc.Attribute("СумНДССчф").Value, new CultureInfo("en-US"));
                if (doc.Attribute("ПризФО") != null)
                    registryDoc.IsScan = Convert.ToInt32(doc.Attribute("ПризФО").Value) != 2;
                if (doc.Attribute("НомерПак") != null && !string.IsNullOrWhiteSpace(doc.Attribute("НомерПак").Value))
                    registryDoc.PackageIndex = doc.Attribute("НомерПак").Value;
                if (doc.Attribute("ВидКорр") != null)
                    if (doc.Attribute("ВидКорр").Value == "C")
                        registryDoc.CorrectionTypeId = 1;
                    else if (doc.Attribute("ВидКорр").Value == "R")
                        registryDoc.CorrectionTypeId = 2;
                    else if (doc.Attribute("ВидКорр").Value == "S")
                        registryDoc.CorrectionTypeId = 3;
                registryDocs.Add(registryDoc);
            }
            //SqlBulkCopy bl = new SqlBulkCopy(null);
            var operationResult = _dbRepository.InsertRegistry(registry, registryDocs.ToArray(), claimUid);
            return new OperationResult<Guid>(operationResult, registryUid);
        }*/

        /*private OperationResult<Guid> CreateRegistryFromXmlFast(XDocument xml, Guid claimUid, string userName, string userNameFull)
        {
            var registryXml = xml.XPathSelectElement("Файл/Книга");
            string bookType = registryXml.Attribute("ТипКн").Value;
            string user = registryXml.Attribute("Пользователь").Value;
            string dateCreate = registryXml.Attribute("ДатаФорм").Value;
            string timeCreate = registryXml.Attribute("ВремяФорм").Value;
            var docs = registryXml.XPathSelectElements("СведДок");
            //Получаем пользователя
            var getUserResult = _dbRepository.GetUserByName(userNameFull);
            if (!getUserResult.IsSucceed)
                return new OperationResult<Guid>(OperationResult.Succeed, Guid.Empty);
            var aspUser = getUserResult.Data;
            if (aspUser == null)//Если не найден пользователь.
                return new OperationResult<Guid>(OperationResult.UserNotFound, Guid.Empty);
            DataTable registryTb = DBRepository.CreateRegistryTable();
            DataTable documentTb = DBRepository.CreateDocumentTable();
            DataTable registryDocTb = DBRepository.CreateRegistryDocTable();
            //DataTable docRelationTable = DBRepository.CreateDocRelationTable();

            //Создаем реестр
            //Добавляем в таблицу document
            var documentRow = documentTb.NewRow();
            Guid registryUid = Guid.NewGuid();
            documentRow["UId"] = registryUid;
            documentRow["DocTypeId"] = 2;
            documentRow["DocStateId"] = 2;
            documentRow["Number"] = user + " " + dateCreate;
            DateTime dateCreated = DateTime.Now;
            documentRow["DateCreated"] = dateCreated;
            documentRow["CreatorUserUid"] = aspUser.UserId;
            documentRow["ParentDocUId"] = claimUid;
            documentRow["DocSourceId"] = 1;
            documentTb.Rows.Add(documentRow);
            //Документ добавляется в Registry
            var registryRow = registryTb.NewRow();
            registryRow["UId"] = registryUid;
            registryTb.Rows.Add(registryRow);
            int counter = 0;
            foreach (var doc in docs)
            {
                Guid documentUid = Guid.NewGuid();
                documentRow = documentTb.NewRow();
                documentRow["UId"] = documentUid;
                documentRow["DocTypeId"] = 0;
                documentRow["DocStateId"] = 2;
                documentRow["CreatorUserUid"] = aspUser.UserId;
                documentRow["DateCreated"] = dateCreated;
                documentRow["DocSourceId"] = 1; 
                documentRow["ParentDocUId"] = registryUid;
                var registryDocRow = registryDocTb.NewRow();
                registryDocRow["UId"] = documentUid;

                if (doc.Attribute("КодЗав") != null)
                    registryDocRow["FactoryCode"] = doc.Attribute("КодЗав").Value;
                if (doc.Attribute("НаимЗав") != null)
                    registryDocRow["FactoryName"] = doc.Attribute("НаимЗав").Value;
                if (doc.Attribute("НаимПок") != null)
                    registryDocRow["PartnerName"] = doc.Attribute("НаимПок").Value;
                if (doc.Attribute("ИННПок") != null)
                    registryDocRow["PartnerINN"] = doc.Attribute("ИННПок").Value;
                if (doc.Attribute("КПППок") != null)
                    registryDocRow["PartnerKPP"] = doc.Attribute("КПППок").Value;
                if (doc.Attribute("НомСчФ") != null)
                    documentRow["Number"] = doc.Attribute("НомСчФ").Value;
                if (doc.Attribute("ДатаСчф") != null)
                    documentRow["DocDate"] = DateTime.ParseExact(doc.Attribute("ДатаСчф").Value, "dd.MM.yyyy", null);
                if (doc.Attribute("НомерДокОсн") != null)
                    registryDocRow["ParentDocNumber"] = doc.Attribute("НомерДокОсн").Value;
                if (doc.Attribute("ДатаДокОсн") != null)
                    registryDocRow["ParentDocDate"] = DateTime.ParseExact(doc.Attribute("ДатаДокОсн").Value, "dd.MM.yyyy", null);
                if (doc.Attribute("СтоимСчФВс") != null)
                    registryDocRow["SumTotal"] = string.IsNullOrWhiteSpace(doc.Attribute("СтоимСчФВс").Value) ? (decimal)0 : Convert.ToDecimal(doc.Attribute("СтоимСчФВс").Value, new CultureInfo("en-US"));
                if (doc.Attribute("СумНДССчф") != null)
                    registryDocRow["SumTax"] = string.IsNullOrWhiteSpace(doc.Attribute("СумНДССчф").Value) ? (decimal)0 : Convert.ToDecimal(doc.Attribute("СумНДССчф").Value, new CultureInfo("en-US"));
                if (doc.Attribute("ПризФО") != null)
                    registryDocRow["IsScan"] = Convert.ToInt32(doc.Attribute("ПризФО").Value) != 2;
                else
                    registryDocRow["IsScan"] = true;
                if (doc.Attribute("НомерПак") != null && !string.IsNullOrWhiteSpace(doc.Attribute("НомерПак").Value))
                    registryDocRow["PackageIndex"] = doc.Attribute("НомерПак").Value;
                if (doc.Attribute("ВидКорр") != null)
                    if (doc.Attribute("ВидКорр").Value == "C")
                        registryDocRow["CorrectionTypeId"] = 1;
                    else if (doc.Attribute("ВидКорр").Value == "R")
                        registryDocRow["CorrectionTypeId"] = 2;
                    else if (doc.Attribute("ВидКорр").Value == "S")
                        registryDocRow["CorrectionTypeId"] = 3;
                //Добавляем связь//
                /*var relationRow = docRelationTable.NewRow();
                relationRow["ParentDocUId"] = registryUid;
                relationRow["ChildDocUId"] = documentUid;
                relationRow["DocRelTypeId"] = 1;
                docRelationTable.Rows.Add(relationRow);*/
               /* documentTb.Rows.Add(documentRow);
                registryDocTb.Rows.Add(registryDocRow);
            }

            var operationResult = _dbRepository.InsertRegistry(registryTb, documentTb, registryDocTb);
            return new OperationResult<Guid>(operationResult, registryUid);
        }*/

        

        public OperationResult GetOperationResult(Exception exception)
        {
            int codeEx = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            int code = codeEx == 0 ? 1 : codeEx;
            string message = exception.ToString();
            string source = OperationResult.WebSite;
            WriteToLog(message);
            return new OperationResult(code, message, source, true);
        }

        private void WriteToLog(string message)
        {
            if (_logger == null) return;
            lock (_logger)
            {
                _logger.Error(message);
            }
        }

        public ILogger Logger
        {
            get
            {
                return _logger;
            }
            set
            {
                _logger = value;
            }
        }
    }
}
