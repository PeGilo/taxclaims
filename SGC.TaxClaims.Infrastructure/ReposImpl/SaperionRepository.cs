﻿//using Com.Saperion.UBI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
//using Saperion;
using Com.Saperion.UBI;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Saperion;
using SGC.TaxClaims.Infrastructure.Saperion;
using System.IO;
using SGC.TaxClaims.Core.Contracts;
using System.Collections;

namespace SGC.TaxClaims.Infrastructure
{

    public class SaperionRepository: ISaperion
    {
        private bool _isDisposed = false;
        private SPRApplication _application;
        private ILogger _logger;
        private string _login;
        private string _password;
        private bool _needLogined = true;
        public SaperionRepository()
        {
            _application = new SPRApplication();
            _login = System.Configuration.ConfigurationManager.AppSettings["SapirionLogin"];
            _password = System.Configuration.ConfigurationManager.AppSettings["SapirionPassword"];
        }

        public OperationResult<SPRDocInfo[]> GetIndexes(string[] docNumbers)
        {
            List<SPRDocInfo> indexes = new List<SPRDocInfo>();
            OperationResult result = OperationResult.Succeed;
            foreach (var number in docNumbers)
            {
                if (string.IsNullOrWhiteSpace(number)) continue;
                var operationResult = GetIndex(number);
                if (!operationResult.IsSucceed) return new OperationResult<SPRDocInfo[]>(operationResult.Result, indexes.ToArray());
                indexes.Add(operationResult.Data);
                _needLogined = false;
            }
            _needLogined = true;
            return new OperationResult<SPRDocInfo[]>(result, indexes.ToArray());
        }

        public OperationResult<SPRDocInfo> GetIndex(string docNumber)
        {
            try
            {
                if(_needLogined)//Если необходимо залогироваться
                    _application.Login(_login,_password);
                using (var cursor = _application.SelectSQL("SUEK_1", string.Format("[6_nom_doc] = '{0}' AND SYSINDEXSTATE <> 65002", docNumber)))
                {
                    if (!cursor.First()) return new OperationResult<SPRDocInfo>(OperationResult.DocNotFound, new SPRDocInfo() { DocNumber = docNumber });
                    List<string> docIndexes = new List<string>();
                    //Пробигаем по всем документам
                    do
                    {
                        using (SPRDocument document = cursor.Document)
                        {
                            document.Load();
                            string docIndex = document.GetPackageIndex();
                            docIndexes.Add(docIndex);
                        }
                    } while (cursor.Next());
                    return new OperationResult<SPRDocInfo>(OperationResult.Succeed, new SPRDocInfo() { DocNumber = docNumber, DocIndexes = docIndexes.ToArray() });
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<SPRDocInfo>(result, new SPRDocInfo());
            }
        }

        public OperationResult<SPRFile[]> GetFiles(string packageIndex)
        {
            return GetFiles(packageIndex, null, true);
        }

        public OperationResult<SPRFile[]> GetFiles(string packageIndex, int[] fileOrderNumbers)
        {
            return GetFiles(packageIndex, fileOrderNumbers, false);
        }

        private OperationResult<SPRFile[]> GetFiles(string packageIndex, int[] fileOrderNumbers, bool allFile)
        {
            List<SPRFile> resultList = new List<SPRFile>();
            _application.Login(_login, _password);
            using (var cursor = _application.SelectSQL("SUEK_1", string.Format("[18_INDEX_PAKETA] = '{0}' AND SYSINDEXSTATE <> 65002", packageIndex)))
            {
                //Получаем первый элемент в курсоре
                if (!cursor.First()) return new OperationResult<SPRFile[]>(OperationResult.DocNotFound, resultList.ToArray());
                using (SPRDocument document = cursor.Document)
                {
                    if (!document.Load()) return new OperationResult<SPRFile[]>(OperationResult.CanNotLoadDocument, resultList.ToArray());
                    int count = document.GetNumElements();
                    //Проходим по всем поддокументам документа
                    for (int i = 1; i <= count; i++)
                    {
                        if (!allFile)
                            if (!fileOrderNumbers.Contains(i))//Загрузка по страницам
                                continue;
                        using (SPRDocument currentDoc = document.SubDocument(i))
                        {
                            // if (!currentDoc.Load()) return new OperationResult<SPRFile[]>(OperationResult.CanNotLoadDocument, resultList.ToArray());
                            string tmpFileName = Path.GetTempFileName();//Создаем временный файл
                            if (File.Exists(tmpFileName)) File.Delete(tmpFileName);
                            currentDoc.SaveAs(0, tmpFileName);//Сохраняем временный файл
                            byte[] file = File.ReadAllBytes(tmpFileName);
                            string fileName = currentDoc.GetFileName();
                            //string hex = currentDoc.HexUID;
                            resultList.Add(new SPRFile() { Page = i, Content = file, Name = fileName, FileType = GetFileTypeByName(fileName) });
                        }
                    }
                }
                return new OperationResult<SPRFile[]>(OperationResult.Succeed, resultList.ToArray());
            }
        }

        public OperationResult<SPRFile[]> GetEDocFiles(string packageIndex, SPRDocType docType)
        {
            List<SPRFile> resultList = new List<SPRFile>();
            using (var cursor = _application.SelectSQL("EDOCS", string.Format("[SUEKINDEX] = '{0}'", packageIndex)))
            {
                //Получаем первый элемент в курсоре
                if (!cursor.First()) return new OperationResult<SPRFile[]>(OperationResult.DocNotFound, resultList.ToArray());
                using (SPRDocument document = cursor.Document)
                {
                    if (!document.Load()) return new OperationResult<SPRFile[]>(OperationResult.CanNotLoadDocument, resultList.ToArray());
                    int count = document.GetNumElements();
                    //Проходим по всем поддокументам документа
                    for (int i = 1; i <= count; i++)
                    {
                        using (SPRDocument currentDoc = document.SubDocument(i))
                        {
                            string fileName = currentDoc.GetFileName();
                            //Если есть файл с таким именем, то не записываем его еще раз
                            if (resultList.Select(s => s.Name.ToUpper()).Contains(fileName.ToUpper())) continue;
                            if (!ContainName(fileName, docType)) //Если Файл не соответствует типу документа
                                continue;
                            string tmpFileName = Path.GetTempFileName();//Создаем временный файл
                            if (File.Exists(tmpFileName)) File.Delete(tmpFileName);//Удаляем временный файл
                            currentDoc.SaveAs(0, tmpFileName);//Сохраняем временный файл
                            byte[] file = File.ReadAllBytes(tmpFileName);
                            resultList.Add(new SPRFile() { Page=0, Content = file, Name = fileName, FileType = GetFileTypeByName(fileName) });
                        }
                    }
                }
                return new OperationResult<SPRFile[]>(OperationResult.Succeed, resultList.ToArray());
            }
        }

        private int GetFileTypeByName(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return 1;
            fileName = fileName.ToUpper().Trim();
            if (fileName.StartsWith("DP_IAKTPRM") && fileName.EndsWith(".XML")) return 4;
            if (fileName.StartsWith("DP_IAKTPRM") && fileName.EndsWith(".XML.SIGN")) return 5;
            if (fileName.StartsWith("DP_ZAKTPRM") && fileName.EndsWith(".XML")) return 6;
            if (fileName.StartsWith("DP_ZAKTPRM") && fileName.EndsWith(".XML.SIGN")) return 7;
            if (fileName.StartsWith("ON_KORSFAKT") && fileName.EndsWith(".XML")) return 8;
            if (fileName.StartsWith("ON_KORSFAKT") && fileName.EndsWith(".XML.SIGN")) return 9;
            if (fileName.StartsWith("ON_SFAKT") && fileName.EndsWith(".XML")) return 10;
            if (fileName.StartsWith("ON_SFAKT") && fileName.EndsWith(".XML.SIGN")) return 11;
            if (fileName.StartsWith("DP_OTORG12") && fileName.EndsWith(".XML")) return 12;
            if (fileName.StartsWith("DP_OTORG12") && fileName.EndsWith(".XML.SIGN")) return 13;
            if (fileName.StartsWith("DP_PTORG12") && fileName.EndsWith(".XML")) return 14;
            if (fileName.StartsWith("DP_PTORG12") && fileName.EndsWith(".XML.SIGN")) return 15;
            return 1;
        }
        /// <summary>
        /// Соответствует ли имя файла данному типу
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <param name="docType">Тип документа</param>
        /// <returns>Истина, если соответствует</returns>
        private bool ContainName(string fileName, SPRDocType docType)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return false;
            string[] fileParts = GetPartFileName(docType);
            foreach (string part in fileParts)
                if (fileName.ToUpper().Contains(part))
                    return true;
            return false;
        }

        private string[] GetPartFileName(SPRDocType docType)
        {
            if (docType == SPRDocType.Invoice)
                return new string[] { "ON_SFAKT" };
            else if (docType == SPRDocType.AcceptanceCertificate)
                return new string[] { "DP_IAKTPRM", "DP_ZAKTPRM" };
            else if (docType == SPRDocType.InvoiceCorrection)
                return new string[] { "ON_KORSFAKT" };
            else if (docType == SPRDocType.Torg12)
                return new string[] { "DP_OTORG12","DP_PTORG12" };
            else
                return new string[] { };
        }

        #region Высвобождение ресурсов

        protected virtual void Disposing(bool isDisposing)
        {
            if (_isDisposed) return;
            if (_application == null) return;
            _application.Dispose();
            _application = null;
            _isDisposed = true;
        }

        public void Dispose()
        {
            Disposing(true);
        }

        ~SaperionRepository()
        {
            Disposing(false);
        }

        #endregion

        public OperationResult GetOperationResult(Exception exception)
        {
            int codeEx = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            int code = codeEx == 0 ? 1 : codeEx;
            string message = exception.ToString();
            string source = OperationResult.WebSite;
            WriteToLog(message);
            return new OperationResult(code, message, source,true);
        }

        private void WriteToLog(string message)
        {
            if (_logger == null) return;
            lock (_logger)
            {
                _logger.Error(message);
            }
        }

        public ILogger Logger
        {
            get
            {
                return _logger;
            }
            set
            {
                _logger = value;
            }
        }
    }
}
