﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.EntityClient;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using System.Xml.Linq;

using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Common.Filtering;
using SGC.TaxClaims.Infrastructure.Model;

namespace SGC.TaxClaims.Infrastructure.ReposImpl
{
    public class DBRepository: IDBRepository
    {
        private ILogger _logger;

        public OperationResult<aspnet_Users> GetUserByName(string userName)
        {
            if(String.IsNullOrWhiteSpace(userName))
            {
                return new OperationResult<aspnet_Users>(OperationResult.InvalidArgument, null);
            }
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    string loweredUserName = userName.ToLowerInvariant();
                    var userDB = entities.aspnet_Users.FirstOrDefault(s => s.LoweredUserName == loweredUserName);
                    return new OperationResult<aspnet_Users>(OperationResult.Succeed, userDB);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<aspnet_Users>(result, null);
            }
        }

        public OperationResult<RegistryDoc> GetRegistryDocById(Guid uid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    RegistryDoc result = entities
                        .Document
                        .Include("DocType")
                        //.Include("DocRelationChild")
                        //.Include("DocRelationChild.DocumentParent") // загрузка связи с родительским документом
                        //.Include("DocRelationParent")
                        .Include("Document1")
                        .Include("Document1.Document1") // Считаем, что может быть только такой уровень вложенности документов.
                        .Include("Buyer")
                        .Include("Seller")
                        .Include(doc => doc.CreatorUser)
                        .OfType<RegistryDoc>()
                        .Where(s => s.UId == uid)
                        .FirstOrDefault();
                    return new OperationResult<RegistryDoc>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<RegistryDoc>(result, null);
            }
        }

        public OperationResult<Claim> GetClaimById(Guid uid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    Claim result = entities
                        .Document
                        //.Include("DocFile")
                        .Include("Organization")
                        //.Include("State")
                        .OfType<Claim>()
                        .Where(s => s.UId == uid)
                        .FirstOrDefault();
                    return new OperationResult<Claim>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<Claim>(result, null);
            }
        }

        public OperationResult<Claim[]> GetClaims(string userName)
        {
            try
            {
                
                using (DataEntities entities = new DataEntities())
                {
                    //entities.Database.ExecuteSqlCommand(
                    var userDB = entities.aspnet_Users.FirstOrDefault(s => s.LoweredUserName == userName);
                    Claim[] result = entities
                        .Document.OfType<Claim>()
                        .Where(
                        s =>
                            s.DocTypeId == 1
                            &&
                            s.DocState.aspnet_Roles.Any(
                                role => role.aspnet_Users.Any(
                                    user => user.UserId == userDB.UserId))).ToArray();
                    return new OperationResult<Claim[]>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<Claim[]>(result, null);
            }
        }

        public OperationResult<Registry> GetRegistryByUid(Guid registryUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    var result = entities.Document
                        .Include("DocState")
                        .Include("Document1")
                        /*.Include("DocRelationChild")
                        .Include("DocRelationChild.DocumentParent")*/
                        .OfType<Registry>()
                        .Where(reg => reg.UId == registryUid)
                        .FirstOrDefault();

                    return new OperationResult<Registry>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<Registry>(result, null);
            }
        }

        public OperationResult<Registry[]> GetRegistriesByClaimUid(Guid claimUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    var result = entities.Document
                        .Include("DocState")
                        .Include("DocType")
                        .Include("CreatorUser")
                        .Include("Document1")
                        /*.Include("DocRelationChild")
                        .Include("DocRelationChild.DocumentParent")*/
                        .OfType<Registry>()
                        .Where(childDoc => 
                            childDoc.DocTypeId == 2 &&
                            childDoc.ParentDocUId == claimUid
                            /*childDoc.DocRelationChild.Any(rel => 
                                rel.ParentDocUId == claimUid && rel.DocumentParent.DocTypeId == 1)*/).ToArray();
                    return new OperationResult<Registry[]>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<Registry[]>(result, null);
            }
        }

        public OperationResult<PageResponse<Claim>> GetClaimsPage(int pageSize, int pageIndex, string sortColumn, bool? asc, FilterGroup filter, out int totalRecords)
        {
            //TODO: [Михаил] Реализовать постраничную загрузку
            totalRecords = 0;

            PageResponse<Claim> page = new PageResponse<Claim>()
            {
                 PageIndex = pageIndex
            };

            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    Claim[] result = entities
                        .Document
                        .Include("DocState")
                        .Include("Organization")
                        .OfType<Claim>()
                        .Where(
                        s =>
                            s.DocTypeId == 1
                            //&&
                            //(s.EditorUserUId == null || s.EditorUserUId == userDB.UserId)
                            //&&
                            //s.DocState.aspnet_Roles.Any(
                            //    role => role.aspnet_Users.Any(
                            //        user => user.UserId == userDB.UserId))
                                    ).ToArray();

                    totalRecords = entities.Document.OfType<Claim>().Count();
                    page.RecordsCount = result.Count();
                    page.PagesCount = (pageSize != 0) ? (Convert.ToInt32(Math.Ceiling(((double)totalRecords) / pageSize))) : 0;
                    page.Rows = result;

                    return new OperationResult<PageResponse<Claim>>(OperationResult.Succeed, page);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<PageResponse<Claim>>(result, page);
            }
        }      

        /// <summary>
        /// Получение первоначального состояния для документа и выбранного маршрута
        /// </summary>
        /// <param name="docTypeId"></param>
        /// <param name="docRoutId"></param>
        /// <returns></returns>
        public OperationResult<DocState> GetInitialState(int docTypeId, int docRouteId)
        {
            using (DataEntities entities = new DataEntities())
            {
                var initialState = entities.DocState.Where(
                    st => 
                        st.DocRouteId == docRouteId 
                        && st.DocTypeEvent.Any(tpEv => tpEv.DocEventTypeId == 1 && tpEv.DocTypeId == docTypeId))
                        .OrderBy(s => s.Id).FirstOrDefault();   
                return new OperationResult<DocState>(OperationResult.Succeed, initialState);
            }
        }

        /// <summary>
        /// Прикрепляет иерархию объектов к контексту и инициализирует EntityState
        /// </summary>
        /// <param name="document"></param>
        /// <param name="parentDocUid"></param>
        /// <param name="entities"></param>
        private static void AttachToContext(DataEntities entities, Document document, Guid? parentDocUid)
        {
            document.ParentDocUId = parentDocUid;
            entities.Document.Attach(document);
            entities.Entry(document).State = System.Data.Entity.EntityState.Detached;
            var docFiles = document.DocFile;//Смотрим есть ли файлы
            //Обновляем документ
            entities.Document.Attach(document);
            
            entities.Entry(document).State = document.ChangeStatus;
            document.ParentDocUId = parentDocUid;
            /*if (document.DocRelationParent != null)
                foreach (var relation in document.DocRelationParent)
                {
                    entities.DocRelation.Attach(relation);
                    entities.Entry(relation).State = relation.ChangeStatus;
                    if (relation.DocumentChild != null)
                    {
                        entities.Document.Attach(relation.DocumentChild);
                        entities.Entry(relation.DocumentChild).State = relation.DocumentChild.ChangeStatus;
                    }
                }*/

            ////Обновляем связь
            //DocRelation relationParent = entities.DocRelation.FirstOrDefault(s => s.ChildDocUId == document.UId && s.DocRelTypeId == 1);
            //if (relationParent != null && parentDocUid == null)//Удаляем
            //    entities.DocRelation.Remove(relationParent);
            //else if (relationParent == null && parentDocUid != null)//Добавляем
            //    entities.DocRelation.Add(new DocRelation() { DocRelTypeId = 1, ChildDocUId = document.UId, ParentDocUId = parentDocUid.Value });
            //else if (relationParent != null && parentDocUid != null)//Обновляем
            //    relationParent.ParentDocUId = parentDocUid.Value;
            //Добавляем файлы
            if (docFiles != null)
            {
                var docFileList = docFiles.ToList();
                for (int i = 0; i < docFileList.Count(); i++)
                {
                    var docfile = docFileList[i];
                    entities.DocFile.Attach(docfile);
                    entities.Entry(docfile).State = docfile.ChangeStatus;
                    docfile.ChangeStatus = System.Data.Entity.EntityState.Unchanged;
                }
            }
        }

        public OperationResult UpdateFiles(DocFile[] docFiles)
        {
            if (docFiles.Length == 0) return OperationResult.Succeed;
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    foreach (var docfile in docFiles)
                    {
                        if (docfile.ChangeStatus == System.Data.Entity.EntityState.Unchanged) continue;
                        entities.DocFile.Attach(docfile);
                        entities.Entry(docfile).State = docfile.ChangeStatus;
                    }
                    entities.SaveChanges();
                    return OperationResult.Succeed;
                }
            }
            catch (Exception ex)
            {
                return GetOperationResult(ex);
            }
        }

        public OperationResult UpdateFileDocs(RegistryDoc[] documents, DocFile[] docFiles)
        {
            if (docFiles.Length == 0 && documents.Length == 0) return OperationResult.Succeed;
            SqlTransaction transaction = null;
            try
            {
                DataTable documentTb = DBRepository.CreateTableFromDocument(documents);
                DataTable registryDocTb = DBRepository.CreateTableFromRegistryDoc(documents);
                DataTable docFileTb = DBRepository.CreateTableFromDocFile(docFiles);
                using (DataEntities entities = new DataEntities())
                using (var connection = new SqlConnection(entities.Database.Connection.ConnectionString))
                {
                    connection.Open();
                    transaction = connection.BeginTransaction();
                    using (SqlBulkCopy bcp = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bcp.DestinationTableName = documentTb.TableName;
                        bcp.WriteToServer(documentTb);
                        bcp.DestinationTableName = registryDocTb.TableName;
                        bcp.WriteToServer(registryDocTb);
                        bcp.DestinationTableName = docFileTb.TableName;
                        bcp.WriteToServer(docFileTb);
                    }
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                    connection.Close();
                    return OperationResult.Succeed;
                }
            }
            catch (Exception ex)
            {
                if (transaction == null)
                    return GetOperationResult(ex);
                transaction.Rollback();
                transaction.Dispose();
                transaction = null;
                return GetOperationResult(ex);
            }
        }

        //public OperationResult UpdateFilesAndDocs(RegistryDoc[] docs,DocFile[])

        public OperationResult SaveDocument(Document document, Guid? parentDocUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    if (document.ChangeStatus == System.Data.Entity.EntityState.Unchanged) 
                        return OperationResult.Succeed;
                    AttachToContext(entities, document, parentDocUid);
                    //Сохраняем изменения
                    entities.SaveChanges();
                    document.ChangeStatus = System.Data.Entity.EntityState.Unchanged;
                    return OperationResult.Succeed;
                }
            }
            catch (Exception ex)
            {
                return GetOperationResult(ex);
            }
        }

        public OperationResult SaveDocuments(IEnumerable<Document> documents, Guid? parentDocUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    foreach (var document in documents.ToArray()) // Чтобы можно было изменять коллекцию
                    {
                        if (document.ChangeStatus == System.Data.Entity.EntityState.Unchanged)
                            continue;

                        AttachToContext(entities, document, parentDocUid);
                        document.ChangeStatus = System.Data.Entity.EntityState.Unchanged;
                    }

                    //Сохраняем изменения
                    entities.SaveChanges();
                    
                    return OperationResult.Succeed;
                }
            }
            catch (Exception ex)
            {
                return GetOperationResult(ex);
            }
        }

        /*public OperationResult InsertRegistry(Registry registry,RegistryDoc[] registryDocs,Guid claimUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    
                    //Добавляем реестр//
                    entities.Document.Attach(registry);
                    entities.Entry(registry).State = System.Data.Entity.EntityState.Added;
                    registry.ParentDocUId = claimUid;
                    //Создаем связь с требованием//
                    DocRelation docRelation = new DocRelation();
                    docRelation.DocRelTypeId = 1;
                    docRelation.ParentDocUId = claimUid;
                    docRelation.ChildDocUId = registry.UId;
                    //Добавляем связь с требованием//
                    entities.DocRelation.Attach(docRelation);
                    entities.Entry(docRelation).State = System.Data.Entity.EntityState.Added;
                    List<RegistryDoc> registryDocList = new List<RegistryDoc>();
                    List<DocRelation> docRelationList = new List<DocRelation>();
                    //Добавляем документы//
                    for (int i = 0; i < registryDocs.Length; i++)
                    {
                        //Добавляем документ
                        var registryDoc = registryDocs[i];
                        registryDoc.UId = registryDoc.UId == Guid.Empty? Guid.NewGuid() : registryDoc.UId;
                        registryDoc.ParentDocUId = registry.UId;
                        registryDocList.Add(registryDoc);
                        //Создаем связь с реестром
                        docRelation = new DocRelation();
                        docRelation.DocRelTypeId = 1;
                        docRelation.ParentDocUId = registry.UId;
                        docRelation.ChildDocUId = registryDoc.UId;
                        docRelationList.Add(docRelation);
                        
                    }
                    
                    entities.Document.AddRange(registryDocList);
                    entities.DocRelation.AddRange(docRelationList);
                    entities.Configuration.ValidateOnSaveEnabled = false;
                    //Сохраняем изменения
                    entities.SaveChanges();
                    return OperationResult.Succeed;
                }
            }
            catch (Exception ex)
            {
                return GetOperationResult(ex);
            }
        }*/

       /* public OperationResult InsertRegistry(DataTable registry, DataTable document, DataTable registryDoc, DataTable docRelation)
        {
            SqlTransaction transaction = null;
            try
            {
                using (DataEntities entities = new DataEntities())
                using (var connection = new SqlConnection(entities.Database.Connection.ConnectionString))
                {   
                    connection.Open();
                    transaction = connection.BeginTransaction();
                    using (SqlBulkCopy bcp = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default,transaction))
                    {
                        bcp.DestinationTableName = document.TableName;
                        bcp.WriteToServer(document);
                        bcp.DestinationTableName = registry.TableName;
                        bcp.WriteToServer(registry);
                        bcp.DestinationTableName = registryDoc.TableName;
                        bcp.WriteToServer(registryDoc);
                        bcp.DestinationTableName = docRelation.TableName;
                        bcp.WriteToServer(docRelation);
                    }
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                    connection.Close();
                    return OperationResult.Succeed;
                }
        }
            catch (Exception ex)
            {
                if (transaction == null) 
                    return GetOperationResult(ex);
                transaction.Rollback();
                transaction.Dispose();
                transaction = null;
                return GetOperationResult(ex);
            }
        }*/

        /*public OperationResult InsertRegistry(DataTable registry, DataTable document, DataTable registryDoc)
        {
            SqlTransaction transaction = null;
            try
            {
                using (DataEntities entities = new DataEntities())
                using (var connection = new SqlConnection(entities.Database.Connection.ConnectionString))
                {   
                    connection.Open();
                    transaction = connection.BeginTransaction();
                    using (SqlBulkCopy bcp = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bcp.DestinationTableName = document.TableName;
                        bcp.WriteToServer(document);
                        bcp.DestinationTableName = registry.TableName;
                        bcp.WriteToServer(registry);
                        bcp.DestinationTableName = registryDoc.TableName;
                        bcp.WriteToServer(registryDoc);
                    }
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                    connection.Close();
                    return OperationResult.Succeed;
                }
            }
            catch (Exception ex)
            {
                if (transaction == null) 
                    return GetOperationResult(ex);
                transaction.Dispose();
                transaction = null;
                return GetOperationResult(ex);
            }
        }*/

        public OperationResult<Guid> InsertRegistry(XDocument xml, Guid claimUid, string fullUserName)
        {
            try
            {
                Guid registryUid = Guid.NewGuid();
                using (DataEntities entities = new DataEntities())
                {
                    using (SqlConnection connection = new SqlConnection(entities.Database.Connection.ConnectionString))
                    {
                        connection.Open();
                        using (SqlCommand command = new SqlCommand("CreateRegistrySap", connection))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            //Xml parameter//
                            SqlParameter xmlParameter = new SqlParameter("@xml", SqlDbType.Xml);
                            xmlParameter.Direction = ParameterDirection.Input;
                            string xmlString = xml.ToString();
                            xmlParameter.Value = xml.ToString();
                            command.Parameters.Add(xmlParameter);

                            //Идентификатор требования//
                            SqlParameter claimUidParameter = new SqlParameter();
                            claimUidParameter.ParameterName = "@claimUid";
                            claimUidParameter.Value = claimUid;
                            claimUidParameter.DbType = DbType.Guid;
                            command.Parameters.Add(claimUidParameter);
                            //Идентификатор реестра//
                            SqlParameter registryUidParameter = new SqlParameter();
                            registryUidParameter.ParameterName = "@registryUid";
                            registryUidParameter.Value = registryUid;
                            registryUidParameter.DbType = DbType.Guid;
                            command.Parameters.Add(registryUidParameter);
                            //Имя пользователя//
                            SqlParameter userNameParameter = new SqlParameter();
                            userNameParameter.ParameterName = "@userName";
                            userNameParameter.Value = fullUserName;
                            userNameParameter.DbType = DbType.String;
                            userNameParameter.Size = 100;
                            command.Parameters.Add(userNameParameter);
                            //Источкник
                            SqlParameter sourceParameter = new SqlParameter();
                            sourceParameter.ParameterName = "@source";
                            sourceParameter.DbType = DbType.String;
                            sourceParameter.Direction = ParameterDirection.Output;
                            sourceParameter.Size = 30;
                            command.Parameters.Add(sourceParameter);
                            //Сообщение 
                            SqlParameter messageParameter = new SqlParameter();
                            messageParameter.ParameterName = "@message";
                            messageParameter.Direction = ParameterDirection.Output;
                            messageParameter.DbType = DbType.String;
                            messageParameter.Size = 4000;
                            command.Parameters.Add(messageParameter);
                            //Системная или не системная ошибка
                            SqlParameter isSystemErrorParamter = new SqlParameter();
                            isSystemErrorParamter.ParameterName = "@isSystemError";
                            isSystemErrorParamter.DbType = DbType.Int32;
                            isSystemErrorParamter.Direction = ParameterDirection.Output;
                            command.Parameters.Add(isSystemErrorParamter);
                            //Возвращающий пармер//
                            SqlParameter prOperationResult = new SqlParameter();
                            prOperationResult.ParameterName = "@Return_Value";
                            prOperationResult.SqlDbType = System.Data.SqlDbType.Int;
                            prOperationResult.Direction = System.Data.ParameterDirection.ReturnValue;
                            command.Parameters.Add(prOperationResult);
                            command.ExecuteNonQuery();
                            connection.Close();
                            int code = Convert.ToInt32(prOperationResult.Value);
                            if (code != 0)
                                return new OperationResult<Guid>(new OperationResult(code, Convert.ToString(messageParameter.Value), Convert.ToString(sourceParameter.Value), Convert.ToBoolean(isSystemErrorParamter.Value)), registryUid);
                        }
                    }
                    
                    /*int code = entities.Database.ExecuteSqlCommand(
                        "dbo.CreateRegistrySap @xml, @claimUid, @registryUid, @userName, @operationSource OUT, @operationMessage OUT, @isSystemError OUT",
                        xmlParameter, claimUidParameter, registryUidParameter, userNameParameter, sourceParameter, messageParameter, isSystemErrorParamter);*/
                    /*int code = entities.Database.ExecuteSqlCommand(
                        "dbo.CreateRegistrySap @xml, @claimUid, @registryUid, @userName, @operationSource OUT, @operationMessage OUT",
                        xmlParameter, claimUidParameter, registryUidParameter, userNameParameter, sourceParameter, messageParameter);*/
                    /*int code = entities.Database.ExecuteSqlCommand(
                        "dbo.CreateRegistrySap @xml, @userName, @message OUT", xmlParameter, userNameParameter, messageParameter);*/
                }
                return new OperationResult<Guid>(OperationResult.Succeed, registryUid);
            }
            catch(Exception ex)
            {
                return new OperationResult<Guid>(GetOperationResult(ex),Guid.Empty);
            }
        }

        public OperationResult<ValidationInfo[]> SaveAndMove(Document document, Guid? parentDocUid, int nextStateId, Guid userUid)
        {
            List<ValidationInfo> valids = new List<ValidationInfo>();
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    if (document.ChangeStatus == System.Data.Entity.EntityState.Unchanged) 
                        return new OperationResult<ValidationInfo[]>(OperationResult.Succeed, valids.ToArray());

                    AttachToContext(entities, document, parentDocUid);

                    //Начинаем транзакцию
                    var transaction = entities.Database.BeginTransaction();
                    //Сохраняем изменения
                    entities.SaveChanges();
                    //Проверяем валидацию
                    var row = entities.Database.SqlQuery<ValidationInfo>("dbo.spCanMove @p0, @p1, @p2, @p3, @p4", document.DocStateId, nextStateId, 1, document.UId, userUid);
                    //Переводим в другое состояние
                    var moveRes = MoveToState(entities, document.UId, document.DocStateId, document.DocTypeId);
                    if (!moveRes.IsSucceed)
                    {
                        transaction.Rollback();
                        return new OperationResult<ValidationInfo[]>(moveRes, valids.ToArray());
                    }
                    //Переводим в следующее состояние
                    document.DocStateId = nextStateId;
                    entities.Entry(document).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    transaction.Commit();
                    document.ChangeStatus = System.Data.Entity.EntityState.Unchanged;
                    return new OperationResult<ValidationInfo[]>(OperationResult.Succeed, valids.ToArray());
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<ValidationInfo[]>(result, valids.ToArray());
            }
        }

        private OperationResult MoveToState(DataEntities entities, Guid docUid, int stateId, int typeId)
        {
            //docUid
            SqlParameter docUidParam = new SqlParameter();
            docUidParam.Direction = ParameterDirection.Input;
            docUidParam.ParameterName = "@docUid";
            docUidParam.DbType = DbType.Guid;
            docUidParam.Value = docUid;
            //stateId
            SqlParameter stateIdParam = new SqlParameter();
            stateIdParam.Direction = ParameterDirection.Input;
            stateIdParam.ParameterName = "@stateId";
            stateIdParam.DbType = DbType.Int32;
            stateIdParam.Value = stateId;
            //docTypeId
            SqlParameter docTypeIdParam = new SqlParameter();
            docTypeIdParam.Direction = ParameterDirection.Input;
            docTypeIdParam.ParameterName = "@docTypeId";
            docTypeIdParam.DbType = DbType.Int32;
            docTypeIdParam.Value = typeId;
            //source
            SqlParameter sourceParam = new SqlParameter();
            sourceParam.Direction = ParameterDirection.Output;
            sourceParam.ParameterName = "@source";
            sourceParam.Size = 30;
            sourceParam.DbType = DbType.String;
            //message
            SqlParameter messageParam = new SqlParameter();
            messageParam.Direction = ParameterDirection.Output;
            messageParam.ParameterName = "@message";
            messageParam.Size = 4000;
            messageParam.DbType = DbType.String;
            //isSystemError
            SqlParameter isSystemErrorParam = new SqlParameter();
            isSystemErrorParam.Direction = ParameterDirection.Output;
            isSystemErrorParam.ParameterName = "@isSystemError";
            isSystemErrorParam.DbType = DbType.Int32;
            //Код операции
            var returnCode = new SqlParameter();
            returnCode.ParameterName = "@ReturnCode";
            returnCode.SqlDbType = SqlDbType.Int;
            returnCode.Direction = ParameterDirection.Output;

            entities.Database.ExecuteSqlCommand("exec @ReturnCode = dbo.MoveToState @docUid,@stateId,@docTypeId,@source OUT,@message OUT,@isSystemError OUT",
                returnCode,docUidParam, stateIdParam, docTypeIdParam, sourceParam, messageParam, isSystemErrorParam);

            return new OperationResult(Convert.ToInt32(returnCode.Value ?? 1),
                Convert.ToString(messageParam.Value ?? string.Empty),
                Convert.ToString(sourceParam.Value ?? string.Empty),
                Convert.ToBoolean(isSystemErrorParam.Value ?? true));
        }

        public OperationResult<Document[]> GetChildDocuments(Guid parentDocUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    /*Document[] result =
                        entities
                        .DocRelation
                        .Include("Document")
                        .Where(rel => rel.ParentDocUId == parentDocUid && rel.DocRelTypeId == 1)
                        .Select(rel => rel.DocumentChild)
                        .ToArray();*/
                    Document[] result = entities
                        .Document
                        .Include("DocType")
                        .Where(s => s.DocTypeId != 1 && s.ParentDocUId.Value == parentDocUid /*s.DocRelationChild.Any(rel => rel.ParentDocUId == parentDocUid)*/)
                        .ToArray();
                    return new OperationResult<Document[]>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<Document[]>(result, null);
            }
        }

        public OperationResult<RegistryDoc[]> GetRegistryDocs(Guid registryUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    //entities.Database.SqlQuery(
                    RegistryDoc[] result = entities
                        .Document
                        .Include("DocType")
                        .Include("Document1")
                        .Include("Document1.Document1") //Считаем, что может быть только такой уровень вложенности документов.
                        .Include("Buyer")
                        .Include("Seller")
                        .Include(doc => doc.CreatorUser)
                        .OfType<RegistryDoc>()
                        //.Include("Document1.Buyer")
                        //.Include("Document1.Seller")
                        //.Include(rdoc => rdoc.Document1.OfType<RegistryDoc>().Select(rdc => rdc.Buyer))
                        //.Include(rdoc => rdoc.Document1.OfType<RegistryDoc>().Select(rdc => rdc.Seller))
                        .Where(s => s.DocTypeId != 1 && s.DocTypeId != 2 && s.ParentDocUId.Value == registryUid /*s.DocRelationChild.Any(rel => rel.ParentDocUId == registryUid)*/ )
                        .OrderBy(rdoc => rdoc.DocDate)
                        .ToArray();

                    // Workaround- подгрузить все Buyer и Seller (заодно Пользователя)
                    foreach (var doc in result)
                    {
                        var b = doc.Buyer; var s = doc.Seller; var user = doc.CreatorUser;
                        foreach (var child in doc.Document1.OfType<RegistryDoc>())
                        {
                            b = child.Buyer; s = child.Seller; user = child.CreatorUser;
                        }
                    }

                    return new OperationResult<RegistryDoc[]>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<RegistryDoc[]>(result, null);
            }
        }

        public OperationResult<RegistryDocSet[]> GetRegistryDocsSP(Guid registryUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    RegistryDocSet[] result = entities
                        .Database
                        .SqlQuery<RegistryDocSet>("dbo.spGetRegistryDocs @p0", registryUid)
                        .ToArray();
                    return new OperationResult<RegistryDocSet[]>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<RegistryDocSet[]>(result, null);
            }
        }

        public OperationResult<DocFile[]> GetDocumentFiles(Guid documentUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    DocFile[] result = entities
                        .DocFile
                        .Where(file => file.DocumentUid == documentUid).ToArray();
                    return new OperationResult<DocFile[]>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<DocFile[]>(result,new DocFile[]{ });
            }
        }

        public OperationResult<DocFile[]> GetDocumentFiles(Guid documentUid, int fileTypeId)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    DocFile[] result = entities 
                        .DocFile 
                        .Where( 
                        file => 
                            file.DocumentUid == documentUid 
                            && 
                            file.FileTypeId == fileTypeId).ToArray(); 
                    return new OperationResult<DocFile[]>(OperationResult.Succeed, result); 
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<DocFile[]>(result, new DocFile[] { });
            }
        }

        public OperationResult<DocFile[]> GetDocumentFilesDiscritionOnly(Guid documentUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    var resultArray = entities
                        .DocFile
                        .Include("FileType")
                        .Where(file => file.DocumentUid == documentUid)
                        .Select(file => new { 
                            UId = file.UId, 
                            DocumentUid = file.DocumentUid,
                            FileTypeId = file.FileTypeId,
                            Name = file.Name,
                            NextFileUid = file.NextFileUid,
                            FileType = file.FileType
                        })
                        .ToArray();

                    List<DocFile> result = new List<DocFile>();
                    foreach (var item in resultArray)
                        result.Add(new DocFile() { 
                            UId = item.UId, 
                            DocumentUid = item.DocumentUid,
                            FileTypeId = item.FileTypeId,
                            Name = item.Name,
                            FileType = item.FileType,
                            NextFileUid = item.NextFileUid
                        });
                    return new OperationResult<DocFile[]>(OperationResult.Succeed, result.ToArray());
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<DocFile[]>(result, null);
            }
        }

        public OperationResult<DocFile> GetFile(Guid fileUid)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    DocFile result = entities.DocFile
                        .FirstOrDefault(file => file.UId == fileUid);
                    return new OperationResult<DocFile>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<DocFile>(result, null);
            }
        }

        public OperationResult<DocState[]> GetNextPossibleStates(DocState docState)
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    DocState[] result =
                        entities.DocMove.Include("DocState")
                        .Where(move =>
                            move.DocStateIdFrom == docState.Id
                            &&
                            move.DocRouteId == docState.DocRouteId)
                            .Select(move => move.DocState1).ToArray();
                    return new OperationResult<DocState[]>(OperationResult.Succeed, result);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<DocState[]>(result, null);
            }
        }

        public OperationResult<ValidationInfo[]> CanMoveNextState(int docStateIdFrom, int docStateIdTo, int routeId, Guid documentUid, Guid userUid)
        {
            List<ValidationInfo> result = new List<ValidationInfo>();
            using (DataEntities entities = new DataEntities())
            {
                var row = entities.Database.SqlQuery<ValidationInfo>("dbo.spCanMove @p0, @p1, @p2, @p3, @p4", docStateIdFrom, docStateIdTo, routeId, documentUid, userUid);
                result.AddRange(row);
            }
            return new OperationResult<ValidationInfo[]>(OperationResult.Succeed, result.ToArray());
        }
        
        public OperationResult<bool> TryLockDocument(Guid documentUid, Guid userUid, out Guid blockHolderUid)
        {
            blockHolderUid = Guid.Empty;
            return new OperationResult<bool>(OperationResult.Succeed, true);
            //TODO: [Михаил] написать блокировку разблокировку объекта
            /*try
            {
                using (DataEntities entities = new DataEntities())
                {
                    Document document = entities.Document.FirstOrDefault(doc => doc.UId == documentUid);
                    if(document == null) return new OperationResult<bool>(OperationResult.DocNotFound, true);
                    //if(document.h)
                    return new OperationResult<bool>(OperationResult.Succeed, true);
                }
            }
            catch (Exception ex)
            {
                OperationResult result = GetOperationResult(ex);
                return new OperationResult<bool>(result,false);
            }*/
        }

        public OperationResult UnlockDocument(Guid documentUid, Guid userUid)
        {
            return OperationResult.Succeed;
        }

        public OperationResult<Organization[]> GetListOfOrganizations()
        {
            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    return new OperationResult<Organization[]>(OperationResult.Succeed, entities.Organization.ToArray());
                }
            }
            catch (Exception ex)
            {
                OperationResult operationResult = GetOperationResult(ex);
                return new OperationResult<Organization[]>(operationResult, new Organization[0]);
            }
        }

        public OperationResult<Partner[]> GetPartners(string text, int maxCount)
        {
            if(String.IsNullOrEmpty(text) || maxCount <= 0)
            {
                return new OperationResult<Partner[]>(OperationResult.Succeed, new Partner[0]);
            }

            try
            {
                using (DataEntities entities = new DataEntities())
                {
                    var partners = entities.Partner.Where(p => p.Name.Contains(text)).OrderBy(p => p.Name).Take(maxCount);
                    return new OperationResult<Partner[]>(OperationResult.Succeed, partners.ToArray());
                }
            }
            catch (Exception ex)
            {
                OperationResult operationResult = GetOperationResult(ex);
                return new OperationResult<Partner[]>(operationResult, new Partner[0]);
            }
        }

        public OperationResult GetOperationResult(Exception exception)
        {
            int codeEx = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            int code = codeEx == 0 ? 1 : codeEx;
            string message = exception.ToString();
            string source = OperationResult.WebSite;
            WriteToLog(message);
            return new OperationResult(code, message, source,true);
        }

        #region Создание таблиц

        public static DataTable CreateDocumentTable()
        {
            DataTable document = new DataTable();
            document.TableName = "Document";
            DataColumn uid = new DataColumn("UId", typeof(Guid));
            document.Columns.Add(uid);
            DataColumn docTypeId = new DataColumn("DocTypeId", typeof(int));
            document.Columns.Add(docTypeId);
            DataColumn docStateId = new DataColumn("DocStateId", typeof(int));
            document.Columns.Add(docStateId);
            DataColumn docSourceId = new DataColumn("DocSourceId", typeof(int));
            document.Columns.Add(docSourceId);
            DataColumn creatorUserUid = new DataColumn("CreatorUserUid", typeof(Guid));
            document.Columns.Add(creatorUserUid);
            DataColumn documentHolderUserUId = new DataColumn("DocumentHolderUserUId", typeof(Guid));
            document.Columns.Add(documentHolderUserUId);
            DataColumn hierarchyHolderUserUId = new DataColumn("HierarchyHolderUserUId", typeof(Guid));
            document.Columns.Add(hierarchyHolderUserUId);
            DataColumn name = new DataColumn("Name", typeof(string));
            document.Columns.Add(name);
            DataColumn number = new DataColumn("Number", typeof(string));
            document.Columns.Add(number);
            DataColumn docDate = new DataColumn("DocDate", typeof(DateTime));
            document.Columns.Add(docDate);
            DataColumn comment = new DataColumn("Comment", typeof(string));
            document.Columns.Add(comment);
            DataColumn dateCreated = new DataColumn("DateCreated", typeof(DateTime));
            document.Columns.Add(dateCreated);
            DataColumn dateChanged = new DataColumn("DateChanged", typeof(DateTime));
            document.Columns.Add(dateChanged);
            DataColumn parentDocUId = new DataColumn("ParentDocUId", typeof(Guid));
            document.Columns.Add(parentDocUId);
            return document;
        }

        public static DataTable CreateTableFromDocument(Document[] documents)
        {
            DataTable table = CreateDocumentTable();
            foreach (var document in documents)
                table.Rows.Add(FillDocumentRow(table.NewRow(), document));
            return table;
        }

        private static DataRow FillDocumentRow(DataRow row, Document document)
        {
            row["UId"] = document.UId;
            row["DocTypeId"] = document.DocTypeId;
            row["DocStateId"] = document.DocStateId;
            row["DocSourceId"] = document.DocSourceId;
            row["CreatorUserUid"] = document.CreatorUserUid;
            if (document.DocumentHolderUserUId != null)
                row["DocumentHolderUserUId"] = document.DocumentHolderUserUId.Value;
            if (document.HierarchyHolderUserUId != null)
                row["HierarchyHolderUserUId"] = document.HierarchyHolderUserUId.Value;
            row["Name"] = document.Name;
            row["Number"] = document.Number;
            if (document.DocDate != null)
                row["DocDate"] = document.DocDate.Value;
            row["Comment"] = document.Comment;
            row["DateCreated"] = document.DateCreated;
            if (document.DateChanged != null)
                row["DateChanged"] = document.DateChanged.Value;
            if (document.ParentDocUId != null)
                row["ParentDocUId"] = document.ParentDocUId.Value;
            return row;
        }

        public static DataTable CreateDocRelationTable()
        {
            DataTable docRelation = new DataTable();
            docRelation.TableName = "DocRelation";
            DataColumn parentDocUId = new DataColumn("ParentDocUId", typeof(Guid));
            docRelation.Columns.Add(parentDocUId);
            DataColumn childDocUId = new DataColumn("ChildDocUId", typeof(Guid));
            docRelation.Columns.Add(childDocUId);
            DataColumn docRelTypeId = new DataColumn("DocRelTypeId", typeof(int));
            docRelation.Columns.Add(docRelTypeId);
            return docRelation;
        }

        public static DataTable CreateRegistryDocTable()
        {
            DataTable registryDoc = new DataTable();
            registryDoc.TableName = "RegistryDoc";
            DataColumn uid = new DataColumn("Uid", typeof(Guid));
            registryDoc.Columns.Add(uid);
            DataColumn packageIndex = new DataColumn("PackageIndex", typeof(string));
            registryDoc.Columns.Add(packageIndex);
            DataColumn pageNumbers = new DataColumn("PageNumbers", typeof(string));
            registryDoc.Columns.Add(pageNumbers);
            DataColumn isScan = new DataColumn("IsScan", typeof(bool));
            registryDoc.Columns.Add(isScan);
            DataColumn sumTotal = new DataColumn("SumTotal", typeof(decimal));
            registryDoc.Columns.Add(sumTotal);
            DataColumn sumTax = new DataColumn("SumTax", typeof(decimal));
            registryDoc.Columns.Add(sumTax);
            DataColumn parentDocNumber = new DataColumn("ParentDocNumber", typeof(string));
            registryDoc.Columns.Add(parentDocNumber);
            DataColumn parentDocDate = new DataColumn("ParentDocDate", typeof(DateTime));
            registryDoc.Columns.Add(parentDocDate);
            DataColumn correctionTypeId = new DataColumn("CorrectionTypeId", typeof(int));
            registryDoc.Columns.Add(correctionTypeId);
            DataColumn factoryCode = new DataColumn("FactoryCode", typeof(string));
            registryDoc.Columns.Add(factoryCode);
            DataColumn factoryName = new DataColumn("FactoryName", typeof(string));
            registryDoc.Columns.Add(factoryName);
            DataColumn partnerName = new DataColumn("PartnerName", typeof(string));
            registryDoc.Columns.Add(partnerName);
            DataColumn partnerINN = new DataColumn("PartnerINN", typeof(string));
            registryDoc.Columns.Add(partnerINN);
            DataColumn partnerKPP = new DataColumn("PartnerKPP", typeof(string));
            registryDoc.Columns.Add(partnerKPP);
            return registryDoc;
        }

        public static DataTable CreateTableFromRegistryDoc(RegistryDoc[] registryDocs)
        {
            DataTable table = CreateRegistryDocTable();
            foreach (var document in registryDocs)
                table.Rows.Add(FillRegistryDocRow(table.NewRow(), document));
            return table;
        }

        private static DataRow FillRegistryDocRow(DataRow row, RegistryDoc registryDoc)
        {
            row["Uid"] = registryDoc.UId;
            row["PackageIndex"] = registryDoc.PackageIndex;
            row["PageNumbers"] = registryDoc.PageNumbers;
            row["IsScan"] = registryDoc.IsScan;
            if (registryDoc.SumTotal != null)
                row["SumTotal"] = registryDoc.SumTotal.Value;
            if (registryDoc.SumTax != null)
                row["SumTax"] = registryDoc.SumTax.Value;
            row["ParentDocNumber"] = registryDoc.ParentDocNumber;
            if (registryDoc.ParentDocDate != null)
                row["ParentDocDate"] = registryDoc.ParentDocDate.Value;
            if(registryDoc.CorrectionTypeId != null)
                row["CorrectionTypeId"] = registryDoc.CorrectionTypeId;
            row["FactoryCode"] = registryDoc.FactoryCode;
            row["FactoryName"] = registryDoc.FactoryName;
            row["PartnerName"] = registryDoc.PartnerName;
            row["PartnerINN"] = registryDoc.PartnerINN;
            row["PartnerKPP"] = registryDoc.PartnerKPP;
            return row;
        }

        public static DataTable CreateRegistryTable()
        {
            DataTable registry = new DataTable();
            registry.TableName = "Registry";
            DataColumn uid = new DataColumn("UId", typeof(Guid));
            registry.Columns.Add(uid);
            DataColumn orgId = new DataColumn("OrgId", typeof(int));
            registry.Columns.Add(orgId);
            DataColumn dateReady = new DataColumn("DateReady", typeof(DateTime));
            registry.Columns.Add(dateReady);
            DataColumn claimItemNo = new DataColumn("ClaimItemNo", typeof(string));
            registry.Columns.Add(claimItemNo);
            DataColumn bookType = new DataColumn("BookType", typeof(string));
            registry.Columns.Add(bookType);
            return registry;
        }

        public static DataTable CreateTableFromRegistry(Registry[] registryDocs)
        {
            DataTable table = CreateRegistryTable();
            foreach (var document in registryDocs)
                table.Rows.Add(FillRegistryRow(table.NewRow(), document));
            return table;
        }

        private static DataRow FillRegistryRow(DataRow row, Registry registry)
        {
            row["UId"] = registry.UId;
            if (registry.OrgId != null)
                row["OrgId"] = registry.OrgId.Value;
            if (registry.DateReady != null)
                row["DateReady"] = registry.DateReady.Value;
            row["ClaimItemNo"] = registry.ClaimItemNo;
            row["BookType"] = registry.BookType;
            return row;
        }

        public static DataTable CreateDocFile()
        {
            DataTable docFile = new DataTable();
            docFile.TableName = "DocFile";
            DataColumn uid = new DataColumn("Uid", typeof(Guid));
            docFile.Columns.Add(uid);
            DataColumn documentUid = new DataColumn("DocumentUid", typeof(Guid));
            docFile.Columns.Add(documentUid);
            DataColumn fileTypeId = new DataColumn("FileTypeId", typeof(int));
            docFile.Columns.Add(fileTypeId);
            DataColumn name = new DataColumn("Name", typeof(string));
            docFile.Columns.Add(name);
            DataColumn content = new DataColumn("Content", typeof(string));
            docFile.Columns.Add(content);
            DataColumn nextFileUid = new DataColumn("NextFileUid", typeof(Guid));
            docFile.Columns.Add(nextFileUid);
            return docFile;
        }

        public static DataTable CreateTableFromDocFile(DocFile[] docFiles)
        {
            DataTable table = CreateRegistryDocTable();
            foreach (var file in docFiles)
                table.Rows.Add(FillDocFileRow(table.NewRow(), file));
            return table;
        }

        private static DataRow FillDocFileRow(DataRow row, DocFile docFile)
        {
            row["Uid"] = docFile.UId;
            row["DocumentUid"] = docFile.DocumentUid;
            row["FileTypeId"] = docFile.FileTypeId;
            row["Name"] = docFile.Name;
            row["Content"] = docFile.Content;
            if (docFile.NextFileUid != null)
                row["NextFileUid"] = docFile.NextFileUid.Value;
            return row;
        }


        #endregion

        private void WriteToLog(string message)
        {
            if (_logger == null) return;
            lock (_logger)
            {
                _logger.Error(message);
            }
        }

        public ILogger Logger
        {
            get
            {
                return _logger;
            }
            set
            {
                _logger = value;
            }
        }


        
    }
}
