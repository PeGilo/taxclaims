﻿using SGC.TaxClaims.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Excel;
using System.Data;
using SGC.TaxClaims.Core.ImportXls;
using SGC.TaxClaims.Core.Contracts;


namespace SGC.TaxClaims.Infrastructure
{
    public class ExcelImport: IExcelImport, IDisposable
    {
        private bool _isOpend = false;
        private bool _isDisposed = false;
        private DataSet _dataSet;
        private ILogger _logger;
        public OperationResult Open(string fullFileName)
        {
            try
            {
                if (!fullFileName.EndsWith(".xls") && !fullFileName.EndsWith(".xlsx"))
                    return OperationResult.NotRecognizedExcelFormat;
                using (FileStream stream = File.Open(fullFileName, FileMode.Open, FileAccess.Read))
                using (IExcelDataReader excelReader =
                        fullFileName.EndsWith(".xlsx") ?
                        Excel.ExcelReaderFactory.CreateOpenXmlReader(stream) :
                        Excel.ExcelReaderFactory.CreateBinaryReader(stream))
                {
                    _dataSet = excelReader.AsDataSet();
                    _isOpend = true;
                }
                return OperationResult.Succeed;
            }
            catch (Exception ex)
            {
                var operationResult = GetOperationResult(ex);
                return operationResult;
            }
        }

        public string GetValue(int row,int column,int sheet)
        {
            if (!_isOpend) return string.Empty;
            var table = _dataSet.Tables[--sheet];
            return Convert.ToString(table.Rows[--row][--column]);
        }

        public int Count(int sheet)
        {
            if (_dataSet == null) return 0;
            return  _dataSet.Tables[--sheet].Rows.Count;
        }

        public int SheetCount()
        {
            if (_dataSet == null) return 0;
            return _dataSet.Tables.Count;
        }

        public void Close()
        {
            _isOpend = false;
            if(_dataSet != null) _dataSet.Dispose();
        }

        public virtual void Dispose(bool disposing)
        {
            if (_isDisposed) return;
            Close();
            _isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~ExcelImport()
        {
            Dispose(false);
        }
    
        public OperationResult<RegistryXsl[]> GetRegistries(string fullFilePath)
        {
            try
            {
                List<RegistryXsl> result = new List<RegistryXsl>();
                using (ExcelImport excelImport = new ExcelImport())
                {
                    var resultOpen = excelImport.Open(fullFilePath);
                    if (!resultOpen.IsSucceed) return new OperationResult<RegistryXsl[]>(resultOpen, result.ToArray());
                  
                    int sheetCount = excelImport.SheetCount();
                    for (int i = 1; i <= sheetCount; i++)
                    {
                        if (excelImport.Count(i) <= 1) continue;
                        result.Add(GetRegistry(excelImport,i));
                    }
                }
                return new OperationResult<RegistryXsl[]>(OperationResult.Succeed,result.ToArray());
            }
            catch (Exception ex)
            {
                var operationResult = GetOperationResult(ex);
                return new OperationResult<RegistryXsl[]>(operationResult, new RegistryXsl[] { });
            }
        }

        private RegistryXsl GetRegistry(ExcelImport exporter, int sheetNumber)
        {
            //Создаем реестр
            int rowCount = exporter.Count(sheetNumber);
            string bookType = exporter.GetValue(1, 2, sheetNumber);
            string dateCreate = exporter.GetValue(2, 2, sheetNumber);
            string orgName = exporter.GetValue(3, 2, sheetNumber);
            string orgInn = exporter.GetValue(4, 2, sheetNumber);
            string orgKpp = exporter.GetValue(5, 2, sheetNumber);
            RegistryXsl registry = new RegistryXsl(sheetNumber, bookType, dateCreate, orgName, orgInn, orgKpp);
            //Добавляем документы
            for (int i = 7; i <= rowCount; i++)
            {
                string name = exporter.GetValue(i,1,sheetNumber);
                string inn = exporter.GetValue(i,2,sheetNumber);
                string kpp = exporter.GetValue(i,3,sheetNumber);
                string docNumber = exporter.GetValue(i,4,sheetNumber); 
                string docDate = exporter.GetValue(i,5,sheetNumber);
                string docNumParent = exporter.GetValue(i,5,sheetNumber);
                string docDateParent = exporter.GetValue(i,7,sheetNumber);
                string sum = exporter.GetValue(i,8,sheetNumber);
                string tax = exporter.GetValue(i,9,sheetNumber);
                string packageIndex = exporter.GetValue(i,17,sheetNumber);
                string sign = exporter.GetValue(i,18,sheetNumber);
                DocumentXsl doc = new DocumentXsl(
                    i,
                    sheetNumber,
                    name,
                    inn,
                    kpp,
                    docNumber,
                    docDate,
                    docNumParent,
                    docDateParent,
                    sum,
                    tax,
                    packageIndex,
                    sign);
                if (doc.IsEmpty()) break;//Если наткнулись на пустую строку то прекращаем обход
                registry.AddDocument(doc);
            }
            return registry;
        }

        public OperationResult GetOperationResult(Exception exception)
        {
            int codeEx = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            int code = codeEx == 0 ? 1 : codeEx;
            string message = exception.ToString();
            string source = OperationResult.WebSite;
            WriteToLog(message);
            return new OperationResult(code, message, source, true);
        }

        private void WriteToLog(string message)
        {
            if (_logger == null) return;
            lock (_logger)
            {
                _logger.Error(message);
            }
        }

        public ILogger Logger
        {
            get
            {
                return _logger;
            }
            set
            {
                _logger = value;
            }
        }
    }
}
