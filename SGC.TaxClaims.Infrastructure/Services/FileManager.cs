﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Infrastructure.Services
{
    public class FileManager
    {
        /// <summary>
        /// Сохраняет содержимое файл во временное хранилище
        /// </summary>
        /// <param name="content"></param>
        /// <returns>Возвращает полное имя файла</returns>
        public string StoreTemporaryFile(byte[] content, string fileName)
        {
            string fullPath = Path.Combine(Path.GetTempPath(), fileName);

            File.WriteAllBytes(fullPath, content);

            return fullPath;
        }
    }
}
