﻿using System;
using System.Linq;
using System.Text;
using SGC.TaxClaims.Core;
using System.Threading.Tasks;
using SGC.TaxClaims.Core.Model;
using System.Collections.Generic;
using SGC.TaxClaims.Core.Saperion;
using SGC.TaxClaims.Core.ExportIFNS;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Data;
using System.Text.RegularExpressions;

namespace SGC.TaxClaims.Infrastructure.Services
{
    /// <summary>
    /// Содержит бизнес-логику, связанную с обработкой документов
    /// </summary>
    public class DocumentService
    {
        private IDBRepository _documentRepository;
        private ISaperion _saperionRepository;
        private IIFNSExporter _ifnsExporter;

        public DocumentService(IDBRepository documentRepository,ISaperion saperionRepository,IIFNSExporter ifnsExporter)
	    {
            _documentRepository = documentRepository;
            _saperionRepository = saperionRepository;
            _ifnsExporter = ifnsExporter;
	    }

        /// <summary>
        /// Можно ли создавать документы для текущего состояния требования
        /// </summary>
        /// <param name="claimUid"></param>
        /// <returns></returns>
        public OperationResult CanCreateDocumentInCurrentState(Claim parentClaim, int docTypeId)
        {
            // Получить состояние, в котором возможно создание реестра
            OperationResult<DocState> result = _documentRepository.GetInitialState(docTypeId, (int)DocumentRoute.ToIFNS);
            if (result.IsSystemError)
            {
                return result.Result; //new OperationResult<bool>(result.Result, false); 
            }
            else
            {
                return (parentClaim.DocStateId == result.Data.Id) ? OperationResult.Succeed : OperationResult.StateIncompatibleOperation;
            }
        }

        /// <summary>
        /// Создание реестра с проверкой статуса
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="claim"></param>
        /// <param name="registry"></param>
        /// <returns></returns>
        public OperationResult CreateRegistry(Guid userId, Claim claim, Registry registry,int[] docTypes)
        {
            Guid newGuid = Guid.NewGuid();
            // Инициализация реестра
            registry.DocStateId = claim.DocStateId; // Состояние (DocState) всех дочерних документов должно быть таким же как у корневого документа (требования)
            registry.Number = newGuid.ToString(); // Т.к. номер обязательный, заполняем его просто идентификатором реестра
            registry.CreatorUserUid = userId;
            // На время добавления реестра необходимо удоствериться, что требование не изменит своего статуса. Поэтому надо заблокировать его.
            Guid blockHolderUid;
            OperationResult<bool> lockResult = _documentRepository.TryLockDocument(claim.UId, userId, out blockHolderUid);
            if (lockResult.IsSucceed && lockResult.Data == true)
            {
                try
                {
                    // После блокировки документа делаем еще одну проверку можем ли мы поменять статус
                    OperationResult result = CanCreateDocumentInCurrentState(claim, registry.DocTypeId);
                    if (result.IsSystemError)
                    {
                        return result;
                    }
                    else if (result.IsSucceed)
                    {
                        //Сохранение реестра
                        result = _documentRepository.SaveDocument(registry, claim.UId);
                        if (!result.IsSucceed) return result;
                        //Добавляем файлы к документам реестра
                        return AttachRegistryDoc(registry.UId,docTypes);
                    }
                    else
                    {
                        return OperationResult.StateIncompatibleOperation;
                    }
                }
                finally
                {
                    // Убрать блокировку даже в случае ошибки.
                    _documentRepository.UnlockDocument(claim.UId, userId);
                }
            }
            else if (lockResult.IsSucceed && lockResult.Data == false)
            {
                return OperationResult.DocumentLocked;
            }
            else
            {
                return lockResult.Result;
            }
        }

        #region Прикрепляем файлы электронных документов

        public OperationResult AttachRegistryDoc(Guid registryUid,int[] docTypes)
        {
            var getDocOperation = _documentRepository.GetRegistryDocs(registryUid);//Получаем документы
            if (!getDocOperation.IsSucceed) 
                return getDocOperation.Result;
            var docs = getDocOperation.Data;
            List<DocFile> files = new List<DocFile>();//Полученные файлы
            List<RegistryDoc> cloneDocs = new List<RegistryDoc>();//Клонированные документы
            foreach (var doc in docs)
            {
                if (doc.IsScan)//Если сканированный, то пропускаем
                    continue;
                if (!docTypes.Contains(doc.DocTypeId))//Пропускаем типы докуметов, которые нужно 
                    continue;
                if (string.IsNullOrWhiteSpace(doc.PackageIndex))//Если нет индекса пакета, то не подгружаем файлы
                    continue;
                OperationResult<DocFile[]> attachResult;
                if (doc.DocSourceId != 1) //Если документ пришел не из Sap
                    attachResult = AttachFileToDocument(doc,true);//Добавляем файлы к документу
                else //Документ из сап
                    attachResult = GetCloneFiles(doc,cloneDocs, docTypes);
                if (!attachResult.IsSucceed) 
                    return attachResult.Result;
                files.AddRange(attachResult.Data);
            }
            return _documentRepository.UpdateFileDocs(cloneDocs.ToArray(), files.ToArray());
        }

        private OperationResult<DocFile[]> GetCloneFiles(RegistryDoc registryDoc, List<RegistryDoc> cloneDocs, int[] docTypes)
        {
            List<DocFile> files = new List<DocFile>();
            foreach (var docTypeId in docTypes)//Обходим все типы файлов
            {
                var cloneDoc = registryDoc.CloneDocument();
                cloneDoc.DocTypeId = docTypeId;
                cloneDoc.ParentDocUId = registryDoc.UId;
                var result = AttachFileToDocument(cloneDoc, false);
                if (!result.IsSucceed) return result;//Если ошибка, то выходим.
                files.AddRange(result.Data);//Добавляем файлы
        }
            return new OperationResult<DocFile[]>(OperationResult.Succeed,files.ToArray());
        }

        //Прикрепляем файлы к документу
        public OperationResult<DocFile[]> AttachFileToDocument(RegistryDoc doc,bool checkFileName)
        {
            List<DocFile> files = new List<DocFile>();
            if (string.IsNullOrWhiteSpace(doc.PackageIndex)) 
                return new OperationResult<DocFile[]>(OperationResult.Succeed, files.ToArray());
            string[] currentDocFileNames = new string[] { };
            if (checkFileName)//Если нужна проверка имен файлов
            {
                var getFileResult = GetDocumentFileNames(doc.UId);
                if (!getFileResult.IsSucceed)
                    return new OperationResult<DocFile[]>(getFileResult.Result, new DocFile[] { });
                currentDocFileNames = getFileResult.Data;
            }
            SPRDocType type = (SPRDocType)Enum.Parse(typeof(SPRDocType), doc.DocTypeId.ToString());//Получаем тип документа
            var edocsResult = _saperionRepository.GetEDocFiles(doc.PackageIndex,type);//Получаем файлы для данного типа и пакета из Saperion
            if (!edocsResult.IsSucceed) 
                return new OperationResult<DocFile[]>(edocsResult.Result, files.ToArray());//Если ошибка, то выходим
            var data = edocsResult.Data;
            foreach (var file in data)//Обходим все файлы
            {
                if (currentDocFileNames.Contains(file.Name.ToUpper())) 
                    continue;//Если файл с таким именем уже есть, то пропускаем 
                int fileTypeId = GetFileTypeByName(file.Name);//Получаем тип файла по имени
                if (fileTypeId == 1) continue;
                DocFile docFile = new DocFile();
                docFile.ChangeStatus = System.Data.Entity.EntityState.Added;
                docFile.UId = Guid.NewGuid();
                docFile.FileTypeId = fileTypeId;
                docFile.Name = file.Name;
                docFile.Content = file.Content;
                docFile.DocumentUid = doc.UId;
                files.Add(docFile);
            }
            return new OperationResult<DocFile[]>(OperationResult.Succeed,files.ToArray());
        }

        private OperationResult<string[]> GetDocumentFileNames(Guid documentUid)
        {
            var docFilesResult = _documentRepository.GetDocumentFilesDiscritionOnly(documentUid);//Получаем уже существующие файлы документа
            if (!docFilesResult.IsSucceed)
                return new OperationResult<string[]>(docFilesResult.Result, new string[]{ });//Если ошибка, то выходим
            var currentDocFileNames = docFilesResult.Data.Select(s => s.Name.ToUpper()).ToArray();//Уже существующие имена файлов
            return new OperationResult<string[]>(OperationResult.Succeed, currentDocFileNames);
        }
        //Получаем тип файла по имени файла 
        private int GetFileTypeByName(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return 1;
            fileName = fileName.ToUpper().Trim();
            if (fileName.StartsWith("DP_IAKTPRM") && fileName.EndsWith(".XML")) return 4;
            if (fileName.StartsWith("DP_IAKTPRM") && fileName.EndsWith(".XML.SIGN")) return 5;
            if (fileName.StartsWith("DP_ZAKTPRM") && fileName.EndsWith(".XML")) return 6;
            if (fileName.StartsWith("DP_ZAKTPRM") && fileName.EndsWith(".XML.SIGN")) return 7;
            if (fileName.StartsWith("ON_KORSFAKT") && fileName.EndsWith(".XML")) return 8;
            if (fileName.StartsWith("ON_KORSFAKT") && fileName.EndsWith(".XML.SIGN")) return 9;
            if (fileName.StartsWith("ON_SFAKT") && fileName.EndsWith(".XML")) return 10;
            if (fileName.StartsWith("ON_SFAKT") && fileName.EndsWith(".XML.SIGN")) return 11;
            if (fileName.StartsWith("DP_OTORG12") && fileName.EndsWith(".XML")) return 12;
            if (fileName.StartsWith("DP_OTORG12") && fileName.EndsWith(".XML.SIGN")) return 13;
            if (fileName.StartsWith("DP_PTORG12") && fileName.EndsWith(".XML")) return 14;
            if (fileName.StartsWith("DP_PTORG12") && fileName.EndsWith(".XML.SIGN")) return 15;
            return 1;
        }

        #endregion

        #region Экспортируем требование
        public OperationResult<ExpError[]> ExportClaim(Guid claimGuid,string fullFileName)
        {
            List<ExpError> expErrors = new List<ExpError>();
            List<ExpFileInfo> filesXml = new List<ExpFileInfo>();
            List<ExpFileInfo> filesImages = new List<ExpFileInfo>();
            ExpClaim claimExp = new ExpClaim(claimGuid);
            var resultGetRegistry = _documentRepository.GetRegistriesByClaimUid(claimGuid);//Получаем реестры требования
            if (!resultGetRegistry.IsSucceed) new OperationResult<ExpError[]>(resultGetRegistry.Result, expErrors.ToArray());
            Registry[] registries = resultGetRegistry.Data;//Реестры требования
            foreach (var registry in registries)
            {
                RegistryValidate(registry, expErrors);//Валедируем реестр
                var exportRegistryResult = ExportRegistry(registry, claimExp, filesXml, filesImages, expErrors);
                if (!exportRegistryResult.IsSucceed) new OperationResult<ExpError[]>(exportRegistryResult, expErrors.ToArray());
            }
            expErrors.AddRange(claimExp.CanBuild());
            //Если есть ошибки, то не экспортируем архив, а просто возвращаем ошибки.
            if (expErrors.Count > 0) return new OperationResult<ExpError[]>(OperationResult.ClaimExportError, expErrors.ToArray());
            //Экспортируем данные
            var exportResult = _ifnsExporter.CreateArchive(fullFileName, claimExp, filesXml.ToArray(), filesImages.ToArray());
            if (!exportResult.IsSucceed) new OperationResult<ExpError[]>(exportResult, expErrors.ToArray());
            return new OperationResult<ExpError[]>(OperationResult.Succeed , expErrors.ToArray());
        }

        //Праверяем правильно ли заполнен реестр
        private void RegistryValidate(Registry registry, List<ExpError> expErrors)
        {
            if (string.IsNullOrWhiteSpace(registry.ClaimItemNo))
            {
                expErrors.Add(ExpError.NoClaimItem(registry.UId));
            }
            else
            {
                if (!Regex.IsMatch(registry.ClaimItemNo, @"^1\.\d\d$"))
                    expErrors.Add(ExpError.WrongClaimItemNo(registry.UId, registry.ClaimItemNo));
            }
        }

        //Экспортирует Реест//
        private OperationResult ExportRegistry(
            Registry registry, 
            ExpClaim claimExp, 
            List<ExpFileInfo> filesXml, 
            List<ExpFileInfo> filesImages, 
            List<ExpError> expErrors)
        {
            var docsResult = _documentRepository.GetRegistryDocsSP(registry.UId);
            if (!docsResult.IsSucceed) return docsResult.Result;
            var docs = docsResult.Data.Where(s=>s.DocSourceId == 0).ToArray();//Документы
            foreach (var doc in docs)//Обходим все документы
            {
                var exportDocResult = ExportDocument(registry, doc, registry.ClaimItemNo, claimExp, filesXml, filesImages, expErrors);
                if (!exportDocResult.IsSucceed) return exportDocResult;
            }
            return OperationResult.Succeed;
        }
        //Экспортирует документ//
        private OperationResult ExportDocument(
            Registry registy,
            RegistryDocSet document, 
            string claimItemNo, 
            ExpClaim claimExp, 
            List<ExpFileInfo> filesXml, 
            List<ExpFileInfo> filesImages, 
            List<ExpError> expErrors)
        {
            var fileResult = _documentRepository.GetDocumentFiles(document.UId);
            var files = fileResult.Data;
            if (!fileResult.IsSucceed || files == null || files.Length == 0) return fileResult.Result;
           // FixExctention(files);//Фиксим расширения меняем .xml.sign на SGN.sgn
            //----------------------------//
            //Выгружаем Акт (электронный) //
            //---------------------------//
            #region
            if (document.DocTypeId == 4 && !document.IsScan)
            {
                //Получаем файл продовца//
                var fileDPIAKTPRM = fileResult.Data.Where(file => file.FileTypeId == 4).FirstOrDefault();
                if (fileDPIAKTPRM == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У акта №{0} нет файла DP_IAKTPRM...xml (Продавец)", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPIAKTPRM.Name, fileDPIAKTPRM.Content));//Добавляем файл
                //Получаем файл подписи продовца//
                var fileDPIAKTPRMsign = fileResult.Data.Where(file => file.FileTypeId == 5).FirstOrDefault();
                if (fileDPIAKTPRMsign == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У акта №{0} нет файла подписи DP_IAKTPRM...sign (Продавец)", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPIAKTPRMsign.Name, fileDPIAKTPRMsign.Content));//Добавляем файл
                //Получаем файл покупателя//
                var fileDPZAKTPRM = fileResult.Data.Where(file => file.FileTypeId == 6).FirstOrDefault();
                if (fileDPZAKTPRM == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У акта №{0} нет файла DP_ZAKTPRM...xml (Покупатель)", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPZAKTPRM.Name, fileDPZAKTPRM.Content));//Добавляем файл
                //Получаем файл подписи покупателя//
                var fileDPZAKTPRMsign = fileResult.Data.Where(file => file.FileTypeId == 7).FirstOrDefault();
                if (fileDPZAKTPRMsign == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У акта №{0} нет файла подписи DP_ZAKTPRM......sign (Покупатель)", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPZAKTPRMsign.Name, fileDPZAKTPRMsign.Content));//Добавляем файл
                if (string.IsNullOrWhiteSpace(document.ParentDocNumber))
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У акта №{0} не заполнен номер родительского документа", document.Number)));
                    return OperationResult.Succeed;
                }
                if (document.ParentDocDate == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У акта №{0} не заполнена дата родительского документа", document.Number)));
                    return OperationResult.Succeed;
                }
                ExpFormalizedAccCert act = new ExpFormalizedAccCert(
                    document.UId,
                    registy.ClaimItemNo,
                    string.Empty,
                    fileDPIAKTPRM.Name,
                    fileDPIAKTPRMsign.Name,
                    fileDPZAKTPRM.Name,
                    fileDPZAKTPRMsign.Name,
                    document.ParentDocNumber,
                    document.ParentDocDate);
                claimExp.AddDocument(act);
            }
            #endregion
            //----------------------------------------------------------------//
            //Выгружаем электронную корректировочную счет-фактуру (электронный)//
            //----------------------------------------------------------------//
            #region
            else if (document.DocTypeId == 11 && !document.IsScan)
            {
                //Получаем файл корректировочной счет-фактуры//
                var fileONKORSFAKT = fileResult.Data.Where(file => file.FileTypeId == 8).FirstOrDefault();
                if (fileONKORSFAKT == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У корректировочной счет-фактруы №{0} нет файла ON_KORSFAKT...xml", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileONKORSFAKT.Name, fileONKORSFAKT.Content));//Добавляем файл
                //Получаем файл подписи//
                var fileONKORSFAKTsign = fileResult.Data.Where(file => file.FileTypeId == 9).FirstOrDefault();
                if (fileONKORSFAKTsign == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У корректировочной счет-фактруы №{0} нет файла подписи ON_KORSFAKT...sign", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileONKORSFAKTsign.Name, fileONKORSFAKTsign.Content));//Добавляем файл
                //Созадаем описание//
                ExpFormalizedInvoiceCorrection expFormalizedInvoice = new ExpFormalizedInvoiceCorrection(
                    document.UId, 
                    registy.ClaimItemNo, 
                    string.Empty, 
                    fileONKORSFAKT.Name, 
                    fileONKORSFAKTsign.Name);
                claimExp.AddDocument(expFormalizedInvoice);
            }
            #endregion
            //-----------------------------------------------//
            //Выгружаем электронную счет-фактуру (электронный)//
            //-----------------------------------------------//
            #region
            else if (document.DocTypeId == 3 && !document.IsScan)
            {
                //Получаем файл счет фактуры//
                var fileONSFAKT = fileResult.Data.Where(file => file.FileTypeId == 10).FirstOrDefault();
                if (fileONSFAKT == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У счет фактруы №{0} нет файла ON_SFAKT...xml", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileONSFAKT.Name,fileONSFAKT.Content));//Добавляем файл
                //Получаем файл подписи//
                var fileONSFAKTsign = fileResult.Data.Where(file => file.FileTypeId == 11).FirstOrDefault();
                if (fileONSFAKT == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У счет фактруы №{0} нет файла подписи ON_SFAKT...sign", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileONSFAKTsign.Name, fileONSFAKTsign.Content));//Добавляем файл
                //Созадаем описание//
                ExpFormalizedInvoice expFormalizedInvoice = new ExpFormalizedInvoice(
                    document.UId,
                    registy.ClaimItemNo, 
                    string.Empty, 
                    fileONSFAKT.Name, 
                    fileONSFAKTsign.Name);
                claimExp.AddDocument(expFormalizedInvoice);
            }
            #endregion
            //-----------------------------------------//
            //Выгружаем товарную накладную (электронный)//
            //-----------------------------------------//
            #region
            else if (document.DocTypeId == 5 && !document.IsScan)
            {
                //Получаем файл продовца//
                var fileDPOTORG12 = fileResult.Data.Where(file => file.FileTypeId == 12).FirstOrDefault();
                if (fileDPOTORG12 == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У товарнаой накладной №{0} нет файла DP_OTORG12 (Продавец)", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPOTORG12.Name, fileDPOTORG12.Content));//Добавляем файл
                //Получаем файл подписи продовца//
                var fileDPOTORG12sign = fileResult.Data.Where(file => file.FileTypeId == 13).FirstOrDefault();
                if (fileDPOTORG12sign == null)
                {
                    expErrors.Add(new ExpError(document.UId,string.Format("У товарнаой накладной №{0} нет файла подписи DP_OTORG12...sign (Продавец)", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPOTORG12sign.Name, fileDPOTORG12sign.Content));//Добавляем файл
                //Получаем файл покупателя//
                var fileDPPTORG12 = fileResult.Data.Where(file => file.FileTypeId == 14).FirstOrDefault();
                if (fileDPPTORG12 == null)
                {
                    expErrors.Add(new ExpError(document.UId,string.Format("У товарнаой накладной №{0} нет файла DP_PTORG12 (Покупатель)", document.Number)));
                return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPPTORG12.Name, fileDPPTORG12.Content));//Добавляем файл
                //Получаем файл подписи покупателя//
                var fileDPPTORG12sign = fileResult.Data.Where(file => file.FileTypeId == 15).FirstOrDefault();
                if (fileDPPTORG12sign == null)
                {
                    expErrors.Add(new ExpError(document.UId, string.Format("У товарнаой накладной №{0} нет файла подписи DP_PTORG12...sign (Продавец)", document.Number)));
                    return OperationResult.Succeed;
                }
                filesXml.Add(new ExpFileInfo(fileDPPTORG12sign.Name, fileDPPTORG12sign.Content));//Добавляем файл
                ExpFormalizedTorg12 expFormalizedTorg12 = new ExpFormalizedTorg12(
                    document.UId, 
                    registy.ClaimItemNo,
                    string.Empty,
                    fileDPOTORG12.Name,fileDPOTORG12sign.Name,fileDPPTORG12.Name,fileDPPTORG12sign.Name);
                claimExp.AddDocument(expFormalizedTorg12);
            }
            #endregion
           
            #region
            else if (document.IsScan)
            {
                AgentInfo buyer = null;
                if (document.BuyerUId != null)
                {
                    buyer = document.BuyerPartnerTypeId == 1 ?
                    (AgentInfo)new OrganizationInfo(document.UId, document.BuyerKpp, document.BuyerInn, document.BuyerName) :
                    (AgentInfo)new PersonalInfo(document.UId, document.BuyerInn, document.BuyerName, document.BuyerMiddleName, document.BuyerLastName);
                }
                AgentInfo seller = null;
                if (document.SellerUId != null)
                {
                    seller =
                    document.SellerPartnerTypeId == 1 ?
                    (AgentInfo)new OrganizationInfo(document.UId, document.SellerKpp, document.SellerInn, document.SellerName) :
                    (AgentInfo)new PersonalInfo(document.UId, document.SellerInn, document.SellerName, document.SellerMiddleName, document.SellerLastName);
                }
                
                //ImageFileInfo
                ImageFileInfo[] images = files.Select(file => new ImageFileInfo(file.Name, file.PageFrom, file.PageTo)).ToArray();
                ExpFileInfo[] expImages = files.Select(file=> new ExpFileInfo(file.Name,file.Content)).ToArray();
                filesImages.AddRange(expImages);
                  //------------------------------//
                 //Выгружаем Акт (сканированный) //
                //-----------------------------//
                if (document.DocTypeId == 4)
                {
                    ExpScannedAccCert act = new ExpScannedAccCert(
                        document.UId,
                        claimItemNo,
                        string.Empty,
                        images,
                        document.DocDate,
                        document.ParentDocNumber,
                        document.ParentDocDate,
                        document.SumTotal,
                        buyer,
                        seller);
                    claimExp.AddDocument(act);
                }
                  //-------------------------------------------------------//
                 //Выгружаем корретировочную счет-фактруру (сканированния)-//
                //-------------------------------------------------------//
                else if (document.DocTypeId == 11)
                {
                    ExpScannedInvoiceCorrection invoiceCorrection = new ExpScannedInvoiceCorrection(
                        document.UId,
                        claimItemNo,
                        string.Empty,
                        images,
                        document.Number,
                        document.DocDate,
                        document.ParentDocNumber,
                        document.ParentDocDate,
                        document.SumTax,
                        document.SumTotal,
                        buyer,
                        seller);
                    claimExp.AddDocument(invoiceCorrection);
                }
                  //----------------------//
                 //Выгружаем счет-фактуру//
                //----------------------//
                else if (document.DocTypeId == 3)
                {
                    ExpScannedInvoice invoce = new ExpScannedInvoice(
                        document.UId,
                        claimItemNo,
                        string.Empty,
                        images,
                        document.Number,
                        document.DocDate,
                        document.SumTax,
                        document.SumTotal,
                        buyer,
                        seller);
                    claimExp.AddDocument(invoce);
                }
                  //------------------//
                 //Товарную накладную//
                //------------------//
                else if (document.DocTypeId == 5)
                {
                    ExpScannedTorg12 torg12 = new ExpScannedTorg12(
                        document.UId, 
                        claimItemNo, 
                        string.Empty, 
                        images, 
                        document.DocDate, 
                        document.SumTotal, 
                        buyer, 
                        seller);
                    claimExp.AddDocument(torg12);
                }
            }
            #endregion
            return OperationResult.Succeed;
        }

        #endregion
        //Правим расширения файлов
        public void FixExctention(DocFile[] docFiles)
        {
            string extention = ".xml.sign";
            foreach (var file in docFiles)
            {
                if (file.Name.ToLower().EndsWith(extention))//Если это подпись
                {
                    string fileName = file.Name.Substring(0, file.Name.Length - extention.Length);
                    file.Name = fileName + "SGN.sgn";
                }
            }
        }

        /*public OperationResult GetOperationResult(Exception exception)
        {
            int codeEx = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            int code = codeEx == 0 ? 1 : codeEx;
            string message = exception.ToString();
            string source = OperationResult.WebSite;
            //WriteToLog(message);
            return new OperationResult(code, message, source, true);
        }*/
    }
}
