﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGC.TaxClaims.Common.Filtering;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Infrastructure.Services;
using SGC.TaxClaims.WebUI.AppCode;
using SGC.TaxClaims.WebUI.ViewModels;

namespace SGC.TaxClaims.WebUI.Controllers
{
    /// <summary>
    /// Контроллер реестров
    /// </summary>
    public class RegistryController : BaseController
    {
        private IDBRepository _documentRepository;
        private ISaperion _sprRepository;
        private IExcelImport _excelImport;
        private IIFNSExporter _ifnsExporter;

        public RegistryController(
            IDBRepository documentRepository, 
            ISaperion sprRepository,
            IExcelImport excelImport,
            IIFNSExporter ifnsExporter)
        {
            _documentRepository = documentRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ByClaim(Guid cid)
        {
            RegistriesListModel model = new RegistriesListModel();
            model.ClaimUid = cid.ToString();

            // Проверить, что требование с заданным идентификатором существует
            OperationResult<Registry[]> result = _documentRepository.GetRegistriesByClaimUid(cid);

            if (result.Data != null)
            {
                // TODO: [Петр] Вынести в общий код (похожий код в Claim/Details)
                model.Registries = result.Data.Select(registry =>
                        new RegistryDisplayModel()
                        {
                            Uid = registry.UId,
                            ClaimItemNo = registry.ClaimItemNo,
                            DateReady = registry.DateReady,
                            RegistryName = registry.Name,
                            UserCreator = registry.CreatorUser.UserName,
                            DateCreated = registry.DateCreated
                        })
                        .ToList();

                // Заполнение данных навигации
                ViewData["NavigationArea"] = new NavigationArea(new List<NavigationLink>()
                {
                    new NavigationLink() { Title = "Требование", Tooltip = "Требование", Url = Url.Action("Details", "Claim", new { claimUid = cid}) },
                    new NavigationLink() { Title = "Реестры", Tooltip = "Реестры", Url = Url.Action("ByClaim", "Registry", new { cid = cid}), IsActive = true }
                });

                return View(model);

            }
            else if (result.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении списка реестров по требованию (Идентификатор требования = {0}).", cid), result.Result);
                return RedirectToGenericError();
            }
            else
            {
                return NotFound<Claim>(cid.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid">Claim Id - Идентификатор требования, к которому прикрепляем реестр</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create(Guid cid)
        {
            RegistryCreateModel viewModel = new RegistryCreateModel();
            viewModel.ClaimId = cid;

            // Проверить, что требование с заданным идентификатором существует
            OperationResult<Claim> result = _documentRepository.GetClaimById(cid);

            if (result.Data != null)
            {
                return View(viewModel);
            }
            else if (result.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении требования (Идентификатор требования = {0}).", cid), result.Result);
                return RedirectToGenericError();
            }
            else
            {
                return NotFound<Claim>(viewModel.ClaimId.ToString());
            }
        }

        [HttpPost]
        public ActionResult Create(RegistryCreateModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            //return View(viewModel);

            Registry registry = Registry.Create();
            viewModel.UpdateModel(registry);

            // Получение текущего пользователя
            OperationResult<aspnet_Users> currentUser = _documentRepository.GetUserByName(User.Identity.Name);
            if (currentUser.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении пользователя (UserName = {0}).", User.Identity.Name), currentUser.Result);
                return RedirectToGenericError();
            }
            else if (currentUser.Data == null)
            {
                return NotFound<aspnet_Users>(User.Identity.Name);
            }

            DocumentService docService = new DocumentService(_documentRepository,_sprRepository,_ifnsExporter);

            // 1. Получить требование
            // 
            OperationResult<Claim> claimResult = _documentRepository.GetClaimById(viewModel.ClaimId);

            if (claimResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при создании реестра (Идентификатор требования = {0}).", viewModel.ClaimId), claimResult.Result);
                return RedirectToGenericError();
            }
            else if (claimResult.Data == null)
            {
                return NotFound<Claim>(viewModel.ClaimId);
            }

            // 2. Проверить, что требование находится в состоянии, в котором еще возможно создание реестра
            // 
            OperationResult grantResult = docService.CanCreateDocumentInCurrentState(claimResult.Data, registry.DocTypeId);

            if (grantResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при создании реестра (Идентификатор требования = {0}).", viewModel.ClaimId), grantResult);
                return RedirectToGenericError();
            }
            else if (!grantResult.IsSucceed)
            {
                ModelState.AddRuleViolation(new RuleViolation(grantResult.Message, ""));
                return View(viewModel);
            }

            // 3. Инициализация вложенных документов
            foreach (var childDoc in registry.Document1)
            {
                //var childDoc = relation.DocumentChild;

                //OperationResult<DocState> initialState = _documentRepository.GetInitialState(childDoc.DocTypeId, (int)DocumentRoute.ToIFNS);
                //childDoc.DocStateId = initialState.Data.Id;
                childDoc.DocStateId = claimResult.Data.DocStateId; // Состояние (DocState) всех дочерних документов должно быть таким же как у корневого документа (требования)

                childDoc.CreatorUserUid = currentUser.Data.UserId;
            }

            // 4. Создание реестра
            //
            OperationResult createResult = docService.CreateRegistry(currentUser.Data.UserId, claimResult.Data, registry,new int[]{3,4,5,11});

            if (createResult.IsSucceed)
            {
                return RedirectToAction("Index", "Claim");
            }
            else if (createResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при создании реестра (Идентификатор требования = {0}).", viewModel.ClaimId), createResult);
                return RedirectToGenericError();
            }
            else
            {
                ModelState.AddRuleViolation(new RuleViolation(createResult.Message, ""));
                return View(viewModel);
            }
        }

        [HttpPost]
        public ActionResult AddDoc(SourceDocCreateModel vm)
        {
            if (!ModelState.IsValid)
            {
                // TODO: [Петр] нормально валидировать
                return Json(new HtmlResult()
                {
                    errorState = true,
                    errorMessage = "Неправильно заполнены данные"
                });
            }

            string html;

            // TODO: [Петр] переделать
            switch (vm.DocTypeValue)
            {
                case "Invoice":
                    html = MvcHelper.RenderPartialViewToString(ControllerContext, ViewData, TempData, "InvoiceCreateModel", new InvoiceCreateModel() { DocNumber = vm.DocNumber, DocDate = vm.DocDate, Index = 0, PackageIndex = vm.PackageIndex });
                    break;
                case "Torg12":
                    html = MvcHelper.RenderPartialViewToString(ControllerContext, ViewData, TempData, "Torg12CreateModel", new Torg12CreateModel() { DocNumber = vm.DocNumber, DocDate = vm.DocDate, Index = 0, PackageIndex = vm.PackageIndex });
                    break;
                default:
                    throw new NotImplementedException();
                    
            }
            return Json(new HtmlResult(){
                 htmlContent = html
            });
        }

        [HttpGet]
        public ActionResult Edit(Guid rid)
        {
            // 1. Получить реестр
            // 
            OperationResult<Registry> regResult = _documentRepository.GetRegistryByUid(rid);

            if (regResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении реестра (Идентификатор реестра = {0}).", rid), regResult.Result);
                return RedirectToGenericError();
            }
            else if (regResult.Data == null)
            {
                return NotFound<Claim>(rid);
            }

            // 2. Получить документы, входящие в реестр
            //OperationResult<Document[]> docsResult = _documentRepository.GetChildDocuments(rid);
            OperationResult<RegistryDoc[]> docsResult = _documentRepository.GetRegistryDocs(rid);

            if (docsResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении дочерних документов реестра (Идентификатор реестра = {0}).", rid), docsResult.Result);
                return RedirectToGenericError();
            }
            RegistryEditModel viewModel = new RegistryEditModel(regResult.Data, docsResult.Data);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(RegistryEditModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            return View();
        }

        [HttpGet]
        public ActionResult Fill(Guid rid)
        {
            // 1. Получить реестр
            // 
            OperationResult<Registry> regResult = _documentRepository.GetRegistryByUid(rid);

            if (regResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении реестра (Идентификатор реестра = {0}).", rid), regResult.Result);
                return RedirectToGenericError();
            }
            else if (regResult.Data == null)
            {
                return NotFound<Claim>(rid);
            }

            // 2. Получить документы, входящие в реестр
            //OperationResult<Document[]> docsResult = _documentRepository.GetChildDocuments(rid);
            OperationResult<RegistryDoc[]> docsResult = _documentRepository.GetRegistryDocs(rid);

            if (docsResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении дочерних документов реестра (Идентификатор реестра = {0}).", rid), docsResult.Result);
                return RedirectToGenericError();
            }

            // Получить идентификатор родительского требования
            Guid cid = Guid.Empty;
            var parentRelation = regResult.Data.ParentDocUId;
            if(parentRelation != null) {
                cid = parentRelation.Value;
            }

            // Заполнение данных навигации
            ViewData["NavigationArea"] = new NavigationArea(new List<NavigationLink>()
                {
                    new NavigationLink() { Title = "Требование", Tooltip = "Требование", Url = Url.Action("Details", "Claim", new { claimUid = cid }) },
                    new NavigationLink() { Title = "Реестры", Tooltip = "Реестры", Url = Url.Action("ByClaim", "Registry", new { cid = cid}) },
                    new NavigationLink() { Title = "Реестр", Tooltip = "Реестр", Url = Url.Action("Fill", "Registry", new { rid = rid}), IsActive = true }
                });

            RegistryFillModel viewModel = new RegistryFillModel(regResult.Data, docsResult.Data);
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Fill(RegistryFillModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            return View();
        }

        /// <summary>
        /// Просмотр реестра
        /// </summary>
        /// <param name="rid"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult View(Guid rid)
        {
            // 1. Получить реестр
            // 
            OperationResult<Registry> regResult = _documentRepository.GetRegistryByUid(rid);

            if (regResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении реестра (Идентификатор реестра = {0}).", rid), regResult.Result);
                return RedirectToGenericError();
            }
            else if (regResult.Data == null)
            {
                return NotFound<Claim>(rid);
            }

            // 2. Получить документы, входящие в реестр
            OperationResult<Document[]> docsResult = _documentRepository.GetChildDocuments(rid);

            if (docsResult.IsSystemError)
            {
                AppLogger.Error(String.Format("Возникла ошибка при получении дочерних документов реестра (Идентификатор реестра = {0}).", rid), docsResult.Result);
                return RedirectToGenericError();
            }

            RegistryDisplayModel viewModel = new RegistryDisplayModel(regResult.Data, docsResult.Data);

            return View(viewModel);
        }

        /// <summary>
        /// Метод, к которому обращается Grid для загрузки данных
        /// </summary>
        /// <param name="gridSettings"></param>
        /// <returns></returns>
        public JsonResult GridData(MvcJqGrid.GridSettings gridSettings,Guid claimUid)
        {
            int pageSize = gridSettings.PageSize;
            int pageIndex = gridSettings.PageIndex - 1;
            string sortColumn = gridSettings.SortColumn;
            bool? asc = gridSettings.SortOrder == "asc";
            int totalRecords;

            FilterGroup filter = JqGridHelper.ParseFilter(gridSettings.Where);

            var operationResult = _documentRepository.GetRegistriesByClaimUid(claimUid);
            //TODO: [Петр] тут наверно что-то нужно вывести на экран в случае неудачи

            //OperationResult<PageResponse<Registry>> registries = _documentRepository.GetRegistriesPage(pageSize, pageIndex, sortColumn, asc, filter, out totalRecords);
            
            return Json(
                new
                {
                    total = 0, // registries.Data.PagesCount,
                    page = 1, // registries.Data.PageIndex + 1,
                    records = 0, // registries.Data.RecordsCount,
                    rows = operationResult.Data.Select(s =>
                            new RegistryIndexModel()
                            {
                                Uid = s.UId,
                                ClaimItemNo = s.ClaimItemNo,
                                ReadyDate = s.DateReady,
                                RegistryName = s.Name,
                                Status = s.DocState.DisplayName
                            }).ToList() 
                }
                , JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Модель создания требования дополнить списокм предприятий-получателей
            if (filterContext.Controller.ViewData.Model is RegistryCreateModel)
            {
                var model = (RegistryCreateModel)filterContext.Controller.ViewData.Model;
                model.SourceDocCreateModel.SourceDocTypes = GetSourceDocTypes();
            }
            else if (filterContext.Controller.ViewData.Model is RegistryEditModel)
            {
                var model = (RegistryEditModel)filterContext.Controller.ViewData.Model;
                model.SourceDocCreateModel.SourceDocTypes = GetSourceDocTypes();
            }
        }

        /// <summary>
        /// Получение типов первичных документов, которые можно добавить вручную к реестру.
        /// Т.е. для которых можно указать номер и дату документа.
        /// </summary>
        /// <param name="model"></param>
        private static SelectList GetSourceDocTypes()
        {
            // TODO: [Петр] Заполнить типы документов реальными значениями
            return new SelectList(new List<SelectListItem>()
                {
                    new SelectListItem() { Value = "Invoice", Text = "Счет-фактура", Selected = true },
                    new SelectListItem() { Value = "AccCert", Text = "Акт приемки-сдачи работ/услуг" },
                    new SelectListItem() { Value = "Torg12", Text = "Товарная накладная ТОРГ-12" },
                    new SelectListItem() { Value = "InvoiceCorrection", Text = "Корректировочный счет-фактура" }
                    //new SelectListItem() { Value = "PurchaseBook", Text = "Книга покупок" },
                    //new SelectListItem() { Value = "SalesBook", Text = "Книга продаж" },
                    //new SelectListItem() { Value = "InvoicesBook", Text = "Журнал полученных и выставленных счетов-фактур" },
                    //new SelectListItem() { Value = "PurchaseBookList", Text = "Дополнительный лист Книги покупок" },
                    //new SelectListItem() { Value = "SalesBookList", Text = "Дополнительный лист Книги продаж" }
                    //new SelectListItem() { Value = "ScanInvoice", Text = "Счет-фактура (скан)", Selected = true },
                    //new SelectListItem() { Value = "XmlInvoice", Text = "Счет-фактура (эл.)" },
                    //new SelectListItem() { Value = "ScanAccCert", Text = "Акт приемки-сдачи работ/услуг (скан)" },
                    //new SelectListItem() { Value = "XmlAccCert", Text = "Акт приемки-сдачи работ/услуг (эл.)" },
                    //new SelectListItem() { Value = "ScanTorg12", Text = "Товарная накладная ТОРГ-12 (скан)" },
                    //new SelectListItem() { Value = "XmlTorg12", Text = "Товарная накладная ТОРГ-12 (эл.)" },
                    //new SelectListItem() { Value = "ScanInvoiceCorrection", Text = "Корректировочный счет-фактура (скан)" },
                    //new SelectListItem() { Value = "XmlInvoiceCorrection", Text = "Корректировочный счет-фактура (эл.)" },
                    //new SelectListItem() { Value = "XmlPurchaseBook", Text = "Книга покупок" },
                    //new SelectListItem() { Value = "XmlSalesBook", Text = "Книга продаж" },
                    //new SelectListItem() { Value = "XmlInvoicesBook", Text = "Журнал полученных и выставленных счетов-фактур" },
                    //new SelectListItem() { Value = "XmlPurchaseBookList", Text = "Дополнительный лист Книги покупок" },
                    //new SelectListItem() { Value = "XmlSalesBookList", Text = "Дополнительный лист Книги продаж" }
                }, "Value", "Text");
        }
    }
}