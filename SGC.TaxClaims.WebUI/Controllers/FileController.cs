﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGC.TaxClaims.WebUI.ViewModels;

namespace SGC.TaxClaims.WebUI.Controllers
{
    public class FileController : Controller
    {
        private string UploadFolder { get { return Path.GetTempPath(); } }

        ////DONT USE THIS IF YOU NEED TO ALLOW LARGE FILES UPLOADS
        //[HttpGet]
        //public void Download(string id)
        //{
        //    var filename = id;
        //    var filePath = Path.Combine(Server.MapPath("~/Files"), filename);

        //    var context = HttpContext;

        //    if (System.IO.File.Exists(filePath))
        //    {
        //        context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        //        context.Response.ContentType = "application/octet-stream";
        //        context.Response.ClearContent();
        //        context.Response.WriteFile(filePath);
        //    }
        //    else
        //        context.Response.StatusCode = 404;
        //}

        //DONT USE THIS IF YOU NEED TO ALLOW LARGE FILES UPLOADS
        [HttpPost]
        public ActionResult UploadFiles()
        {
            var r = new UploadFilesResult();

            //foreach (string file in Request.Files)
            //{
            int itemIndex = 0;
            Int32.TryParse(Request.Params["itemIndex"], out itemIndex);

                var statuses = new List<FileEditModel>();
                var headers = Request.Headers;

                if (string.IsNullOrEmpty(headers["X-File-Name"]))
                {
                    UploadWholeFile(Request, statuses);
                }
                else
                {
                    //UploadPartialFile(headers["X-File-Name"], Request, statuses);
                }

                //JsonResult result = Json(statuses);
                //result.ContentType = "text/plain";

                //return result;

            foreach(var status in statuses)
            {
                status.Index = itemIndex++;
                r.html.Add(MvcHelper.RenderPartialViewToString(ControllerContext, ViewData, TempData, "FileItem", status));
            }
            //}

            return Json(r);
        }

        //DONT USE THIS IF YOU NEED TO ALLOW LARGE FILES UPLOADS
        //Credit to i-e-b and his ASP.Net uploader for the bulk of the upload helper methods - https://github.com/i-e-b/jQueryFileUpload.Net
        private void UploadWholeFile(HttpRequestBase request, List<FileEditModel> statuses)
        {
            for (int i = 0; i < request.Files.Count; i++)
            {
                var file = request.Files[i];

                // Сгенерировать имя для файла - GUID
                string systemFileName = Guid.NewGuid().ToString();

                string fullPath = Path.Combine(UploadFolder, systemFileName);

                file.SaveAs(fullPath);

                statuses.Add(new FileEditModel()
                {
                    Name = file.FileName,
                    SystemName = fullPath,
                    Size = file.ContentLength,
                    Type = file.ContentType,
                    Url = "/File/Download?f=" + file.FileName,
                    UrlDelete = "/File/Delete?f=" + file.FileName,
                    //thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath),
                    //delete_type = "GET",
                });
            }
        }

        // TODO: [Петр] заглушка
        [HttpGet]
        public ActionResult Download(string f)
        {
            string fileName = Path.Combine(Path.GetTempPath(), f);

            Bitmap bmp = new Bitmap(fileName);

            bmp.Save(fileName + ".jpg", ImageFormat.Jpeg);

            return new FilePathResult(fileName + ".jpg", "image/jpeg");
        }

        private string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }
	}


}