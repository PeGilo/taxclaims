﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.WebUI.Controllers
{
    public class ErrorController : Controller // Не наследуется от ControllerBase, чтобы не воспроизводить ошибки, возникающие в базовом классе
    {
        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Generic()
        {
            return View("Error");
        }
	}
}