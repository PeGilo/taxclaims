﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Core;

namespace SGC.TaxClaims.WebUI.Controllers
{
    public class HomeController : BaseController
    {
        /*private IUow _uow;*/
        //private ISampleService _sampleService;

        public HomeController(IDBRepository documentRepository)
        {
            /*_uow = uow;
            _sampleService = sampleService;*/
        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Claim");
        }
    }
}