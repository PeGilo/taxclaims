﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using SGC.TaxClaims.Common.Filtering;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Infrastructure;
using SGC.TaxClaims.WebUI.AppCode;
using SGC.TaxClaims.WebUI.ViewModels;

namespace SGC.TaxClaims.WebUI.Controllers
{
    /// <summary>
    /// Контроллер требований
    /// </summary>
    public class ClaimController : BaseController
    {
        private IDBRepository _documentRepository;

        public ClaimController(IDBRepository documentRepository)
        {
            _documentRepository = documentRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Метод, к которому обращается Grid для загрузки данных
        /// </summary>
        /// <param name="gridSettings"></param>
        /// <returns></returns>
        public JsonResult GridData(MvcJqGrid.GridSettings gridSettings)
        {
            int pageSize = gridSettings.PageSize;
            int pageIndex = gridSettings.PageIndex - 1;
            string sortColumn = gridSettings.SortColumn;
            bool? asc = gridSettings.SortOrder == "asc";
            int totalRecords;

            FilterGroup filter = JqGridHelper.ParseFilter(gridSettings.Where);

            // TODO: [Петр] Check OperationResults
            OperationResult<PageResponse<Claim>> claims = _documentRepository.GetClaimsPage(pageSize, pageIndex, sortColumn, asc, filter, out totalRecords);

            return Json(
                new
                {
                    total = claims.Data.PagesCount,
                    page = claims.Data.PageIndex + 1,
                    records = claims.Data.RecordsCount,
                    rows = claims.Data.Rows
                        .OrderByDescending(claim => claim.DateReady)
                        .Select(claim => new ClaimIndexModel() {
                             Uid = claim.UId,
                             ClaimNumber = claim.Number,
                             Comment = claim.Comment,
                             InspectionName = claim.InspectionName,
                             Status = claim.DocState.DisplayName,
                             TargetOrganization = claim.Organization == null? string.Empty : claim.Organization.Name,
                             DateReady = claim.DateReady
                    })
                }
                , JsonRequestBehavior.AllowGet);

            //return new JsonResult()
            //{ Data = ""
            //   , JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //};
        }

        [HttpGet]
        public ActionResult Create()
        {
            ClaimCreateModel viewModel = new ClaimCreateModel();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Create(ClaimCreateModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            Claim claim = Claim.Create();
            viewModel.UpdateModel(claim);

            // ... Получение текущего пользователя
            OperationResult<aspnet_Users> currentUser = _documentRepository.GetUserByName(User.Identity.Name);

            // TODO: [Петр] Перенести в ModelConverter?
            // Инициализация документа
            claim.UId = Guid.NewGuid();
            claim.CreatorUserUid = currentUser.Data.UserId;

            // TODO: [Петр] Check OperationResult
            // ... Получение состояния документооборота, в который надо выставить документ при создании
            OperationResult<DocState> initialState = _documentRepository.GetInitialState(claim.DocTypeId, (int)DocumentRoute.ToIFNS);
            claim.DocStateId = initialState.Data.Id;

            // ... Получение состояния документоборота, в которое надо перевести документ после создания
            // TODO: [Петр] Check OperationResult
            OperationResult<DocState[]> nextStates = _documentRepository.GetNextPossibleStates(initialState.Data);

            // Присоединить файлы
            AttachFiles(claim, viewModel);

            // Сохранение документа
            claim.ChangeStatus = System.Data.Entity.EntityState.Added;
            // TODO: [Петр] Check ValidationInfo
            // TODO: [Петр] Добавь передачу идентификатора пользователя
            OperationResult<ValidationInfo[]> result = _documentRepository.SaveAndMove(claim, null, nextStates.Data[0].Id,Guid.Empty); // Передвигаем требование в следующее состояние

            return RedirectToAction("Index");
        }

        // TODO: [Петр] Перенести в ModelConverter?
        /// <summary>
        /// Прикрепление файлов к требованию
        /// </summary>
        /// <param name="claim"></param>
        /// <param name="viewModel"></param>
        private void AttachFiles(Claim claim, ClaimCreateModel viewModel)
        {
            foreach (var file in viewModel.Files)
            {
                if (!file.IsDeleted)
                {
                    DocFile docFile = DocFile.Create();
                    file.UpdateModel(docFile);
                    docFile.UId = Guid.NewGuid();
                    docFile.Content = System.IO.File.ReadAllBytes(file.SystemName);
                    docFile.ChangeStatus = System.Data.Entity.EntityState.Added;
                    docFile.DocumentUid = claim.UId;
                    docFile.FileTypeId = 1; // Unspecified
                    claim.DocFile.Add(docFile);
                }
            }
        }

        [HttpGet]
        public ActionResult Edit(Guid claimUid)
        {
            var operationResult = _documentRepository.GetClaimById(claimUid);
            //TODO: [Петр] тут нужно прописать вывод окна ошибки если операция завершенна не успешно
            var claim = operationResult.Data;
            ClaimEditModel model = new ClaimEditModel()
            {
                 ClaimNumber = claim.Number,
                 Comment = claim.Comment,
                 
                 InspectionName = claim.InspectionName
                 
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ClaimEditModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            return View();
        }

        [HttpGet]
        public ActionResult Details(Guid claimUid)
        {
            /*if (!ModelState.IsValid)
            {
                return View(viewModel);
            }*/
            var operationResult = _documentRepository.GetClaimById(claimUid);
            var claim = operationResult.Data;
            var operationResultRegistry = _documentRepository.GetRegistriesByClaimUid(claimUid);
            var registries = operationResultRegistry.Data;
            List<RegistryDisplayModel> registriesDisplay
                = registries.Select(registry => 
                    new RegistryDisplayModel()
                    {
                        Uid = registry.UId,
                        ClaimItemNo = registry.ClaimItemNo,
                        DateReady = registry.DateReady,
                        RegistryName = registry.Name,
                        UserCreator = registry.CreatorUser.UserName,
                        DateCreated = registry.DateCreated
                    })
                    .ToList();

            //TODO: [Петр] надо вставить оконо об ошибке
            ClaimViewModel claimViewModel = new ClaimViewModel()
            {
                ClaimNumber = claim.Number,
                Comment = claim.Comment,
                DateReady = claim.DateReady,
                InspectionName = claim.InspectionName,
                Organization = claim.Organization != null ? claim.Organization.Name : string.Empty,
                Registries = registriesDisplay
            };
            var getFileOperationResult = _documentRepository.GetDocumentFilesDiscritionOnly(claimUid);

            //TODO: [Петр] надо вставить оконо об ошибке
            claimViewModel.Files = getFileOperationResult.Data
                .Where(file => file.DocumentUid == claimUid)
                .Select(file => new FileViewModel() { UId = file.UId, Name = file.Name, TypeName = file.FileType.Name })
                .ToList();

            // Заполнение данных навигации
            ViewData["NavigationArea"] = new NavigationArea(new List<NavigationLink>()
            {
                new NavigationLink() { Title = "Требование", Tooltip = "Требование", Url = Url.Action("Details", "Claim", new { claimUid = claimUid}), IsActive = true },
                new NavigationLink() { Title = "Реестры", Tooltip = "Реестры", Url = Url.Action("ByClaim", "Registry", new { cid = claimUid}) }
            });

            return View(claimViewModel);
        }

        [HttpGet]
        public FileResult GetFile(Guid fid)
        {
            var operationResult = _documentRepository.GetFile(fid);
            var file = operationResult.Data;
            //TODO: [Петр] надо вставить оконо об ошибке
            return File(file.Content, System.Net.Mime.MediaTypeNames.Application.Octet, file.Name);
        }

        /// <summary>
        /// Переопределенный метод для загрузки в модели общих данных
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            // Модель создания требования дополнить списокм предприятий-получателей
            if (filterContext.Controller.ViewData.Model is ClaimCreateModel)
            {
                
                var model = (ClaimCreateModel)filterContext.Controller.ViewData.Model;
                OperationResult<Organization[]> result = _documentRepository.GetListOfOrganizations();
                // TODO: [Петр] Вывести окно об ошибке

                model.OrganizationSelectList = new SelectList(
                    result.Data.Select(org => new SelectListItem() { Value = org.Id.ToString(), Text = org.Name  })
                    , "Value", "Text");
                /*model.OrganizationSelectList = new SelectList(new List<SelectListItem>()
                {
                    new SelectListItem() { Value = "", Text = "", Selected = true },
                    new SelectListItem() { Value = "1", Text = "Пример 1" },
                    new SelectListItem() { Value = "2", Text = "Пример 2" }
                }, "Value", "Text");*/
            }
        }
	}
}