﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SGC.TaxClaims.WebUI.ViewModels;

namespace SGC.TaxClaims.WebUI.Controllers
{
    public class SapController : BaseController
    {
        /// <summary>
        /// Асинхронный метод импорта документов из SAP
        /// </summary>
        /// <returns></returns>
        public ActionResult Import()
        {
            List<DocBaseCreateModel> docs = new List<DocBaseCreateModel>()
            {
                new InvoiceCreateModel() { DocNumber="1" },
                new Torg12CreateModel() { DocNumber="2" }
            };

            StringBuilder sb = new StringBuilder();

            foreach (var doc in docs)
            {
                sb.Append(MvcHelper.RenderPartialViewToString(ControllerContext, ViewData, TempData, doc.GetType().Name, doc));
            }

            ImportSapResult result = new ImportSapResult()
            {
                 htmlContent = sb.ToString()
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}