﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using SGC.TaxClaims.Core;
using SGC.TaxClaims.Core.Saperion;
using SGC.TaxClaims.Infrastructure.Services;
using SGC.TaxClaims.WebUI.ViewModels;

namespace SGC.TaxClaims.WebUI.Controllers
{
    public class SaperionController : BaseController
    {
        private ISaperion _sprRepository;

        public SaperionController(ISaperion sprRepository)
        {
            _sprRepository = sprRepository;
        }

        /// <summary>
        /// Асинхронный метод импорта документов из Saperion
        /// </summary>
        /// <param name="packageIndex">Индекс пакета</param>
        /// <returns></returns>
        public ActionResult GetFiles(string packageIndex)
        {
            SaperionFilesResult result = new SaperionFilesResult();

            //result.files.Add(new FileEditModel()
            //{
            //    Name = "1.tiff", // оригинальное имя
            //    SystemName = "c:\\1.tiff", // полный путь к файлу во временном хранилище
            //    Size = 614,
            //    Type = ".tiff",
            //    Url = "/File/Download/" + "123.tiff",
            //    UrlDelete = "/File/Delete/" + "123.tiff",
            //    //thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath),
            //    //delete_type = "GET",
            //});

            //result.files.Add(new FileEditModel()
            //{
            //    Name = "2.tiff", // оригинальное имя
            //    SystemName = "c:\\2.tiff", // полный путь к файлу во временном хранилище
            //    Size = 614,
            //    Type = ".tiff",
            //    Url = "/File/Download/" + "24.tiff",
            //    UrlDelete = "/File/Delete/" + "24.tiff",
            //    //thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath),
            //    //delete_type = "GET",
            //});
            //Thread.Sleep(3000);
            //return Json(result, JsonRequestBehavior.AllowGet);

            if(!String.IsNullOrWhiteSpace(packageIndex))
            {
                OperationResult<SPRFile[]> fileResult = _sprRepository.GetFiles(packageIndex); 

                // TODO: [Петр] сделать нормальную проверку
                if (!fileResult.IsSystemError)
                {
                    FileManager fm = new FileManager();

                    foreach (var file in fileResult.Data)
                    {
                        // Сгенерировать имя для файла - GUID + расширение
                        string name = Guid.NewGuid().ToString();
                        string ext = Path.GetExtension(file.Name);

                        // Сохранить файл во временное хранилище
                        string fullName = fm.StoreTemporaryFile(file.Content, name + ext);

                        result.files.Add(new FileEditModel()
                        {
                            Name = file.Name, // оригинальное имя
                            SystemName = fullName, // полный путь к файлу во временном хранилище
                            Size = file.Content.Length,
                            Type = ext,
                            Url = "/File/Download?f=" + name + ext,
                            UrlDelete = "/File/Delete?f=" + name + ext,
                            //thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath),
                            //delete_type = "GET",
                        });
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}