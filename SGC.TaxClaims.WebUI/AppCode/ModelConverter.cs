﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.WebUI.ViewModels;

namespace SGC.TaxClaims.WebUI.AppCode
{
    /// <summary>
    /// Конвертирование Model во ViewModel и обратно
    /// </summary>
    public static class ModelConverter
    {
        #region "Claim converters"

        //public static ClaimCreateModel ToCreateViewModel(this Claim model)
        //{
        //    return new ClaimCreateModel()
        //    {
        //        ClaimNumber = model.Number,
        //        Comment = model.Comment,
        //        InspectionName = model.Name
        //    };
        //}

        //public static ClaimIndexModel ToIndexViewModel(this Claim model)
        //{
        //    return new ClaimIndexModel()
        //    {
        //        ClaimNumber = model.Number,
        //        Comment = model.Comment,
        //        InspectionName = model.InspectionName
        //    };
        //}

        #endregion


        #region "Registry converters"

        //public static RegistryEditModel ToEditViewModel(this Registry model)
        //{
        //    return new RegistryEditModel()
        //    {


        //    };
        //}

        //public static RegistryIndexModel ToIndexViewModel(this Registry model)
        //{
        //    return new RegistryIndexModel()
        //    {
        //        RegistryName = model.Name
        //    };
        //}

        //public static RegistryDisplayModel ToDisplayViewModel(this Registry model)
        //{

        //}



        public static string DateToString(DateTime? dateTime)
        {
            return dateTime == null ? string.Empty : DateToString(dateTime.Value);
        }

        public static string DateToString(DateTime dateTime)
        {
            return dateTime.ToString("dd.MM.yyyy");
        }

        #endregion
    }
}