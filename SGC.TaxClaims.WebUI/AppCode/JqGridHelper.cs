﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGC.TaxClaims.Common.Filtering;

namespace SGC.TaxClaims.WebUI.AppCode
{
    /// <summary>
    /// Класс, содержащий вспомогательные методы для работы с JqGrid
    /// </summary>
    public static class JqGridHelper
    {
        /// <summary>
        /// Конвертирование типа JqGrid во внутренний тип
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static FilterGroup ParseFilter(MvcJqGrid.Filter filter)
        {
            FilterGroup fg = new FilterGroup();

            if (filter != null)
            {
                fg.GroupOperation = (filter.groupOp.ToLower() == "and") ? FilterGroupOperation.And : FilterGroupOperation.Or;

                fg.Rules = new List<FilterRule>();

                foreach (var rule in filter.rules)
                {
                    fg.Rules.Add(ConvertFilterRule(rule));
                }
            }

            return fg;
        }

        private static FilterRule ConvertFilterRule(MvcJqGrid.Rule rule)
        {
            FilterRule fr = new FilterRule();

            fr.Data = rule.data;
            fr.FieldName = rule.field;

            switch (rule.op.ToLower())
            {
                case "eq":
                    fr.FilterRuleOperation = FilterRuleOperation.Equal;
                    break;
                case "ne":
                    fr.FilterRuleOperation = FilterRuleOperation.NotEqual;
                    break;
                case "bw":
                    fr.FilterRuleOperation = FilterRuleOperation.BeginsWith;
                    break;
                case "bn":
                    fr.FilterRuleOperation = FilterRuleOperation.NotBeginsWith;
                    break;
                case "ew":
                    fr.FilterRuleOperation = FilterRuleOperation.EndsWith;
                    break;
                case "en":
                    fr.FilterRuleOperation = FilterRuleOperation.NotEndsWith;
                    break;
                case "cn":
                    fr.FilterRuleOperation = FilterRuleOperation.Contains;
                    break;
                case "nc":
                    fr.FilterRuleOperation = FilterRuleOperation.NotContains;
                    break;
                case "in":
                    fr.FilterRuleOperation = FilterRuleOperation.IncludedIn;
                    break;
                case "ni":
                    fr.FilterRuleOperation = FilterRuleOperation.NotIncludedIn;
                    break;
                case "le":
                    fr.FilterRuleOperation = FilterRuleOperation.LessOrEqual;
                    break;
                case "lt":
                    fr.FilterRuleOperation = FilterRuleOperation.LessThen;
                    break;
                case "gt":
                    fr.FilterRuleOperation = FilterRuleOperation.GreaterThen;
                    break;
                case "ge":
                    fr.FilterRuleOperation = FilterRuleOperation.GreaterOrEqual;
                    break;
                default:
                    throw new InvalidOperationException("Неизвестный параметр фильтрации");
            }
            return fr;
        }
    }
}