﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI
{
    public class ImportSapResult : CustomResult
    {
        public virtual string htmlContent { get; set; }

        public ImportSapResult()
        {
            htmlContent = "";
        }
    }
}