﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI
{
    public class UploadFilesResult : CustomResult
    {
        public List<string> html;

        public UploadFilesResult()
        {
            html = new List<string>();
        }
    }
}