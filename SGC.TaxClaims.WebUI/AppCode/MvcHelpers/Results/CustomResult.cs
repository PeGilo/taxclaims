﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI
{
    public class CustomResult
    {
        public virtual bool errorState { get; set; }
        public virtual string errorMessage { get; set; }

        public CustomResult()
        {
            errorState = false;
            errorMessage = "";
        }
    }
}