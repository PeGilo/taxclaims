﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI
{
    public class SaperionFilesResult : CustomResult
    {
        // TODO: [Петр] Custom result использует View - что-то с этим не так. Передавать OperationResult<FileEditModel> ?
        public virtual List<SGC.TaxClaims.WebUI.ViewModels.FileEditModel> files { get; set; }

        public SaperionFilesResult()
        {
            files = new List<SGC.TaxClaims.WebUI.ViewModels.FileEditModel>();
        }
    }
}