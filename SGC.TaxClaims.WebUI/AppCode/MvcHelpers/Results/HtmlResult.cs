﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI
{
    public class HtmlResult : CustomResult
    {
        public virtual string htmlContent { get; set; }

        public HtmlResult()
        {
            htmlContent = "";
        }
    }
}