﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.WebUI
{
    public static class ModelStateExtensions
    {
        /// <summary>
        /// Удаляет из ModelState все элементы, у которых отсутствуют ошибки валидации.
        /// Применяется, когда некоторые поля обновились (бизнес-логикой), но результаты валидации
        /// других полей необходимо сохранить.
        /// </summary>
        /// <param name="modelState"></param>
        public static void RemoveValidElements(this ModelStateDictionary modelState)
        {
            List<string> keysToRemove = new List<string>();

            keysToRemove.AddRange(from i in modelState
                                  where i.Value.Errors.Count == 0
                                  select i.Key);

            foreach (var k in keysToRemove)
            {
                modelState.Remove(k);
            }
        }

        /// <summary>
        /// Добавляет валидационную ошибку в список ошибок.
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="errors"></param>
        public static void AddRuleViolations(this ModelStateDictionary modelState, IEnumerable<RuleViolation> errors)
        {
            foreach (RuleViolation issue in errors)
            {
                modelState.AddModelError(issue.PropertyName, issue.ErrorMessage);
            }
        }

        public static void AddRuleViolation(this ModelStateDictionary modelState, RuleViolation error)
        {
            modelState.AddModelError(error.PropertyName, error.ErrorMessage);
        }
    }
}