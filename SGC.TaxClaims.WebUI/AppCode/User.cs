﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI
{
    /// <summary>
    /// Класс для хранения информации о текущем пользователе
    /// </summary>
    public class User
    {
        public string Name { get; set; }
        //public Guid UId { get; set; }
    }
}