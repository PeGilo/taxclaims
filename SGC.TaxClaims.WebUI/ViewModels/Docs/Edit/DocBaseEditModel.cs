﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class DocBaseEditModel
    {
        [DisplayName("Номер документа")]
        [Description("Номер документа")]
        [Required(ErrorMessage = "Не заполнено поле \"Номер документа\"")]
        public virtual string DocNumber { get; set; }

        [DisplayName("Дата документа")]
        [Description("Дата документа")]
        public virtual DateTime? DocDate { get; set; }

        public virtual int Index { get; set; }

        [DisplayName("Индекс пакета")]
        [Description("Индекс пакета")]
        public string PackageIndex { get; set; }
        /// <summary>
        /// Идентификатор типа для связывания объектов при POST-запросе
        /// </summary>
        public virtual string DocTypeName
        {
            get
            {
                return this.GetType().FullName;
            }
        }

        public virtual void UpdateModel(Core.Model.RegistryDoc doc)
        {
            doc.Number = this.DocNumber;
            doc.DocDate = this.DocDate;
            doc.PackageIndex = this.PackageIndex;
            doc.Name = "";
            doc.DateChanged = DateTime.UtcNow;
            doc.ChangeStatus = System.Data.Entity.EntityState.Modified;
        }
    }
}