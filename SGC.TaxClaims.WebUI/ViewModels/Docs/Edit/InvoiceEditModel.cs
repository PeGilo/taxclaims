﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class InvoiceEditModel : DocBaseEditModel
    {
        public override void UpdateModel(Core.Model.RegistryDoc doc)
        {
            base.UpdateModel(doc);

            doc.DocTypeId = 3; // Счет-фактура // TODO: Че-то с этими цифрами надо делать
        }
    }
}