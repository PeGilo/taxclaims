﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class Torg12EditModel : DocBaseEditModel
    {
        public override void UpdateModel(Core.Model.RegistryDoc doc)
        {
            base.UpdateModel(doc);

            doc.DocTypeId = 5; // Товарная накладная (Торг-12)
        }
    }
}