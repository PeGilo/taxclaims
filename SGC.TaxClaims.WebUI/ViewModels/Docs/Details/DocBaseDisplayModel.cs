﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class DocBaseDisplayModel
    {
        [DisplayName("Номер документа")]
        [Description("Номер документа")]
        public virtual string DocNumber { get; set; }

        [DisplayName("Дата документа")]
        [Description("Дата документа")]
        public virtual DateTime? DocDate { get; set; }

        public virtual int Index { get; set; }

        /// <summary>
        /// Идентификатор типа для связывания объектов при POST-запросе
        /// </summary>
        public virtual string DocTypeName
        {
            get
            {
                return this.GetType().FullName;
            }
        }
    }
}