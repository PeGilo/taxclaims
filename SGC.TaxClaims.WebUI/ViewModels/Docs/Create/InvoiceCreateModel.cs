﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class InvoiceCreateModel : DocBaseCreateModel
    {
        public override void UpdateModel(Core.Model.RegistryDoc doc)
        {
            base.UpdateModel(doc);

            doc.DocTypeId = 3; // Счет-фактура
        }
    }
}