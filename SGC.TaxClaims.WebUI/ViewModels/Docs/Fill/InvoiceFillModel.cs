﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class InvoiceFillModel : DocBaseFillModel
    {
        public override void UpdateModel(Core.Model.Document doc)
        {
            base.UpdateModel(doc);

            // TODO: Че-то с этими цифрами надо делать
            doc.DocTypeId = 3; // Счет-фактура 
        }
    }
}