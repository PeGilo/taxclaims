﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class NavigationArea
    {
        public IList<NavigationLink> Links { get; private set; }

        public NavigationArea(IEnumerable<NavigationLink> links)
        {
            Links = new List<NavigationLink>(links);
        }
    }

    public class NavigationLink
    {
        public string Title { get; set; }
        public string Tooltip { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }
    }
}