﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class FileViewModel
    {
        public Guid UId { get; set; }
        public string Name { get; set; }
        public string TypeName { get; set; }
    }
}