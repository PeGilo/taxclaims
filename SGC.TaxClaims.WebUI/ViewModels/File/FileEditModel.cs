﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class FileEditModel
    {
        public int FileTypeId { get; set; }
        public Guid UId { get; set; }

        public int Index { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string UrlDelete { get; set; }
        public string UrlThumbnail { get; set; }
        //public string delete_type { get; set; }
        public bool IsDeleted { get; set; }

        public void UpdateModel(SGC.TaxClaims.Core.Model.DocFile model)
        {
            model.Name = this.Name;
            model.UId = this.UId;
            model.FileTypeId = this.FileTypeId;
        }
    }
}