﻿using SGC.TaxClaims.WebUI.AppCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class ClaimViewModel
    {
        [DisplayName("Наименование инспекции")]
        [Description("Наименование инспекции")]
        public string InspectionName { get; set; }

        [DisplayName("Номер требования")]
        [Description("Номер требования")]
        public string ClaimNumber { get; set; }

        [DisplayName("Предприятие-получатель")]
        [Description("Предприятие-получатель")]
        public string Organization { get; set; }

        [DisplayName("Комментарий")]
        [Description("Комментарий")]
        public string Comment { get; set; }

        [DisplayName("Срок подготовки ответа на требование")]
        [Description("Срок подготовки ответа на требование")]
        public DateTime? DateReady { get; set; }

        [DisplayName("Срок подготовки ответа на требование")]
        [Description("Срок подготовки ответа на требование")]
        public string DateReadyString
        {
            get
            {
                return DateReady == null? string.Empty : ModelConverter.DateToString(DateReady);
            }
        }
        public List<RegistryDisplayModel> Registries { get; set; }
        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        public List<FileViewModel> Files { get; set; }

        public ClaimViewModel()
        {
            Files = new List<FileViewModel>();
            Registries = new List<RegistryDisplayModel>();
        }
    }
}