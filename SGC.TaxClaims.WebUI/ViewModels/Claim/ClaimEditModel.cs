﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class ClaimEditModel
    {
       [DisplayName("Наименование инспекции")]
        [Description("Наименование инспекции")]
        public string InspectionName { get; set; }

        [DisplayName("Номер требования")]
        [Description("Номер требования")]
        [Required(ErrorMessage="Не заполнено поле \"Номер требования\"")]
        public string ClaimNumber { get; set; }

        [DisplayName("Предприятие-получатель")]
        [Description("Предприятие-получатель")]
        public int? OrgId { get; set; }

        [DisplayName("Комментарий")]
        [Description("Комментарий")]
        public string Comment { get; set; }

        [DisplayName("Срок подготовки ответа на требование")]
        [Description("Срок подготовки ответа на требование")]
        [UIHint("DateTimePicker")]
        public DateTime? DateReady { get; set; }

        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        public List<FileEditModel> Files { get; set; }

        /// <summary>
        /// Список предприятий-получателей требования.
        /// Инициализируется контроллером.
        /// </summary>
        public SelectList OrganizationSelectList { get; set; }

        public ClaimEditModel()
        {
            Files = new List<FileEditModel>();
        }
    }
}