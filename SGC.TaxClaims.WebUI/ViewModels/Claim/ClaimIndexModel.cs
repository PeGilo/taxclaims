﻿using SGC.TaxClaims.WebUI.AppCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class ClaimIndexModel
    {
        public Guid Uid { get; set; }

        public string InspectionName { get; set; }

        public string ClaimNumber { get; set; }

        public string TargetOrganization { get; set; }

        public string Comment { get; set; }

        public DateTime? DateReady { get; set; }

        public string DateReadyString 
        { 
            get 
            {
                return ModelConverter.DateToString(DateReady);
            } 
        }

        public string Status { get; set; }
    }
}