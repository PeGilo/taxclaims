﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    /// <summary>
    /// View model для создания требования
    /// </summary>
    public class ClaimCreateModel
    {
        [DisplayName("Наименование инспекции")]
        [Description("Наименование инспекции")]
        public string InspectionName { get; set; }

        [DisplayName("Номер требования")]
        [Description("Номер требования")]
        [Required(ErrorMessage="Не заполнено поле \"Номер требования\"")]
        public string ClaimNumber { get; set; }

        [DisplayName("Предприятие-получатель")]
        [Description("Предприятие-получатель")]
        public int? OrgId { get; set; }

        [DisplayName("Комментарий")]
        [Description("Комментарий")]
        public string Comment { get; set; }

        [DisplayName("Срок подготовки ответа на требование")]
        [Description("Срок подготовки ответа на требование")]
        [UIHint("DateTimePicker")]
        public DateTime? DateReady { get; set; }

        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        public List<FileEditModel> Files { get; set; }

        /// <summary>
        /// Список предприятий-получателей требования.
        /// Инициализируется контроллером.
        /// </summary>
        public SelectList OrganizationSelectList { get; set; }

        public ClaimCreateModel()
        {
            Files = new List<FileEditModel>();
        }

        public void UpdateModel(SGC.TaxClaims.Core.Model.Claim model)
        {
            model.Number = this.ClaimNumber;
            model.Comment = this.Comment;
            model.InspectionName = this.InspectionName;
            model.Name = "";
            model.DateCreated = DateTime.UtcNow;
            model.DateReady = this.DateReady;
            model.OrgId = this.OrgId;
        }
    }
}