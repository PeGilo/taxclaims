﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.WebUI.AppCode;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    /// <summary>
    /// View model для просмотра реестра
    /// </summary>
    public class RegistryDisplayModel
    {
        ///// <summary>
        ///// Идентификатор требования, к которому прикрепляется реестр
        ///// </summary>
        //public Guid ClaimId { get; set; }
        /// <summary>
        /// Идентификатор реестра
        /// </summary>
        public Guid Uid { get; set; }

        [DisplayName("Наименование реестра")]
        [Description("Наименование реестра")]
        public string RegistryName { get; set; }

        [DisplayName("Пункт требования")]
        [Description("Пункт требования, к которому относится реестр")]
        public string ClaimItemNo { get; set; }

        [DisplayName("Срок подготовки данных")]
        [Description("Срок подготовки данных")]
        [UIHint("DateTimePicker")]
        public DateTime? DateReady { get; set; }

        public string DateReadyString
        {
            get
            {
                return ModelConverter.DateToString(DateReady);
            }
        }

        public DateTime DateCreated { get; set; }

        public string DateCreatedString
        {
            get
            {
                return ModelConverter.DateToString(DateCreated);
            }
        }

        public string UserCreator { get; set; }

        /// <summary>
        /// Список документов, входящих в реестр
        /// </summary>
        public IList<DocBaseDisplayModel> Docs { get; set; }

        public RegistryDisplayModel()
        {
            Docs = new List<DocBaseDisplayModel>();
        }

        public RegistryDisplayModel(Registry model, Document[] children)
        {
            Docs = new List<DocBaseDisplayModel>();

            //RegistryDisplayModel viewModel = new RegistryDisplayModel()
            //{
            Uid = model.UId;
            RegistryName = model.Name;
            ClaimItemNo = model.ClaimItemNo;
            DateReady = model.DateReady;

            UserCreator = model.CreatorUser.UserName;
            DateCreated = model.DateCreated;
            //ClaimId = model
            //};

            foreach (var doc in children)
            {
                // TODO: [Петр] Создание VM по Id документа - Изменить когда будут конкретные классы для каждого типа документа
                switch (doc.DocTypeId)
                {
                    case 3: // Invoice
                        this.Docs.Add(new InvoiceDisplayModel()
                        {
                            DocNumber = doc.Number,
                            DocDate = doc.DocDate,
                        });
                        break;
                    case 5: // Torg12
                        this.Docs.Add(new Torg12DisplayModel()
                        {
                            DocNumber = doc.Number,
                            DocDate = doc.DocDate,
                        });
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }

            //return viewModel;
        }
    }
}