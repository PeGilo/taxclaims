﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SGC.TaxClaims.Core.Model;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    /// <summary>
    /// View model для заполнения реестра
    /// </summary>
    public class RegistryFillModel
    {
        // TODO: [Петр] Выделить в общее с RegistryCreateModel / RegistryEditModel
        //

        /// <summary>
        /// Идентификатор требования, к которому прикрепляется реестр
        /// </summary>
        public Guid ClaimId { get; set; }

        [DisplayName("Наименование реестра")]
        [Description("Наименование реестра")]
        public string RegistryName { get; set; }

        [DisplayName("Пункт требования")]
        [Description("Пункт требования, к которому относится реестр")]
        public string ClaimItemNo { get; set; }

        [DisplayName("Срок подготовки данных")]
        [Description("Срок подготовки данных")]
        [UIHint("DateTimePicker")]
        public DateTime? DateReady { get; set; }

        /// <summary>
        /// Список документов, входящих в реестр
        /// </summary>
        public IList<DocBaseFillModel> Docs { get; set; }

        public RegistryFillModel()
        {
            Docs = new List<DocBaseFillModel>();
        }

        public RegistryFillModel(Registry model, RegistryDoc[] childDocuments)
            : this()
        {
            RegistryName = model.Name;
            ClaimItemNo = model.ClaimItemNo;
            DateReady = model.DateReady;

            foreach (var childDoc in childDocuments)
            {
                DocBaseFillModel doc;

                // TODO: [Петр] переделать в фабрику (?)
                switch (childDoc.DocTypeId)
                {
                    case 3: // Invoice
                        doc = new InvoiceFillModel();
                        break;
                    case 5: // Torg12
                        doc = new Torg12FillModel();
                        break;
                    default:
                        throw new NotImplementedException();
                }
                doc.DocTypeString = childDoc.DocType.Name;
                doc.DocNumber = childDoc.Number;
                doc.DocDate = childDoc.DocDate;
                doc.PackageIndex = childDoc.PackageIndex;

                Docs.Add(doc);
            }
        }

        public void UpdateModel(Registry model)
        {
            // model.DateChanged = DateTime.UtcNow;

            foreach (var doc in this.Docs)
            {
                // TODO: [Петр] сделать редактирование входящих документов
                // Какие-то документы могут быть, добавлены, какие-то удалены и какие-то изменены
                //

            //    Document childDoc = new Document()
            //    {
            //    };

            //    doc.UpdateModel(childDoc);

            //    DocRelation relation = new DocRelation()
            //    {
            //        ParentDocUId = registryGuid,
            //        DocumentParent = model,
            //        ChildDocUId = childDoc.UId,
            //        DocumentChild = childDoc,
            //        DocRelTypeId = 1, // "Включает в себя"
            //        ChangeStatus = System.Data.Entity.EntityState.Added
            //    };

            //    model.DocRelationParent.Add(relation);
            }

            model.ChangeStatus = System.Data.Entity.EntityState.Modified;
        }
    }
}