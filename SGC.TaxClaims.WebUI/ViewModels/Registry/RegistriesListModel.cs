﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class RegistriesListModel
    {
        public string ClaimUid { get; set; }
        public List<RegistryDisplayModel> Registries { get; set; }

        public RegistriesListModel()
        {
            Registries = new List<RegistryDisplayModel>();
        }
    }
}