﻿using SGC.TaxClaims.WebUI.AppCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class RegistryIndexModel
    {
        public Guid Uid { get; set; }
        public string RegistryName { get; set; }
        public string ClaimItemNo { get; set; }
        public DateTime? ReadyDate { get; set; }
        public string ReadyDateString
        {
            get
            {
                return ModelConverter.DateToString(ReadyDate);
            }
        }
        public string Status { get; set; }
    }
}