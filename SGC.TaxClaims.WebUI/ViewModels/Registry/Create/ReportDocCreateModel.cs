﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    public class ReportDocCreateModel
    {
        [DisplayName("Тип документа")]
        [Description("Тип документа")]
        [Required(ErrorMessage = "Не выбран тип документа")]
        public string DocTypeValue { get; set; }

        [DisplayName("Ссылочный номер документа")]
        [Description("Ссылочный номер документа")]
        [Required(ErrorMessage = "Не заполнено поле \"Ссылочный номер документа\"")]
        public string DocNumber { get; set; }

        [DisplayName("Дата документа")]
        [Description("Дата документа")]
        [UIHint("DateTimePicker")]
        [Required(ErrorMessage = "Не заполнено поле \"Дата документа\"")]
        public DateTime DocDate { get; set; }

        /// <summary>
        /// Список типов файлов, которые можно добавлять в реестр
        /// </summary>
        public SelectList SourceDocTypes { get; set; }
    }
}