﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGC.TaxClaims.Core.Model;

namespace SGC.TaxClaims.WebUI.ViewModels
{
    /// <summary>
    /// View model для создания реестра
    /// </summary>
    public class RegistryCreateModel
    {
        /// <summary>
        /// Идентификатор требования, к которому прикрепляется реестр
        /// </summary>
        public Guid ClaimId { get; set; }

        [DisplayName("Наименование реестра")]
        [Description("Наименование реестра")]
        public string RegistryName { get; set; }

        [DisplayName("Пункт требования")]
        [Description("Пункт требования, к которому относится реестр")]
        [MaxLength(20, ErrorMessage="Текст не должен содержать более 20 симоволов.")]
        public string ClaimItemNo { get; set; }

        [DisplayName("Срок подготовки данных")]
        [Description("Срок подготовки данных")]
        [UIHint("DateTimePicker")]
        public DateTime? DateReady { get; set; }

        /// <summary>
        /// Список документов, входящих в реестр
        /// </summary>
        public IList<DocBaseCreateModel> Docs { get; set; }

        /// <summary>
        /// Модель для формы создания документа
        /// </summary>
        public SourceDocCreateModel SourceDocCreateModel { get; set; }

        public ReportDocCreateModel ReportDocCreateModel { get; set; }

        public RegistryCreateModel()
        {
            Docs = new List<DocBaseCreateModel>();
            SourceDocCreateModel = new SourceDocCreateModel();
            ReportDocCreateModel = new ReportDocCreateModel();
        }

        public void UpdateModel(Registry model)
        {
            Guid registryGuid = Guid.NewGuid();

            model.UId = registryGuid;
            model.Name = this.RegistryName;
            model.DateReady = this.DateReady;
            model.ClaimItemNo = this.ClaimItemNo;
            model.DateCreated = DateTime.UtcNow;

            foreach (var doc in this.Docs)
            {
                // TODO: [Петр] перенести в UpdateModel
                RegistryDoc childDoc = new RegistryDoc()
                {
                    UId = Guid.NewGuid(),
                };

                doc.UpdateModel(childDoc);
                childDoc.ParentDocUId = registryGuid;
               /* DocRelation relation = new DocRelation()
                {
                    ParentDocUId = registryGuid,
                    DocumentParent = model,
                    ChildDocUId = childDoc.UId,
                    DocumentChild = childDoc,
                    DocRelTypeId = 1, // "Включает в себя"
                    ChangeStatus = System.Data.Entity.EntityState.Added
                };

                model.DocRelationParent.Add(relation);*/
            }

            model.ChangeStatus = System.Data.Entity.EntityState.Added;
        }
    }
}