﻿(function ($) {
	
	// Локализация Javascript валидаторов
	//

	// Валидатор чисел
	$.validator.methods.number = function (value, element) {
		if (!value || 0 === value.length) // Не валидировать пустые значения. Для Required есть отдельный атрибут
		{
			return true;
		}
		if (value && Globalize.parseFloat(value)) {
			return true;
		}
		return false;
	}

	// Валидатор дат
	$.validator.methods.date = function (value, element) {
		if (!value || 0 === value.length) // Не валидировать пустые значения. Для Required есть отдельный атрибут
		{
			return true;
		}
		if (value && Globalize.parseDate(value)) {
			return true;
		}
		return false;
	}

}(jQuery));