﻿SGC.Tax.Utils = (function () {
    var

	// Изменяет цифры в атрибутах (id, name, data-valmsg-for) элементов (input, span) 
	// так, чтобы они шли по порядку.
	reindexElementCollection = function (collection) {
	    for (var i = 0; i < collection.size() ; i++) {
	        var row = collection[i];

	        $(row).find("input, select").each(function (index, el) {

	            $(el).attr('name', function (ind, old_name) {
	                if (old_name) {
	                    return old_name.replace(/\[\d+\]/, "[" + i + "]"); // change name
	                }
	                else
	                    return '';
	            })
				.attr('name', function (ind, old_name) {
				    if (old_name) {
				        return old_name.replace(/\d+/, i.toString()); // in case the name has just a digit
				    }
				    else
				        return '';
				})
				.attr('id', function (ind, old_id) {
				    if (old_id) {
				        return old_id.replace(/_\d+_/, "_" + i + "_");   // change id
				    }
				    return '';
				});
	        });

	        $(row).find("span[data-valmsg-for]").each(function (index, el) {
	            $(el).attr('data-valmsg-for', function (ind, old_val) {
	                if (old_val) {
	                    return old_val.replace(/\[\d+\]/, "[" + i + "]"); // change name
	                }
	                else
	                    return '';
	            })
	        });
	    }
	},

    // Показывает / скрывает окно с индикатором занятости
    cover = function (show) {
        if (show) {
            $("#cover").show();
        }
        else {
            $("#cover").hide();
        }
    };

	return {
	    reindexElementCollection: reindexElementCollection,
	    cover: cover
	};
}());