﻿SGC.Tax.Registry = SGC.Tax.Registry || {};

//
// Модуль для формы добавления документа в реестр
//
SGC.Tax.Registry.addDocForm = (function () {

    var

    _options = { onAdding: function () { } },

    //_resposeData = null,

    open = function () {
        $("#adddoc-dialog").dialog("open");
    },

    submit = function (callbacks) {

        var onSuccess = callbacks && callbacks.success ? callbacks.success : function () { };
        var onError = callbacks && callbacks.error ? callbacks.error : function () { };

        $form = $("#formAddDoc");

        $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            //context: this, // Передать контекст для обработчиков success, error, ...
            beforeSend: function (jqXHR, settings) {

            },
            success: function (data, textStatus, jqXHR) {
                if (data.errorState) {
                    alert(data.errorMessage);
                    onError();
                }
                else {
                    _options.onAdding(data.htmlContent);
                    onSuccess();
                }
            },

            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + ' : ' + errorThrown);
                onError();
            },
            complete: function (jqXHR, textStatus) { }
        });
    },

    init = function (options) {

        _options.onAdding = (options || {}).onAdding || _options.onAdding;

        $("#adddoc-dialog").dialog({
            autoOpen: false,
            height: 300,
            width: 570,
            modal: true,
            buttons: {
                "Добавить": function () {

                    var $dialog = $(this);

                    submit({
                        error: function () { },
                        success: function () { $dialog.dialog("close"); }
                    });
                },
                "Отмена": function () {
                    $(this).dialog("close");
                }
            },
            //open: function() {

            //},
            close: function () {

            }
        });
    };

    return {
        init: init,
        open: open
    };
}());