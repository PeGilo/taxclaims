﻿// Модуль для формы импорта из SAP
SGC.Tax.importSapForm = (function () {

    var

    _options = { onImporting: function () { } },

    _resposeData = null,

    open = function () {
        $("#importsap-form").dialog("open");
    },

    init = function (options) {

        _options.onImporting = (options || {}).onImporting || _options.onImporting;

        $("#importsap-form").dialog({
            autoOpen: false,
            height: 300,
            width: 570,
            modal: true,
            buttons: {
                "Импорт": function () {
                    $(this).dialog("close");

                    if(_resposeData) {
                        _options.onImporting(_resposeData.htmlContent);
                    }
                },
                "Отмена": function () {
                    $(this).dialog("close");
                }
            },
            //open: function() {

            //},
            close: function () {

            }
        });

        $("#btn-searchsap").click(function () {

            $.ajax({
                type: 'GET',
                url: "/sap/import",
                data: { sample: 1 },
                dataType: 'json',
                //context: this, // Передать контекст для обработчиков success, error, ...
                beforeSend: function (jqXHR, settings) {
                    
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.errorState) {
                        alert(data.errorMessage);
                    }
                    else {
                        $("#importsap-result").html(data.htmlContent);

                        _resposeData = data;
                    }
                    
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    alert(textStatus + ' : ' + errorThrown);
                },
                complete: function (jqXHR, textStatus) {
                    
                }
            });

        });
    };

    return {
        init: init,
        open: open
    };
}());