﻿SGC.Tax.Registry = SGC.Tax.Registry || {};

//
// Модуль для выбора файлов из Сапериона
//
SGC.Tax.Registry.saperionClient = (function () {

    var

    _options = { onAdding: function () { } },

    getFiles = function (packageIndex, callbacks) {

        var onSuccess = callbacks && callbacks.success ? callbacks.success : function () { };
        var onError = callbacks && callbacks.error ? callbacks.error : function () { };

        $.ajax({
            type: 'POST',
            url: '/Saperion/GetFiles',
            data: { packageIndex: packageIndex },
            dataType: 'json',
            //context: this, // Передать контекст для обработчиков success, error, ...
            beforeSend: function (jqXHR, settings) {

            },
            success: function (data, textStatus, jqXHR) {
                if (data.errorState) {
                    alert(data.errorMessage);
                    onError();
                }
                else {
                    
                    onSuccess(data.files);
                }
            },

            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + ' : ' + errorThrown);
                onError();
            },
            complete: function (jqXHR, textStatus) { }
        });
    },

    init = function (options) {

        _options.onAdding = (options || {}).onAdding || _options.onAdding;


    };

    return {
        getFiles: getFiles,
        init: init
    };
}());