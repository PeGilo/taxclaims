﻿using System;
using System.Web.Security;

namespace SGC.TaxClaims.WebUI.App_Start
{
    public class SecurityConfig
    {
        public static void Initialize()
        {
            // При отсутствии ролей в базе данных, добавляем их.
            if (!Roles.RoleExists("Administrators"))
            {
                Roles.CreateRole("Administrators");
                Roles.AddUserToRole(@"sibgenco\giloyanps", "Administrators");
            }
        }
    }
}