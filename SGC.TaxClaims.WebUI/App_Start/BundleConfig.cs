﻿using System.Web;
using System.Web.Optimization;

namespace SGC.TaxClaims.WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        
                        "~/Scripts/globalize/globalize.js",
                        "~/Scripts/globalize/cultures/globalize.culture.ru-RU.js",

                        "~/Scripts/plugins/jquery.ui.datepicker-ru.js",
                        "~/Scripts/free-jqGrid/jquery.jqGrid.js",
                        "~/Scripts/free-jqGrid/i18n/grid.locale-ru.js",
                        "~/Scripts/jsrender.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                      "~/Scripts/jquery.validate.js",
                      "~/Scripts/jquery.validate.unobtrusive.js",
                      "~/Scripts/App/jquery.validate.localization.js"
                      //"~/Scripts/localization/messages_ru.js"
                      ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqfileupload").Include(
                      "~/Scripts/jQuery.FileUpload/jquery.iframe-transport.jss",
                      "~/Scripts/jQuery.FileUpload/jquery.fileupload.js",
                      "~/Scripts/jQuery.FileUpload/jquery.fileupload-jquery-ui.js"
                      ));

            // Общие скрипты для всего проекта
            bundles.Add(new ScriptBundle("~/bundles/sgc.tax.basic").Include(
                      "~/Scripts/App/sgc.tax.utils.js"));

            bundles.Add(new ScriptBundle("~/bundles/sgc.tax.registry").Include(
                      "~/Scripts/App/sgc.tax.importSapForm.js",
                      "~/Scripts/App/sgc.tax.registry.saperionClient.js",
                      "~/Scripts/App/sgc.tax.registry.addDocForm.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/jquery-ui.structure.css",
                      "~/Content/jquery-ui.theme.css",
                      "~/Content/site.css",
                      "~/Content/ui.jqgrid.css",
                      "~/Content/jQuery.FileUpload/css/jquery.fileupload.css",
                      "~/Content/jQuery.FileUpload/css/jquery.fileupload-ui.css",
                      "~/Content/font-awesome.css"
                      ));
        }
    }
}
