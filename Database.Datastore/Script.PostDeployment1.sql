﻿/*Post-Deployment Script Template 
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------*/
--Инициализация ASP.NET Membership
--------------------------
--Мгновенно очищает базу--
--------------------------
/*
delete from Registry;
delete from Claim;
delete from DocFile;
delete from RegistryDoc;
delete from Document;
*/

INSERT INTO [dbo].[aspnet_Applications]([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) 
SELECT N'SGC.TaxClaims', N'sgc.taxclaims', N'1E9458A4-C174-4B82-817F-C1996E37F52A', NULL

INSERT INTO [dbo].[aspnet_SchemaVersions](Feature, CompatibleSchemaVersion, IsCurrentVersion)
SELECT 'common', 1, 1 UNION
SELECT 'health monitoring', 1, 1 UNION
SELECT 'membership', 1, 1 UNION
SELECT 'personalization', 1, 1 UNION
SELECT 'profile', 1, 1 UNION
SELECT 'role manager', 1, 1

-----------------------
--Добавляем тип связи--
-----------------------
/*if (select COUNT(*) from DocRelType where id = 1) = 0 
	insert into DocRelType(Id,Name) values(1,'Включает в себя');
---------------------------
--Добавляем тип документа--
---------------------------
if (select COUNT(*) from DocType where id = 0) = 0 
	insert into DocType(Id,Name) values(0,'Не определен');
if (select COUNT(*) from DocType where id = 1) = 0 
	insert into DocType(Id,Name) values(1,'Требование');
if (select COUNT(*) from DocType where id = 2) = 0 
	insert into DocType(Id,Name) values(2,'Реестр');
if (select COUNT(*) from DocType where id = 3) = 0 
	insert into DocType(Id,Name) values(3,'Счет-фактура');
if (select COUNT(*) from DocType where id = 4) = 0 
	insert into DocType(Id,Name) values(4,'Акт приемки-сдачи работ');
if (select COUNT(*) from DocType where id = 5) = 0 
	insert into DocType(Id,Name) values(5,'Товарная накладная (Торг-12)');
if (select COUNT(*) from DocType where id = 6) = 0 
	insert into DocType(Id,Name) values(6,'Книга покупок');
if (select COUNT(*) from DocType where id = 7) = 0 
	insert into DocType(Id,Name) values(7,'Дополнительный лист книги покупок');
if (select COUNT(*) from DocType where id = 8) = 0 
	insert into DocType(Id,Name) values(8,'Дополнительный лист книги продаж');
if (select COUNT(*) from DocType where id = 9) = 0 
	insert into DocType(Id,Name) values(9,'Журнал полученных и выставленных счетов-фактур');
if (select COUNT(*) from DocType where id = 10) = 0 
	insert into DocType(Id,Name) values(10,'Книга продаж');
if (select COUNT(*) from DocType where id = 11) = 0 
	insert into DocType(Id,Name) values(11,'Корректировочная счет-фактура');

--Требование может включать в себя реестр--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 1 and ChildDocId = 2 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(1,2,1);
--Реестр может включать в себя счет-фактуру--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 3 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,3,1);
--Реестр может включать в себя "Акт приемки-сдачи работ"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 4 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,4,1);
--Реестр может включать в себя "Товарная накладная (Торг-12)"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 5 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,5,1);
--Реестр может включать в себя "Книга покупок, книга продаж"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 6 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,6,1);
--Реестр может включать в себя "Дополнительный лист книги покупок"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 7 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,7,1);
--Реестр может включать в себя "Дополнительный лист книги продаж"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 8 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,8,1);
--Журанл полученных и выставленных счетов-фактур--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 9 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,9,1);*/
------------------
--Добавляем роли--
------------------
--Роль сотрудника ОУУ--
if (select count(*) from aspnet_Roles where RoleId = N'0D63240F-B037-43E5-A89C-69AA3B351358') = 0
	insert into aspnet_Roles(RoleId,ApplicationId,RoleName,LoweredRoleName,[Description])
	values(N'0D63240F-B037-43E5-A89C-69AA3B351358',N'1E9458A4-C174-4B82-817F-C1996E37F52A','Сотрудник ОOУ',lower('Сотрудник ОOУ'),'Сотрудник отдела оперативного учета ОЦО');
--Роль сотрудника БиНУ--
if (select count(*) from aspnet_Roles where RoleId = N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0') = 0
	insert into aspnet_Roles(RoleId,ApplicationId,RoleName,LoweredRoleName,[Description])
	values(N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0',N'1E9458A4-C174-4B82-817F-C1996E37F52A','Сотрудник БиНУ',lower('Сотрудник БиНУ'),'Сотрудник отделы Дирекции бухгалтерского и налогового учета ОЦО по участкам учета');
--Роль сотрудника БиНО--
if (select count(*) from aspnet_Roles where RoleId = N'836CC049-8254-401F-A244-364971162233') = 0
	insert into aspnet_Roles(RoleId,ApplicationId,RoleName,LoweredRoleName,[Description])
	values(N'836CC049-8254-401F-A244-364971162233',N'1E9458A4-C174-4B82-817F-C1996E37F52A','Сотрудник ОБиНО',lower('Сотрудник ОБиНО'),'Сотрудник отдел бухгалтерской и налоговой отчетности Дирекции бухгалтерского и налогового учета ОЦО');
--Вставляем пользователя KalininMV--
if (select COUNT(*) from aspnet_Users where UserId = N'53F7D49F-A98B-4E82-989E-E93D22C1F9E1') = 0
begin
	insert into aspnet_Users(UserId,ApplicationId,UserName,LoweredUserName,MobileAlias,IsAnonymous,LastActivityDate)
	values(N'53F7D49F-A98B-4E82-989E-E93D22C1F9E1',N'1E9458A4-C174-4B82-817F-C1996E37F52A','sibgenco\kalininmv','sibgenco\kalininmv',null,0,getdate());
	insert into aspnet_UsersInRoles(UserId,RoleId) 
	values('53F7D49F-A98B-4E82-989E-E93D22C1F9E1','8D17DEC0-7684-426D-98FD-92F792C3F682');
end;
--Вставляем пользователя ForkinPA
if (select COUNT(*) from aspnet_Users where UserId = N'2BA565DF-09EB-4113-BCEF-AC73400A0A16') = 0
begin
	insert into aspnet_Users(UserId,ApplicationId,UserName,LoweredUserName,MobileAlias,IsAnonymous,LastActivityDate)
	values(N'2BA565DF-09EB-4113-BCEF-AC73400A0A16',N'1E9458A4-C174-4B82-817F-C1996E37F52A','sibgenco\ForkinPA','sibgenco\forkinpa',null,0,getdate());
	insert into aspnet_UsersInRoles(UserId,RoleId) 
	values('2BA565DF-09EB-4113-BCEF-AC73400A0A16','8D17DEC0-7684-426D-98FD-92F792C3F682');
end;
------------------
--Вставляем путь--
------------------
if (select count(*) from DocRoute where id = 1) = 0
	insert into DocRoute(id,name) values(1,'Выгрузка документов в ИФНС');
-----------------------
--Добавляем состояния--
-----------------------
--Состояние новое--
if (select count(*) from DocState where id = 1) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(1,1,1,'Новое','Создается требование.','Новое');
end;
else
begin
	update DocState 
	set 
		Id = 1,
		Number = 1,
		DocRouteId = 1,
		Name = 'Новое',
		Comment = 'Сотрудник ООУ создает требование и прикрепляет решение налоговой о правомерности.',
		DisplayName = 'Новое'
	where Id = 1;
end;
--Состояние исполняется--
if (select count(*) from DocState where id = 2) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(2,2,1,'Исполняется','Ответ на требование в процесси формирования','Исполняется');
end;
else
begin
	update DocState 
	set 
		Id = 2,
		Number = 2,
		DocRouteId = 1,
		Name = 'Исполняется',
		Comment = 'Ответ на требование в процесси формирования',
		DisplayName = 'Исполняется'
	where Id = 2;
end;
--Требование готово к выгрузке--
if (select count(*) from DocState where id = 3) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(3,3,1,'Готово к выгрузки','Все документы в требовании проверены и его можно выгрузить в Контур.','Готово к выгрузки');
end;
else
begin
	update DocState 
	set 
		Id = 3,
		Number = 3,
		DocRouteId = 1,
		Name = 'Готово к выгрузки',
		Comment = 'Все документы в требовании проверены и его можно выгрузить в Контур.',
		DisplayName = 'Готово к выгрузки'
	where Id = 3;
end;
--Состояние четвертое (Документы отправлены в ИФНС)--
if (select count(*) from DocState where id = 4) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(4,4,1,'Документы отправлены в ИФНС','Требование выгружено в архив.','Документы отправлены в ИФНС');
end;
else
begin
	update DocState 
	set 
		Id = 4,
		Number = 4,
		DocRouteId = 1,
		Name = 'Документы отправлены в ИФНС',
		Comment = 'Требование выгружено в архив.',
		DisplayName = 'Документы отправлены в ИФНС'
	where Id = 4;
end;
--Состояние пятое (Реестр сформирован)--
if (select count(*) from DocState where id = 5) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName)
	values(5,5,1,'Сформирован','Реестр создан.','Сформирован');
end;
else
begin
	update DocState 
	set 
		Id = 5,
		Number = 5,
		DocRouteId = 1,
		Name = 'Сформирован',
		Comment = 'Реестр создан.',
		DisplayName = 'Сформирован'
	where Id = 5;
end;

--Состояние шетое (Данные в документе отстутствуют)
if (select count(*) from DocState where id = 6) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName)
	values(6,6,1,'Данные отстутствуют','Отсутствуют файлы в документе.','Данные отстутствуют');
end;
else
begin
	update DocState 
	set 
		Id = 6,
		Number = 6,
		DocRouteId = 1,
		Name = 'Данные отстутствуют',
		Comment = 'Реестр создан.',
		DisplayName = 'Данные отстутствуют'
	where Id = 6;
end;

--Реестр или документ подготовлен--
if (select count(*) from DocState where id = 7) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName)
	values(7,7,1,'Подготовлен','Реестр или документ подготовлен.','Подготовлен');
end;
else
begin
	update DocState 
	set 
		Id = 7,
		Number = 7,
		DocRouteId = 1,
		Name = 'Подготовлен',
		Comment = 'Реестр или документ подготовлен.',
		DisplayName = 'Подготовлен'
	where Id = 7;
end;
--Реестр или документ проверен и корректный--
if (select count(*) from DocState where id = 8) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName)
	values(8,8,1,'Проверен/Корректный','Реестр или документ проверен и корректный.','Проверен/Корректный');
end;
else
begin
	update DocState 
	set 
		Id = 8,
		Number = 8,
		DocRouteId = 1,
		Name = 'Проверен/Корректный',
		Comment = 'Реестр или документ проверен и корректный.',
		DisplayName = 'Проверен/Корректный'
	where Id = 8;
end;
--Проверен корректен--
if (select count(*) from DocState where id = 9) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName)
	values(9,9,1,'Исправлен','Реестр или документ исправлен.','Исправлен');
end;
else
begin
	update DocState 
	set 
		Id = 9,
		Number = 9,
		DocRouteId = 1,
		Name = 'Исправлен',
		Comment = 'Реестр или документ исправлен.',
		DisplayName = 'Исправлен'
	where Id = 9;
end;
--Реестр или документ проверен и некорректен.--
if (select count(*) from DocState where id = 10) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName)
	values(10,10,1,'Проверен/некорректный','Реестр или документ проверен и некорректен.','Некорректный');
end;
else
begin
	update DocState 
	set 
		Id = 10,
		Number = 10,
		DocRouteId = 1,
		Name = 'Проверен/некорректный',
		Comment = 'Реестр или документ проверен и некорректен.',
		DisplayName = 'Некорректный'
	where Id = 10;
end;

--Состояние первое (Документ у сотрудника ООУ).--
/*if (select count(*) from DocState where id = 1) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(1,1,1,'Документ у сотрудника ООУ','Сотрудник ООУ создает требование и прикрепляет решение налоговой о правомерности.','Документ у сотрудника ООУ');
end;
else
begin
	update DocState 
	set 
		Id = 1,
		Number = 1,
		DocRouteId = 1,
		Name = 'Документ у сотрудника ООУ',
		Comment = 'Сотрудник ООУ создает требование и прикрепляет решение налоговой о правомерности.',
		DisplayName = 'Документ у сотрудника ООУ'
	where Id = 1;
end;
--Состояние второе (Документ у сотрудника БиНУ).--
if (select count(*) from DocState where id = 2) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(2,2,1,'Документ у сотрудника БиНУ','Сотрудник БиНУ создает реестр и загружает атрибуты документов из САП.','Документ у сотрудника БиНУ');
end;
else
begin
	update DocState 
	set 
		Id = 2,
		Number = 2,
		DocRouteId = 1,
		Name = 'Документ у сотрудника БиНУ',
		Comment = 'Сотрудник БиНУ создает реестр и загружает атрибуты документов из САП.',
		DisplayName = 'Документ у сотрудника БиНУ'
	where Id = 2;
end;
--Состояние третье (Документ у сотрудника ОБиНО).--
if (select count(*) from DocState where id = 3) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(3,3,1,'Документ у сотрудника ОБиНО','Сотрудник ОБиНО подгружает файлы для документов из Sapirion.','Документ у сотрудника ОБиНО');
end;
else
begin
	update DocState 
	set 
		Id = 3,
		Number = 3,
		DocRouteId = 1,
		Name = 'Документ у сотрудника ОБиНО',
		Comment = 'Сотрудник ОБиНО подгружает файлы для документов из Sapirion.',
		DisplayName = 'Документ у сотрудника ОБиНО'
	where Id = 3;
end;
--Состояние четвертое (Документ у сотрудника БиНУ)--
if (select count(*) from DocState where id = 4) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName) 
	values(4,4,1,'Документ у сотрудника БиНУ','Сотрудник БиНУ проверяет документы.','Документ у сотрудника БиНУ');
end;
else
begin
	update DocState 
	set 
		Id = 4,
		Number = 4,
		DocRouteId = 1,
		Name = 'Документ у сотрудника БиНУ',
		Comment = 'Сотрудник БиНУ проверяет документы.',
		DisplayName = 'Документ у сотрудника БиНУ'
	where Id = 4;
end;
--Состояние пятое (Документ у сотрудника ООУ)--
if (select count(*) from DocState where id = 5) = 0
begin
	insert into DocState(Id,Number,DocRouteId,Name,Comment,DisplayName)
	values(5,5,1,'Документ у сотрудника ООУ','Сотрудник ООУ проверяет документы и выгруажет в архив.','Документ у сотрудника ООУ');
end;
else
begin
	update DocState 
	set 
		Id = 5,
		Number = 5,
		DocRouteId = 1,
		Name = 'Документ у сотрудника ООУ',
		Comment = 'Сотрудник ООУ проверяет документы и выгруажет в архив.',
		DisplayName = 'Документ у сотрудника ООУ'
	where Id = 5;
end;*/
----------------------------
--Добавляем роли состояний--
----------------------------
/*if (select count(*) from DocStateRole where DocStateId = 1 and RoleUid = N'0D63240F-B037-43E5-A89C-69AA3B351358') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(1,N'0D63240F-B037-43E5-A89C-69AA3B351358');
if (select count(*) from DocStateRole where DocStateId = 2 and RoleUid = N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(2,N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0');
if (select count(*) from DocStateRole where DocStateId = 3 and RoleUid = N'836CC049-8254-401F-A244-364971162233') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(3,N'836CC049-8254-401F-A244-364971162233');
if (select count(*) from DocStateRole where DocStateId = 4 and RoleUid = N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(4,N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0');
if (select count(*) from DocStateRole where DocStateId = 5 and RoleUid = N'0D63240F-B037-43E5-A89C-69AA3B351358') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(5,N'0D63240F-B037-43E5-A89C-69AA3B351358');*/
-------------------------------
--Добавляем маршрут состояний--
-------------------------------
/*delete from DocMove;
if (select count(*) from DocMove where DocStateIdFrom = 1 and DocStateIdTo = 2 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(1,2,1,'fnCanMoveFrom1To2');
if (select count(*) from DocMove where DocStateIdFrom = 2 and DocStateIdTo = 3 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(2,3,1,'fnCanMoveFrom2To3');
if (select count(*) from DocMove where DocStateIdFrom = 3 and DocStateIdTo = 4 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(3,4,1,'fnCanMoveFrom3To4');
if (select count(*) from DocMove where DocStateIdFrom = 4 and DocStateIdTo = 5 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(4,5,1,'fnCanMoveFrom4To5');
if (select count(*) from DocMove where DocStateIdFrom = 4 and DocStateIdTo = 3 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(4,3,1,'fnCanMoveFrom4To3');
if (select count(*) from DocMove where DocStateIdFrom = 5 and DocStateIdTo = 3 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(5,3,1,'fnCanMoveFrom5To3');*/
---------------------------
--Заполняем типы статусов--
---------------------------
delete from DocEventType;
if (select count(*) from DocEventType where id = 1) = 0
	insert into DocEventType(id,name) values(1,'Создан');
--------------------------------------
--Вставялем статусы типов документов--
--------------------------------------
delete from DocTypeEvent;
--Можно создать требование на первом этапе
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(1,1,1);
--На втором этапе можно создать все остальные докумнеты
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(2,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(3,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(4,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(5,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(6,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(7,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(8,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(9,1,2);
-------------------------
--Вставляем типы файлов--
-------------------------
if (select COUNT(*) from FileType where Id = 1) = 0
	insert into FileType(id,name) values(1,'Неопределен');
-----------------------------------
--Нет такой функции для валидации--
-----------------------------------
delete from ValidationInfo;
insert into ValidationInfo([id],[message],[comment]) values(1,'Нет функции для перехода.','При переходе из одного состояния в другое вызывается функция, которая хранится в таблице DocMove.');
insert into ValidationInfo([id],[message],[comment]) values(2,'Пользователь не зарегистрирован в базе данных.','Небходимо довавить пользователя в таблицу aspnet_users');
insert into ValidationInfo([id],[message],[comment]) values(3,'Не достаточно прав доступа.','Нет доступа, для выполнения данной операции');
/*
	Заполнение справочника типов предприятий
----------------------------------------------*/
if (select COUNT(*) from OrgType where Id = 1) = 0 
	insert into OrgType (Id, Name) values(1, '?');
/*
	Заполнение справочника предприятий
----------------------------------------------*/
if (select COUNT(*) from Organization where Id = 1) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(1, 1, 'ОАО "Красноярская ТЭЦ-1"', null);
if (select COUNT(*) from Organization where Id = 2) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(2, 1, 'ОАО "Ново-Кемеровская ТЭЦ"', null);
if (select COUNT(*) from Organization where Id = 3) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(3, 1, 'Енисейская ТГК (ТГК-13)', null);
if (select COUNT(*) from Organization where Id = 4) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(4, 1, 'Филиал "Енисейская ТГК (ТГК-13)" Минусинская ТЭЦ', 3);
if (select COUNT(*) from Organization where Id = 5) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(5, 1, 'Филиал "Енисейская ТГК (ТГК-13)" Абаканская ТЭЦ', 3);
if (select COUNT(*) from Organization where Id = 6) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(6, 1, 'Филиал "Енисейская ТГК (ТГК-13)" Красноярская ТЭЦ-3', 3);
if (select COUNT(*) from Organization where Id = 7) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(7, 1, 'Филиал "Енисейская ТГК (ТГК-13)" Красноярская ТЭЦ-2', 3);
if (select COUNT(*) from Organization where Id = 8) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(8, 1, 'ОАО "Назаровская ГРЭС"', null);
if (select COUNT(*) from Organization where Id = 9) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(9, 1, 'ОАО "Канская ТЭЦ"', null);
if (select COUNT(*) from Organization where Id = 10) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(10, 1, 'ОАО "Кузбассэнерго"', null);
if (select COUNT(*) from Organization where Id = 11) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(11, 1, 'Филиал ОАО "Кузбасэнерго" Томь-Усинская ГРЭС', 10);
if (select COUNT(*) from Organization where Id = 12) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(12, 1, 'Филиал ОАО "Кузбасэнерго" Беловская ГРЭС', 10);
if (select COUNT(*) from Organization where Id = 13) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(13, 1, 'ОАО "Кемеровская генерация"', null);
if (select COUNT(*) from Organization where Id = 14) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(14, 1, 'Филиал ОАО "Кемеровская генерация" Кемеровская ТЭЦ', 13);
if (select COUNT(*) from Organization where Id = 15) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(15, 1, 'Филиал ОАО "Кемеровская генерация" Кемеровская ГРЭС', 13);
if (select COUNT(*) from Organization where Id = 16) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(16, 1, 'ОАО "Кузнецкая ТЭЦ"', null);
if (select COUNT(*) from Organization where Id = 17) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(17, 1, 'ОАО «Барнаульская ТЭЦ-3»', null);
if (select COUNT(*) from Organization where Id = 18) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(18, 1, 'ОАО «Барнаульская генерация»', null);
if (select COUNT(*) from Organization where Id = 19) = 0 
	insert into Organization (Id, OrgTypeId, Name, ParentId) values(19, 1, 'Филиал ОАО "Барнаульская генерация" Барнаульская ТЭЦ-2', 18);
-------------------------
--Вставляем типы файлов--
-------------------------
--Требование--
if(select count(*) from filetype where id = 2) = 0
	insert into filetype(id,name,comment) values(2,'Требование','Непосредственно файл требования');
if(select count(*) from filetype where id = 3) = 0
	insert into filetype(id,name,comment) values(3,'Обоснование о правомерности','Заключение налоговой о предоставлении документов в требовании');
--Акт приемки и сдачи работ--
if(select count(*) from filetype where id = 4) = 0
	insert into filetype(id,name,comment) values(4,'Акт приемки сдачи работ (Продавец)','Файл DP_IAKTPRM');
if(select count(*) from filetype where id = 5) = 0
	insert into filetype(id,name,comment) values(5,'Подпись акта приемки сдачи работ (Продавец)','Файл DP_IAKTPRM....sign');
if(select count(*) from filetype where id = 6) = 0
	insert into filetype(id,name,comment) values(6,'Акт приемки сдачи работ (Покупатель)','Файл DP_ZAKTPRM');
if(select count(*) from filetype where id = 7) = 0
	insert into filetype(id,name,comment) values(7,'Подпись aкта приемки сдачи работ (Покупатель)','Файл DP_ZAKTPRM......sign');
--Корректировочная счет-фактура--
if(select count(*) from filetype where id = 8) = 0
	insert into filetype(id,name,comment) values(8,'Корректировочная счет-фактура','Файл ON_KORSFAKT');
if(select count(*) from filetype where id = 9) = 0
	insert into filetype(id,name,comment) values(9,'Подпись корректировочной счет-фактуры','Файл ON_KORSFAKT........sign');
--Счет-фактура--
if(select count(*) from filetype where id = 10) = 0
	insert into filetype(id,name,comment) values(10,'Счет-фактура','Фйл ON_SFAKT');
if(select count(*) from filetype where id = 11) = 0
	insert into filetype(id,name,comment) values(11,'Подпись счет-фактуры','Файл ON_SFAKT....sign');
--Товарная накладная--
if(select count(*) from filetype where id = 12) = 0
	insert into filetype(id,name,comment) values(12,'Товарная накладная (Продавец)','Файл DP_OTORG12');
if(select count(*) from filetype where id = 13) = 0
	insert into filetype(id,name,comment) values(13,'Подпись товарной накладной (Продавец)','Файл DP_OTORG12......sign');
if(select count(*) from filetype where id = 14) = 0
	insert into filetype(id,name,comment) values(14,'Товарная накладная (Покупатель)','Файл DP_PTORG12');
if(select count(*) from filetype where id = 15) = 0
	insert into filetype(id,name,comment) values(15,'Подпись товарной накладной (Покупатель)','Файл DP_PTORG12.......sign');
--Дополнительный лист книги покупок--
if(select count(*) from filetype where id = 16) = 0
	insert into filetype(id,name,comment) values(16,'Дополнительный лист книги покупок','');
--Дополнительный лист книги продаж--
if(select count(*) from filetype where id = 17) = 0
	insert into filetype(id,name,comment) values(17,'Дополнительный лист книги продаж','');
--Журнал полученных и выставленных счетов-фактур--
if(select count(*) from filetype where id = 18) = 0
	insert into filetype(id,name,comment) values(18,'Журнал полученных и выставленных счетов-фактур','');
--Книга покупок--
if(select count(*) from filetype where id = 19) = 0
	insert into filetype(id,name,comment) values(19,'Книга покупок','');
--Книга продаж--
if(select count(*) from filetype where id = 20) = 0
	insert into filetype(id,name,comment) values(20,'Книга продаж','');
--Скан--
if(select count(*) from filetype where id = 21) = 0
	insert into filetype(id,name,comment) values(21,'Скан','Сканированный файл jpg, pdf и т.д.');
-------------------------------------
--Вставляем в таблицу CanCraeteFile--
-------------------------------------
--Акт--
if (select count(*) from CanCreateFile where DocTypeId = 4 and FileTypeId = 4 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(4,4,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 4 and FileTypeId = 5 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(4,5,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 4 and FileTypeId = 6 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(4,6,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 4 and FileTypeId = 7 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(4,7,1,1);
--сканы--
if (select count(*) from CanCreateFile where DocTypeId = 4 and FileTypeId = 21) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(4,21,1,1000);
--Корректировочная счет-фактура--
if (select count(*) from CanCreateFile where DocTypeId = 11 and FileTypeId = 8 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(11,8,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 11 and FileTypeId = 9 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(4,9,1,1);
--сканы--
if (select count(*) from CanCreateFile where DocTypeId = 11 and FileTypeId = 21) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(11,21,1,1000);
--Счет-фактура
if (select count(*) from CanCreateFile where DocTypeId = 3 and FileTypeId = 10 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(3,10,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 3 and FileTypeId = 11 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(4,11,1,1);
--сканы--
if (select count(*) from CanCreateFile where DocTypeId = 3 and FileTypeId = 21) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(3,21,1,1000);
--Товарная накладная--
if (select count(*) from CanCreateFile where DocTypeId = 5 and FileTypeId = 12 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(5,12,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 5 and FileTypeId = 13 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(5,13,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 5 and FileTypeId = 14 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(5,14,1,1);
if (select count(*) from CanCreateFile where DocTypeId = 5 and FileTypeId = 15 ) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(5,15,1,1);
--сканы--
if (select count(*) from CanCreateFile where DocTypeId = 5 and FileTypeId = 21) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(5,21,1,1000);
--Дополнительный лист книги покупок--
if (select count(*) from CanCreateFile where DocTypeId = 7 and FileTypeId = 16) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(7,16,1,1);
--Дополнительный лист книги продаж--
if (select count(*) from CanCreateFile where DocTypeId = 8 and FileTypeId = 17) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(8,17,1,1);
--Журнал полученных и выставленных счетов-фактур--
if (select count(*) from CanCreateFile where DocTypeId = 8 and FileTypeId = 18) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(8,18,1,1);
--Книга покупок--
if (select count(*) from CanCreateFile where DocTypeId = 6 and FileTypeId = 19) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(6,19,1,1);
--Книга продаж--
if (select count(*) from CanCreateFile where DocTypeId = 10 and FileTypeId = 20) = 0
	insert into CanCreateFile(DocTypeId,FileTypeId,MinCount,MaxCount) values(10,20,1,1);
---------------------------
--Вставляем статусы задач--
---------------------------
if (select count(*) from TaskStatus where id = 1) = 0
	insert into TaskStatus(id, name) values(1,'Задача созаднна и во очереди');
if (select count(*) from TaskStatus where id = 2) = 0
	insert into TaskStatus(id, name) values(2,'Задача выполняется');
if (select count(*) from TaskStatus where id = 3) = 0
	insert into TaskStatus(id, name) values(3,'Задача выполнена');
if (select count(*) from TaskStatus where id = 4) = 0
	insert into TaskStatus(id, name) values(4,'Произошла ошибка при выполнении');
-----------------------------------
--Вставляем корректировочные типы--
-----------------------------------
if (select count(*) from TaskStatus where id = 0) = 0
	insert into CorrectionType(id,name,short) values(0,'Не корректура','');
if (select count(*) from TaskStatus where id = 1) = 0
	insert into CorrectionType(id,name,short) values(1,'Корректура','C');
if (select count(*) from TaskStatus where id = 2) = 0
	insert into CorrectionType(id,name,short) values(2,'Пересмотр','R');
if (select count(*) from TaskStatus where id = 3) = 0
	insert into CorrectionType(id,name,short) values(3,'Пересмотр корректуры','S');
-----------------------
--Заполняем DocSource--
-----------------------
if (select count(*) from DocSource where id = 0) = 0
	insert into DocSource(id,name) values(0,'Неопределен');
if (select count(*) from DocSource where id = 1) = 0
	insert into DocSource(id,name) values(1,'Из Sap');
---------------------------------
--Заполняем таблицу PartnerType--
---------------------------------
if (select count(*) from PartnerType where id = 1) = 0
	insert into PartnerType(id,name) values(1,'Юридическое лицо');
if (select count(*) from PartnerType where id = 2) = 0
	insert into PartnerType(id,name) values(2,'Физическое лицо');