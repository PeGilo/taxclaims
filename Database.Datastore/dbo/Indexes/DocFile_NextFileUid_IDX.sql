﻿CREATE INDEX [DocFile_NextFileUid_IDX]
	ON [dbo].[DocFile]
	(NextFileUid)
