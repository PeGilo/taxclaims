﻿CREATE INDEX [Document_DocTypeId_IDX]
	ON [dbo].[Document]
	(DocTypeId)
