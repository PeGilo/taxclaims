﻿CREATE FUNCTION [dbo].[fnCanMoveFrom3To4]
(
	@DocumentUid uniqueidentifier,
	@UserUid uniqueidentifier
)
RETURNS @infoRes TABLE ([id] int, [message] varchar(max))
AS
BEGIN
	--------------------------------------
	--Смотрим есть ли такой пользователь--
	--------------------------------------
	if (select COUNT(*) from aspnet_Users where UserId = @UserUid) = 0
		insert into @infoRes([id], [message])
		select [id], [message] from ValidationInfo where ID = 2;
	-------------------------
	--Смотрим есть ли права--
	-------------------------
	if (select COUNT(*) from aspnet_UsersInRoles where 
		UserId = @UserUid
		and
		RoleId in 
		('836CC049-8254-401F-A244-364971162233',
		'7C8F0E4B-594E-4DCF-92A6-3DD4389E335A')) = 0
		insert into @infoRes([id], [message])
		select [id], [message] from ValidationInfo where ID = 3;
	RETURN;
END

