﻿CREATE TABLE [dbo].[DocType]
(
	[Id] [int] NOT NULL,
	[Name] [varchar](max) NULL, 
    CONSTRAINT [PK_DocType] PRIMARY KEY ([Id]),
)
