﻿CREATE TABLE [dbo].[RegistryDoc]
(
	[UId] [uniqueidentifier] NOT NULL,
	[CorrectionTypeId] int null,
	[BuyerUId] uniqueidentifier,
	[SellerUId] uniqueidentifier,
	[PackageIndex] varchar(15),
	[PageNumbers] varchar(50),
	[IsScan] bit not null default(0),
	[SumTotal] money default(0),
	[SumTax] money default(0),
	[ParentDocNumber] [varchar](50),
	[ParentDocDate] datetime,
	[FactoryCode] [varchar](10),
	[FactoryName] [varchar](200),
	[PartnerName] [varchar](200),
	[PartnerINN] [varchar](50),
	[PartnerKPP] [varchar](50),
	constraint [PK_RegistryDoc] PRIMARY KEY([UId]),
	constraint [FK_RegistryDocCorrectionType] foreign key(CorrectionTypeId) references [dbo].[CorrectionType](Id),
	constraint [FK_RegistryDocDocument] FOREIGN KEY([UId]) REFERENCES [dbo].[Document]([UId]),
	constraint [FK_RegistryDocPartnerBuyer] foreign key([BuyerUId]) references [dbo].[Partner]([Uid]),
	constraint [FK_RegistryDocPartnerSeller] foreign key([SellerUId]) references [dbo].[Partner]([Uid])
)
