﻿CREATE TABLE [dbo].[CorrectionType]
(
	[Id] INT NOT NULL,
	[Name] varchar(200) not null,
	[Short] varchar(10) null,
	constraint CorrectionType_PK primary key([Id])
)
