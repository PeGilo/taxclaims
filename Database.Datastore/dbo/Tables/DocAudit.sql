﻿CREATE TABLE [dbo].[DocAudit]
(
	[Id] uniqueidentifier not null default NewID(),
	[DocumentUid] uniqueidentifier not null,
	[RoleUid] uniqueidentifier not null,
	[UserUid] uniqueidentifier not null,
	[DocStateIdFrom] int,
	[DocStateIdTo] int not null,
    [DateChange] datetime not null default GetDate(),
	constraint PK_DocAudit primary key(Id)
)
