﻿CREATE TABLE [dbo].[DocStateRole]
(
	DocStateId int not null,
	RoleUid uniqueidentifier not null,
	constraint PK_DocStateRole primary key(DocStateId,RoleUid),
	constraint FK_DocStateRole_DocState foreign key(DocStateId)
	references DocState(Id),
	constraint FK_DocStateRole_aspnet_Roles foreign key(RoleUid)
	references aspnet_roles(roleid)
)
