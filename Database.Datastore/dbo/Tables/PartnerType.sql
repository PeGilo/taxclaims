﻿CREATE TABLE [dbo].[PartnerType]
(
	[Id] INT NOT NULL,
	[Name] varchar(20) not null,
	constraint PK_PartnerType primary key(Id)
)
