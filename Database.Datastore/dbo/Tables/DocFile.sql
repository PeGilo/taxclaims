﻿CREATE TABLE [dbo].[DocFile]
(
	[UId] [uniqueidentifier] NOT NULL,
	[DocumentUid] [uniqueidentifier] NOT NULL,
	[FileTypeId] [int] NOT NULL,
	[SprPage] [int] NOT NULL DEFAULT(0),
	[Name] varchar(max) not null,
	[Content] varbinary(max) NOT NULL,
	[PageFrom] int null,
	[PageTo] int null,
	[NextFileUid] uniqueidentifier NULL,
	CONSTRAINT [FK_DocFile_DocFile] FOREIGN KEY([NextFileUid])
		REFERENCES [dbo].[DocFile] ([UId]),
	CONSTRAINT [FK_DocFile_Document] FOREIGN KEY([DocumentUid])
		REFERENCES [dbo].[Document] ([UId]) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT [FK_DocFile_FileType] FOREIGN KEY([FileTypeId])
		REFERENCES [dbo].[FileType] ([id]) ,
    CONSTRAINT [PK_DocFile] PRIMARY KEY ([UId])
)