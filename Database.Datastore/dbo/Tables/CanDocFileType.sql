﻿CREATE TABLE [dbo].[CanCreateFile]
(
	[FileTypeId] int NOT NULL,
	[DocTypeId] int NOT NULL,
	[MinCount] int NOT NULL,
	[MaxCount] int NULL,
	constraint PK_CanCreateFile primary key(FileTypeId, DocTypeId),
	constraint FK_CanCreateFile_FileType foreign key(FileTypeId) references FileType([Id]),
	constraint FK_CanCreateFile_DocTypeId foreign key(DocTypeId) references DocType([Id])
)
