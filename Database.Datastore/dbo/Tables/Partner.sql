﻿CREATE TABLE [dbo].[Partner]
(
	[UId] uniqueidentifier NOT NULL,
	[PartnerTypeId] int not null,
	[Inn] varchar(20),
	[Kpp] varchar(20),
	[Name] varchar(200),
	[MiddleName] varchar(200),
	[LastName] varchar(200),
	constraint PK_Partner primary key([UId]),
	constraint FK_Partner_PartnerType foreign key(PartnerTypeId)
	references PartnerType([Id])
)
