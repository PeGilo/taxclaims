﻿CREATE TABLE [dbo].[DocAccess]
(
	[DocTypeId] int not null,
	[DocStateId] int not null,
	[RoleUid] uniqueidentifier not null,
	[DocAccessTypeId] int not null,
	constraint PK_DocAccess primary key([DocTypeId],[DocStateId],[RoleUid]),
	constraint FK_DocAccess_DocType foreign key([DocTypeId])
	references DocType([id]),
	constraint FK_DocAccess_DocState foreign key([DocStateId])
	references DocState([id]),
	constraint FK_DocAccess_Aspnet_Roles foreign key([RoleUid])
	references aspnet_roles([roleid]),
	constraint FK_DocAccess_DocAccessType foreign key([DocAccessTypeId])
	references DocAccessType([id])
)
