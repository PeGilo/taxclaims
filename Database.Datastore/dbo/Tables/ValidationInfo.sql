﻿CREATE TABLE [dbo].[ValidationInfo]
(
	[Id] int NOT NULL,
	[Message] varchar(max) NOT NULL,
	[Comment] varchar(max) NOT NULL,
	constraint PK_ValidationInfo primary key([Id])
)
