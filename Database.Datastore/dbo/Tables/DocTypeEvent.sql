﻿CREATE TABLE [dbo].[DocTypeEvent]
(
	DocEventTypeId int not null,
	DocTypeId int not null,
	DocStateId int not null,
	constraint PK_DocTypeEvent primary key(DocEventTypeId,DocTypeId,DocStateId),
	constraint FK_DocTypeEvent_DocEventType foreign key(DocEventTypeId) references DocEventType(Id),
	constraint FK_DocTypeEvent_DocType foreign key(DocTypeId) references DocType(Id),
	constraint FK_DocTypeEvent_DocState foreign key(DocStateId) references DocState(Id)
)
