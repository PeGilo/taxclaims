﻿CREATE TABLE [dbo].[Document]
(
	[UId] [uniqueidentifier] NOT NULL,
	[DocTypeId] [int] NOT NULL,
	[DocStateId] [int] NOT NULL,
	[DocSourceId] [int] NOT NULL default(0),
	[CreatorUserUid] [uniqueidentifier] NOT NULL,
	[DocumentHolderUserUId] [uniqueidentifier] NULL,
	[HierarchyHolderUserUId] [uniqueidentifier] NULL,
	[Name] [varchar](max) NULL,
	[Number] [varchar](50) NULL,
	[DocDate] [datetime] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] datetime NOT NULL default getdate(),
	[DateChanged] datetime NULL default getdate(),
	[ParentDocUId] [uniqueidentifier],
    CONSTRAINT [PK_Document] PRIMARY KEY ([UId]),
	CONSTRAINT [FK_DocType] FOREIGN KEY([DocTypeId]) 
		REFERENCES [dbo].[DocType] ([Id]),
	CONSTRAINT [FK_Document_DocState] FOREIGN KEY([DocStateId]) 
		REFERENCES [dbo].[DocState] ([Id]),
	CONSTRAINT [FK_Document_aspnet_Users] FOREIGN KEY([DocumentHolderUserUId]) 
		REFERENCES [dbo].[aspnet_Users] ([UserId]),
	CONSTRAINT [FK_Document_aspnet_Users_Hierarchy] FOREIGN KEY([HierarchyHolderUserUId]) 
		REFERENCES [dbo].[aspnet_Users] ([UserId]),
	CONSTRAINT [FK_Document_aspnet_Users_CreatorUser] FOREIGN KEY([CreatorUserUid]) 
		REFERENCES [dbo].[aspnet_Users] ([UserId]),
	constraint [FK_Document_Document] foreign key([ParentDocUId])
		references [dbo].[Document]([Uid]),
	constraint [FK_Document_DocSource] foreign key([DocSourceId])
		references [dbo].[DocSource]([Id])
)