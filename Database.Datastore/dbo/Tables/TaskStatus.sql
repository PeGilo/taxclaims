﻿CREATE TABLE [dbo].[TaskStatus]
(
	[Id] INT NOT NULL,
	[Name] varchar(200) not null,
	constraint TaskStatus_PK primary key([Id])
)
