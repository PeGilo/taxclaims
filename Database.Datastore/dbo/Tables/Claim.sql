﻿CREATE TABLE [dbo].[Claim]
(
	[UId] [uniqueidentifier] NOT NULL,
	[OrgId] [int] NULL,
	[DateReady] datetime null,
	[InspectionName] varchar(1000),
	CONSTRAINT [PK_Claim] PRIMARY KEY ([UId]),
	CONSTRAINT [FK_Document] FOREIGN KEY([UId])
		REFERENCES [dbo].[Document] ([UId]), 
    constraint [FK_Claim_Organization] foreign key([OrgId])
		references [dbo].[Organization]([Id])
)