﻿CREATE TABLE [dbo].[DocRoute]
(
	[Id] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
    CONSTRAINT [PK_DocRoute] PRIMARY KEY ([Id])
)
