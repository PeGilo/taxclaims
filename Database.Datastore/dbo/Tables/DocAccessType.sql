﻿CREATE TABLE [dbo].[DocAccessType]
(
	[Id] int not null,
	[Name] varchar(50) not null,
	constraint PK_DocAccessType primary key(Id)
)
