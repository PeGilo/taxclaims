﻿CREATE TABLE [dbo].[OrgType]
(
	[Id] int NOT NULL,
	[Name] varchar(100) not null,
	constraint PK_OrgType primary key([Id])
)