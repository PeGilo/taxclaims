﻿CREATE TABLE [dbo].[DocState]
(
	[Id] [int] NOT NULL,
	[DocRouteId] [int] NOT NULL,
	[Number] int not null,
	[Name] [varchar](200) NOT NULL,
	[Comment] [varchar](max) NULL,
	[DisplayName] [varchar](max) NULL,
    CONSTRAINT [PK_DocState] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_DocState_DocRoute] FOREIGN KEY([DocRouteId])
	REFERENCES [dbo].[DocRoute] ([Id])
)