﻿CREATE TABLE [dbo].[Organization]
(
	[Id] int NOT NULL,
	[OrgTypeId] int NOT NULL,
	[Name] varchar(1000) NOT NULL,
	[ParentId] int NULL,
	constraint PK_Organization primary key([Id]),
	constraint FK_Organization_ParentId foreign key(ParentId)
		references Organization([Id]),
	constraint FK_Organization foreign key(OrgTypeId) 
		references OrgType([id])
)
