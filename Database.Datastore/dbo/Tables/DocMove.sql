﻿CREATE TABLE [dbo].[DocMove]
(
	[DocStateIdFrom] [int] NOT NULL,
	[DocStateIdTo] [int] NOT NULL,
	[DocRouteId] [int] not NULL,
	[PrCanMove] varchar(100) NOT NULL,
	[Comment] varchar(max) NULL,
    CONSTRAINT [PK_DocMove] PRIMARY KEY ([DocStateIdFrom],[DocStateIdTo]),
	CONSTRAINT [FK_DocMove_DocRoute] FOREIGN KEY([DocRouteId])
	REFERENCES [dbo].[DocRoute] ([Id]),
	CONSTRAINT [FK_DocState_From] FOREIGN KEY([DocStateIdFrom])
	REFERENCES [dbo].[DocState] ([Id]),
	CONSTRAINT [FK_DocState_To] FOREIGN KEY([DocStateIdTo])
	REFERENCES [dbo].[DocState] ([Id])
)