﻿CREATE TABLE [dbo].[Registry]
(
	[UId] [uniqueidentifier] not null,
	[OrgId] [int] null,
	[DateReady] datetime null,
	[ClaimItemNo] nvarchar(20) null,
	[BookType] varchar(1) null,
	CONSTRAINT [FK_RegistryDocument] FOREIGN KEY([UId]) REFERENCES [dbo].[Document] ([UId]), 
    CONSTRAINT [PK_Registry] PRIMARY KEY ([UId])
)