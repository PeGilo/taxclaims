﻿CREATE TABLE [dbo].[FileType]
(
	[id] [int] NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[Comment] [varchar](max) null,
    CONSTRAINT [PK_FileType] PRIMARY KEY ([id])
)
