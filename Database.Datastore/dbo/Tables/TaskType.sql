﻿CREATE TABLE [dbo].[TaskType]
(
	[Id] INT NOT NULL,
	[Name] varchar(200) NOT NULL,
	constraint TaskType_PK primary key([Id])
)
