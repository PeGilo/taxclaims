﻿CREATE TABLE [dbo].[Task]
(
	[UId] uniqueidentifier NOT NULL,
	[TaskTypeId] int not null,
	[StatusTypeId] int not null,
	[DateCreate] datetime not null,
	[Enabled] bit not null default(1),
	[UserCreatorUid] uniqueidentifier,
	[Parameters] xml,
	[Result] xml,
	[DateStart] datetime null,
	[DateEnd] datetime null,
	[TaskException] varchar(max),
	CONSTRAINT [Task_TaskType_FK] FOREIGN KEY([TaskTypeId])
		REFERENCES [dbo].[TaskType] ([Id]),
    CONSTRAINT [Task_StatusType_FK] FOREIGN KEY([StatusTypeId])
		REFERENCES [dbo].[TaskStatus] ([Id]),
	constraint [Task_Aspnet_Users_FK] foreign key([UserCreatorUid])
		references [dbo].[aspnet_Users],
	constraint Task_PK primary key([UId])
)
