﻿create procedure spCanMove
(
    @StateFrom int, 
	@StateTo int, 
	@RouteId int, 
	@DocumentUid uniqueidentifier, 
	@UserUid uniqueidentifier 
)
as 
begin 
	declare @funcName nvarchar(250); 
	declare @sql nvarchar(max); 
	--Всмотрим есть ли такая функция--
	if (select COUNT(*) from DocMove where DocStateIdFrom = @StateFrom and DocStateIdTo = @StateTo) = 0  
	begin
		select [id], [message] from ValidationInfo where ID = 1; 
	end;
	else
	begin
		--Получаем имя функции-- 
		set @funcName = (select PrCanMove from DocMove where DocStateIdFrom = @StateFrom and DocStateIdTo = @StateTo and DocRouteId = @RouteId); 
		--Формируем функцию-- 
		set @sql = 
		'select [id],[message] from ' + @funcName 
		+ '( ''' + cast(@DocumentUid AS VARCHAR(40)) + ''' ' 
		+ ', ''' + cast(@UserUid AS VARCHAR(40))     + ''') '; 
		exec sp_executesql @sql; 
	end; 
end;