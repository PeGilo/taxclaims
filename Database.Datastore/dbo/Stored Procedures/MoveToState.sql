﻿create procedure MoveToState(
@docUid uniqueidentifier,
@stateId int,
@docTypeId int,
@source varchar(30) out,
@message varchar(4000) out,
@isSystemError int out
)
as
begin try
	declare @claimUid uniqueidentifier;
	declare @registryUid uniqueidentifier;
	declare @sourceDocUid uniqueidentifier;
	set @isSystemError = 1;
	--------------
	--Требование--
	--------------
	if(@docTypeId = 1)
	begin
		if(@stateId in (1,4))
			update Document set DocStateId = @stateId where uid = @docUid;
	end;
	----------
	--Реестр--
	----------
	else if(@docTypeId = 2)
	begin
		if(@stateId = 5)
		begin
			set @claimUid = (select ParentDocUId from Document where [Uid] = @docUid);
			update Document set DocStateId = @stateId where uid = @docUid; --Переводим реестр в статус сформирован
			update Document set DocStateId = 2 where uid = @claimUid;--Переводим требование в состояние Исполняется
		end;
	end;
	------------
	--Документ--
	------------
	else
	begin
		--Получаем вышестоящие документы--
		with doc( [UId] ,[DocTypeId] ,[ParentDocUId])
		as (SELECT [UId] ,[DocTypeId] ,[ParentDocUId] FROM [Document] where [Uid] = @docUid
		union all
		SELECT rec.[UId] ,rec.[DocTypeId] ,rec.[ParentDocUId]
		FROM [Document] as rec 
		inner join doc on doc.[ParentDocUId] = rec.[uid])
		select [UId],[DocTypeId],[ParentDocUId] into #tmpParentDocs from doc where doc.[UId] = @docUid;
		--Передаем в переменные
		set @claimUid = (select [uid] from #tmpParentDocs where DocTypeId = 2);
		set @registryUid = (select [uid] from #tmpParentDocs where DocTypeId = 2);
		if((select COUNT(*) from #tmpParentDocs where DocTypeId not in(1, 2)) != 0)
		begin
			set @sourceDocUid = (select [uid] from #tmpParentDocs where DocTypeId not in(1, 2));
		end;
		drop table #tmpParentDocs;--удаляем временную таблицу
		--------------------------------
		--Состояние данные отсутствуют--
		--------------------------------
		if(@stateId = 6)
		begin
			update Document set DocStateId = @stateId where uid = @docUid;
			update Document set DocStateId = 2 where [uid] = @claimUid;	
		end;
		----------------------------------
		--Состояние документ подготовлен--
		----------------------------------
		else if(@stateId = 7 and (select COUNT(*) from DocFile where DocumentUid = @docUid) > 0)
		begin
			--Смотрим есть ли файлы--
			update Document set DocStateId = @stateId where [uid] = @docUid;
			--Переводим родительский документ в 7-е состояние--
			if(@sourceDocUid is not null )--Если все документы реестра не в шестом состоянии
				if((select COUNT(*) from Document where ParentDocUId = @sourceDocUid and DocStateId = 6) = 0)
					update Document set DocStateId = @stateId where [uid] = @sourceDocUid;	
			--Переводим реестр в 7-е состояние
			if((select COUNT(*) from Document where ParentDocUId = @registryUid and DocStateId = 6) = 0)
					update Document set DocStateId = @stateId where [uid] = @sourceDocUid;	
			--Переводим требование в статус исполняется
			update Document set DocStateId = 2 where [uid] = @claimUid;	
		end;
		---------------------------------
		--Состояние Проверен/корректный--
		---------------------------------
		else if(@stateId = 8)
		begin
			update Document set DocStateId = @stateId where [uid] = @docUid;
			--Переводим родительский документ в 8-е состояние--
			if(@sourceDocUid is not null )--Если все документы реестра не в шестом состоянии
				if((select COUNT(*) from Document where ParentDocUId = @sourceDocUid and DocStateId != 8) = 0)
					update Document set DocStateId = @stateId where [uid] = @sourceDocUid;	
			--Переводим реестр в 8-е состояние
			if((select COUNT(*) from Document where ParentDocUId = @registryUid and DocStateId != 8) = 0)
				update Document set DocStateId = @stateId where [uid] = @sourceDocUid;
			--Переводим требование в статус докуметы проверены
			update Document set DocStateId = 3 where [uid] = @claimUid;	
		end;
		-----------------------
		--Состояние Исправлен--
		-----------------------
		else if(@stateId = 9)
		begin
			update Document set DocStateId = @stateId where [uid] = @docUid;
			--Переводим родительский документ в 9-е состояние--
			if(@sourceDocUid is not null)--Если все документы реестра не в шестом состоянии
				if((select COUNT(*) from Document where ParentDocUId = @sourceDocUid and DocStateId != 8 and DocStateId != 9) = 0)
					update Document set DocStateId = @stateId where [uid] = @sourceDocUid;
			--Переводим реестр в 9-е состояние
			if((select COUNT(*) from Document where ParentDocUId = @registryUid and DocStateId != 8 and DocStateId != 9) = 0)
				update Document set DocStateId = @stateId where [uid] = @sourceDocUid;
			--Переводим требование в статус исполняется
			update Document set DocStateId = 2 where [uid] = @claimUid;	
		end;
		-----------------------------------
		--Состояние Проверен/Некорректный--
		-----------------------------------
		else if(@stateId = 10)
		begin
			update Document set DocStateId = @stateId where [uid] = @docUid;
			--Переводим родительский документ в 10-е состояние--
			if(@sourceDocUid is not null)
				update Document set DocStateId = @stateId where [uid] = @sourceDocUid;
			--Переводим реестр в 9-е состояние
			update Document set DocStateId = @stateId where [uid] = @sourceDocUid;
			--Переводим требование в статус исполняется
			update Document set DocStateId = 2 where [uid] = @claimUid;	
		end;
	end;
	set @message = 'Успешно';
    return 0;
end try
begin catch
	set @isSystemError = 1;
	set @message = 'Database TaxClaims: ' + isnull(DB_NAME(),'')
							+ ' Procedure: ' + isnull(ERROR_PROCEDURE(),'')
							+ '; line: ' + isnull(str(ERROR_LINE()),'')
							+ '; code: ' + isnull(str(ERROR_NUMBER()),'')
							+ '; message: '+ isnull(ERROR_MESSAGE(),'');
	return Error_Number();
end catch