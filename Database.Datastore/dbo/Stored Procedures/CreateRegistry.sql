﻿create PROCEDURE [dbo].[CreateRegistrySap](
@xml xml,
@claimUid uniqueidentifier,
@registryUid uniqueidentifier,
@userName varchar(4000),
@source varchar(30) out,
@message varchar(4000) out,
@isSystemError int out)
AS
begin try
begin transaction;
declare @date varchar(10);
declare @time varchar(10);
declare @datecreate datetime;
declare @userUid uniqueidentifier;
declare @dateReady datetime;
declare @orgId int;

declare @orgUid uniqueidentifier;
declare @bookType varchar(1);
declare @INNOrg varchar(30);
declare @KPPOrg varchar(30);
declare @nameOrg varchar(max);

set @source = 'DatabaseTaxClaims';
set @dateReady = (select DateReady from claim where [UId] = @claimUid);
set @orgId = (select OrgId from claim where [UId] = @claimUid);
set @isSystemError = 0;
-------------------------
--Получаем пользователя--
-------------------------
/*set @userName = (select 
properties.property.value(N'@Пользователь', N'nvarchar(MAX)') AS [User]
FROM @xml.nodes(N'Файл/Книга') AS properties(property));*/
--------------------------
--Получаем дату создания--
--------------------------
set @date = (select 
properties.property.value(N'@ДатаФорм', N'nvarchar(MAX)') AS [DateFrom]
FROM @xml.nodes(N'Файл/Книга') AS properties(property));
---------------------------
--Получаем время создания--
---------------------------
set @time = (select 
properties.property.value(N'@ВремяФорм', N'nvarchar(MAX)') AS [TimeFrom]
FROM @xml.nodes(N'Файл/Книга') AS properties(property));
----------------------------------
--Получаем дату и время создания--
----------------------------------
set @datecreate =(select right(@date,4) + '-' + SUBSTRING(@date,4,2) + '-' + LEFT(@date,2) + ' ' + @time);
-------------------------------
--Получиаем гуид пользователя--
-------------------------------
set @userUid = (select UserId from aspnet_Users where LoweredUserName = lower(@userName));
--set @userUid = (select UserId from aspnet_Users where LoweredUserName = 'sibgenco\' + lower(@userName));
------------------------
--Получаем тип покупок--
------------------------
set @bookType = (select 
case properties.property.value(N'@ТипКн', N'nvarchar(MAX)') when 'S' then 'S' when 'P' then 'B' else '' end as [BookType]
FROM @xml.nodes(N'Файл/Книга') AS properties(property));
/*if @bookType = ''
begin
	set @message = 'В файле отстутствует тип книги';
	rollback transaction;
	return 1;
end;*/
-------------------------------------
--Получаем наименование организцаии--
-------------------------------------
set @nameOrg = (select top 1 
properties.property.value(N'@НаимОрг', N'nvarchar(MAX)')
FROM @xml.nodes(N'Файл/Книга/СведОрг') AS properties(property));
/*if @nameOrg is null 
begin
	set @message = 'В файле отсутвтует наименование орагницазии';
	rollback transaction;
	return 2;
end;*/
----------------------------
--Получаем КПП органицации--
----------------------------
set @KPPOrg = (select top 1 
properties.property.value(N'@КППОрг', N'nvarchar(MAX)')
FROM @xml.nodes(N'Файл/Книга/СведОрг') AS properties(property));
/*if @KPPOrg is null 
begin
	set @message = 'В файле отсутствует КПП организцаии';
	rollback transaction;
	return 3;
end;*/
----------------------------
--Получаем ИНН организации--
----------------------------
set @INNOrg = (select top 1 
properties.property.value(N'@ИННОрг', N'nvarchar(MAX)')
FROM @xml.nodes(N'Файл/Книга/СведОрг') AS properties(property));
/*if @INNOrg is null
begin
	set @message = 'В файле отсутствует ИНН организации';
	rollback transaction;
	return 4;
end;*/
-----------------------------------
--Вставляем партнера (организцию)--
-----------------------------------
if (select COUNT(*) from [partner] where inn = @INNOrg and kpp = @KPPOrg) = 0
begin
	set @orgUid = NEWID();
	insert into [partner]([uid],partnerTypeId,kpp,inn,name) values(@orgUid,1,@KPPOrg,@INNOrg,@nameOrg);
end;
else
begin
	set @orgUid = (select [uid] from [partner] where inn = @INNOrg and kpp = @KPPOrg);
end;
-------------------------------------------
--Передаем документы во временную таблицу--
-------------------------------------------
select
NEWID() as [Uid]
,properties.property.value(N'@КодЗав', N'nvarchar(MAX)') AS FactoryCode
,properties.property.value(N'@НаимЗав', N'nvarchar(MAX)') AS FactoryName
,properties.property.value(N'@НаимПартн', N'nvarchar(MAX)') AS PartnerName
,properties.property.value(N'@ИННПартн', N'nvarchar(MAX)') AS PartnerINN
,properties.property.value(N'@КПППартн', N'nvarchar(MAX)') AS PartnerKPP
,properties.property.value(N'@НомСчФ', N'nvarchar(MAX)') AS Number
,CONVERT(datetime,properties.property.value(N'@ДатаСчф', N'nvarchar(MAX)'),104) AS DocDate
,properties.property.value(N'@НомерДокОсн', N'nvarchar(MAX)') AS ParentDocNumber
,convert(datetime,properties.property.value(N'@ДатаДокОсн', N'nvarchar(MAX)'),104) AS ParentDocDate
,cast(properties.property.value(N'@СтоимСчФВс', N'nvarchar(MAX)') as money) AS SumTotal
,cast(properties.property.value(N'@СумНДССчф', N'nvarchar(MAX)') as money) AS SumTax
,case properties.property.value(N'@ПризФО', N'nvarchar(MAX)') when 2 then 0 else 1 end AS IsScan
,properties.property.value(N'@НомерПак', N'nvarchar(MAX)') AS PackageIndex
,case properties.property.value(N'@ВидКорр', N'nvarchar(MAX)') when 'C' then 1 when 'R' then 2 when 'S' then 3 else null end AS CorrectionTypeId
into #tmpRegistry
FROM @xml.nodes(N'Файл/Книга/СведДок') AS properties(property);
--------------------------------------------
--Вставляем партнеров во временную таблицу--
--------------------------------------------
select 
(select top 1 PartnerName from #tmpRegistry reg2 
where 
reg2.PartnerINN = reg1.PartnerINN 
and 
reg2.PartnerKPP = reg1.PartnerKPP) as PartnerName,
reg1.PartnerINN,
reg1.PartnerKPP into #tmpPartner from #tmpRegistry reg1 group by PartnerINN,PartnerKPP;

-----------------------
--Вставляем Партнеров--
-----------------------
merge [dbo].[partner] as dest
using (select PartnerName,PartnerINN,PartnerKPP from #tmpPartner group by PartnerName,PartnerINN,PartnerKPP) src
on (dest.[inn] = src.[PartnerINN] and dest.[kpp] = src.[PartnerKPP])
when matched then
update set dest.name = src.PartnerName
when not matched then
insert ([uid],partnertypeid,inn,kpp,name) values(newid(),1,src.PartnerINN,src.PartnerKPP,src.PartnerName);
	--------------------
	--Вставляем реестр--
	--------------------
	--Вставляем в табилцу документ--
	INSERT INTO [TestTaxClaimsData].[dbo].[Document]
           ([UId]
           ,[DocTypeId]
           ,[DocStateId]
           ,[DocSourceId]
           ,[CreatorUserUid]
           /*,[DocumentHolderUserUId]
           ,[HierarchyHolderUserUId]*/
           ,[Name]
           ,[Number]
           ,[DocDate]
           ,[Comment]
           ,[DateCreated]
           ,[DateChanged]
           ,[ParentDocUId])
     VALUES
           (@registryUid --UId
            ,2 --DocTypeId
            ,5 --DocStateId
            ,1 --DocSourceId
            ,@userUid --CreatorUserUid
            /*,null --DocumentHolderUserUId
            ,null --HierarchyHolderUserUId*/
            ,null --Name
            ,null --Number
            ,@datecreate --DocDate
            ,null --Comment
            ,@datecreate --DateCreated
            ,null --DateChanged
            ,@claimUid); --ParentDocUId
	--------------------------------
	--Вставляем в таблицу Registry--
	--------------------------------
	INSERT INTO [TestTaxClaimsData].[dbo].[Registry]
           ([UId]
           ,[OrgId]
           ,[DateReady]
           ,[ClaimItemNo]
           ,[BookType])
     VALUES
           (@registryUid --UId
           ,@orgId --OrgId
           ,@dateReady --DateReady
           ,null --ClaimItemNo
           ,@bookType) --BookType
	-----------------------
	--Вставляем документы--
	-----------------------
	INSERT INTO [TestTaxClaimsData].[dbo].[Document]
           ([UId]
           ,[DocTypeId]
           ,[DocStateId]
           ,[DocSourceId]
           ,[CreatorUserUid]
           /*,[DocumentHolderUserUId]
           ,[HierarchyHolderUserUId]*/
           ,[Name]
           ,[Number]
           ,[DocDate]
           ,[Comment]
           ,[DateCreated]
           ,[DateChanged]
           ,[ParentDocUId])
     select
           [Uid] --UId
           ,0 --DocTypeId
           ,6 --DocStateId
           ,1 --DocSourceId
           ,@userUid --CreatorUserUid 
           /*,null
           ,null*/
           ,null --Name
           ,Number --Number
           ,DocDate --DocDate
           ,null --Comment
           ,@datecreate --DateCreated
           ,null --DateChanged
           ,@registryUid --ParentDocUId
           from #tmpRegistry;


	-- ТЕСТ

	INSERT INTO [TestTaxClaimsData].[dbo].[Document]
           ([UId]
           ,[DocTypeId]
           ,[DocStateId]
           ,[DocSourceId]
           ,[CreatorUserUid]
           ,[DocumentHolderUserUId]
           ,[HierarchyHolderUserUId]
           ,[Name]
           ,[Number]
           ,[DocDate]
           ,[Comment]
           ,[DateCreated]
           ,[DateChanged]
           ,[ParentDocUId])
     select
           [Uid]
           ,0
           ,2
           ,0
           ,@userUid
           ,null
           ,null
           ,null
           ,Number
           ,DocDate
           ,null
           ,@datecreate
           ,null
           ,@registryUid
           from #tmpRegistry;

	-- END ТЕСТ

	---------------------------
	--Вставляем в RegistryDoc--
	---------------------------
	INSERT INTO [TestTaxClaimsData].[dbo].[RegistryDoc]
           ([UId]
           ,[CorrectionTypeId]
           ,[BuyerUId]
           ,[SellerUId]
           ,[PackageIndex]
           ,[PageNumbers]
           ,[IsScan]
           ,[SumTotal]
           ,[SumTax]
           ,[ParentDocNumber]
           ,[ParentDocDate]
           ,[FactoryCode]
           ,[FactoryName])
      select 
           reg.[UId] --UId
           ,reg.CorrectionTypeId --CorrectionTypeId
           ,case @bookType when 'S' then @orgUid else pr.[Uid] end --продавец 
           ,case @bookType when 'B' then @orgUid else pr.[Uid]  end --покупатель
           ,reg.PackageIndex --PackageIndex
           ,null --PageNumbers
           ,reg.IsScan --IsScan
           ,reg.SumTotal --SumTotal
           ,reg.SumTax --SumTax
           ,reg.ParentDocNumber --ParentDocNumber
           ,reg.ParentDocDate --ParentDocDate
           ,reg.FactoryCode --FactoryCode
           ,reg.FactoryName --FactoryName
    from 
		#tmpRegistry reg
	left join [partner] pr on (pr.Inn = reg.PartnerINN and pr.Kpp = reg.PartnerKPP);
	
	--Переводим требование в состояние "Исполняется"
	update Document set DocStateId = 2 where [uid] = @claimUid;
	
	drop table #tmpRegistry;
	drop table #tmpPartner;
	commit transaction;
	
	set @message = 'Успешно';
    return 0;
end try
begin catch
	if @@TRANCOUNT > 0
		rollback transaction;
	set @isSystemError = 1;
	set @message = 'Database TaxClaims: ' + isnull(DB_NAME(),'')
							+ ' Procedure: ' + isnull(ERROR_PROCEDURE(),'')
							+ '; line: ' + isnull(str(ERROR_LINE()),'')
							+ '; code: ' + isnull(str(ERROR_NUMBER()),'')
							+ '; message: '+ isnull(ERROR_MESSAGE(),'');
	-----------------------------
	--Удаляем временные таблицы--
	-----------------------------
	if OBJECT_ID('tempdb..#tmpRegistry') IS NOT NULL
	begin
		drop table #tmpRegistry;
	end;
	if OBJECT_ID('tempdb..#tmpRegistry') IS NOT NULL
	begin
		drop table #tmpPartner;
	end;
	return Error_Number();
end catch
