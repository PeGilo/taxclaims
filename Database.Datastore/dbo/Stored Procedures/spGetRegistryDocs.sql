﻿create procedure spGetRegistryDocs(@parentUid uniqueidentifier)
as
begin
with doc( 
	   [UId]
      ,[DocTypeId]
      ,[DocStateId]
      ,[DocSourceId]
      ,[CreatorUserUid]
      ,[DocumentHolderUserUId]
      ,[HierarchyHolderUserUId]
      ,[Name]
      ,[Number]
      ,[DocDate]
      ,[Comment]
      ,[DateCreated]
      ,[DateChanged]
      ,[ParentDocUId])
as
(SELECT
   [UId]
  ,[DocTypeId]
  ,[DocStateId]
  ,[DocSourceId]
  ,[CreatorUserUid]
  ,[DocumentHolderUserUId]
  ,[HierarchyHolderUserUId]
  ,[Name]
  ,[Number]
  ,[DocDate]
  ,[Comment]
  ,[DateCreated]
  ,[DateChanged]
  ,[ParentDocUId]
FROM [Document]
where [ParentDocUId] = @parentUid
union all
SELECT 
   rec.[UId]
  ,rec.[DocTypeId]
  ,rec.[DocStateId]
  ,rec.[DocSourceId]
  ,rec.[CreatorUserUid]
  ,rec.[DocumentHolderUserUId]
  ,rec.[HierarchyHolderUserUId]
  ,rec.[Name]
  ,rec.[Number]
  ,rec.[DocDate]
  ,rec.[Comment]
  ,rec.[DateCreated]
  ,rec.[DateChanged]
  ,rec.[ParentDocUId]
FROM [Document] as rec 
inner join doc on rec.[ParentDocUId] = doc.[uid])

select 
 doc.[UId]
,doc.[DocTypeId]
,doc.[DocStateId]
,doc.[DocSourceId]
,doc.[CreatorUserUid]
,doc.[DocumentHolderUserUId]
,doc.[HierarchyHolderUserUId]
,doc.[Name]
,doc.[Number]
,doc.[DocDate]
,doc.[Comment]
,doc.[DateCreated]
,doc.[DateChanged]
,doc.[ParentDocUId] 
,regDoc.[PackageIndex]
,regDoc.[PageNumbers]
,regDoc.[IsScan]
,regDoc.[SumTotal]
,regDoc.[SumTax]
,regDoc.[ParentDocNumber]
,regDoc.[ParentDocDate]
,regDoc.[CorrectionTypeId]
,regDoc.[FactoryCode]
,regDoc.[FactoryName]
,regDoc.[PartnerName]
,regDoc.[PartnerINN]
,regDoc.[PartnerKPP]
,regDoc.[BuyerUId]
,regDoc.[SellerUId]
--Покупатель--
,buyer.[PartnerTypeId] as BuyerPartnerTypeId
,buyer.[Inn] as BuyerInn
,buyer.[Kpp] as BuyerKpp
,buyer.[Name] as BuyerName
,buyer.[MiddleName] as BuyerMiddleName
,buyer.[LastName] as BuyerLastName
--Продавец--
,seller.[PartnerTypeId] as SellerPartnerTypeId
,seller.[Inn] as SellerInn
,seller.[Kpp] as SellerKpp
,seller.[Name] as SellerName
,seller.[MiddleName] as SellerMiddleName
,seller.[LastName] as SellerLastName

from doc
left join [dbo].[RegistryDoc] regDoc on regDoc.[UId] = doc.[Uid]
left join [dbo].[Partner] buyer on regDoc.[BuyerUid] = buyer.[Uid]
left join [dbo].[Partner] seller on regDoc.[SellerUid] = seller.[Uid];

end;