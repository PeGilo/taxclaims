//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGC.TaxClaims.Core.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class DocAudit
    {
        public System.Guid Id { get; set; }
        public System.Guid DocumentUid { get; set; }
        public System.Guid RoleUid { get; set; }
        public System.Guid UserUid { get; set; }
        public Nullable<int> DocStateIdFrom { get; set; }
        public int DocStateIdTo { get; set; }
        public System.DateTime DateChange { get; set; }
    }
}
