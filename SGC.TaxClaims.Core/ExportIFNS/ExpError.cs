﻿using SGC.TaxClaims.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    public class ExpError
    {
        private bool _isComman = false;

        public static ExpError NoClaimItem(Guid documentUid)
        {
            return new ExpError(documentUid, "Не указан пункт требования",true);
        }

        public static ExpError WrongClaimItemNo(Guid documentUid, string claimItemNo)
        {
            return new ExpError(documentUid, string.Format("Не верный формат пункта требования {0}, пункт требования должен быть в формате 1.XX, где X - это число", claimItemNo), true);
        }

        public ExpError(Guid documentUid,string message)
        {
            this.DocumentUid = documentUid;
            this.Message = message;
            _isComman = false;
        }

        public ExpError(Guid documentUid, string message,bool isComman)
            : this(documentUid,message)
        {
            _isComman = isComman;
        }

        public Guid DocumentUid { get; private set; }
        public string Message { get; private set; }
        public bool IsComman { get { return _isComman; } }
    }
}
