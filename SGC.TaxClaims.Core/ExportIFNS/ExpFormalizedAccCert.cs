﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Акт приемки сдачи работ в XML 
    /// </summary>
    public class ExpFormalizedAccCert: ExpDocument
    {
        private string _claimItemNo;
        private string _extRef;
        private string _DP_IAKTPRMFileName;
        private string _DP_IAKTPRMFileNameSign;
        private string _DP_ZAKTPRMFileName;
        private string _DP_ZAKTPRMFileNameSign;
        private string _parentDocNumber;
        private DateTime? _parentDocDate;

        public ExpFormalizedAccCert(
            Guid documentUid,
            string claimItemNo, 
            string extRef, 
            string DP_IAKTPRMFileName,
            string DP_IAKTPRMFileNameSign,
            string DP_ZAKTPRMFileName,
            string DP_ZAKTPRMFileNameSign,
            string parentDocNumber,
            DateTime? parentDocDate):
            base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _extRef = extRef;
            _DP_IAKTPRMFileName = DP_IAKTPRMFileName;
            _DP_IAKTPRMFileNameSign = DP_IAKTPRMFileNameSign;
            _DP_ZAKTPRMFileName = DP_ZAKTPRMFileName;
            _DP_ZAKTPRMFileNameSign = DP_ZAKTPRMFileNameSign;
            _parentDocNumber = parentDocNumber;
            _parentDocDate = parentDocDate;
        }


        public override XContainer BuildDocument()
        {
            XElement element =
                new XElement("Document",
                    new XAttribute("claimItemNo",_claimItemNo),
                    new XElement("FormalizedAccCert",
                        new XElement("ExtRef", new XText(_extRef)),
                        new XElement("XmlContent", new XText(GetFullNameXml(_DP_IAKTPRMFileName))),
                        new XElement("Signature", new XText(GetFullNameXml(_DP_IAKTPRMFileNameSign))),
                        new XElement("BuyerTitleXmlContent", new XText(GetFullNameXml(_DP_ZAKTPRMFileName))),
                        new XElement("BuyerTitleSignature", new XText(GetFullNameXml(_DP_ZAKTPRMFileNameSign))),
                        new XElement("ParentDocId",
                            new XElement("DocNumber",_parentDocNumber),
                            new XElement("DocDate",DateTimeToString(_parentDocDate))
                            )
                        )
                    );
            return element;
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();

            if (string.IsNullOrWhiteSpace(_DP_IAKTPRMFileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл акта приемки сдачи работ DP_IAKTPRM (Продавец)"));
            if (string.IsNullOrWhiteSpace(_DP_IAKTPRMFileNameSign))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл подписи акта приемки сдачи работ DP_IAKTPRM....sign (Продавец)"));
            if (string.IsNullOrWhiteSpace(_DP_ZAKTPRMFileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл акт приемки сдачи работ DP_ZAKTPRM (Покупатель)"));
            if (string.IsNullOrWhiteSpace(_DP_ZAKTPRMFileNameSign))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл подписи aкта приемки сдачи работ DP_ZAKTPRM......sign (Покупатель)"));
            if (string.IsNullOrWhiteSpace(_parentDocNumber))
                result.Add(new ExpError(DocumentUid, "Отсутствует номер файла родительского документа"));
            if (_parentDocDate == null)
                result.Add(new ExpError(DocumentUid, "Отсутствует дата родетльского документа"));
            return result.ToArray();
        }
    }
}
