﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Счет фактура сканированная.
    /// </summary>
    public class ExpScannedInvoice: ExpDocument
    {
        private string _extRef;
        private string _claimItemNo;
        private readonly List<ImageFileInfo> _imageFileInfo = new List<ImageFileInfo>();
        private string _docNumber;
        private DateTime? _docDate;
        private decimal? _total;
        private decimal? _tax;
        private AgentInfo _buyer;
        private AgentInfo _seller;

        public ExpScannedInvoice(
            Guid documentUid,
            string claimItemNo,
            string extRef,
            ImageFileInfo[] imageFileInfo, 
            string docNumber,
            DateTime? docDate,
            decimal? tax,
            decimal? total,
            AgentInfo buyer,
            AgentInfo seller)
            :base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _extRef = extRef;
            _imageFileInfo.AddRange(imageFileInfo);
            _docNumber = docNumber;
            _docDate = docDate;
            _tax = tax;
            _total = total;
            _buyer = buyer;
            _buyer.AgentType = AgentType.Buyer;
            _seller = seller;
            _seller.AgentType = AgentType.Seller;
        }

        public override XContainer BuildDocument()
        {
            XElement document = new XElement("Document", new XAttribute("claimItemNo", _claimItemNo));
            XElement scannedInvoice = new XElement("ScannedInvoice");
            scannedInvoice.Add(new XElement("ExtRef", new XText(_extRef)));
            //Добавляем картинки
            foreach (var image in _imageFileInfo)
            {
                XElement imageElement = new XElement("ImageFile",
                    new XText(GetFullNameImages(image.FileName)));
                if (image.PageTo != null)
                    imageElement.Add(image.PageTo.Value);
                if (image.PageFrom != null)
                    imageElement.Add(image.PageFrom.Value);
                scannedInvoice.Add(imageElement);
            }
            //Атрибуты документа
            scannedInvoice.Add(
                new XElement("DocId",
                    new XElement("DocNumber", new XText(_docNumber)),
                    new XElement("DocDate", new XText(DateTimeToString(_docDate)))
                ));
            //Атрибуты родительского документа
            /*scannedInvoiceCorrection.Add(
                new XElement("ParentDocId",
                    new XElement("DocNumber", new XText(_parentDocNumber)),
                    new XElement("DocDate", new XText(DateTimeToString(_parentDocDate)))
                    )
                );*/
            //Налоги
            scannedInvoice.Add(
                new XElement("DocAmounts",
                    new XAttribute("tax", DecimalToString(_tax)),
                    new XAttribute("total", DecimalToString(_total))
                    ));
            //Добавляем продавца
            scannedInvoice.Add(
                new XElement("Participant", new XAttribute("role", "Seller"),
                    _seller.BuildDocument()
                    ));
            //Добавляем покупателя
            scannedInvoice.Add(
                new XElement("Participant", new XAttribute("role", "Buyer"),
                    _buyer.BuildDocument()
                    )
                );
            document.Add(scannedInvoice);
            return document;
        }


        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (_imageFileInfo == null || _imageFileInfo.Count == 0)
                result.Add(new ExpError(DocumentUid, "Отсутствуют файлы изображейний у сканированной счет-фактуры"));
            if (string.IsNullOrWhiteSpace(_docNumber))
                result.Add(new ExpError(DocumentUid, "Отсутствуют номер документа у сканированной счет-фактуры"));
            if (_docDate == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют дата документа у сканированной счет-фактуры"));
            if (_total == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют сумма у сканированной счет-фактуры"));
            if (_tax == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют налог у сканированной счет-фактуры"));
            if(_buyer == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют покупатель у сканированной счет-фактуры"));
            if(_seller == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют продавец у сканированной счет-фактуры"));
            result.AddRange(_buyer.CanBuild());
            result.AddRange(_seller.CanBuild());
            return result.ToArray();
        }
    }
}
