﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Книга покупок
    /// </summary>
    public class ExpPurchasesLedger: ExpDocument
    {
        private string _claimItemNo;
        private string _fileName;
        public ExpPurchasesLedger(Guid documentUid,string claimItemNo,string fileName):base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _fileName = fileName;
        }

        public override XContainer BuildDocument()
        {
            XElement element =
                new XElement("Document",
                    new XAttribute("claimItemNo", _claimItemNo),
                    new XElement("PurchasesLedger",
                        new XElement("XmlContent",
                            new XText(XmlDirectory + "/" + _fileName))
                        ));
            return element;
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_fileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл книги покупок"));
            return result.ToArray();
        }
    }
}
