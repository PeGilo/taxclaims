﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Класс экспорта требования
    /// </summary>
    public class ExpClaim: ExpDocument
    {
        public ExpClaim(Guid documentUid) : base(documentUid) { }
        private List<ExpDocument> _childDocument = new List<ExpDocument>();
        /// <summary>
        /// Добавляет экспортируемый документ в требование
        /// </summary>
        /// <param name="document">Дочерний документ</param>
        public void AddDocument(ExpDocument document)
        {
            _childDocument.Add(document);
        }

        public override XContainer BuildDocument()
        {
            XNamespace xsiNameSpace = "http://www.w3.org/2001/XMLSchema-instance";
            XNamespace xsdNameSpace = "http://www.w3.org/2001/XMLSchema";
            XAttribute xsiAttribute = new XAttribute(XNamespace.Xmlns + "xsi",xsiNameSpace);
            XAttribute xsdAttribute = new XAttribute(XNamespace.Xmlns + "xsd",xsdNameSpace);
            XElement result = new XElement("DocumentSet", xsiAttribute, xsdAttribute);
            foreach (var document in _childDocument)
                result.Add(document.BuildDocument());
            return result;
        }

        public string CreateClaim()
        {
            XContainer element = this.BuildDocument();
            return string.Format("<?xml version=\"1.0\"?>\n{0}", element);
        }

        public string CreatePackage()
        {
            return 
            @"<?xml version=""1.0""?>" +
            @"<Package xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""ttp://www.w3.org/2001/XMLSchema"">" +
            "<PackageType>claim</PackageType>" +
            "<PackageRegistry>claim.xml</PackageRegistry>" +
            "</Package>";
        }

        public override string ToString()
        {
            return CreateClaim();
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            foreach (var document in _childDocument)
                result.AddRange(document.CanBuild());
            return result.ToArray();
        }
    }
}
