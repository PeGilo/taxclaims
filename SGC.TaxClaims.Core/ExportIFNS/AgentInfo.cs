﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    public enum AgentType { Buyer, Seller }
    public abstract class AgentInfo : ExpDocument
    {
        private AgentType _agentType = AgentType.Buyer;
        public AgentType AgentType { get { return _agentType; } set { _agentType = value; } }
        public AgentInfo(Guid documentUid): base(documentUid) { }
    }
}
