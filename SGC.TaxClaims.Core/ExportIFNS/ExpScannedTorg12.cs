﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Товарная накладная сканированная.
    /// </summary>
    public class ExpScannedTorg12: ExpDocument
    {
        private string _extRef;
        private string _claimItemNo;
        private readonly List<ImageFileInfo> _imageFileInfo = new List<ImageFileInfo>();
        //private string _docNumber;
        private DateTime? _docDate;
        //private string _parentDocNumber;
        //private DateTime _parentDocDate;
        private decimal? _total;
        private AgentInfo _buyer;
        private AgentInfo _seller;

        public ExpScannedTorg12(
            Guid documentUid,
            string claimItemNo,
            string extRef,
            ImageFileInfo[] imageFileInfo, 
            //string docNumber,
            DateTime? docDate,
            //string parentDocNumber,
            //DateTime parentDocDate,
            decimal? total,
            AgentInfo buyer,
            AgentInfo seller):base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _extRef = extRef;
            _imageFileInfo.AddRange(imageFileInfo);
            /*_docNumber = docNumber;*/
            _docDate = docDate;
            /*_parentDocNumber = parentDocNumber;
            _parentDocDate = parentDocDate;*/
            _total = total;
            _buyer = buyer;
            _buyer.AgentType = AgentType.Buyer;
            _seller = seller;
            _seller.AgentType = AgentType.Seller;
        }

        public override XContainer BuildDocument()
        {
            XElement document = new XElement("Document", new XAttribute("claimItemNo", _claimItemNo));
            XElement scannedTorg12 = new XElement("ScannedTorg12");
            scannedTorg12.Add(new XElement("ExtRef", new XText(_extRef)));
            //Добавляем картинки
            foreach (var image in _imageFileInfo)
            {
                XElement imageElement = new XElement("ImageFile",
                    new XText(GetFullNameImages(image.FileName)));
                if (image.PageTo != null)
                    imageElement.Add(image.PageTo.Value);
                if (image.PageFrom != null)
                    imageElement.Add(image.PageFrom.Value);
                scannedTorg12.Add(imageElement);
            }
            //Атрибуты документа
            scannedTorg12.Add(
                new XElement("DocId",
                    //new XElement("DocNumber", new XText(_docNumber)),
                    new XElement("DocDate", new XText(DateTimeToString(_docDate)))
                ));
            //Атрибуты родительского документа
            /*scannedInvoiceCorrection.Add(
                new XElement("ParentDocId",
                    new XElement("DocNumber", new XText(_parentDocNumber)),
                    new XElement("DocDate", new XText(DateTimeToString(_parentDocDate)))
                    )
                );*/
            //Налоги
            scannedTorg12.Add(
                new XElement("DocAmounts",
                    new XAttribute("total", DecimalToString(_total))
                    ));
            //Добавляем продавца
            scannedTorg12.Add(
                new XElement("Participant", new XAttribute("role", "Seller"),
                    _seller.BuildDocument()
                    ));
            //Добавляем покупателя
            scannedTorg12.Add(
                new XElement("Participant", new XAttribute("role", "Buyer"),
                    _buyer.BuildDocument()
                    )
                );
            document.Add(scannedTorg12);
            return document;
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (_imageFileInfo == null || _imageFileInfo.Count == 0)
                result.Add(new ExpError(DocumentUid, "Отсутствуют файлы изображейний у сканированной товарной накладной"));
            if (_docDate == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют дата документа у сканированной товарной накладной"));
            if (_total == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют сумма у сканированной товарной накладной"));
            if (_buyer == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют покупатель у сканированной товарной накладной"));
            if (_seller == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют продавец у сканированной товарной накладной"));
            result.AddRange(_buyer.CanBuild());
            result.AddRange(_seller.CanBuild());
            return result.ToArray();
        }
    }
}
