﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    public abstract class ExpDocument
    {
        private static string _xmlDirectory = "xml";
        private static string _imageDirectory = "images";
        protected const string _dateFormat = "yyyy-MM-dd";
        private Guid _documentUid;

        public ExpDocument(Guid documentUid)
        {
            _documentUid = documentUid;
        }
        protected Guid DocumentUid { get { return _documentUid; } }
        public abstract XContainer BuildDocument();

        public abstract ExpError[] CanBuild();

        public static string XmlDirectory { get { return _xmlDirectory; } }
        public static string ImageDirectory { get { return _imageDirectory; } }
        /// <summary>
        /// Полное имя файла, который лежит в папке xml
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Полное имя файла</returns>
        protected string GetFullNameXml(string fileName)
        {
            return XmlDirectory + "/" + fileName;
        }

        /// <summary>
        /// Получение полного имени, который лежит в папке images
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Полное имя</returns>
        protected string GetFullNameImages(string fileName)
        {
            return ImageDirectory + "/" + fileName;
        }

        protected string DoubleToString(double digit)
        {
            return digit.ToString("f2").Replace(',', '.');
        }

        protected string DoubleToString(double? digit)
        {
            if (digit == null) return string.Empty;
            return digit.Value.ToString("f2").Replace(',', '.');
        }

        protected string DecimalToString(decimal digit)
        {
            return digit.ToString("f2").Replace(',', '.');
        }

        protected string DecimalToString(decimal? digit)
        {
            if (digit == null) return string.Empty;
            return digit.Value.ToString("f2").Replace(',', '.');
        }

        protected string DateTimeToString(DateTime dateTime)
        {
            return dateTime.ToString(_dateFormat);
        }

        protected string DateTimeToString(DateTime? dateTime)
        {
            if (dateTime == null) return string.Empty;
            return dateTime.Value.ToString(_dateFormat);
        }

       
    }
}