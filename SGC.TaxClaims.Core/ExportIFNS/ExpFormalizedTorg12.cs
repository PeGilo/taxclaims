﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Товарная накладаня Торг-12
    /// </summary>
    public class ExpFormalizedTorg12: ExpDocument
    {
        private string _claimItemNo;
        private string _extRef;
        private string _DP_OTORGFileName;
        private string _DP_OTORGFileNameSign;
        private string _DP_PTORGFileName;
        private string _DP_PTORGFileNameSign;

        public ExpFormalizedTorg12(
            Guid documentUid,
            string claimItemNo,
            string extRef,
            string DP_OTORGFileName,
            string DP_OTORGFileNameSign,
            string DP_PTORGFileName,
            string DP_PTORGFileNameSign
            ):base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _extRef = extRef;
            _DP_OTORGFileName = DP_OTORGFileName;
            _DP_OTORGFileNameSign = DP_OTORGFileNameSign;
            _DP_PTORGFileName = DP_PTORGFileName;
            _DP_PTORGFileNameSign = DP_PTORGFileNameSign;
        }

        public override XContainer BuildDocument()
        {
            XElement element = new XElement("Document", 
                new XAttribute("claimItemNo", _claimItemNo),
                new XElement("FormalizedTorg12",
                    new XElement("ExtRef", new XText(_extRef)),
                    new XElement("XmlContent", new XText(XmlDirectory + "/" + _DP_OTORGFileName)),
                    new XElement("Signature", new XText(XmlDirectory + "/" + _DP_OTORGFileNameSign)),
                    new XElement("BuyerTitleXmlContent", new XText(XmlDirectory + "/" + _DP_PTORGFileName)),
                    new XElement("BuyerTitleSignature", new XText(XmlDirectory + "/" + _DP_PTORGFileNameSign))
                    )
                );
            return element;
        }



        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_DP_OTORGFileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл товарной накладной DP_OTORG12 (Продавец)"));
            if (string.IsNullOrWhiteSpace(_DP_OTORGFileNameSign))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл подписи товарной накладной Файл DP_OTORG12......sign (Продавец)"));
            if (string.IsNullOrWhiteSpace(_DP_PTORGFileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл товарной накладной DP_PTORG12 (Покупатель)"));
            if (string.IsNullOrWhiteSpace(_DP_PTORGFileNameSign))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл подписи товарной накладной DP_PTORG12.......sign (Покупатель)"));
            return result.ToArray();
        }
    }
}
