﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    public class OrganizationInfo : AgentInfo
    {
        private string _kpp;
        private string _inn;
        private string _name;
        
        public OrganizationInfo(Guid documentUid, string kpp, string inn, string name)
            :base(documentUid)
        {
            _kpp = kpp;
            _inn = inn;
            _name = name;
        }

        public string Kpp { get { return _kpp; } }
        public string Inn { get { return _inn; } }
        public string Name { get { return _name; } }

        public override ExpError[] CanBuild()
        {
            string agent = AgentType == ExportIFNS.AgentType.Buyer? "покупателя" : "продавца";
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_kpp))
                result.Add(new ExpError(DocumentUid, string.Format("Отсутствует КПП у {0}.", agent)));
            if (string.IsNullOrWhiteSpace(_inn))
                result.Add(new ExpError(DocumentUid, string.Format("Отсутствует ИНН y {0}.", agent)));
            if (string.IsNullOrWhiteSpace(_name))
                result.Add(new ExpError(DocumentUid, string.Format("Отсутствует наименование организации {0}.",agent)));
            return result.ToArray();
        }

        public override XContainer BuildDocument()
        {
            var result = new XElement("OrgInfo",
                                new XAttribute("kpp", _kpp),
                                new XAttribute("inn", _inn),
                                new XElement("Name", new XText(_name)));
            return result;
        }

    }
}
