﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    public class PersonalInfo: AgentInfo
    {
        private string _inn;
        private string _name;
        private string _middleName;
        private string _lastName;
        public PersonalInfo(Guid documentUid, string inn,string name,string middleName,string lastName)
            :base(documentUid)
        {
            _inn = inn;
            _name = name;
            _middleName = middleName;
            _lastName = lastName;
        }

        public string Inn { get { return _inn; } }
        public string Name { get { return _name; } }
        public string MiddleName { get { return _middleName; } }
        public string LastName { get { return _lastName; } }

        public override ExpError[] CanBuild()
        {
            string agent = AgentType == ExportIFNS.AgentType.Buyer ? "покупателя" : "продавца";
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_lastName))
                result.Add(new ExpError(DocumentUid, string.Format("Отсутствует фамилия у {0}.", agent)));
            if (string.IsNullOrWhiteSpace(_middleName))
                result.Add(new ExpError(DocumentUid, string.Format("Отсутствует отчество у {0}.", agent)));
            if (string.IsNullOrWhiteSpace(_inn))
                result.Add(new ExpError(DocumentUid, string.Format("Отсутствует ИНН у {0}.", agent)));
            if (string.IsNullOrWhiteSpace(_name))
                result.Add(new ExpError(DocumentUid, string.Format("Отсутствует имя у {0}.", agent)));
            return result.ToArray();
        }

        public override XContainer BuildDocument()
        {
            var result = new XElement("PersonInfo", 
                        new XAttribute("inn",_inn),
                        new XElement("Name",new XText(_name)),
                        new XElement("MiddleName",new XText(_middleName)),
                        new XElement("LastName",new XText(_lastName)));
            return result;
        }
    }
}
