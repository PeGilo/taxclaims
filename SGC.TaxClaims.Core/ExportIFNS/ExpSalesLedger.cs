﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Книга продаж
    /// </summary>
    public class ExpSalesLedger: ExpDocument
    {
        private string _claimItemNo;
        private string _fileName;
        public ExpSalesLedger(Guid documentUid,string claimItemNo,string fileName)
            :base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _fileName = fileName;
        }

        public override XContainer BuildDocument()
        {
            XElement element = 
                new XElement(
                    "Document",
                    new XElement(
                        "SalesLedger",
                        new XElement(
                            "XmlContent",
                            new XText(GetFullNameXml(_fileName))
                            )
                        )
                    );

            return element;
        }
        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_fileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл книги продаж"));
            return result.ToArray();
        }
    }
}
