﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    public class ExpFileInfo
    {
        private string _name;
        private byte[] _content;
        public ExpFileInfo(string name,byte[] content)
        {
            _name = name;
            _content = content;
        }
        public string Name { get { return _name; } }
        public byte[] Content { get { return _content; } }
    }
}
