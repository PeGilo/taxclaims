﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    public class ImageFileInfo
    {
        private int? _pageFrom;
        private int? _pageTo;
        private string _fileName;
        public ImageFileInfo(string fileName): this(fileName,null,null) { }

        public ImageFileInfo(string fileName,int? pageFrom,int? pageTo)
        {
            _pageFrom = pageFrom;
            _pageTo = pageTo;
            _fileName = fileName;
        }

        public string FileName { get { return _fileName; } }
        public int? PageTo { get { return _pageTo; } }
        public int? PageFrom { get { return _pageFrom; } }
    }
}
