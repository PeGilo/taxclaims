﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Корректировочная счет фактура сканированная
    /// </summary>
    public class ExpScannedInvoiceCorrection: ExpDocument
    {
        private string _extRef;
        private string _claimItemNo;
        private readonly List<ImageFileInfo> _imageFileInfo = new List<ImageFileInfo>();
        private string _docNumber;
        private DateTime? _docDate;
        private string _parentDocNumber;
        private DateTime? _parentDocDate;
        private decimal? _total;
        private decimal? _tax;
        private AgentInfo _buyer;
        private AgentInfo _seller;

        public ExpScannedInvoiceCorrection(
            Guid documentUid,
            string claimItemNo,
            string extRef,
            ImageFileInfo[] imageFileInfo, 
            string docNumber,
            DateTime? docDate,
            string parentDocNumber,
            DateTime? parentDocDate,
            decimal? tax,
            decimal? total,
            AgentInfo buyer,
            AgentInfo seller)
            :base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _extRef = extRef;
            _imageFileInfo.AddRange(imageFileInfo);
            _docNumber = docNumber;
            _docDate = docDate;
            _parentDocNumber = parentDocNumber;
            _parentDocDate = parentDocDate;
            _tax = tax;
            _total = total;
            _buyer = buyer;
            _buyer.AgentType = AgentType.Buyer;
            _seller = seller;
            _seller.AgentType = AgentType.Seller;
        }

        public override XContainer BuildDocument()
        {
            XElement document = new XElement("Document", new XAttribute("claimItemNo",_claimItemNo));
            XElement scannedInvoiceCorrection = new XElement("ScannedInvoiceCorrection");
            scannedInvoiceCorrection.Add(new XElement("ExtRef",new XText(_extRef)));
            //Добавляем картинки
            foreach (var image in _imageFileInfo)
            {
                XElement imageElement = new XElement("ImageFile",
                    new XText(GetFullNameImages(image.FileName)));
                if (image.PageTo != null)
                    imageElement.Add(image.PageTo.Value);
                if (image.PageFrom != null)
                    imageElement.Add(image.PageFrom.Value);
                scannedInvoiceCorrection.Add(imageElement);
            }
            //Атрибуты документа
            scannedInvoiceCorrection.Add(
                new XElement("DocId",
                    new XElement("DocNumber",new XText(_docNumber)),
                    new XElement("DocDate",new XText(DateTimeToString(_docDate.Value)))
                ));
            //Атрибуты родительского документа
            scannedInvoiceCorrection.Add(
                new XElement("ParentDocId",
                    new XElement("DocNumber",new XText(_parentDocNumber)),
                    new XElement("DocDate",new XText(DateTimeToString(_parentDocDate.Value)))
                    )
                );
            //Налоги
            scannedInvoiceCorrection.Add(
                new XElement("DocAmounts",
                    new XAttribute("tax",DecimalToString(_tax)),
                    new XAttribute("total",DecimalToString(_total))
                    ));
            //Добавляем продавца
            scannedInvoiceCorrection.Add(
                new XElement("Participant", new XAttribute("role", "Seller"),
                    _seller.BuildDocument()
                    ));
            //Добавляем покупателя
            scannedInvoiceCorrection.Add(
                new XElement("Participant",new XAttribute("role","Buyer"),
                    _buyer.BuildDocument()
                    )
                );
            document.Add(scannedInvoiceCorrection);
            return document;
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (_imageFileInfo == null || _imageFileInfo.Count == 0)
                result.Add(new ExpError(DocumentUid, "Отсутствуют файлы изображейний у корректировочной сканированной счет-фактуры"));
            if (string.IsNullOrWhiteSpace(_docNumber))
                result.Add(new ExpError(DocumentUid, "Отсутствуют номер документа у корректировочной сканированной счет-фактуры"));
            if (_docDate == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют дата документа у корректировочной сканированной счет-фактуры"));
            if (string.IsNullOrWhiteSpace(_parentDocNumber))
                result.Add(new ExpError(DocumentUid, "Отсутствуют номер документа основания у корректировочной сканированной счет-фактуры"));
            if (_parentDocDate == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют дата документа основания у корректировочной сканированной счет-фактуры"));
            if (_tax == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют налог у корректировочной сканированной счет-фактуры"));
            if (_total == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют сумма у корректировочной сканированной счет-фактуры"));
            if (_buyer == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют покупатель у корректировочной сканированной счет-фактуры"));
            if (_seller == null)
                result.Add(new ExpError(DocumentUid, "Отсутствуют продавец у корректировочной сканированной счет-фактуры"));
            result.AddRange(_buyer.CanBuild());
            result.AddRange(_seller.CanBuild());
            return result.ToArray();
        }
    }
}
