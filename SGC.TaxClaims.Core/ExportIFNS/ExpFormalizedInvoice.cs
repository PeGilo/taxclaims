﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Счет-фактура
    /// </summary>
    public class ExpFormalizedInvoice: ExpDocument
    {
        private string _claimItemNo;
        private string _extRef;
        private string _fileName;
        private string _signatureName;
        public ExpFormalizedInvoice(
            Guid documentUid,
            string claimItemNo,
            string extRef,
            string fileName,
            string signatureName
            ):base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _extRef = extRef;
            _fileName = fileName;
            _signatureName = signatureName;
        }

        public override XContainer BuildDocument()
        {
            XElement element = new XElement("Document", new XAttribute("claimItemNo",_claimItemNo),
                new XElement("FormalizedInvoice",
                    new XElement("ExtRef",new XText(_extRef)),
                    new XElement("XmlContent",new XText(GetFullNameXml(_fileName))),
                    new XElement("Signature",new XText(GetFullNameXml(_signatureName)))
                ));
            return element;
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_fileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл счет-фактуры ON_SFAKT"));
            if (string.IsNullOrWhiteSpace(_signatureName))
                result.Add(new ExpError(DocumentUid, "Отстутствует файл подписи счет-фактуры ON_SFAKT....sign"));
            return result.ToArray();
        }
    }
}
