﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Дополнительный лист книги продаж
    /// </summary>
    public class ExtSalesLedgerSupplement: ExpDocument
    {
        private string _claimItemNo;
        private string _fileName;
        public ExtSalesLedgerSupplement(Guid documentUid,string claimItemNo,string fileName):base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _fileName = fileName;
            if (!_fileName.ToLower().Trim().EndsWith(".xml"))
                _fileName = _fileName + ".xml";
        }

        public override XContainer BuildDocument()
        {
            XElement element = 
                new XElement("Document", 
                    new XAttribute("claimItemNo",_claimItemNo),
                    new XElement(
                        "SalesLedgerSupplement",
                        new XElement("XmlContent",
                            new XText(GetFullNameXml(_fileName))
                        )
                    )
                );
            return element;
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_fileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл дополнительный лист книги продаж"));
            return result.ToArray();
        }
    }
}
