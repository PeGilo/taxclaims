﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ExportIFNS
{
    /// <summary>
    /// Корректировочная счет фактура
    /// </summary>
    public class ExpFormalizedInvoiceCorrection: ExpDocument
    {
        private string _claimItemNo;
        private string _extRef;
        private string _fileName;
        private string _signatureName;

        public ExpFormalizedInvoiceCorrection(
            Guid documentUid,
            string claimItemNo,
            string extRef,
            string fileName,
            string signatureName
            ):base(documentUid)
        {
            _claimItemNo = claimItemNo;
            _extRef = extRef;
            _fileName = fileName;
            _signatureName = signatureName;
        }

        public override XContainer BuildDocument()
        {
            XElement element = 
                new XElement(
                    "Document", new XAttribute("claimItemNo",_claimItemNo),
                    new XElement("FormalizedInvoiceCorrection",
                        new XElement("ExtRef", new XText(_extRef)),
                        new XElement("XmlContent",new XText(GetFullNameXml(_fileName))),
                        new XElement("Signature",new XText(GetFullNameXml(_signatureName)))
                        )
                    );
            return element;
        }

        public override ExpError[] CanBuild()
        {
            List<ExpError> result = new List<ExpError>();
            if (string.IsNullOrWhiteSpace(_fileName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл корректировочнай счет-фактуры"));
            if (string.IsNullOrWhiteSpace(_signatureName))
                result.Add(new ExpError(DocumentUid, "Отсутствует файл подписи корректировочной счет-фактуры"));
            return result.ToArray();
        }
    }
}
