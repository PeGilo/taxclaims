﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    public class PageResponse<T>
    {
        public PageResponse()
        {
            // Default values
            PagesCount = 0;
            PageIndex = 0;
            RecordsCount = 0;
            Rows = new List<T>();
        }

        /// <summary>
        /// Общее кол-во страниц
        /// </summary>
        public int PagesCount { get; set; }

        /// <summary>
        /// Номер страницы (начинается с 1)
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Кол-во записей на странице
        /// </summary>
        public int RecordsCount { get; set; }

        public IEnumerable<T> Rows { get; set; }
    }
}
