﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    public class OperationResult
    {
        private int _code;
        private string _message;
        private string _source;
        private bool _isSystemError = true;

        public OperationResult(int code, string message, string source,bool isSystemError)
        {
            _code = code;
            _message = message;
            _source = source;
            _isSystemError = isSystemError;
        }

        public OperationResult(int code, string message, bool isSystemError) : 
            this(code, message, null, isSystemError) { }

        public bool IsSucceed
        {
            get
            {
                return !_isSystemError && _code == Succeed._code;
            }
        }

        public bool IsSystemError
        {
            get
            {
                return _isSystemError;
            }
        }
        /// <summary>
        /// Код операции
        /// </summary>
        public int Code
        {
            get
            {
                return _code;
            }
        }
        /// <summary>
        /// Сообщение операции
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }
        }
        /// <summary>
        /// Источник операции
        /// </summary>
        public string Source
        {
            get
            {
                return _source;
            }
        }

        public static OperationResult Succeed = new OperationResult(0, "Успешно",false);

        public static OperationResult NoRight = new OperationResult(1, "Нет доступа.", WebSite,false);

        public static OperationResult DocNotFound = new OperationResult(2, "Документ не найден.", Database, false);

        public static OperationResult CanNotLoadDocument = new OperationResult(3, "Не возможно загрузить документ.", WebSite,false);

        public static OperationResult DocNotFoundWhenLocked = new OperationResult(4, "Во время блокировки документа, документ не найден.", Database, false);

        public static OperationResult NeedOpenExcel = new OperationResult(5, "Необходимо сначала открыть Excel.", Database, false);

        public static OperationResult NotRecognizedExcelFormat = new OperationResult(7, "Неизвестный формат Excel.", Database, false);

        public static OperationResult ClaimExportError = new OperationResult(8, "Ошибка выгрузки документа.", Database, false);

        public static OperationResult SapFileNotFound = new OperationResult(9, "Файл для выгрузки не найден.", Client, false);

        public static OperationResult NeedSaveDocument = new OperationResult(10, "Необходимо сначала сохранить документ.", Client, false);

        public static OperationResult UserNotFound = new OperationResult(11, "Пользователь не найден.", Client, false);

        public static OperationResult ExcelExportError = new OperationResult(12, "Ошибка выгрузки Excel.", Client, false);

        public static OperationResult StateIncompatibleOperation = new OperationResult(1000, "В данном состоянии невозможно произвести запрашиваемую операцию", BusinessLogic,false);

        public static OperationResult DocumentLocked = new OperationResult(1001, "Документ заблокирован от изменений", BusinessLogic, false);

        public static OperationResult ValidationError = new OperationResult(1002, "Данные некорректны", BusinessLogic, false);

        public static OperationResult InvalidArgument = new OperationResult(10000, "Не указан аргумент.", Database, true);

        public static string BusinessLogic { get { return "BusinessLogic"; } }
        public static string Service { get { return "Service"; } }
        public static string Database { get { return "Database"; } }
        public static string WebSite { get { return "WebSite"; } }
        public static string Client { get { return "Client"; } }

        #region "Comparing"

        public override bool Equals(Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            OperationResult or = (OperationResult)obj;
            return (Code == or.Code) && (Source == or.Source) && (IsSucceed == or.IsSucceed);
        }
        public override int GetHashCode()
        {
            return Code ^ Source.GetHashCode() ^ (IsSucceed ? 1 : 0);
        }

        public static bool operator ==(OperationResult x, OperationResult y)
        {
            return x.Code == y.Code && x.Source == y.Source && x.IsSucceed == y.IsSucceed;
        }

        public static bool operator !=(OperationResult x, OperationResult y)
        {
            return !(x == y);
        }

        #endregion

    }

    public class OperationResult<T>
    {
        private OperationResult _result;
        private T _data;
        public OperationResult(OperationResult result,T data)
        {
            _result = result;
            _data = data;
        }
        /// <summary>
        /// Данные
        /// </summary>
        public T Data
        {
            get
            {
                return _data;
            }
        }

        public int Code
        {
            get
            {
                return _result.Code;
            }
        }

        public string Message
        {
            get
            {
                return _result.Message;
            }
        }

        public string Source
        {
            get
            {
                return _result.Source;
            }
        }

        public OperationResult Result
        {
            get
            {
                return _result;
            }
        }

        public bool IsSucceed
        {
            get
            {
                return _result.IsSucceed;
            }
        }
        public bool IsSystemError
        {
            get
            {
                return _result.IsSystemError;
            }
        }
    }
}
