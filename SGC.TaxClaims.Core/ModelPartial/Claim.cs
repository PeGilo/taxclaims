﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Model
{
    public partial class Claim
    {
        public static Claim Create()
        {
            return new Claim()
            {
                 DocTypeId = 1
            };
        }
    }
}
