﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Model
{
    public partial class RegistryDoc
    {
        /// <summary>
        /// Метод для копирования документа в том же реестре
        /// </summary>
        /// <returns></returns>
        public RegistryDoc CloneDocument()
        {
            // Копируем все свойства исходного документа, кроме номеров страниц и файлов.
            return new RegistryDoc()
            {
                UId = Guid.NewGuid(), // GUID присваиваем новый
                DateCreated = DateTime.UtcNow,
                DateChanged = null,

                // остальные поля просто копируем
                //
                PackageIndex = this.PackageIndex,
                IsScan = this.IsScan,
                SumTotal = this.SumTotal,
                SumTax = this.SumTax,
                ParentDocNumber = this.ParentDocNumber,
                ParentDocDate = this.ParentDocDate,

                DocTypeId = this.DocTypeId,
                DocStateId = this.DocStateId,

                Name = this.Name,
                Number = this.Number,
                DocDate = this.DocDate,
                Comment = this.Comment,
                CorrectionTypeId = this.CorrectionTypeId,
                FactoryCode = this.FactoryCode,
                FactoryName = this.FactoryName,
                PartnerName = this.PartnerName,
                PartnerINN = this.PartnerINN,
                PartnerKPP = this.PartnerKPP,
                ParentDocUId = this.ParentDocUId,
                CreatorUserUid = this.CreatorUserUid
            };
        }
    }
}
