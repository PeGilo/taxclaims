﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGC.TaxClaims.Core.Model;
using System.Data.Entity;
namespace SGC.TaxClaims.Core.Model
{
    public partial class Document
    {
        private EntityState _changeStatus =  EntityState.Unchanged;

        public virtual EntityState ChangeStatus
        {
            get
            {
                return _changeStatus;
            }
            set
            {
                _changeStatus = value;
            }
        }
    }
}
