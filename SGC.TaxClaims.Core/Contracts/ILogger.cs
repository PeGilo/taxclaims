﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Contracts
{
    public interface ILogger
    {
        void Debug(string message);
        void Info(string message);
        void Error(string message, Exception ex);
        void Error(string message, OperationResult result);
        void Error(string message);
    }
}
