﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Contracts
{
    public interface IRepository<T> where T : class
    {
        void SetUnchanged(T entity);
        IQueryable<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        void UpdateList(IEnumerable<T> entities);
        void Delete(T entity);
        void Delete(int id);
    }
}
