﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Contracts
{
    /// <summary>
    /// Interface for the "Unit of Work"
    /// </summary>
    public interface IUow
    {
        // Save pending changes to the data store.
        void Commit();
        void UndoChanges();

        // Repositories
        IUserRepository Users { get; }

    }
}
