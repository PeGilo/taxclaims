﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Contracts
{
    public interface ISampleService
    {
        IEnumerable<Model.aspnet_Users> GetAllUsers();
    }
}
