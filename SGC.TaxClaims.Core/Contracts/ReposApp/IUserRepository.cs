﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SGC.TaxClaims.Core.Model;

namespace SGC.TaxClaims.Core.Contracts
{
    public interface IUserRepository : IRepository<aspnet_Users>
    {

    }
}
