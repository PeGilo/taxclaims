﻿using SGC.TaxClaims.Core.Contracts;
using SGC.TaxClaims.Core.ExportIFNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    public interface IIFNSExporter
    {
        OperationResult CreateArchive(string fullFileName,ExpClaim claim,ExpFileInfo[] xmlFiles,ExpFileInfo[] imageFiles );
        ILogger Logger { get; set; }
    }
}
