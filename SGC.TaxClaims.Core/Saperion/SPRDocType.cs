﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Saperion
{
    public enum SPRDocType
    {
        Invoice = 3,
        InvoiceCorrection = 11,
        Torg12 = 5,
        AcceptanceCertificate = 4
    }
}
