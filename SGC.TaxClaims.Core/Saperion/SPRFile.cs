﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Saperion
{
    public class SPRFile
    {
        public int Page { get; set; }
        public string Name { get; set; }
        public int FileType { get; set; }
        public byte[] Content { get; set; }
    }
}
