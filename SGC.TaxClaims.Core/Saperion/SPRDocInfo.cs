﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.Saperion
{
    public class SPRDocInfo
    {
        public string DocNumber { get; set; }
        public string[] DocIndexes { get; set; }
    }
}