﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGC.TaxClaims.Core.Saperion;
using SGC.TaxClaims.Core.Contracts;

namespace SGC.TaxClaims.Core
{
    public interface ISaperion : IDisposable
    {
        /// <summary>
        /// Получает индексы по номерам документа
        /// </summary>
        /// <param name="docNumbers">Номера документа</param>
        /// <returns>Индексы</returns>
        OperationResult<SPRDocInfo[]> GetIndexes(string[] docNumbers);
        /// <summary>
        /// Получить файлы по индексу пакета
        /// </summary>
        /// <param name="packageIndex">Индекс пакета</param>
        /// <returns>Файлы пакета</returns>
        OperationResult<SPRFile[]> GetFiles(string packageIndex);
        /// <summary>
        /// Получает файлы пакета
        /// </summary>
        /// <param name="packageIndex">Индекс пакета</param>
        /// <param name="fileOrderNumbers">Номер документа в пакете по порядку начиная с нуля</param>
        /// <returns>Файлы</returns>
        OperationResult<SPRFile[]> GetFiles(string packageIndex,int[] fileOrderNumbers);
        /// <summary>
        /// Получает xml файлы и подпеси для пакета
        /// </summary>
        /// <param name="docIndex">индекс пакета</param>
        /// <returns>Возвращает массив фалов</returns>
        OperationResult<SPRFile[]> GetEDocFiles(string packageIndex,SPRDocType docType);
        /// <summary>
        /// Пишет в лог
        /// </summary>
        ILogger Logger { get; set; }
    }
}