﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SGC.TaxClaims.Core
{
    public class ValidationInfo
    {
        private int _id;
        private string _message;

        public int Id { get { return _id; } set { _id = value; } }
        public string Message { get { return _message; } set { _message = value; } }
    }
}
