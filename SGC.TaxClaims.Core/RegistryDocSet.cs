﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    public class RegistryDocSet
    {
        //Таблица документ//
        public System.Guid UId { get; set; }
        public int DocTypeId { get; set; }
        public int DocStateId { get; set; }
        public Nullable<System.Guid> DocumentHolderUserUId { get; set; }
        public Nullable<System.Guid> HierarchyHolderUserUId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public Nullable<System.DateTime> DocDate { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> DateChanged { get; set; }
        public System.Guid CreatorUserUid { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Nullable<System.Guid> ParentDocUId { get; set; }
        public int DocSourceId { get; set; }
        //Таблица документ// 
        public string PackageIndex { get; set; }
        public string PageNumbers { get; set; }
        public bool IsScan { get; set; }
        public Nullable<decimal> SumTotal { get; set; }
        public Nullable<decimal> SumTax { get; set; }
        public string ParentDocNumber { get; set; }
        public Nullable<System.DateTime> ParentDocDate { get; set; }
        public Nullable<int> CorrectionTypeId { get; set; }
        public string FactoryCode { get; set; }
        public string FactoryName { get; set; }
        /*public string PartnerName { get; set; }
        public string PartnerINN { get; set; }
        public string PartnerKPP { get; set; }*/
        public Nullable<System.Guid> BuyerUId { get; set; }
        public Nullable<System.Guid> SellerUId { get; set; }

        //Покупатель//
        public Nullable<int> BuyerPartnerTypeId { get; set; }
        public string BuyerInn { get; set; }
        public string BuyerKpp { get; set; }
        public string BuyerName { get; set; }
        public string BuyerMiddleName { get; set; }
        public string BuyerLastName { get; set; }

        //Продавец//
        public Nullable<int> SellerPartnerTypeId { get; set; }
        public string SellerInn { get; set; }
        public string SellerKpp { get; set; }
        public string SellerName { get; set; }
        public string SellerMiddleName { get; set; }
        public string SellerLastName { get; set; }
    }
}
