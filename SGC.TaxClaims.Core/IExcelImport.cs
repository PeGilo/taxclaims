﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGC.TaxClaims.Core.ImportXls;

namespace SGC.TaxClaims.Core
{
    public interface IExcelImport
    {
        OperationResult<RegistryXsl[]> GetRegistries(string fullFilePath);
    }
}
