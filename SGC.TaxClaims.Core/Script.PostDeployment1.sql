﻿/*
Post-Deployment Script Template
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
-- Инициализация ASP.NET Membership
INSERT INTO [dbo].[aspnet_Applications]([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) 
SELECT N'SGC.TaxClaims', N'sgc.taxclaims', N'1E9458A4-C174-4B82-817F-C1996E37F52A', NULL

INSERT INTO [dbo].[aspnet_SchemaVersions](Feature, CompatibleSchemaVersion, IsCurrentVersion)
SELECT 'common', 1, 1 UNION
SELECT 'health monitoring', 1, 1 UNION
SELECT 'membership', 1, 1 UNION
SELECT 'personalization', 1, 1 UNION
SELECT 'profile', 1, 1 UNION
SELECT 'role manager', 1, 1
-----------------------
--Добавляем тип связи--
-----------------------
if (select COUNT(*) from DocRelType where id = 1) = 0 
	insert into DocRelType(Id,Name) values(1,'Включает в себя');
---------------------------
--Добавляем тип документа--
---------------------------
if (select COUNT(*) from DocType where id = 1) = 0 
	insert into DocType(Id,Name) values(1,'Требование');
if (select COUNT(*) from DocType where id = 2) = 0 
	insert into DocType(Id,Name) values(2,'Реестр');
if (select COUNT(*) from DocType where id = 3) = 0 
	insert into DocType(Id,Name) values(3,'Счет-фактура');
if (select COUNT(*) from DocType where id = 4) = 0 
	insert into DocType(Id,Name) values(4,'Акт приемки-сдачи работ');
if (select COUNT(*) from DocType where id = 5) = 0 
	insert into DocType(Id,Name) values(5,'Товарная накладная (Торг-12)');
if (select COUNT(*) from DocType where id = 6) = 0 
	insert into DocType(Id,Name) values(6,'Книга покупок, книга продаж');
if (select COUNT(*) from DocType where id = 7) = 0 
	insert into DocType(Id,Name) values(7,'Дополнительный лист книги покупок');
if (select COUNT(*) from DocType where id = 8) = 0 
	insert into DocType(Id,Name) values(8,'Дополнительный лист книги продаж');
if (select COUNT(*) from DocType where id = 9) = 0 
	insert into DocType(Id,Name) values(9,'Журнал полученных и выставленных счетов-фактур');
--Требование может включать в себя реестр--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 1 and ChildDocId = 2 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(1,2,1);
--Реестр может включать в себя счет-фактуру--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 3 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,3,1);
--Реестр может включать в себя "Акт приемки-сдачи работ"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 4 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,4,1);
--Реестр может включать в себя "Товарная накладная (Торг-12)"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 5 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,5,1);
--Реестр может включать в себя "Книга покупок, книга продаж"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 6 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,6,1);
--Реестр может включать в себя "Дополнительный лист книги покупок"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 7 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,7,1);
--Реестр может включать в себя "Дополнительный лист книги продаж"--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 8 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,8,1);
--Журанл полученных и выставленных счетов-фактур--
if (select COUNT(*) from doctyperelation where ParentTypeDocId = 2 and ChildDocId = 9 and DocRelTypeId = 1) = 0
	insert into doctyperelation(ParentTypeDocId,ChildDocId,DocRelTypeId)
	values(2,9,1);
------------------
--Добавляем роли--
------------------
--Роль сотрудника ОУУ--
if (select count(*) from aspnet_Roles where RoleId = N'0D63240F-B037-43E5-A89C-69AA3B351358') = 0
	insert into aspnet_Roles(RoleId,ApplicationId,RoleName,LoweredRoleName,[Description])
	values(N'0D63240F-B037-43E5-A89C-69AA3B351358',N'1E9458A4-C174-4B82-817F-C1996E37F52A','Сотрудник ОOУ',lower('Сотрудник ОOУ'),'Сотрудник отдела оперативного учета ОЦО');
--Роль сотрудника БиНУ--
if (select count(*) from aspnet_Roles where RoleId = N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0') = 0
	insert into aspnet_Roles(RoleId,ApplicationId,RoleName,LoweredRoleName,[Description])
	values(N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0',N'1E9458A4-C174-4B82-817F-C1996E37F52A','Сотрудник БиНУ',lower('Сотрудник БиНУ'),'Сотрудник отделы Дирекции бухгалтерского и налогового учета ОЦО по участкам учета');
--Роль сотрудника БиНО--
if (select count(*) from aspnet_Roles where RoleId = N'836CC049-8254-401F-A244-364971162233') = 0
	insert into aspnet_Roles(RoleId,ApplicationId,RoleName,LoweredRoleName,[Description])
	values(N'836CC049-8254-401F-A244-364971162233',N'1E9458A4-C174-4B82-817F-C1996E37F52A','Сотрудник ОБиНО',lower('Сотрудник ОБиНО'),'Сотрудник отдел бухгалтерской и налоговой отчетности Дирекции бухгалтерского и налогового учета ОЦО');
--Вставляем пользователя KalininMV--
if (select COUNT(*) from aspnet_Users where UserId = N'53F7D49F-A98B-4E82-989E-E93D22C1F9E1') = 0
	insert into aspnet_Users(UserId,ApplicationId,UserName,LoweredUserName,MobileAlias,IsAnonymous,LastActivityDate)
	values(N'53F7D49F-A98B-4E82-989E-E93D22C1F9E1',N'1E9458A4-C174-4B82-817F-C1996E37F52A','sibgenco\kalininmv','sibgenco\kalininmv',null,0,getdate());
--------------------------------
--Связка пользователей и ролей--
--------------------------------
--Вставляем роль для kalininmv
delete from aspnet_UsersInRoles where userid = N'53F7D49F-A98B-4E82-989E-E93D22C1F9E1' and RoleId = N'0D63240F-B037-43E5-A89C-69AA3B351358';
insert into aspnet_UsersInRoles(UserId,RoleId) values(N'53F7D49F-A98B-4E82-989E-E93D22C1F9E1',N'0D63240F-B037-43E5-A89C-69AA3B351358');
--Вставляем роли для GiloyanPS--
if (select count(*) from aspnet_UsersInRoles where userid = N'6CF177C4-9795-4D72-99E4-3CEAE22B76FB' and RoleId = N'0D63240F-B037-43E5-A89C-69AA3B351358')  = 0
	insert into aspnet_UsersInRoles(UserId,RoleId) values(N'6CF177C4-9795-4D72-99E4-3CEAE22B76FB',N'0D63240F-B037-43E5-A89C-69AA3B351358');
if (select count(*) from aspnet_UsersInRoles where userid = N'6CF177C4-9795-4D72-99E4-3CEAE22B76FB' and RoleId = N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0') = 0
	insert into aspnet_UsersInRoles(UserId,RoleId) values(N'6CF177C4-9795-4D72-99E4-3CEAE22B76FB',N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0');
if (select count(*) from aspnet_UsersInRoles where userid = N'6CF177C4-9795-4D72-99E4-3CEAE22B76FB' and RoleId = N'836CC049-8254-401F-A244-364971162233') = 0
	insert into aspnet_UsersInRoles(UserId,RoleId) values(N'6CF177C4-9795-4D72-99E4-3CEAE22B76FB',N'836CC049-8254-401F-A244-364971162233');

------------------
--Вставляем путь--
------------------
if (select count(*) from DocRoute where id = 1) = 0
	insert into DocRoute(id,name) values(1,'Выгрузка документов в ИФНС');
-----------------------
--Добавляем состояния--
-----------------------
--Состояние первое (Документ у сотрудника ООУ).
if (select count(*) from DocState where id = 1) = 0
	insert into DocState(Id,Number,DocRouteId,Name,Comment) 
	values(1,1,1,'Документ у сотрудника ООУ','Сотрудник ООУ создает требование и прикрепляет решение налоговой о правомерности.');
--Состояние второе (Документ у сотрудника БиНУ).
if (select count(*) from DocState where id = 2) = 0
	insert into DocState(Id,Number,DocRouteId,Name,Comment) 
	values(2,2,1,'Документ у сотрудника БиНУ','Сотрудник БиНУ создает реестр и загружает атрибуты документов из САП.');
--Состояние третье (Документ у сотрудника ОБиНО).
if (select count(*) from DocState where id = 3) = 0
	insert into DocState(Id,Number,DocRouteId,Name,Comment) 
	values(3,3,1,'Документ у сотрудника ОБиНО','Сотрудник ОБиНО подгружает файлы для документов из Sapirion.');
--Состояние четвертое (Документ у сотрудника БиНУ)
if (select count(*) from DocState where id = 4) = 0
	insert into DocState(Id,Number,DocRouteId,Name,Comment) 
	values(4,4,1,'Документ у сотрудника БиНУ','Сотрудник БиНУ проверяет документы.');
--Состояние пятое (Документ у сотрудника ООУ)
if (select count(*) from DocState where id = 5) = 0
	insert into DocState(Id,Number,DocRouteId,Name,Comment)
	values(5,5,1,'Документ у сотрудника ООУ','Сотрудник ООУ проверяет документы и выгруажет в архив.');
----------------------------
--Добавляем роли состояний--
----------------------------
if (select count(*) from DocStateRole where DocStateId = 1 and RoleUid = N'0D63240F-B037-43E5-A89C-69AA3B351358') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(1,N'0D63240F-B037-43E5-A89C-69AA3B351358');
if (select count(*) from DocStateRole where DocStateId = 2 and RoleUid = N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(2,N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0');
if (select count(*) from DocStateRole where DocStateId = 3 and RoleUid = N'836CC049-8254-401F-A244-364971162233') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(3,N'836CC049-8254-401F-A244-364971162233');
if (select count(*) from DocStateRole where DocStateId = 4 and RoleUid = N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(4,N'B6DA3C0A-5CC8-4500-9E92-E1EFF50F3BA0');
if (select count(*) from DocStateRole where DocStateId = 5 and RoleUid = N'0D63240F-B037-43E5-A89C-69AA3B351358') = 0
	insert into DocStateRole(DocStateId,RoleUid) values(5,N'0D63240F-B037-43E5-A89C-69AA3B351358');
-------------------------------
--Добавляем маршрут состояний--
-------------------------------
delete from DocMove;
if (select count(*) from DocMove where DocStateIdFrom = 1 and DocStateIdTo = 2 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(1,2,1,'fnCanMoveFrom1To2');
if (select count(*) from DocMove where DocStateIdFrom = 2 and DocStateIdTo = 3 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(2,3,1,'fnCanMoveFrom2To3');
if (select count(*) from DocMove where DocStateIdFrom = 3 and DocStateIdTo = 4 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(3,4,1,'fnCanMoveFrom3To4');
if (select count(*) from DocMove where DocStateIdFrom = 4 and DocStateIdTo = 5 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(4,5,1,'fnCanMoveFrom4To5');
if (select count(*) from DocMove where DocStateIdFrom = 4 and DocStateIdTo = 3 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(4,3,1,'fnCanMoveFrom4To3');
if (select count(*) from DocMove where DocStateIdFrom = 5 and DocStateIdTo = 3 and DocRouteId = 1) = 0
	insert into DocMove(DocStateIdFrom,DocStateIdTo,DocRouteId,PrCanMove) values(5,3,1,'fnCanMoveFrom5To3');
---------------------------
--Заполняем типы статусов--
---------------------------
delete from DocEventType;
if (select count(*) from DocEventType where id = 1) = 0
	insert into DocEventType(id,name) values(1,'Создан');
--------------------------------------
--Вставялем статусы типов документов--
--------------------------------------
delete from DocTypeEvent;
--Можно создать требование на первом этапе
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(1,1,1);
--На втором этапе можно создать все остальные докумнеты
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(2,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(3,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(4,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(5,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(6,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(7,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(8,1,2);
insert into DocTypeEvent(DocTypeId,DocEventTypeId,DocStateId) values(9,1,2);