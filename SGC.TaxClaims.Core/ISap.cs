﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    public interface ISap
    {
        OperationResult<Guid> LoadXml(Guid claimUid, string userNameFull);
    }
}
