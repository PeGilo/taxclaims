﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    /// <summary>
    /// Справочник источников документов
    /// </summary>
    public enum DocSourceType : int
    {
        Unspecified = 0,
        SAP = 1 
    }
}
