﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    /// <summary>
    /// Тип документа (справочник, соответствующий базе)
    /// </summary>
    public enum DocumentType
    {
        Unspecified = 0,
        Claim = 1, // Требование
        Registry = 2, // Реестр
        Invoice = 3, //	Счет-фактура
        AcceptanceCertificate = 4, //Акт приемки-сдачи работ
        Torg12 = 5, //Товарная накладная (Торг-12)
        BookPurchases = 6, // Книга покупок
        BookPurchasesAdd = 7, // Дополнительный лист книги покупок
        BookSalesAdd = 8, // Дополнительный лист книги продаж
        InvoiceRegister = 9, // Журнал полученных и выставленных счетов-фактур
        BookSales = 10, // Книга продаж
        InvoiceCorrection = 11, // Корректировочная счет-фактура
    }
}
