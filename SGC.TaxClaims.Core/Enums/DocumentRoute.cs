﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core
{
    /// <summary>
    /// Маршруты документов
    /// </summary>
    public enum DocumentRoute : int
    {
        Unspecified = 0,

        /// <summary>
        /// Выгрузка в ИФНС
        /// </summary>
        ToIFNS = 1
    }
}
