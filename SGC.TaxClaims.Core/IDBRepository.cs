﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SGC.TaxClaims.Common.Filtering;
using SGC.TaxClaims.Core.Model;
using SGC.TaxClaims.Core.Contracts;
using System.Data;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core
{
    public interface IDBRepository
    {
        /// <summary>
        /// Получение пользователя по логину
        /// </summary>
        /// <param name="userName">Логин вместе с доменом</param>
        /// <returns></returns>
        OperationResult<aspnet_Users> GetUserByName(string userName);


        OperationResult<RegistryDoc> GetRegistryDocById(Guid uid);
        /// <summary>
        /// Получить требование по идентификатору
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        OperationResult<Claim> GetClaimById(Guid uid);
        /// <summary>
        /// Метод получения требований
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Требования</returns>
        OperationResult<Claim[]> GetClaims(string userName);
        /// <summary>
        /// Получение реестра по идентификатору
        /// </summary>
        /// <param name="registryUid"></param>
        /// <returns></returns>
        OperationResult<Registry> GetRegistryByUid(Guid registryUid);
        /// <summary>
        /// Получаем реестры по идентификатору требования
        /// </summary>
        /// <param name="claimUid">Идентификатор требования</param>
        /// <returns>Реестры</returns>
        OperationResult<Registry[]> GetRegistriesByClaimUid(Guid claimUid);
        /// <summary>
        /// Получить состояния, в которых может создаваться документ
        /// </summary>
        /// <param name="docTypeId">Тип документа</param>
        /// <param name="docRouteId">Маршрут</param>
        /// <returns></returns>
        OperationResult<DocState> GetInitialState(int docTypeId, int docRouteId);
        /// <summary>
        /// Метод обновления файлов
        /// </summary>
        /// <param name="files">Файлы</param>
        /// <returns>Результат операции</returns>
        OperationResult UpdateFiles(DocFile[] files);
        /// <summary>
        /// Метод сохранения документа
        /// </summary>
        /// <param name="document">Документ</param>
        /// <param name="parentUid">Идентификатор родительского документа</param>
        /// <returns>Результат операции</returns>
        OperationResult SaveDocument(Document document, Guid? parentDocUid);

        OperationResult SaveDocuments(IEnumerable<Document> documents, Guid? parentDocUid);

        /// <summary>
        /// Сохраняет и переводит документ в указанное состояние
        /// </summary>
        /// <param name="document">Документ</param>
        /// <param name="parentDocUid">Родительский документ</param>
        /// <param name="nextStateId">Идентификатор следующего состояния</param>
        /// <param name="userUid">Идентификатор пользователя</param>
        /// <returns>Ошибки валидцаии</returns>
        OperationResult<ValidationInfo[]> SaveAndMove(Document document, Guid? parentDocUid,int nextStateId,Guid userUid);
        /// <summary>
        /// Получение родительских документов
        /// </summary>
        /// <typeparam name="T">Типы наследуемые от Document</typeparam>
        /// <param name="parentDocUid"></param>
        /// <returns></returns>
        OperationResult<Document[]> GetChildDocuments(Guid parentDocUid);
        /// <summary>
        /// Отдает документы реестра
        /// </summary>
        /// <param name="registryUid">Идентификатор реестра</param>
        /// <returns>Документы</returns>
        OperationResult<RegistryDoc[]> GetRegistryDocs(Guid registryUid);
        /// <summary>
        /// Отдает документы реестра
        /// </summary>
        /// <param name="registryUid">Идентификатор реестра</param>
        /// <returns>Документы</returns>
        OperationResult<RegistryDocSet[]> GetRegistryDocsSP(Guid registryUid);
        /// <summary>
        /// Получение файлов документа
        /// </summary>
        /// <param name="documentUid">Идентификатор документа</param>
        /// <returns>Файлы документа</returns>
        OperationResult<DocFile[]> GetDocumentFiles(Guid documentUid);
        /// <summary>
        /// Получение файлов документа
        /// </summary>
        /// <param name="documentUid">Идентификатор документа</param>
        /// <param name="fileTypeId">Тип файла</param>
        /// <returns>Файлы документа</returns>
        OperationResult<DocFile[]> GetDocumentFiles(Guid documentUid,int fileTypeId);
        /// <summary>
        /// Возвращает, только описание файлов без фалов
        /// </summary>
        /// <param name="documentDocUid">Идентификатор документа</param>
        /// <returns>Файлы</returns>
        OperationResult<DocFile[]> GetDocumentFilesDiscritionOnly(Guid documentUid);
        /// <summary>
        /// Файлы документа
        /// </summary>
        /// <param name="fileUid">Идентификатор файла</param>
        /// <returns>Файл</returns>
        OperationResult<DocFile> GetFile(Guid fileUid);
        /// <summary>
        /// Получить следующие возможные состояния
        /// </summary>
        /// <param name="docState">Получить следующие возможные состояния</param>
        /// <returns>Следущие возможные состояния</returns>
        OperationResult<DocState[]> GetNextPossibleStates(DocState docState);
        /// <summary>
        /// Возможность перевезти документ в указанное состояние
        /// </summary>
        /// <param name="docStateIdFrom">Перевод из состояния</param>
        /// <param name="docStateIdTo">Перевод в состояние</param>
        /// <param name="routeId">Маршрут</param>
        /// <param name="documentUid">Идентификатор документа</param>
        /// <param name="userUid">Идентификатор пользователя</param>
        /// <returns>Есть не возможно, то выдает сообщения</returns>
        OperationResult<ValidationInfo[]> CanMoveNextState(int docStateIdFrom,int docStateIdTo,int routeId,Guid documentUid,Guid userUid);
        /// <summary>
        /// Для постраничного просмотра требований
        /// </summary>
        /// <param name="pageSize">Кол-во требований на странице</param>
        /// <param name="pageIndex">Индекс страницы</param>
        /// <param name="sortColumn">Наименование столбца, по которому осуществляется сортирвка</param>
        /// <param name="asc">True - сортировка по возрастанию, False - по убыванию</param>
        /// <param name="filter">Фильтр для выбора требований</param>
        /// <param name="totalRecords">Возвращает общее кол-во требований, удовлетворяющих фильтру</param>
        /// <returns></returns>
        OperationResult<PageResponse<Claim>> GetClaimsPage(int pageSize, int pageIndex, string sortColumn, bool? asc, FilterGroup filter, out int totalRecords);
        /// <summary>
        /// Заблокировать документ от изменения другими пользователями. В частности запретить изменять состояние документа.
        /// Блокирует всю иерархию документов.
        /// </summary>
        /// <param name="documentUid"></param>
        /// <param name="userUid"></param>
        /// <param name="blockHolderUid">Кем заблокирован документ, в случае, если не удается заблокировать документ</param>
        /// <returns>True - документ заблокирован, False - документ не смог быть заблокирован</returns>
        OperationResult<bool> TryLockDocument(Guid documentUid, Guid userUid, out Guid blockHolderUid);
        /// <summary>
        /// Разблокировать документ.
        /// </summary>
        /// <param name="documentUid"></param>
        /// <returns></returns>
        OperationResult UnlockDocument(Guid documentUid, Guid userUid);
        /// <summary>
        /// Получить ораганизации
        /// </summary>
        /// <returns>Организации</returns>
        OperationResult<Organization[]> GetListOfOrganizations();

        /// <summary>
        /// Поиск контрагентов по тексту
        /// </summary>
        /// <param name="text">Текст, по которому необходимо искать</param>
        /// <param name="maxCount">Максимальное кол-во записей, которое необходимо вернуть</param>
        /// <returns></returns>
        OperationResult<Partner[]> GetPartners(string text, int maxCount);

        /// <summary>
        /// Класс, для записи в лог
        /// </summary>
        ILogger Logger { get; set; }
        /*/// <summary>
        /// Вставляет реестр
        /// </summary>
        /// <param name="registry">Реестр</param>
        /// <param name="registryDocs">Документы реестра</param>
        /// <param name="claimUid">Идентификатор требования</param>
        /// <returns>Результат операции</returns>*/
        /*OperationResult InsertRegistry(Registry registry, RegistryDoc[] registryDocs, Guid claimUid);*/
        /*OperationResult InsertRegistry(DataTable registry, DataTable document, DataTable registryDoc);*/

        OperationResult<Guid> InsertRegistry(XDocument xml,Guid claimUid,string fullUserName);

        OperationResult UpdateFileDocs(RegistryDoc[] documents, DocFile[] docFiles);

    }
}
