﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
namespace SGC.TaxClaims.Core.ImportXls
{
    public class DocumentXsl
    {
        private int _row;
        private int _sheet;
        private string _name;
        private string _inn;
        private string _kpp;
        private string _docNumber;
        private string _docDate;
        private string _docNumParent;
        private string _docDateParent;
        private string _sum;
        private string _tax;
        private string _packageIndex;
        private string _sign;

        public DocumentXsl(
            int row,
            int sheet,
            string name,
            string inn,
            string kpp,
            string docNumber, 
            string docDate,
            string docNumParent,
            string docDateParent,
            string sum,
            string tax,
            string packageIndex,
            string sign
            )
        {
            _row = row;
            _sheet = sheet;
            _name = name;
            _inn = inn;
            _kpp = kpp;
            _docNumber = docNumber;
            _docDate = docDate;
            _docNumParent = docNumParent;
            _docDateParent = docDateParent;
            _sum = sum;
            _tax = tax;
            _packageIndex = packageIndex;
            _sign = sign;
        }

        public string Name { get { return _name; } }

        public string Inn { get { return _inn; } }

        public string Kpp { get { return _kpp; } }

        public string DocNumber { get { return _docNumber; } }

        public DateTime? DocDate { get { return ConvertToDate(_docDate); } }

        public string DocNumParent { get { return _docNumParent; } }

        public DateTime? DocDateParent { get { return ConvertToDate(_docDateParent); } }

        public decimal? Sum { get { return ConvertToDecimal(_sum); } }

        public decimal? Tax { get { return ConvertToDecimal(_tax); } }

        public string PackageIndex { get { return _packageIndex; } }

        public int Sign { get { return GetSign(); } }

        private int GetSign()
        {
            if (string.IsNullOrWhiteSpace(_sign)) return 1; //XContainer
            return Convert.ToInt32(_sign);
        }

        private string GetTax()
        {
            if (string.IsNullOrWhiteSpace(_tax)) return string.Empty;
            return _tax.Replace(',', '.');
        }

        private string GetSum()
        {
            if (string.IsNullOrWhiteSpace(_sum)) return string.Empty;
            return _sum.Replace(',','.');
        }

        private decimal? ConvertToDecimal(string digit)
        {
            if(string.IsNullOrWhiteSpace(digit)) return null;
            return Convert.ToDecimal(digit);
        }

        private DateTime? ConvertToDate(string date)
        {
            return string.IsNullOrWhiteSpace(date)? 
                (DateTime?)null : (DateTime?)DateTime.ParseExact(date, "dd.MM.yyyy", null);
        }

        public XContainer BuildXml()
        {
            XElement document = new XElement("СведДок", 
                new XAttribute("НомСтр",string.Empty),
                new XAttribute("КодЗав", string.Empty),
                new XAttribute("НаимЗав", string.Empty),
                new XAttribute("НаимПартн", _name ?? string.Empty),
                new XAttribute("ИННПартн", _inn ?? string.Empty),
                new XAttribute("КПППартн", _kpp ?? string.Empty),
                new XAttribute("НомСчФ", _docNumber ?? string.Empty),
                new XAttribute("ДатаСчф", _docDate ?? string.Empty),
                new XAttribute("НомерДокОсн", _docNumParent ?? string.Empty),
                new XAttribute("ДатаДокОсн", _docDateParent ?? string.Empty),
                new XAttribute("СтоимСчФВс", _sum ?? string.Empty),
                new XAttribute("СумНДССчф", GetTax()),
                new XAttribute("НомерПак", _packageIndex ?? string.Empty),
                new XAttribute("ПризФО", GetSign() )
                );
            return document;
        }
        //Функция валидации документа
        public ErrorXls[] CanBuild()
        {
            List<ErrorXls> errors = new List<ErrorXls>();
            return errors.ToArray();
        }


        public bool IsEmpty()
        {
            return 
                string.IsNullOrWhiteSpace(_name) 
                &&
                string.IsNullOrWhiteSpace(_inn) 
                &&
                string.IsNullOrWhiteSpace(_kpp) 
                &&
                string.IsNullOrWhiteSpace(_docNumber) 
                &&
                string.IsNullOrWhiteSpace(_docDate) 
                &&
                string.IsNullOrWhiteSpace(_docNumParent) 
                &&
                string.IsNullOrWhiteSpace(_docDateParent) 
                &&
                string.IsNullOrWhiteSpace(_sum) 
                &&
                string.IsNullOrWhiteSpace(_tax) 
                &&
                string.IsNullOrWhiteSpace(_packageIndex) 
                &&
                string.IsNullOrWhiteSpace(_sign);
        }
    }
}
