﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGC.TaxClaims.Core.ImportXls
{
    /// <summary>
    /// Ошибка парсирования 
    /// </summary>
    public class ErrorXls
    {
        public ErrorXls(int rowNumber, string message)
        {
            RowNumber = rowNumber;
            Message = message;
        }
        public int RowNumber { get; private set; }
        public string Message { get; private set; }
    }
}
