﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SGC.TaxClaims.Core.ImportXls
{
    public class RegistryXsl
    {
        private readonly List<DocumentXsl> _documents = new List<DocumentXsl>();
        private int _sheet;
        private string _bookType;
        private string _dateCreated;
        private string _orgName;
        private string _inn;
        private string _kpp;

        public RegistryXsl(
            int sheet,
            string bookType,
            string dateCreated,
            string orgName,
            string inn,
            string kpp)
        {
            _sheet = sheet;
            _dateCreated = dateCreated;
            _orgName = orgName;
            _bookType = bookType;
            _inn = inn;
            _kpp = kpp;
        }

        private string GetBookType()
        {
            return _bookType.Trim().ToUpper();
        }

        private string GetDateCreated()
        {
            return _dateCreated != null ? _dateCreated.Trim() : string.Empty;
        }

        private string GetOrgName()
        {
            return _orgName ?? string.Empty;
        }

        private string GetOrgInn()
        {
            return _inn ?? string.Empty;
        }

        private string GetOrgKpp()
        {
            return _kpp ?? string.Empty;
        }

        public void AddDocument(DocumentXsl document)
        {
            _documents.Add(document);
        }

        public DocumentXsl[] Documents
        {
            get
            {
                return _documents.ToArray();
            }
        }

        public XContainer BuidXml()
        {
            XElement file = new XElement("Файл",
                new XAttribute("ВерсПрог", "SAPERD150"),
                new XAttribute("ИдФайл",""));
            XElement book = new XElement("Книга",
                new XAttribute("ТипКн", GetBookType()),
                new XAttribute("Пользователь", string.Empty),
                new XAttribute("ДатаФорм", GetDateCreated()),
                new XAttribute("ВремяФорм", "00:00:00"));
            file.Add(book);
            XElement org = new XElement("СведОрг",
                new XAttribute("НаимОрг", GetOrgName()),
                new XAttribute("ИННОрг", GetOrgInn()),
                new XAttribute("КППОрг", GetOrgKpp()));
            book.Add(book);
            //Добавляем документы
            foreach (var doc in _documents)
                book.Add(doc.BuildXml());
            return file;
        }

        public ErrorXls[] CanBuild()
        {
            List<ErrorXls> result = new List<ErrorXls>(); 
            return result.ToArray();
        }
        
    }
}
